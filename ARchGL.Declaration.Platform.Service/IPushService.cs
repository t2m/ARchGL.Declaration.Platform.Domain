﻿using ARchGL.Declaration.Platform.Domain.Entities;
using ARchGL.Declaration.Platform.Service.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Models;
using TDF.Core.Operator;
using TDF.Data.Repository;

namespace ARchGL.Declaration.Platform.Service
{
    /// <summary>
    /// 数据推送服务
    /// </summary>
    public interface IPushService
    {
        /// <summary>
        /// 保存推送数据
        /// </summary>
        void Save(Push_Project_History entity);

        /// <summary>
        /// 更新重试状态
        /// </summary>
        /// <param name="id">数据Id</param>
        void UpdateStatusById(Guid id);

        /// <summary>
        /// 更新重试状态
        /// </summary>
        /// <param name="list">数据集合</param>
        void UpdateStatus(List<Push_Project_History> list);

        /// <summary>
        /// 获取推送数据-分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns></returns>
        Tuple<int, IPagedList<Push_Project_History>> GetPushPagedList(PushProjectHistoryCriteria criteria);
    }
}
