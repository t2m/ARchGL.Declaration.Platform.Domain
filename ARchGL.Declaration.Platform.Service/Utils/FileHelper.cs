﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Utils
{
    /// <summary>
    /// 文件帮助类
    /// </summary>
    public class FileHelper
    {
        /// <summary>
        /// 获取文件夹信息
        /// </summary>
        /// <returns></returns>
        public static int GetFileCount(string path)
        {
            var di = new DirectoryInfo(path);
            var dict = di.GetDirectories();
            return dict.Count();
        }


    }
}
