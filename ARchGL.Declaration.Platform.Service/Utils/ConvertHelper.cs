﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    /// <summary>
    /// 
    /// </summary>
    public static class ConvertHelper
    {
        /// <summary>
        /// 两个相似实体转换
        /// </summary>
        /// <typeparam name="S">源实体</typeparam>
        /// <typeparam name="T">赋值实体</typeparam>
        /// <param name="source">源实体值</param>
        /// <returns></returns>
        public static T Merge<S, T>(this S source) where T : new()
        {
            if (source == null) return default(T);

            var propertiesT = typeof(S).GetProperties(BindingFlags.Instance | BindingFlags.Public);
            var propertiesL = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public);

            var entity = new T();
            foreach (PropertyInfo itemL in propertiesL)
            {
                foreach (PropertyInfo itemT in propertiesT)
                {
                    if (itemL.Name != itemT.Name || itemL.PropertyType != itemT.PropertyType) continue;

                    var value = itemT.GetValue(source, null);
                    //if (itemT.PropertyType == typeof(decimal))
                    //    value += 0.0m;

                    itemL.SetValue(entity, value, null);
                    break;
                }
            }
            return entity;
        }

        /// <summary>
        /// 加密为 MD5
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToMD5(this string str)
        {
            return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(str + "a7273bce-23a1-450d-902d-7c30a4456635", "MD5");
        }

        /// <summary>
        /// 指示指定的字符串是 null、空还是仅由空白字符组成。
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsNullOrWhiteSpace(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        /// <summary>
        /// 克隆一个对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T Clone<T>(this T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (ReferenceEquals(source, null))
            {
                return default(T);
            }

            var formatter = new BinaryFormatter();
            var stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// 分割字符串
        /// </summary>
        /// <param name="source"></param>
        /// <param name="str">分割标识</param>
        /// <returns></returns>
        public static string[] Split(this string source, string str)
        {
            var list = new List<string>();
            while (true)
            {
                var index = source.IndexOf(str);
                if (index < 0) { list.Add(source); break; }
                var rs = source.Substring(0, index);
                list.Add(rs);
                source = source.Substring(index + str.Length);
            }
            return list.ToArray();
        }
    }
}
