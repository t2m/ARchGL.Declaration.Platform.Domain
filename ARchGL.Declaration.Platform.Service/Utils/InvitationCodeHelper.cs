﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Utils
{
    /// <summary>
    /// 邀请码
    /// </summary>
    public class InvitationCodeHelper
    {
        private string chars;
        /// <summary>
        /// 指定邀请码的组成
        /// </summary>
        /// <param name="str">字符串</param>
        public InvitationCodeHelper(string str)
        {
            chars = str;
        }

        /// <summary>
        /// 获取最新邀请码
        /// </summary>
        public string CurrentInvitationCode
        {
            get
            {
                var value = string.Empty;
                var decvalue = LastInvitationCode;
                var index = 0;
                if (decvalue == 0) return new string(new char[] { chars[0] });
                while (decvalue > 0)
                {
                    index = decvalue % chars.Length;
                    value = chars[index] + value;
                    decvalue = decvalue / chars.Length;
                }
                return value;
            }
            set
            {
                var n = 0;
                //int number(char x)
                Func<char, int> number = (x) =>
                {
                    for (var i = 0; i < chars.Length; i++)
                    {
                        if (x == chars[i]) return i;
                    }
                    return 0;
                };
                for (var i = 0; i < value.Length; i++)
                {
                    n += Convert.ToInt32(Math.Pow(chars.Length, value.Length - i - 1) * number(value[i]));
                }
                LastInvitationCode = n;
            }
        }

        /// <summary>
        /// 当前记数
        /// </summary>
        public int LastInvitationCode { get; set; }
    }
}
