﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service
{
    public class DateTimeHelper
    {
        /// <summary>
        /// 系统最小日期 1970-01-01 00:00:00
        /// </summary>
        public static DateTime MinDateTime
        {
            get
            {
                return DateTime.Parse("1970-01-01 00:00:00");
            }
        }
    }
}
