﻿using ARchGL.Declaration.Platform.Domain.Entities;
using ARchGL.Declaration.Platform.Service.Dtos;
using System;
using System.Collections.Generic;
using TDF.Core.Models;
using TDF.Data.Repository;

namespace ARchGL.Declaration.Platform.Service
{
    /// <summary>
    /// 项目服务
    /// </summary>
    public interface IProjectService
    {
        /// <summary>
        /// 初始化 登录Token
        /// </summary>
        /// <param name="token">登录 Token</param>
        void InitLoginToken(string token);

        #region 报表提供

        /// <summary>
        /// 待审核申请信息-统计
        /// </summary>
        /// <param name="areaId">区域帐号Id</param>
        /// <returns></returns>
        WaitReviewReportDto WaitReviewReport(Guid areaId);

        /// <summary>
        /// 月报统计
        /// </summary>
        /// <param name="date">统计日期</param>
        /// <param name="areaId">区域帐号Id</param>
        /// <returns></returns>
        List<MonthReportDto> MonthReport(DateTime date, Guid areaId);

        /// <summary>
        /// 已报未报项目-统计
        /// </summary>
        /// <param name="areaId">区域帐号Id</param>
        /// <returns></returns>
        ProjectDeclarationReportDto ProjectDeclarationReport(Guid areaId);

        /// <summary>
        /// 已报项目中审核通过与未通过-统计
        /// </summary>
        /// <param name="areaId">区域帐号Id</param>
        /// <returns></returns>
        ProjectCategoryFirstCategoryReportDto ProjectFileReport(Guid areaId);

        /// <summary>
        /// 已报项目中提交文件数-统计
        /// </summary>
        /// <param name="areaId">区域帐号Id</param>
        /// <returns></returns>
        List<ProjectSubmitReportDto> ProjectSubmitReport(Guid areaId);

        /// <summary>
        /// 审核文件数-统计
        /// </summary>
        /// <param name="areaId">区域帐号Id</param>
        /// <returns></returns>
        ProjectReviewReport ProjectReviewReport(Guid areaId);

        /// <summary>
        /// 单项申请状态-统计
        /// </summary>
        /// <param name="areaId"></param>
        /// <param name="type">申请类型：1.BIM认定、2元素表报</param>
        /// <returns></returns>
        ProjectSubitemReportDto ProjectSubitemReport(Guid areaId, ApplyForType type);

        #endregion

        #region 项目
        /*2018年12月25日 15:47:28 项目相关操作通过审核流程完成
        /// <summary>
        /// 创建项目并同步创建项目帐号
        /// </summary>
        /// <param name="dto">项目信息</param>
        Guid CreateProject(ProjectDto dto);

        /// <summary>
        /// 删除项目（软删除）
        /// </summary>
        /// <param name="dto">项目信息</param>
        void RemoveProject(ProjectDto dto);
        */
        /// <summary>
        /// 编辑工程
        /// </summary>
        /// <param name="dto">项目信息</param>
        /// <returns></returns>
        void ModifyProject(ProjectDto dto);

        /// <summary>
        /// 提交创建项目申请
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="manageId">质监员Id</param>
        void SubmitCreateProjectReview(ProjectReviewHistoryDto dto, Guid manageId);

        /// <summary>
        /// 提交修改项目申请
        /// </summary>
        /// <param name="dto"></param>
        void SubmitModifyProjectReview(ProjectReviewHistoryDto dto);

        /// <summary>
        /// 提交删除项目申请
        /// </summary>
        /// <param name="dto"></param>
        void SubmitRemoveProjectReview(ProjectReviewHistoryDto dto);

        /// <summary>
        /// 修改项目名称
        /// </summary>
        /// <param name="projectId">项目Id</param>
        /// <param name="name">项目名称</param>
        /// <returns></returns>
        bool ChangeProjectName(Guid projectId, string name);

        /// <summary>
        /// 修改工程编号
        /// </summary>
        /// <param name="projectId">工程Id</param>
        /// <param name="childCode">工程编号</param>
        /// <returns></returns>
        bool ChangeProjectChildCode(Guid projectId, string childCode);

        /// <summary>
        /// 上传项目设计图片
        /// </summary>
        /// <param name="id">项目Id</param>
        [Obsolete("对接文件服务后，该方法不再使用")]
        string ProjectUploadImage(Guid id);

        /// <summary>
        /// 保存项目设计图片
        /// </summary>
        /// <param name="id">项目Id</param>
        /// <param name="path"></param>
        void ProjectSaveUploadImage(Guid id, string path);

        /// <summary>
        /// 检查工程一级分类最后审核状态
        /// </summary>
        /// <param name="projectId">项目/工程Id</param>
        bool CheckProjectFirstCategoryStatus(Guid projectId);

        /// <summary>
        /// 根据项目编码获取项目信息
        /// </summary>
        /// <param name="code">项目编码</param>
        /// <returns></returns>
        ProjectDto GetProjectByCode(string code, bool isChildParject = false);

        /// <summary>
        /// 根据项目Id获取项目信息
        /// </summary>
        /// <param name="id">项目Id</param>
        /// <returns></returns>
        ProjectDto GetProjectById(Guid id);

        ///获取项目下最大工程编码
        ProjectDto GetProjectMaxChildCode(Guid id);

        /// <summary>
        /// 获取项目分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns></returns>
        IPagedList<ProjectDto> GetProjectPagedList(ProjectCriteria criteria);

        /// <summary>
        /// 获取项目下的工程数量
        /// </summary>
        /// <param name="id">项目Id</param>
        /// <returns></returns>
        int GetProjectChildCountId(Guid id);

        /// <summary>
        /// 获取项目下 一级分类 与 二级分类 概况（分类为主）
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns></returns>
        IPagedList<ProjectCategoryFirstCategoryOverviewDto> GetProjectCategoryFirstCategoryOverview(ProjectOverviewCriteria criteria);

        /// <summary>
        /// 获取项目下 一级分类概况（项目为主）
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns></returns>
        IPagedList<ProjectFirstCategoryOverviewDto> GetProjectFirstCategoryOverview(ProjectOverviewCriteria criteria);

        /// <summary>
        /// 审核项目
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="status">审核状态：true通过，false打回</param>
        void ProjectReview(ProjectReviewHistoryDto dto, bool status);

        /// <summary>
        /// 审核工程
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="firstCategoryId">一级分类Id</param>
        /// <param name="status">审核状态：true通过，false打回</param>
        void ProjectReview(ProjectDto dto, Guid firstCategoryId, bool status);

        /// <summary>
        /// 重审工程（重置审核状态为未上报）
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="firstCategoryId">一级分类Id</param>
        void ResetProjectStatus(ProjectReviewHistoryDto dto, Guid firstCategoryId);

        #endregion

        #region 项目分类

        /// <summary>
        /// 根据分类获取二级分类集合
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        List<ProjectCategoryDto> GetProjectCategoryByType(int type);

        /// <summary>
        /// 获取项目分类分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <param name="repository"></param>
        /// <returns></returns>
        IPagedList<ProjectCategoryDto> GetProjectCategoryPagedList(ProjectCategoryCriteria criteria, IRepositoryBase repository = null);

        /// <summary>
        /// 初始化项目分类
        /// </summary>
        /// <param name="list"></param>
        void InitProjectCategory(List<DP_Project_Category> list);

        /// <summary>
        /// 获取项目分类，根据 父级 Id
        /// </summary>
        /// <param name="parentId">父级 Id</param>
        /// <param name="repository"></param>
        /// <returns></returns>
        ProjectCategoryDto GetProjectCategoryByParentId(Guid parentId, IRepositoryBase repository = null);

        /// <summary>
        /// 获取项目分类，根据 Id
        /// </summary>
        /// <param name="id">分类Id</param>
        /// <param name="repository"></param>
        /// <returns></returns>
        ProjectCategoryDto GetProjectCategoryById(Guid id, IRepositoryBase repository = null);

        /// <summary>
        /// 获取项目分类，根据 子级编号
        /// </summary>
        /// <param name="level">子级编号</param>
        /// <returns></returns>
        ProjectCategoryDto GetProjectCategoryByLevel(int level);

        #endregion

        #region 项目文件

        /// <summary>
        /// 获取项目文件-分页数据集合
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="repository"></param>
        /// <returns></returns>
        IPagedList<ProjectFileDto> GetProjectFilePagedList(ProjectFileCriteria criteria, IRepositoryBase repository = null);

        /// <summary>
        /// 创建项目文件
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Guid CreateProjectFile(ProjectFileDto dto);

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="threeCategoryId"></param>
        /// <returns></returns>
        [Obsolete("对接文件服务后，该方法不再使用")]
        ProjectFileDto ProjectUploadFile(Guid projectId, Guid threeCategoryId);

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Boolean DeleteFile(Guid id);

        #endregion

        #region 审核记录

        /// <summary>
        /// 提交审核（工程）
        /// </summary>
        /// <param name="dto">审核信息</param>
        /// <param name="firstCategoryId">一级分类Id</param>
        void SubmitProjectReview(ProjectReviewHistoryDto dto, Guid firstCategoryId);

        /// <summary>
        /// 撤回审核（工程）
        /// </summary>
        /// <param name="dto">审核信息</param>
        /// <param name="firstCategoryId">一级分类Id</param>
        void RecallProjectReview(ProjectReviewHistoryDto dto, Guid firstCategoryId);

        /// <summary>
        /// 撤回审核（项目）
        /// </summary>
        /// <param name="dto"></param>
        void RecallProjectReview(ProjectReviewHistoryDto dto);

        /// <summary>
        /// 获取项目审核记录分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <param name="repository"></param>
        /// <returns></returns>
        IPagedList<ProjectReviewHistoryDto> GetProjectReviewHistoryPagedList(ProjectReviewHistoryCriteria criteria, IRepositoryBase repository = null);

        #endregion

        #region 变更记录

        /// <summary>
        /// 提交变更审核
        /// </summary>
        /// <param name="dto"></param>
        void SubmitProjectChangeReview(ProjectChangeHistoryDto dto);

        /// <summary>
        /// 撤回变更审核（工程）
        /// </summary>
        /// <param name="id">审核记录Id</param>
        void RecallProjectChangeReview(Guid id);

        /// <summary>
        /// 审核变更申请
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="status">true通过，false打回</param>
        void ProjectChangeReview(ProjectChangeHistoryDto dto, bool status);

        /// <summary>
        /// 获取项目变更记录（待审核放在前面）-分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <param name="repository"></param>
        /// <returns></returns>
        Tuple<int, IPagedList<ProjectChangeHistoryDto>> GetProjectChangeHistorySubmitTimePagedList(ProjectChangeHistoryCriteria criteria, IRepositoryBase repository = null);

        #endregion

        #region 单项申请记录

        /// <summary>
        /// 创建或更新单项申报
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Guid CreateOrUpdateSubitemReview(SubitemReviewDto dto);

        /// <summary>
        /// 撤回单项申报审核记录
        /// </summary>
        /// <param name="dto"></param>
        void RecallSubitemReviewHistory(SubitemReviewHistoryDto dto);

        /// <summary>
        /// 审核单项申报审核记录
        /// </summary>
        /// <param name="dto"></param>
        void ReviewSubitemReviewHistory(SubitemReviewHistoryDto dto);

        /// <summary>
        /// 重审单项申报审核记录
        /// </summary>
        /// <param name="dto"></param>
        void ResetSubitemReviewHistory(SubitemReviewHistoryDto dto);

        /// <summary>
        /// 获取单项申报审核-分页数据集合
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        IPagedList<SubitemReviewDto> GetSubitemReviewPagedList(SubitemReviewHistoryCriteria criteria);

        /// <summary>
        /// 获取单项申报审核记录-分页数据集合
        /// </summary>
        /// <param name="criteria">筛选记录</param>
        /// <returns></returns>
        IPagedList<SubitemReviewHistoryDto> GetSubitemReviewHistoryPagedList(SubitemReviewHistoryCriteria criteria);

        #endregion

        #region 操作人记录相关

        /// <summary>
        /// 根据用户Id获取操作人信息
        /// </summary>
        /// <returns></returns>
        OperatorInfoDto GetOperatorInfoByAccountId();

        #endregion

        /// <summary>
        ///  根据分类查询编码
        /// </summary>
        /// <param name="level"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        string GetCodeByCategory(int level, int sort);
    }
}
