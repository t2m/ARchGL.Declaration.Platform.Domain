﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Service
{
    /// <summary>
    /// 管理员服务
    /// </summary>
    public interface IAdminService
    {
        OperatorModel Login(string userName, string password);

        void LoginOut();
    }
}
