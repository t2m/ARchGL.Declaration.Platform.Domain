﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 简单dto,只有id和value
    /// </summary>
    public class SimpleDto : IDto
    {
        public SimpleDto() { }

        public SimpleDto(Guid id,string value)
        {
            Id = id;
            Value = value;
        }

        public Guid Id { get; set; }

        public string Value { get; set; }
    }
}
