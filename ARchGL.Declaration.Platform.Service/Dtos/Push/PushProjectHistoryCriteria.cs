﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Web.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 推送数据记录筛选条件
    /// </summary>
    public class PushProjectHistoryCriteria : PagedCriteria
    {
        /// <summary>
        /// 推送状态：1成功，2失败
        /// </summary>
        public PushStatus Status { get; set; }

        /// <summary>
        /// 数据类型：1验收记录，2状态更新
        /// </summary>
        public PushType Type { get; set; }

        /// <summary>
        /// 关键字查询
        /// </summary>
        public string Keywords { get; set; }
    }
}
