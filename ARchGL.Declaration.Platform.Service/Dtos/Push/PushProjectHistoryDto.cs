﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 数据推送 ViewModel
    /// </summary>
    public class PushProjectHistoryDto : Push_Project_History
    {
    }


    /// <summary>
    /// 扩展类
    /// </summary>
    public static class PushProjectHistoryDtoExtensions
    {
        public static IQueryable<PushProjectHistoryDto> ToDto(this IQueryable<Push_Project_History> query)
        {
            return query.Select(x => new PushProjectHistoryDto
            {
                Id = x.Id,
                Url = x.Url,
                Data = x.Data,
                Message = x.Message,
                Type = x.Type,
                Status = x.Status,
                Count = x.Count,
                CreatorId = x.CreatorId,
                CreatorName = x.CreatorName,
                CreatedTime = x.CreatedTime,
                ModifierId = x.ModifierId,
                ModifierName = x.ModifierName,
                ModifiedTime = x.ModifiedTime,
            });
        }
    }
}
