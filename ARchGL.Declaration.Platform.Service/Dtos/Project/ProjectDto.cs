﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    public class ProjectReviewDto : ProjectDto
    {
        /// <summary>
        /// BIM状态：1未报审，2审核中/待审核，3已打回，4已通过，5已撤回
        /// </summary>
        public AuditStatus BIMStatus { get; set; }

        /// <summary>
        /// 元素报表状态：1未报审，2审核中/待审核，3已打回，4已通过，5已撤回
        /// </summary>
        public AuditStatus ElementStatus { get; set; }
    }        

    public class ProjectDto : OperatorAndRemarkEntityBase
    {
        /// <summary>
        /// 父级项目Id
        /// </summary>
        public Guid? ParentId { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 项目编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 工程编码，从000开始
        /// </summary>
        public string ChildCode { get; set; }

        /// <summary>
        /// 所在区域
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// 区域帐号Id
        /// </summary>
        public System.Guid AreaId { get; set; }

        /// <summary>
        /// 审核人Id
        /// </summary>
        public Guid AuditId { get; set; }

        /// <summary>
        /// 审核人名称
        /// </summary>
        public string AuditName { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        public DateTime AuditTime { get; set; }

        /// <summary>
        /// 是否 BIM（Building information model）
        /// </summary>
        public bool IsBIM { get; set; }

        /// <summary>
        /// 状态：1未报审，2审核中/待审核，3已打回，4已通过，5已撤回
        /// </summary>
        public AuditStatus Status { get; set; }

        /// <summary>
        /// 变更类型：1创建，2分解，3合并，4正常
        /// </summary>
        public ChangeType ChangeType { get; set; }

        /// <summary>
        /// 删除状态：1正常，2删除
        /// </summary>
        public SoftDeletedStatus? SoftDeleted { get; set; }

        #region 各种单位信息

        /// <summary>
        /// 建设单位名称
        /// </summary>
        public string ConstructionUnit { get; set; }

        /// <summary>
        /// 建设单位Id
        /// </summary>
        public Guid ConstructionUnitId { get; set; }

        /// <summary>
        /// 施工单位
        /// </summary>
        public string BuilderUnitNames { get; set; }

        /// <summary>
        /// 施工单位 Id（企业）
        /// </summary>
        public string BuilderUnitIds { get; set; }

        /// <summary>
        /// 监理单位
        /// </summary>
        public string SupervisionUnit { get; set; }

        /// <summary>
        /// 监理单位Id
        /// </summary>
        public Guid SupervisionUnitId { get; set; }

        /// <summary>
        /// 检测单位
        /// </summary>
        public string TestingUnit { get; set; }

        /// <summary>
        /// 检测单位Id
        /// </summary>
        public Guid TestingUnitId { get; set; }

        /// <summary>
        /// 预混供应商
        /// </summary>
        public string PremixedSupplier { get; set; }

        /// <summary>
        /// 预混供应商Id
        /// </summary>
        public Guid PremixedSupplierId { get; set; }

        /// <summary>
        /// 设计单位Id
        /// </summary>
        public string DesignUnit { get; set; }

        /// <summary>
        /// 设计单位Id
        /// </summary>
        public Guid DesignUnitId { get; set; }

        /// <summary>
        /// 勘测单位
        /// </summary>
        public string SurveyUnit { get; set; }

        /// <summary>
        /// 勘测单位Id
        /// </summary>
        public Guid SurveyUnitId { get; set; }

        #endregion

        /// <summary>
        /// 项目地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 定位（存储 X坐标，Y坐标）
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// 概览图
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// 图片上传时间
        /// </summary>
        public DateTime? ImageUploadTime { get; set; }

        /// <summary>
        /// 工程数量
        /// </summary>
        public int ProjectCount { get; set; }

        /// <summary>
        /// 类型：
        /// 1.房屋建筑工程
        /// 2.城市道路工程
        /// 3.城市桥梁工程
        /// 4.城市隧道工程
        /// 5.城镇给排水构筑物及管道工程
        /// 6.市政工程边坡及档护结构工程
        /// 7.城镇污水处理厂工程
        /// </summary>
        public int Type { get; set; }
    }

    /// <summary>
    /// 扩展类
    /// </summary>
    public static class ProjectDtoExtensions
    {
        public static IQueryable<ProjectDto> ToDto(this IQueryable<DP_Project> query)
        {
            return query.Select(x => new ProjectDto
            {
                Id = x.Id,
                ParentId = x.ParentId,
                Name = x.Name,
                Code = x.Code,
                ChildCode = x.ChildCode,
                Area = x.Area,
                AuditId = x.AuditId,
                AuditName = x.AuditName,
                AuditTime = x.AuditTime,
                IsBIM = x.IsBIM,
                Status = x.Status,
                SoftDeleted = x.SoftDeleted,
                Type = x.Type,

                ConstructionUnit = x.ConstructionUnit,
                ConstructionUnitId = x.ConstructionUnitId,
                BuilderUnitNames = x.BuilderUnitNames,
                BuilderUnitIds = x.BuilderUnitIds,
                SupervisionUnit = x.SupervisionUnit,
                SupervisionUnitId = x.SupervisionUnitId,
                TestingUnit = x.TestingUnit,
                TestingUnitId = x.TestingUnitId,
                PremixedSupplier = x.PremixedSupplier,
                PremixedSupplierId = x.PremixedSupplierId,
                DesignUnit = x.DesignUnit,
                DesignUnitId = x.DesignUnitId,
                SurveyUnit = x.SurveyUnit,
                SurveyUnitId = x.SurveyUnitId,

                OperatorName = x.OperatorName,
                Position = x.Position,
                Telephone = x.Telephone,
                Remark = x.Remark,

                Address = x.Address,
                Location = x.Location,
                Image = x.Image,
                ImageUploadTime = x.ImageUploadTime,
                CreatedTime = x.CreatedTime,
            });
        }
    }
}
