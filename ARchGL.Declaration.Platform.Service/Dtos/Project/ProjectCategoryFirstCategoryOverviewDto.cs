﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 项目下 一级分类 与 二级分类 概况（分类为主） ViewModel
    /// </summary>
    public class ProjectCategoryFirstCategoryOverviewDto : EntityBase
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 父级Id
        /// </summary>
        public System.Guid ParentId { get; set; }

        /// <summary>
        /// 顺序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 审核人名称
        /// </summary>
        public string AuditName { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        public System.DateTime AuditTime { get; set; }

        /// <summary>
        /// 报审时间
        /// </summary>
        public System.DateTime SubmitTime { get; set; }

        /// <summary>
        /// 状态：1未报审，2审核中/待审核，3已打回，4已通过，5已撤回
        /// </summary>
        public AuditStatus Status { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string OperatorName { get; set; }

        /// <summary>
        /// 岗位
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string Telephone { get; set; }

        /// <summary>
        /// 审核备注
        /// </summary>
        public string Remark { get; set; }

        #region 一级分类

        ///// <summary>
        ///// 地基与基础
        ///// </summary>
        //public int FirstCategoryCount1 { get; set; }

        ///// <summary>
        ///// 主体结构
        ///// </summary>
        //public int FirstCategoryCount2 { get; set; }

        ///// <summary>
        ///// 建筑节能
        ///// </summary>
        //public int FirstCategoryCount3 { get; set; }

        ///// <summary>
        ///// 竣工验收
        ///// </summary>
        //public int FirstCategoryCount4 { get; set; }

        #endregion

        #region 二级分类

        /// <summary>
        /// 质量验收记录
        /// </summary>
        public int SecondCategoryCount1 { get; set; }

        /// <summary>
        /// 质量控制资料核查记录
        /// </summary>
        public int SecondCategoryCount2 { get; set; }

        /// <summary>
        /// 安全和功能检验资料核查和主要功能抽查记录
        /// </summary>
        public int SecondCategoryCount3 { get; set; }

        /// <summary>
        /// 观感质量检查记录
        /// </summary>
        public int SecondCategoryCount4 { get; set; }

        public int SecondCategoryCount5 { get; set; }

        public int SecondCategoryCount6 { get; set; }

        public int SecondCategoryCount7 { get; set; }

        public int SecondCategoryCount8 { get; set; }

        public int SecondCategoryCount9 { get; set; }

        #endregion
    }

    /// <summary>
    /// 扩展类
    /// </summary>
    public static class ProjectCategoryFirstCategoryOverviewDtoExtensions
    {
        public static IQueryable<ProjectCategoryFirstCategoryOverviewDto> ToOverview(this IQueryable<DP_Project_Category> query)
        {
            return query.Select(x => new ProjectCategoryFirstCategoryOverviewDto
            {
                Id = x.Id,
                Name = x.Name,
                Sort = x.Sort,
                ParentId = x.ParentId,
                CreatedTime = x.CreatedTime,
            });
        }
    }
}
