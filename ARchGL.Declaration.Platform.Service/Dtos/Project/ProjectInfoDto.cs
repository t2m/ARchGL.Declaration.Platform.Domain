﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    public class ProjectInfoDto : ProjectDto
    {
        /// <summary>
        /// 地基与基础 审核状态
        /// </summary>
        public AuditStatus FirstCategoryStatus1 { get; set; }

        /// <summary>
        /// 主体结构 审核状态
        /// </summary>
        public AuditStatus FirstCategoryStatus2 { get; set; }

        /// <summary>
        /// 建筑节能 审核状态
        /// </summary>
        public AuditStatus FirstCategoryStatus3 { get; set; }

        /// <summary>
        /// 竣工验收 审核状态
        /// </summary>
        public AuditStatus FirstCategoryStatus4 { get; set; }
    }
}
