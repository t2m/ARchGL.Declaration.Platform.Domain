﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 项目分类 ViewModel
    /// </summary>
    public class ProjectCategoryDto : DP_Project_Category
    {

    }

    /// <summary>
    /// 扩展类
    /// </summary>
    public static class ProjectCategoryDtoExtensions
    {
        public static IQueryable<ProjectCategoryDto> ToDto(this IQueryable<DP_Project_Category> query)
        {
            return query.Select(x => new ProjectCategoryDto
            {
                Id = x.Id,
                Name = x.Name,
                ParentId = x.ParentId,
                Level = x.Level,
                Sort = x.Sort,
                Type = x.Type,
                CreatedTime = x.CreatedTime,
            });
        }
    }
}
