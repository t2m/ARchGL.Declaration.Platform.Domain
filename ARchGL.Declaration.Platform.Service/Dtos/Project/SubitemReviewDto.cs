﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    public class SubitemReviewDto : SubitemReview
    {
    }

    /// <summary>
    /// 扩展类
    /// </summary>
    public static class SubitemReviewDtoExtensions
    {
        /// <summary>
        /// 实体赋值
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReviewDto> ToDto(this IQueryable<SubitemReview> query)
        {
            return query.Select(x => new SubitemReviewDto
            {
                Id = x.Id,
                ProjectId = x.ProjectId,
                Name = x.Name,
                Code = x.Code,
                BuilderUnitIds = x.BuilderUnitIds,
                BuilderUnitNames = x.BuilderUnitNames,
                AuditId = x.AuditId,
                AuditName = x.AuditName,
                AuditTime = x.AuditTime,
                FileId = x.FileId,
                FileName = x.FileName,
                Path = x.Path,
                Type = x.Type,
                Status = x.Status,
                OperatorName = x.OperatorName,
                Position = x.Position,
                Telephone = x.Telephone,
                Remark = x.Remark,
                SubmitTime = x.SubmitTime,
                CreatorId = x.CreatorId,
                CreatedTime = x.CreatedTime,
            });
        }

        /// <summary>
        /// 获取单项审核状态
        /// </summary>
        /// <param name="list"></param>
        /// <param name="type"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public static AuditStatus GetStatus(this List<SubitemReviewDto> list, ApplyForType type,  Guid projectId)
        {
            var entity = list.FirstOrDefault(x => x.ProjectId == projectId &&  x.Type == type);

            return !entity.IsNull() ? entity.Status : AuditStatus.未报审;
        }
    }
}
