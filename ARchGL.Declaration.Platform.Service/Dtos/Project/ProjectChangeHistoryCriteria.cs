﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Web.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 项目变更记录筛选条件
    /// </summary>
    public class ProjectChangeHistoryCriteria : PagedCriteria
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 施工单位 Id（企业）
        /// </summary>
        public Guid BuilderUnitId { get; set; }

        /// <summary>
        /// 审核人Id
        /// </summary>
        public Guid AuditId { get; set; }

        /// <summary>
        /// 变更类型：1分解，2合并，3正常
        /// </summary>
        public ChangeType Type { get; set; }

        /// <summary>
        /// 状态：1未报审，2审核中/待审核，3已打回，4已通过，5已撤回
        /// </summary>
        public AuditStatus Status { get; set; }

        /// <summary>
        /// 审核状态集合
        /// </summary>
        public List<AuditStatus> StatusList { get; set; }

        /// <summary>
        /// 关键字查询
        /// </summary>
        public string Keywords { get; set; }

        /// <summary>
        /// 项目Id集合
        /// </summary>
        public List<Guid> ProjectIdList{ get; set; }

        /// <summary>
        /// 申请时间
        /// </summary>
        public List<DateTime> SubmitTimeList { get; set; }

        /// <summary>
        /// 不等于申请时间
        /// </summary>
        public List<DateTime> SubmitTimeNotInList { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        public List<DateTime> AuditTimeList { get; set; }
    }
}
