﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using TDF.Web.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 单项申报记录 筛选条件
    /// </summary>
    public class SubitemReviewHistoryCriteria : PagedCriteria
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 项目Id
        /// </summary>
        public List<Guid> ProjectIdList { get; set; }
        
        /// <summary>
        /// 申请单位Id（施工单位/区县管理员）
        /// </summary>
        public Guid ApplyForUnitId { set; get; }

        /// <summary>
        /// 审核人Id
        /// </summary>
        public Guid AuditId { get; set; }

        /// <summary>
        /// 申请类型
        /// </summary>
        public ApplyForType Type { get; set; }

        /// <summary>
        /// 审核状态：1未上报，2待审核，3已打回，4已通过，5已撤回，6重审
        /// </summary>
        public AuditStatus Status { get; set; }

        /// <summary>
        /// 关键字查询
        /// </summary>
        public string Keywords { get; set; }
    }
}
