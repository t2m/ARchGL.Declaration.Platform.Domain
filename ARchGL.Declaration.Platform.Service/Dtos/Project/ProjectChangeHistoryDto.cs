﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Linq;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 变更记录 ViewModel
    /// </summary>
    public class ProjectChangeHistoryDto : DP_Project_Change_History
    {

    }

    /// <summary>
    /// 扩展类
    /// </summary>
    public static class ProjectChangeHistoryDtoExtensions
    {
        /// <summary>
        /// ViewModel 赋值
        /// </summary>
        /// <param name="query">筛选信息</param>
        /// <returns></returns>
        public static IQueryable<ProjectChangeHistoryDto> ToDto(this IQueryable<DP_Project_Change_History> query)
        {
            return query.Select(x => new ProjectChangeHistoryDto
            {
                Id = x.Id,
                ProjectId = x.ProjectId,
                Name = x.Name,
                Code = x.Code,
                ChildCode = x.ChildCode,
                Type = x.Type,
                Status = x.Status,
                AuditId = x.AuditId,
                AuditName = x.AuditName,
                AuditTime = x.AuditTime,
                SubmitTime = x.SubmitTime,
                Origin = x.Origin,
                Target = x.Target,
                OriginProjectNames = x.OriginProjectNames,
                OriginProjectCodes = x.OriginProjectCodes,
                TargetProjectNames = x.TargetProjectNames,
                TargetProjectCodes = x.TargetProjectCodes,
                TargetProjectTypes = x.TargetProjectTypes,
                Remark = x.Remark,
                OperatorName = x.OperatorName,
                Position = x.Position,
                Telephone = x.Telephone,
                
                ConstructionUnit = x.ConstructionUnit,
                ConstructionUnitId = x.ConstructionUnitId,
                BuilderUnitNames = x.BuilderUnitNames,
                BuilderUnitIds = x.BuilderUnitIds,
                SupervisionUnit = x.SupervisionUnit,
                SupervisionUnitId = x.SupervisionUnitId,
                TestingUnit = x.TestingUnit,
                TestingUnitId = x.TestingUnitId,
                PremixedSupplier = x.PremixedSupplier,
                PremixedSupplierId = x.PremixedSupplierId,
                DesignUnit = x.DesignUnit,
                DesignUnitId = x.DesignUnitId,
                SurveyUnit = x.SurveyUnit,
                SurveyUnitId = x.SurveyUnitId,

                CreatorName = x.CreatorName,
                CreatedTime = x.CreatedTime,
            });
        }
    }
}
