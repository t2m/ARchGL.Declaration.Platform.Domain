﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 项目文件 ViewModel
    /// </summary>
    public class ProjectFileDto : EntityBase
    {
        /// <summary>
        /// 简称
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// 文件名全称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 文件相对路径
        /// </summary>
        public string Path { get; set; }
        
        /// <summary>
        /// 文件大小，保存时统一转换为KB，显示时根据需求转换对应单位
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// 文件后缀名
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// 文件Id（来自文件系统）
        /// </summary>
        public System.Guid FileId { get; set; }
        
        /// <summary>
        /// 删除状态：1正常，2删除
        /// </summary>
        public SoftDeletedStatus SoftDeleted { get; set; }

        /// <summary>
        /// 状态：1未上报，2已上报，3待审核，4已打回，5审核通过
        /// </summary>
        public AuditStatus Status { get; set; }

        /// <summary>
        /// 一级分类Id
        /// </summary>
        public Guid FirstCategoryId { get; set; }

        /// <summary>
        /// 二级分类Id
        /// </summary>
        public Guid SecondCategoryId { get; set; }
        
        /// <summary>
        /// 三级分类Id
        /// </summary>
        public Guid ThreeCategoryId { get; set; }

        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 文件上传时间
        /// </summary>
        public DateTime UploadTime { get; set; }
    }

    /// <summary>
    /// 扩展类
    /// </summary>
    public static class ProjectFileDtoExtensions
    {
        public static IQueryable<ProjectFileDto> ToDto(this IQueryable<DP_Project_File> query)
        {
            return query.Select(x => new ProjectFileDto
            {
                Id = x.Id,
                Name = x.Name,
                ShortName = x.ShortName,
                Path = x.Path,
                Size = x.Size,
                Extension = x.Extension,
                FileId = x.FileId,
                SoftDeleted = x.SoftDeleted,
                Status = x.Status,
                FirstCategoryId = x.FirstCategoryId,
                SecondCategoryId = x.SecondCategoryId,
                ThreeCategoryId = x.ThreeCategoryId,
                ProjectId = x.ProjectId,
                UploadTime = x.UploadTime,
                CreatedTime = x.CreatedTime,
            });
        }
    }
}
