﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Web.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 项目审核记录筛选条件
    /// </summary>
    public class ProjectReviewHistoryCriteria : PagedCriteria
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 审核人Id
        /// </summary>
        public Guid AuditId { get; set; }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public Guid? CreatorId { get; set; }

        /// <summary>
        /// 施工单位 Id（企业）
        /// </summary>
        public Guid BuilderUnitId { get; set; }

        /// <summary>
        /// 状态：1未报审，2审核中/待审核，3已打回，4已通过，5已撤回
        /// </summary>
        public AuditStatus Status { get; set; }

        /// <summary>
        /// 申请类型
        /// </summary>
        public ReviewType Type { get; set; }

        /// <summary>
        /// 申请类型集合
        /// </summary>
        public List<ReviewType> TypeList { get; set; }

        /// <summary>
        /// 一级分类Id
        /// </summary>
        public Guid FirstCategoryId { get; set; }

        /// <summary>
        /// 关键字查询
        /// </summary>
        public string Keywords { get; set; }

        /// <summary>
        /// 项目Id集合
        /// </summary>
        public List<Guid> ProjectIdList{ get; set; }
    }
}
