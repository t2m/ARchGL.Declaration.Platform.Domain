﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 审核记录 ViewModel
    /// </summary>
    public class ProjectReviewHistoryDto : DP_Project_Review_History
    {
        
    }

    /// <summary>
    /// 扩展类
    /// </summary>
    public static class ProjectReviewHistoryDtoExtensions
    {
        /// <summary>
        /// ViewModel 赋值
        /// </summary>
        /// <param name="query">筛选信息</param>
        /// <returns></returns>
        public static IQueryable<ProjectReviewHistoryDto> ToDto(this IQueryable<DP_Project_Review_History> query)
        {
            return query.Select(x => new ProjectReviewHistoryDto
            {
                Id = x.Id,
                Name = x.Name,
                ProjectId = x.ProjectId,
                Code = x.Code,
                ChildCode = x.ChildCode,
                BuilderUnitNames = x.BuilderUnitNames,
                FirstCategoryId = x.FirstCategoryId,
                FirstCategoryName = x.FirstCategoryName,
                FileCount = x.FileCount,
                AuditId = x.AuditId,
                AuditName = x.AuditName,
                AuditTime = x.AuditTime,
                SubmitTime = x.SubmitTime,
                Remark = x.Remark,
                Status = x.Status,
                Type = x.Type,
                OperatorName = x.OperatorName,
                Position = x.Position,
                Telephone = x.Telephone,
                ModifierName = x.ModifierName,
                CreatorName = x.CreatorName,
                CreatedTime = x.CreatedTime,
            });
        }
    }
}
