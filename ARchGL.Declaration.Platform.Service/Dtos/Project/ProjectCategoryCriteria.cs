﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Web.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 项目分类筛选条件
    /// </summary>
    public class ProjectCategoryCriteria : PagedCriteria
    {
        /// <summary>
        /// 父级Id
        /// </summary>
        public System.Guid? ParentId { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 子级编号
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// 类型：
        /// 1.房屋建筑工程
        /// 2.城市道路工程
        /// 3.城市桥梁工程
        /// 4.城市隧道工程
        /// 5.城镇给排水构筑物及管道工程
        /// 6.市政工程边坡及档护结构工程
        /// 7.城镇污水处理厂工程
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 关键字查询
        /// </summary>
        public string Keywords { get; set; }
    }
}
