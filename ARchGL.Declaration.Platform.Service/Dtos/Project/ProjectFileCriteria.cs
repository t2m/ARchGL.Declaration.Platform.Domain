﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Web.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 项目文件筛选条件
    /// </summary>
    public class ProjectFileCriteria : PagedCriteria
    {
        /// <summary>
        /// 简称
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// 文件名全称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 文件相对路径
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 状态：1未上报，2已上报，3待审核，4已打回，5审核通过
        /// </summary>
        public AuditStatus Status { get; set; }

        /// <summary>
        /// 一级分类Id
        /// </summary>
        public System.Guid FirstCategoryId { get; set; }

        /// <summary>
        /// 二级分类Id
        /// </summary>
        public System.Guid SecondCategoryId { get; set; }

        /// <summary>
        /// 三级分类Id
        /// </summary>
        public System.Guid ThreeCategoryId { get; set; }

        /// <summary>
        /// 项目Id
        /// </summary>
        public System.Guid ProjectId { get; set; }

        /// <summary>
        /// 项目Id 集合
        /// </summary>
        public List<System.Guid > ProjectIdList { get; set; }

        /// <summary>
        /// 关键字查询
        /// </summary>
        public string Keywords { get; set; }

        /// <summary>
        /// 用户Id集合
        /// </summary>
        public List<Guid> IdList{ get; set; }
    }
}
