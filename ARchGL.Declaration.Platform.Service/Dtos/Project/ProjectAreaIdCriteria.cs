﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Web.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 项目文件筛选条件
    /// </summary>
    public class ProjectAreaIdCriteria
    {
        /// <summary>
        /// 区域帐号Id
        /// </summary>
        public Guid AreaId { get; set; }
    }

    /// <summary>
    /// 单项申请筛选条件
    /// </summary>
    public class ProjectSubitemReportFrom : ProjectAreaIdCriteria
    {
        /// <summary>
        /// 申请类型：1.BIM认定、2元素表报
        /// </summary>
        public ApplyForType Type { get; set; }
    }

    /// <summary>
    /// 月统计报表筛选条件
    /// </summary>
    public class MonthReportFrom : ProjectAreaIdCriteria
    {
        /// <summary>
        /// 统计日期
        /// </summary>
        public string Date { get; set; }
    }
}
