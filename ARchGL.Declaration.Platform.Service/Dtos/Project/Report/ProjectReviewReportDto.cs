﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 已报项目中审核通过与未通过-统计 ViewModel
    /// </summary>
    public class ProjectReviewReport
    {
        /// <summary>
        /// 审核中
        /// </summary>
        public int ReviewingCount { get; set; }
        
        /// <summary>
        /// 已打回
        /// </summary>
        public int RejectCount { get; set; }

        /// <summary>
        /// 已通过
        /// </summary>
        public int PassCount { get; set; }
    }
}
