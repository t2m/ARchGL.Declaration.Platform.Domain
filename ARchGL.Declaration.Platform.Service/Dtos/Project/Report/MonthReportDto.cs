﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 统计月报 ViewModel
    /// </summary>
    public class MonthReportDto
    {
        /// <summary>
        /// 账户名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 帐号（区县、企业、项目帐号）
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// 月份
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// 数组中数据顺序为
        ///  1.总项目数
        ///  2.未验收项目
        ///  3.验收中项目
        ///  4.已验收项目
        ///  5.总工程数
        ///  6.未验收工程
        ///  7.验收中工程
        ///  8.已验收工程
        ///  9.BIM已认定
        /// 10.元素已验收
        /// </summary>
        public int[] TotalArray { get; set; }
    }
}
