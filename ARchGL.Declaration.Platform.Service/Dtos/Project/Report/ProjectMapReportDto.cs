﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 项目地图-统计 ViewModel
    /// </summary>
    public class ProjectMapReportDto
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 定位（存储 X坐标，Y坐标）
        /// </summary>
        public string Location { get; set; }

        ///// <summary>
        ///// 地基与基础-未报审
        ///// </summary>
        //public int FirstCategoryUnsubmitCount { get; set; }
        ///// <summary>
        ///// 地基与基础-审核中
        ///// </summary>
        //public int FirstCategoryReviewCount { get; set; }
        ///// <summary>
        ///// 地基与基础-已打回
        ///// </summary>
        //public int FirstCategoryRejectCount { get; set; }
        ///// <summary>
        ///// 地基与基础-已通过
        ///// </summary>
        //public int FirstCategoryPassCount { get; set; }

        /// <summary>
        /// 地基与基础 审核状态
        /// </summary>
        public AuditStatus FirstCategoryStatus1 { get; set; }

        /// <summary>
        /// 主体结构 审核状态
        /// </summary>
        public AuditStatus FirstCategoryStatus2 { get; set; }

        /// <summary>
        /// 建筑节能 审核状态
        /// </summary>
        public AuditStatus FirstCategoryStatus3 { get; set; }

        /// <summary>
        /// 竣工验收 审核状态
        /// </summary>
        public AuditStatus FirstCategoryStatus4 { get; set; }
    }
}
