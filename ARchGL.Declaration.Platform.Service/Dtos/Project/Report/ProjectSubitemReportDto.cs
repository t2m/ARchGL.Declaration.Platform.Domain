﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// BIM、质量元素-统计 ViewModel
    /// </summary>
    public class ProjectSubitemReportDto
    {
        /// <summary>
        /// 已完成
        /// </summary>
        public int Status2 { get; set; }

        /// <summary>
        /// 待审核
        /// </summary>
        public int Status4 { get; set; }
    }
}
