﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 已报未报项目-统计 ViewModel
    /// </summary>
    public class ProjectDeclarationReportDto
    {
        /// <summary>
        /// 已报
        /// </summary>
        public int Submit { get; set; }
        
        /// <summary>
        /// 未报
        /// </summary>
        public int Unsubmit { get; set; }
    }
}
