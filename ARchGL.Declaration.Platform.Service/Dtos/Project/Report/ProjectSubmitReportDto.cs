﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 已报项目中提交文件数-统计 ViewModel
    /// </summary>
    public class ProjectSubmitReportDto
    {
        /// <summary>
        /// 时间
        /// </summary>
        public string Date { get; set; }
        
        /// <summary>
        /// 文件数
        /// </summary>
        public int Count { get; set; }
    }
}
