﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 申请信息概况-统计 ViewModel
    /// </summary>
    public class WaitReviewReportDto
    {
        /// <summary>
        /// 1.项目申请
        /// 2.工程变更
        /// 3.工程验收
        /// 4.BIM申请
        /// 5.元素报表
        /// </summary>
        public int[] TotalArray { get; set; }

        /// <summary>
        /// 项目申请
        /// </summary>
        public int TotalCount1 { get; set; }
        /// <summary>
        /// 工程变更
        /// </summary>
        public int TotalCount2 { get; set; }
        /// <summary>
        /// 工程验收
        /// </summary>
        public int TotalCount3 { get; set; }
        /// <summary>
        /// BIM申请
        /// </summary>
        public int TotalCount4 { get; set; }
        /// <summary>
        /// 元素报表
        /// </summary>
        public int TotalCount5 { get; set; }
    }
}
