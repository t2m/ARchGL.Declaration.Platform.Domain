﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 项目下 一级分类 与 二级分类 统计（分类为主） ViewModel
    /// </summary>
    public class ProjectCategoryFirstCategoryReportDto
    {
        #region 一级分类

        /// <summary>
        /// 地基与基础-未报审
        /// </summary>
        public int FirstCategoryUnsubmitCount1 { get; set; }
        /// <summary>
        /// 地基与基础-审核中
        /// </summary>
        public int FirstCategoryReviewCount1 { get; set; }
        /// <summary>
        /// 地基与基础-已打回
        /// </summary>
        public int FirstCategoryRejectCount1 { get; set; }
        /// <summary>
        /// 地基与基础-已通过
        /// </summary>
        public int FirstCategoryPassCount1{ get; set; }


        /// <summary>
        /// 主体结构-未报审
        /// </summary>
        public int FirstCategoryUnsubmitCount2 { get; set; }
        /// <summary>
        /// 主体结构-审核中
        /// </summary>
        public int FirstCategoryReviewCount2 { get; set; }
        /// <summary>
        /// 主体结构-已打回
        /// </summary>
        public int FirstCategoryRejectCount2 { get; set; }
        /// <summary>
        /// 主体结构-已通过
        /// </summary>
        public int FirstCategoryPassCount2 { get; set; }


        /// <summary>
        /// 建筑节能-未报审
        /// </summary>
        public int FirstCategoryUnsubmitCount3 { get; set; }
        /// <summary>
        /// 建筑节能-审核中
        /// </summary>
        public int FirstCategoryReviewCount3 { get; set; }
        /// <summary>
        /// 建筑节能-已打回
        /// </summary>
        public int FirstCategoryRejectCount3 { get; set; }
        /// <summary>
        /// 建筑节能-已通过
        /// </summary>
        public int FirstCategoryPassCount3 { get; set; }


        /// <summary>
        /// 竣工验收-未报审
        /// </summary>
        public int FirstCategoryUnsubmitCount4 { get; set; }
        /// <summary>
        /// 竣工验收-审核中
        /// </summary>
        public int FirstCategoryReviewCount4 { get; set; }
        /// <summary>
        /// 竣工验收-已打回
        /// </summary>
        public int FirstCategoryRejectCount4 { get; set; }
        /// <summary>
        /// 竣工验收-已通过
        /// </summary>
        public int FirstCategoryPassCount4 { get; set; }

        #endregion
    }
}
