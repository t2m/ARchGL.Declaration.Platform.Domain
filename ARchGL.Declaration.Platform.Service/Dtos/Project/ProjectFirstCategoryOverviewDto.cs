﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 项目一级分类概况（项目为主） ViewModel
    /// </summary>
    public class ProjectFirstCategoryOverviewDto : EntityBase
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// 项目编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 工程编码，从000开始
        /// </summary>
        public string ChildCode { get; set; }

        /// <summary>
        /// 施工单位（实时获取）
        /// </summary>
        public string BuilderUnitNames { get; set; }

        /// <summary>
        /// 施工单位 Id（企业）
        /// </summary>
        public string BuilderUnitIds { get; set; }

        /// <summary>
        /// 所在区域
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// 文件数
        /// </summary>
        public int FileCount { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public AuditStatus Status { get; set; }

        /// <summary>
        /// 待审核分部工程数
        /// </summary>
        public int WaitReviewStatusCount { get; set; }

        /// <summary>
        /// 分部工程当前状态 1/5
        /// </summary>
        public string FirstCategoryStatus { get; set; }

        /// <summary>
        /// 审核人Id
        /// </summary>
        public System.Guid AuditId { get; set; }

        /// <summary>
        /// 审核人名称
        /// </summary>
        public string AuditName { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        public DateTime AuditTime { get; set; }

        /// <summary>
        /// 是否 BIM（Building information model）
        /// </summary>
        public bool IsBIM { get; set; }

        /// <summary>
        /// 定位（存储 X坐标，Y坐标）
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// 类型：
        /// 1.房屋建筑工程
        /// 2.城市道路工程
        /// 3.城市桥梁工程
        /// 4.城市隧道工程
        /// 5.城镇给排水构筑物及管道工程
        /// 6.市政工程边坡及档护结构工程
        /// 7.城镇污水处理厂工程
        /// </summary>
        public int Type { get; set; }

        #region 一级分类

        ///// <summary>
        ///// 地基与基础
        ///// </summary>
        //public int FirstCategoryCount1 { get; set; }

        ///// <summary>
        ///// 主体结构
        ///// </summary>
        //public int FirstCategoryCount2 { get; set; }

        ///// <summary>
        ///// 建筑节能
        ///// </summary>
        //public int FirstCategoryCount3 { get; set; }

        ///// <summary>
        ///// 竣工验收
        ///// </summary>
        //public int FirstCategoryCount4 { get; set; }


        /// <summary>
        /// 地基与基础 审核状态
        /// </summary>
        public AuditStatus FirstCategoryStatus1 { get; set; }
               
        /// <summary>
        /// 主体结构 审核状态
        /// </summary>
        public AuditStatus FirstCategoryStatus2 { get; set; }
        
        /// <summary>
        /// 建筑节能 审核状态
        /// </summary>
        public AuditStatus FirstCategoryStatus3 { get; set; }
               
        /// <summary>
        /// 竣工验收 审核状态
        /// </summary>
        public AuditStatus FirstCategoryStatus4 { get; set; }

        #endregion
    }

    /// <summary>
    /// 扩展类
    /// </summary>
    public static class ProjectFirstCategoryOverviewDtoExtensions
    {
        public static IQueryable<ProjectFirstCategoryOverviewDto> ToOverview(this IQueryable<DP_Project> query)
        {
            return query.Select(x => new ProjectFirstCategoryOverviewDto
            {
                Id = x.Id,
                Name = x.Name,
                CreatedTime = x.CreatedTime,
            });
        }
    }

}
