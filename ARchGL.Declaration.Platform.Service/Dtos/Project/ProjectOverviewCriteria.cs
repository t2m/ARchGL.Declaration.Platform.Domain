﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Web.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 项目概况筛选条件
    /// </summary>
    public class ProjectOverviewCriteria : PagedCriteria
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 工程编码，从000开始
        /// </summary>
        public string ChildCode { get; set; }

        /// <summary>
        /// 类型：
        /// 1.房屋建筑工程
        /// 2.城市道路工程
        /// 3.城市桥梁工程
        /// 4.城市隧道工程
        /// 5.城镇给排水构筑物及管道工程
        /// 6.市政工程边坡及档护结构工程
        /// 7.城镇污水处理厂工程
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 父级项目Id
        /// </summary>
        public Guid? ParentId { get; set; }

        /// <summary>
        /// 变更类型：1分解，2合并，3正常
        /// </summary>
        public ChangeType ChangeType { get; set; }

        /// <summary>
        /// 是否工程
        /// </summary>
        public bool? IsChildProject { get; set; }

        /// <summary>
        /// 审核人Id
        /// </summary>
        public Guid AuditId { get; set; }

        /// <summary>
        /// 施工单位 Id（企业）
        /// </summary>
        public Guid BuilderUnitId { get; set; }

        /// <summary>
        /// 区域帐号Id
        /// </summary>
        public Guid AreaId { get; set; }

        /// <summary>
        /// 项目 Id 集合
        /// </summary>
        public List<Guid> ProjectIdList { get; set; }

        /// <summary>
        /// 关键字查询
        /// </summary>
        public string Keywords { get; set; }
    }
}
