﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    public class SubitemReviewHistoryDto : SubitemReviewHistory
    {
    }

    /// <summary>
    /// 扩展类
    /// </summary>
    public static class SubitemReviewHistoryDtoExtensions
    {
        public static IQueryable<SubitemReviewHistoryDto> ToDto(this IQueryable<SubitemReviewHistory> query)
        {
            return query.Select(x => new SubitemReviewHistoryDto
            {
                Id = x.Id,
                ProjectId = x.ProjectId,
                Name = x.Name,
                Code = x.Code,
                BuilderUnitIds = x.BuilderUnitIds,
                BuilderUnitNames = x.BuilderUnitNames,
                AuditId = x.AuditId,
                AuditName = x.AuditName,
                AuditTime = x.AuditTime,
                FileId = x.FileId,
                FileName = x.FileName,
                Path = x.Path,
                Type = x.Type,
                Status = x.Status,
                OperatorName = x.OperatorName,
                Position = x.Position,
                Telephone = x.Telephone,
                Remark = x.Remark,
                SubmitTime = x.SubmitTime,
                CreatorId = x.CreatorId,
                CreatedTime = x.CreatedTime,
            });
        }
    }
}
