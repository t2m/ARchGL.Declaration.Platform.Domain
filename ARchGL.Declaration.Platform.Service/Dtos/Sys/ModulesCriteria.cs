﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Web.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 项目筛选条件
    /// </summary>
    public class ModulesCriteria : PagedCriteria
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 链接地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 状态：1启用、2禁用
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 操作项Ids
        /// </summary>
        public string Actions { get; set; }

        /// <summary>
        /// 关键字查询
        /// </summary>
        public string Keywords { get; set; }

        /// <summary>
        /// 模块（菜单）Id集合
        /// </summary>
        public List<Guid> IdList{ get; set; }

        /// <summary>
        /// 模块（菜单）Code 集合
        /// </summary>
        public List<string> CodeList { get; set; }
    }
}
