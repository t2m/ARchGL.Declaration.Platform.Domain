﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Web.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 帐号登录日志 筛选条件
    /// </summary>
    public class LoggingCriteria : PagedCriteria
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public System.Guid AccountId { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// 用户帐号
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// IP地址
        /// </summary>
        public string IPAddress { get; set; }

        /// <summary>
        /// 浏览器 User-Agent 完整信息
        /// </summary>
        public string UserAgent { get; set; }

        /// <summary>
        /// 关键字查询
        /// </summary>
        public string Keywords { get; set; }

        /// <summary>
        /// 操作项 Id 集合
        /// </summary>
        public List<Guid> AccountIdList{ get; set; }
    }
}
