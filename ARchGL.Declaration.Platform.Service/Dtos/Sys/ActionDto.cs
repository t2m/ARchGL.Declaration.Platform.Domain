﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 操作项 ViewModel
    /// </summary>
    public class ActionDto : Sys_Action
    {

    }


    /// <summary>
    /// 扩展类
    /// </summary>
    public static class ActionDtoExtensions
    {
        public static IQueryable<ActionDto> ToDto(this IQueryable<Sys_Action> query)
        {
            return query.Select(x => new ActionDto
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                Status = x.Status,
                CreatedTime = x.CreatedTime,
            });
        }
    }
}
