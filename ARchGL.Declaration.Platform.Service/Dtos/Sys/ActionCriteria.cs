﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Web.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 操作项 筛选条件
    /// </summary>
    public class ActionCriteria : PagedCriteria
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 关键字查询
        /// </summary>
        public string Keywords { get; set; }

        /// <summary>
        /// 操作项 Id 集合
        /// </summary>
        public List<Guid> IdList{ get; set; }

        /// <summary>
        /// 操作项 Code 集合
        /// </summary>
        public List<string> CodeList { get; set; }
    }
}
