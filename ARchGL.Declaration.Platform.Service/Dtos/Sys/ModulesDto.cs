﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 模块（菜单） ViewModel
    /// </summary>
    public class ModulesDto : Sys_Modules
    {

    }


    /// <summary>
    /// 扩展类
    /// </summary>
    public static class ModulesDtoExtensions
    {
        /// <summary>
        /// 实体赋值
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryable<ModulesDto> ToDto(this IQueryable<Sys_Modules> query)
        {
            return query.Select(x => new ModulesDto
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                Url = x.Url,
                Status = x.Status,
                Actions = x.Actions,
                CreatedTime = x.CreatedTime,
            });
        }
    }
}
