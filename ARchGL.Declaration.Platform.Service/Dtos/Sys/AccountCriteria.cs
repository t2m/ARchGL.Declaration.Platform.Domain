﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Web.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 帐号 筛选条件
    /// </summary>
    public class AccountCriteria : PagedCriteria
    {
        /// <summary>
        /// 帐号Id
        /// </summary>
        public string AccountId { get; set; }

        /// <summary>
        /// 类型：1区县、2企业、4项目、8质监员
        /// </summary>
        public AccountType Type { get; set; }

        /// <summary>
        /// 状态：1启用、2禁用
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 关键字查询
        /// </summary>
        public string Keywords { get; set; }

        /// <summary>
        /// 用户Id集合
        /// </summary>
        public List<Guid> IdList{ get; set; }
    }
}
