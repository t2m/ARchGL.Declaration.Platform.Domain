﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Linq;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 企业帐号
    /// </summary>
    public class CompanyAccountDto : AccountDto
    {
        /// <summary>
        /// 项目数
        /// </summary>
        public int ProjectCount { get; set; }
    }

    /// <summary>
    /// 区县帐号
    /// </summary>
    public class AreaAccountDto : CompanyAccountDto
    {
        /// <summary>
        /// 项目数
        /// </summary>
        public int AccountCount { get; set; }
    }

    /// <summary>
    /// 帐号 ViewModel
    /// </summary>
    public class AccountDto : Account
    {
        /// <summary>
        /// 隐藏密码信息
        /// </summary>
        public sealed override string Password { get; set; }

        /// <summary>
        /// 父级帐号名称
        /// </summary>
        public string ParentAccountName { get; set; }

        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 最后登录时间
        /// </summary>
        public DateTime Lastlogintime { get; set; }

        /// <summary>
        /// 最后登录时间
        /// </summary>
        public bool HasChangePassword { get; set; }

        /// <summary>
        /// 工程数量
        /// </summary>
        public int ProjectChildCount { get; set; }

        /// <summary>
        /// 登录 Token
        /// </summary>
        public string LoginToken { get; set; }
    }

    /// <summary>
    /// 扩展类
    /// </summary>
    public static class AccountDtoExtensions
    {
        /// <summary>
        /// 实体赋值
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryable<AccountDto> ToDto(this IQueryable<Account> query)
        {
            return query.Select(x => new AccountDto
            {
                Id = x.Id,
                Name = x.Name,
                Number = x.Number,
                Type = x.Type,
                Status = x.Status,
                AccountIds = x.AccountIds,
                Remark = x.Remark,
                CreatedTime = x.CreatedTime,
            });
        }

        /// <summary>
        /// 实体赋值
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryable<CompanyAccountDto> ToCompanyAccountDto(this IQueryable<Account> query)
        {
            return query.Select(x => new CompanyAccountDto
            {
                Id = x.Id,
                Name = x.Name,
                Number = x.Number,
                Type = x.Type,
                Status = x.Status,
                AccountIds = x.AccountIds,
                Remark = x.Remark,
                CreatedTime = x.CreatedTime,
            });
        }

        /// <summary>
        /// 实体赋值
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryable<AreaAccountDto> ToAreaAccountDto(this IQueryable<Account> query)
        {
            return query.Select(x => new AreaAccountDto
            {
                Id = x.Id,
                Name = x.Name,
                Number = x.Number,
                Type = x.Type,
                Status = x.Status,
                AccountIds = x.AccountIds,
                Remark = x.Remark,
                CreatedTime = x.CreatedTime,
            });
        }
    }
}
