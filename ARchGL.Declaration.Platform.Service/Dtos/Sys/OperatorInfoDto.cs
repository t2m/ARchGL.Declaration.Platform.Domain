﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 操作人信息 ViewModel
    /// </summary>
    public class OperatorInfoDto
    {
        /// <summary>
        /// 操作人
        /// </summary>
        public string OperatorName { get; set; }

        /// <summary>
        /// 岗位
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string Telephone { get; set; }
    }

    /// <summary>
    /// 扩展类
    /// </summary>
    public static class OperatorInfoDtoExtensions
    {
        /// <summary>
        /// 实体赋值
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryable<OperatorInfoDto> ToDto(this IQueryable<OperatorInfo> query)
        {
            return query.Select(x => new OperatorInfoDto
            {
                OperatorName = x.OperatorName,
                Position = x.Position,
                Telephone = x.Telephone,
            });
        }
    }
}
