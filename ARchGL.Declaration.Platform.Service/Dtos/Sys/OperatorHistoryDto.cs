﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 操作人记录 ViewModel
    /// </summary>
    public class OperatorHistoryDto : OperatorHistory
    {

    }

    /// <summary>
    /// 扩展类
    /// </summary>
    public static class OperatorHistoryDtoExtensions
    {
        /// <summary>
        /// 实体赋值
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryable<OperatorHistoryDto> ToDto(this IQueryable<OperatorHistory> query)
        {
            return query.Select(x => new OperatorHistoryDto
            {
                Id = x.Id,
                DataId = x.DataId,
                Type = x.Type,
                OperatorName = x.OperatorName,
                Position = x.Position,
                Telephone = x.Telephone,
                Remark = x.Remark,
            });
        }
    }
}
