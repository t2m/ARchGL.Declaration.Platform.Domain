﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 角色 ViewModel
    /// </summary>
    public class RoleDto : Sys_Role
    {

    }


    /// <summary>
    /// 扩展类
    /// </summary>
    public static class RoleDtoExtensions
    {
        public static IQueryable<RoleDto> ToDto(this IQueryable<Sys_Role> query)
        {
            return query.Select(x => new RoleDto
            {
                Id = x.Id,
                Name = x.Name,
                Status = x.Status,
                Modules = x.Modules,
                CreatedTime = x.CreatedTime,
            });
        }
    }
}
