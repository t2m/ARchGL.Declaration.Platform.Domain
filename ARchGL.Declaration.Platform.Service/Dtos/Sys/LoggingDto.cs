﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 帐号登录日志 ViewModel
    /// </summary>
    public class LoggingDto : Sys_Logging
    {

    }

    /// <summary>
    /// 扩展类
    /// </summary>
    public static class LoggingDtoExtensions
    {
        public static IQueryable<LoggingDto> ToDto(this IQueryable<Sys_Logging> query)
        {
            return query.Select(x => new LoggingDto
            {
                Id = x.Id,
                AccountId = x.AccountId,
                AccountName = x.AccountName,
                AccountNumber = x.AccountNumber,
                IPAddress = x.IPAddress,
                UserAgent = x.UserAgent,
                Status = x.Status,
                Descr = x.Descr,
                CreatedTime = x.CreatedTime,
            });
        }
    }
}
