﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Web.Models;

namespace ARchGL.Declaration.Platform.Service.Dtos
{
    /// <summary>
    /// 角色筛选条件
    /// </summary>
    public class RoleCriteria : PagedCriteria
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 状态：1启用、2禁用
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 模块Ids
        /// </summary>
        public string Modules { get; set; }

        /// <summary>
        /// 关键字查询
        /// </summary>
        public string Keywords { get; set; }

        /// <summary>
        /// 用户Id集合
        /// </summary>
        public List<Guid> IdList{ get; set; }
    }
}
