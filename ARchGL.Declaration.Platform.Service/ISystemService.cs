﻿using ARchGL.Declaration.Platform.Domain.Entities;
using ARchGL.Declaration.Platform.Service.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Models;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Service
{
    /// <summary>
    /// 系统服务
    /// </summary>
    public interface ISystemService
    {
        /// <summary>
        /// 初始化 Token
        /// </summary>
        /// <param name="token"></param>
        void InitLoginToken(string token);

        #region 用户相关

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="name">帐号</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        AccountDto Login(string name, string password);

        /// <summary>
        /// 根据账户 Id 查询账户信息
        /// </summary>
        /// <param name="id">账户Id</param>
        /// <returns></returns>
        AccountDto GetAccountById(Guid id);

        /// <summary>
        /// 返回当前区县下最大质监员编号
        /// </summary>
        /// <returns></returns>
        int GetAccountMaxNumber();

        /// <summary>
        /// 获取帐号分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns></returns>
        Tuple<int, IPagedList<AccountDto>> GetAccountPagedList(AccountCriteria criteria);

        /// <summary>
        /// 获取帐号集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns></returns>
        IList<AccountDto> GetAccountList(AccountCriteria criteria);

        /// <summary>
        /// 获取企业帐号缓存数据
        /// </summary>
        /// <returns></returns>
        IPagedList<AccountDto> GetCompanyCacheAccountList();

        /// <summary>
        /// 获取企业帐号-分页数据集合
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        Tuple<int, IPagedList<CompanyAccountDto>> GetCompanyAccountPagedList(AccountCriteria criteria);

        /// <summary>
        /// 获取区县帐号-分页数据集合
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        Tuple<int, IPagedList<AreaAccountDto>> GetAreaAccountPagedList(AccountCriteria criteria);

        /// <summary>
        /// 创建账号（审核、企业、项目）
        /// </summary>
        /// <param name="dto">帐号信息</param>
        /// <param name="areaId">区域帐号Id</param>
        /// <returns></returns>
        AccountDto CreateAccount(AccountDto dto, Guid areaId);

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="accountId">帐号Id</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        bool ChangePassword(Guid accountId, string password);

        /// <summary>
        /// 修改帐号名称
        /// </summary>
        /// <param name="accountId">帐号Id</param>
        /// <param name="name">帐号名称</param>
        /// <returns></returns>
        bool ChangeAccountName(Guid accountId, string name);

        /// <summary>
        /// 修改帐号状态
        /// </summary>
        /// <param name="accountId">帐号Id</param>
        /// <param name="remark">备注</param>
        /// <returns></returns>
        bool ChangeAccountStatus(Guid accountId, string remark);

        /// <summary>
        /// 重置密码为帐号
        /// </summary>
        /// <param name="accountId">帐号Id</param>
        /// <returns></returns>
        bool ResetPasswordById(Guid accountId);

        /// <summary>
        /// 重置密码为帐号
        /// </summary>
        /// <param name="number">帐号编码</param>
        /// <returns></returns>
        bool ResetPasswordByNumber(string number);

        /// <summary>
        /// 注销
        /// </summary>
        void Logout(string token);

        /// <summary>
        /// 获取帐号登录日志 分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns></returns>
        IPagedList<LoggingDto> GetLoggingPagedList(LoggingCriteria criteria);

        #endregion

        #region 登录 Session

        /// <summary>
        /// 清理过期 Session
        /// </summary>
        void ClearExpireSession();

        #endregion

        #region 角色相关

        /// <summary>
        /// 获取模块（菜单）分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns></returns>
        IPagedList<ModulesDto> GetModulesPagedList(ModulesCriteria criteria);

        #endregion
    }
}
