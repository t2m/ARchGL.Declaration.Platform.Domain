﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using TDF.Core.Configuration;

namespace ARchGL.Declaration.Platform.Service.EventHandles
{
    /// <summary>
    /// 公用方法
    /// </summary>
    public class BaseMethod
    {
        /// <summary>
        /// 获取HTTP请求头中包含的签名信息
        /// </summary>
        /// <returns></returns>
        public static List<string> GetCheckCode()
        {
            var configs = new Configs();
            List<string> list = new List<string>();

            string keyId = configs.GetValue("keyId");// "84983712-7cda-11e7-857d-00163e32d704"; 
            string keySecret = configs.GetValue("keySecret");// "8Yn8vptQYzsx3WX9nV72NyB4Q3p6U8"; 
            list.Add(keyId);//添加授权id

            var startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)); // 当地时区
            long timeStamp = (long)(DateTime.Now - startTime).TotalSeconds; // 相差秒数 即 时间戳
            list.Add(timeStamp.ToString());//添加时间戳

            Random rd = new Random();
            string str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            string randStr = "";
            for (int i = 0; i < 12; i++)
            {
                randStr += str[rd.Next(str.Length)];
            }
            list.Add(randStr);//添加随机字符串
            string sourceStr = randStr + "_" + timeStamp.ToString() + "_" + keySecret;
            string signature = SHA1_Encrypt(sourceStr);
            list.Add(signature);//添加密钥签名
            return list;
        }

        /// <summary>
        /// 对字符串进行SHA1加密
        /// </summary>
        /// <param name="sourceString">需要加密的字符串</param>
        /// <returns>密文</returns>
        public static string SHA1_Encrypt(string sourceString)
        {
            byte[] StrRes = Encoding.Default.GetBytes(sourceString);
            HashAlgorithm iSHA = new SHA1CryptoServiceProvider();
            StrRes = iSHA.ComputeHash(StrRes);
            StringBuilder EnText = new StringBuilder();
            foreach (byte iByte in StrRes)
            {
                EnText.AppendFormat("{0:x2}", iByte);
            }
            return EnText.ToString();
        }

        /// <summary>
        /// 执行请求
        /// </summary>
        /// <param name="request"></param>
        /// <param name="data">推送数据</param>
        /// <returns></returns>
        public static IRestResponse ExecuteRequest(RestRequest request, string data)
        {
            var client = new RestClient(new Configs().GetValue("QianBianUrl"));
            var checkList = GetCheckCode();
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "*/*");
            request.AddHeader("keyId", checkList[0]);
            request.AddHeader("ts", checkList[1]);
            request.AddHeader("rcode", checkList[2]);
            request.AddHeader("signature", checkList[3]);

            request.AddParameter("application/json; charset=utf-8", data, ParameterType.RequestBody);
            return client.Execute(request);
        }
    }
}
