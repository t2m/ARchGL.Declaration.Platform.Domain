﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Service
{
    /// <summary>
    /// 自动任务服务
    /// </summary>
    public interface IAutoTaskService
    {
        /// <summary>
        /// 启动自动任务
        /// </summary>
        void StartTask();
    }
}
