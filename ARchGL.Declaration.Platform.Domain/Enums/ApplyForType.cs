﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 单项申请类型
    /// </summary>
    public enum ApplyForType
    {
        BIM认定 = 1,
        元素表报 = 2,
    }
}
