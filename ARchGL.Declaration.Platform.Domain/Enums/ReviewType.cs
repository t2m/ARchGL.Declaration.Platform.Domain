﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 项目审核申请类型
    /// </summary>
    public enum ReviewType
    {
        创建项目 = 1,
        修改项目 = 2,
        删除项目 = 4,

        分部工程 = 8
    }
}
