﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 操作类型
    /// </summary>
    public enum OperatorType
    {
        /*
            1、项目方：申请工程变更、工程验收、BIM申请、元素报表
            2、管理员：项目增删改的申请
            3、质监员：审核工程变更、工程验收、BIM申请、元素报表
            4、总站方：审核项目增删改，审核元素报表
        */
        申请创建项目 = 1,
        申请删除项目 = 2,
        申请修改项目 = 3,

        审核创建项目 = 4,
        审核删除项目 = 5,
        审核修改项目 = 6,


        申请工程变更 = 7,
        申请工程验收 = 8,
        申请BIM申请 = 9,
        申请元素报表 = 10,

        审核工程变更 = 11,
        审核工程验收 = 12,
        审核BIM申请 = 13,
        审核元素报表 = 14,
    }
}
