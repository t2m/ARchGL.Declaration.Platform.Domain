﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    public partial class DP_Project_Change_History
    {
        #region 各种单位信息

        /// <summary>
        /// 建设单位名称
        /// </summary>
        public string ConstructionUnit { get; set; }

        /// <summary>
        /// 建设单位Id
        /// </summary>
        public Guid ConstructionUnitId { get; set; }

        /// <summary>
        /// 施工单位
        /// </summary>
        public string BuilderUnitNames { get; set; }

        /// <summary>
        /// 施工单位 Id（企业）
        /// </summary>
        public string BuilderUnitIds { get; set; }

        /// <summary>
        /// 监理单位
        /// </summary>
        public string SupervisionUnit { get; set; }

        /// <summary>
        /// 监理单位Id
        /// </summary>
        public Guid SupervisionUnitId { get; set; }

        /// <summary>
        /// 检测单位
        /// </summary>
        public string TestingUnit { get; set; }

        /// <summary>
        /// 检测单位Id
        /// </summary>
        public Guid TestingUnitId { get; set; }

        /// <summary>
        /// 预混供应商
        /// </summary>
        public string PremixedSupplier { get; set; }

        /// <summary>
        /// 预混供应商Id
        /// </summary>
        public Guid PremixedSupplierId { get; set; }

        /// <summary>
        /// 设计单位Id
        /// </summary>
        public string DesignUnit { get; set; }

        /// <summary>
        /// 设计单位Id
        /// </summary>
        public Guid DesignUnitId { get; set; }

        /// <summary>
        /// 勘测单位
        /// </summary>
        public string SurveyUnit { get; set; }

        /// <summary>
        /// 勘测单位Id
        /// </summary>
        public Guid SurveyUnitId { get; set; }

        #endregion
    }
}
