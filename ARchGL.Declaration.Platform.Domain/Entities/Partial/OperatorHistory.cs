﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 操作人信息
    /// </summary>
    [Table("dp_operator_info")]
    [Serializable]
    public partial class OperatorInfo : OperatorAndRemarkEntityBase
    {
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifiedTime { get; set; }
    }

    /// <summary>
    /// 操作记录
    /// </summary>
    [Table("dp_operator_history")]
    [Serializable]
    public partial class OperatorHistory : OperatorAndRemarkEntityBase
    {
        /// <summary>
        /// 操作人Id
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// 操作人名称
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// 数据Id（所有操作项的Id，使用时用于获取最后数据）
        /// </summary>
        public Guid DataId { get; set; }

        /// <summary>
        /// 操作类型
        /// </summary>
        public OperatorType Type { get; set; }
    }

    [Serializable]
    public class OperatorAndRemark : OperatorEntityBase
    {
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }

    /// <summary>
    /// 操作人待备注信息（EntityBase）
    /// </summary>
    [Serializable]
    public class OperatorAndRemarkEntityBase : OperatorEntityBase
    {
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }

    /// <summary>
    /// 操作人待备注信息（SystemEntity）
    /// </summary>
    [Serializable]
    public class OperatorAndRemarkSystemEntity : OperatorSystemEntity
    {
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }

    /// <summary>
    /// 操作人基类（EntityBase）
    /// </summary>
    [Serializable]
    public class OperatorEntityBase : EntityBase
    {
        /// <summary>
        /// 操作人
        /// </summary>
        public string OperatorName { get; set; }

        /// <summary>
        /// 岗位
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string Telephone { get; set; }
    }

    /// <summary>
    /// 操作人基类（SystemEntity）
    /// </summary>
    [Serializable]
    public class OperatorSystemEntity : SystemEntity
    {
        /// <summary>
        /// 操作人
        /// </summary>
        public string OperatorName { get; set; }

        /// <summary>
        /// 岗位
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string Telephone { get; set; }
    }
}
