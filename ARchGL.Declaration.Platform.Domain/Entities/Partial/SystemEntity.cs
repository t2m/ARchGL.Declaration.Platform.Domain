﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARchGL.Declaration.Platform.Domain.Infrastructure;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    public abstract partial class SystemEntity : ICreationAudited, IModificationAudited, IDeleteAudited, IBetweenCreatedTime
    {

    }
}
