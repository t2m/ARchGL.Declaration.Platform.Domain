﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    public partial class DP_Project
    {
        #region 各种单位信息

        /// <summary>
        /// 建设单位名称
        /// </summary>
        public string ConstructionUnit { get; set; }

        /// <summary>
        /// 建设单位Id
        /// </summary>
        public Guid ConstructionUnitId { get; set; }

        /// <summary>
        /// 施工单位
        /// </summary>
        public string BuilderUnitNames { get; set; }

        /// <summary>
        /// 施工单位 Id（企业）
        /// </summary>
        public string BuilderUnitIds { get; set; }

        /// <summary>
        /// 监理单位
        /// </summary>
        public string SupervisionUnit { get; set; }

        /// <summary>
        /// 监理单位Id
        /// </summary>
        public Guid SupervisionUnitId { get; set; }

        /// <summary>
        /// 检测单位
        /// </summary>
        public string TestingUnit { get; set; }

        /// <summary>
        /// 检测单位Id
        /// </summary>
        public Guid TestingUnitId { get; set; }

        /// <summary>
        /// 预混供应商
        /// </summary>
        public string PremixedSupplier { get; set; }

        /// <summary>
        /// 预混供应商Id
        /// </summary>
        public Guid PremixedSupplierId { get; set; }

        /// <summary>
        /// 设计单位Id
        /// </summary>
        public string DesignUnit { get; set; }

        /// <summary>
        /// 设计单位Id
        /// </summary>
        public Guid DesignUnitId { get; set; }

        /// <summary>
        /// 勘测单位
        /// </summary>
        public string SurveyUnit { get; set; }

        /// <summary>
        /// 勘测单位Id
        /// </summary>
        public Guid SurveyUnitId { get; set; }

        #endregion

        /// <summary>
        /// 操作人
        /// </summary>
        public string OperatorName { get; set; }

        /// <summary>
        /// 岗位
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string Telephone { get; set; }

        /// <summary>
        /// 审核备注
        /// </summary>
        public string Remark { get; set; }
    }
}
