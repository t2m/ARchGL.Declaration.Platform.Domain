﻿using ARchGL.Declaration.Platform.Domain.Infrastructure;
using System;
using TDF.Core.Entity;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    public  abstract partial class EntityBase : IEntity, IBetweenCreatedTime
    {

    }
}
