﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 单项申报
    /// </summary>
    [Table("dp_subitem_review")]
    public partial class SubitemReview : Lkooojasdfasdfasasdfasdfdf
    {
        
    }

    /// <summary>
    /// 单项申报记录
    /// </summary>
    [Table("dp_subitem_review_history")]
    public partial class SubitemReviewHistory : Lkooojasdfasdfasasdfasdfdf
    {
        
    }

    /// <summary>
    /// 单项申报记录基类
    /// </summary>
    public class Lkooojasdfasdfasasdfasdfdf : OperatorAndRemarkEntityBase
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 项目编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 申请单位Id（施工单位/区县管理员）
        /// </summary>
        public string BuilderUnitIds { set; get; }

        /// <summary>
        /// 申请单位名称（施工单位/区县管理员）
        /// </summary>
        public string BuilderUnitNames { set; get; }

        /// <summary>
        /// 审核人Id
        /// </summary>
        public Guid AuditId { get; set; }

        /// <summary>
        /// 审核人名称
        /// </summary>
        public string AuditName { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        public DateTime? AuditTime { get; set; }

        /// <summary>
        /// 申请类型：1.BIM认定、2元素表报
        /// </summary>
        public ApplyForType Type { get; set; }

        /// <summary>
        /// 审核状态：1未上报，2待审核，3已打回，4已通过，5已撤回，6重审
        /// </summary>
        public AuditStatus Status { get; set; }

        /// <summary>
        /// 文件Id（来自文件系统）
        /// </summary>
        public Guid FileId { get; set; }

        /// <summary>
        /// 申请文件
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件相对路径，多个用“|”分隔
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public Guid CreatorId { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName { get; set; }

        /// <summary>
        /// 报审时间
        /// </summary>
        public DateTime SubmitTime { get; set; }
    }
}
