﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Ioc;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    public static class ProjectCategoryExtensions
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Category> WhereByKeywords(this IQueryable<DP_Project_Category> query, string keywords)
        {
            if (string.IsNullOrWhiteSpace(keywords)) return query;
            return query.Where(x => x.Name.Contains(keywords));
        }

        /// <summary>
        /// 子级编号
        /// </summary>
        /// <param name="query"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Category> WhereByLevel(this IQueryable<DP_Project_Category> query, int level)
        {
            if (level == 0) return query;
            return query.Where(x => x.Level == level);
        }

        /// <summary>
        /// 项目分类类型
        /// </summary>
        /// <param name="query"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Category> WhereByType(this IQueryable<DP_Project_Category> query, int type)
        {
            if (type == 0) return query;
            return query.Where(x => x.Type == type);
        }

        /// <summary>
        /// 父级Id查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parentId"></param>
        /// <returns></returns> 
        public static IQueryable<DP_Project_Category> WhereByParentId(this IQueryable<DP_Project_Category> query, Guid? parentId)
        {
            if (!parentId.HasValue) return query;
            return query.Where(x => x.ParentId == parentId.Value);
        }
    }
}
