﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Ioc;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    public static class ProjectExtensions
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByKeywords(this IQueryable<DP_Project> query, string keywords)
        {
            if (string.IsNullOrWhiteSpace(keywords))
                return query;

            return query.Where(x => x.Name.Contains(keywords) || x.Code.Contains(keywords)
               || x.Area.Contains(keywords)
               || x.AuditName.Contains(keywords)
               || x.ConstructionUnit.Contains(keywords)
               || x.BuilderUnitNames.Contains(keywords)
               || x.SupervisionUnit.Contains(keywords)
               || x.TestingUnit.Contains(keywords)
               || x.PremixedSupplier.Contains(keywords)
               || x.Address.Contains(keywords)
               || x.Location.Contains(keywords));
        }

        /// <summary>
        /// 小于创建时间
        /// </summary>
        /// <param name="query"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByCreatedTime(this IQueryable<DP_Project> query, DateTime date)
        {
            if (date == DateTime.MinValue) return query;
            return query.Where(x => x.CreatedTime.Year == date.Year && x.CreatedTime.Month <= date.Month);
        }

        /// <summary>
        /// 变更状态查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByChangeType(this IQueryable<DP_Project> query, ChangeType type)
        {
            if (type == 0) return query;
            return query.Where(x => x.ChangeType == type);
        }

        /// <summary>
        /// 项目分类
        /// </summary>
        /// <param name="query"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByType(this IQueryable<DP_Project> query, int type)
        {
            if (type == 0) return query;
            return query.Where(x => x.Type == type);
        }

        /// <summary>
        /// 软删除状态
        /// </summary>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereBySoftDeletedStatus(this IQueryable<DP_Project> query, SoftDeletedStatus status)
        {
            if (status == 0) return query;
            return query.Where(x => x.SoftDeleted == status);
        }

        /// <summary>
        /// 查询工程编码
        /// </summary>
        /// <param name="query"></param>
        /// <param name="childCode"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByChildCode(this IQueryable<DP_Project> query, string childCode)
        {
            if (string.IsNullOrWhiteSpace(childCode)) return query;
            return query.Where(x => x.ChildCode == childCode);
        }

        /// <summary>
        /// 标识集合
        /// </summary>
        /// <param name="query"></param>
        /// <param name="idList"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByIdList(this IQueryable<DP_Project> query, List<Guid> idList)
        {
            if (idList == null || idList.Count == 0) return query;
            return query.Where(x => idList.Contains(x.Id));
        }

        /// <summary>
        /// 项目/工程 编号集合查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="codeList">编号集合</param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByCodeList(this IQueryable<DP_Project> query, List<string> codeList)
        {
            if (codeList == null || codeList.Count == 0) return query;
            return query.Where(x => codeList.Contains(x.Code));
        }

        /// <summary>
        /// 根据工程父级Id查询项目
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parentId">工程父级Id</param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByParentId(this IQueryable<DP_Project> query, Guid? parentId)
        {
            if (!parentId.HasValue) return query;
            return query.Where(x => x.ParentId == parentId.Value);
        }

        public static IQueryable<DP_Project> WhereByParentIdList(this IQueryable<DP_Project> query, List<Guid> idList)
        {
            if (idList == null || idList.Count == 0) return query;
            return query.Where(x => idList.Contains(x.ParentId.Value));
        }

        /// <summary>
        /// 项目Id查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByProjectId(this IQueryable<DP_Project> query, Guid? projectId)
        {
            if (!projectId.HasValue) return query;
            return query.Where(x => x.Id == projectId.Value);
        }

        /// <summary>
        /// 状态查询（不等于）
        /// </summary>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByStatusNotEquals(this IQueryable<DP_Project> query, AuditStatus status)
        {
            if (status == 0) return query;
            return query.Where(x => x.Status != status);
        }

        /// <summary>
        /// 审核人查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="auditId">审核人Id</param>
        /// <returns></returns> 
        public static IQueryable<DP_Project> WhereByAuditId(this IQueryable<DP_Project> query, Guid auditId)
        {
            if (auditId == Guid.Empty) return query;
            return query.Where(x => x.AuditId == auditId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByAuditIdList(this IQueryable<DP_Project> query, List<Guid> list)
        {
            if (list == null || list.Count == 0) return query;
            return query.Where(x => list.Contains(x.AuditId));
        }

        /// <summary>
        /// 施工单位 Id 查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="builderUnitId">施工单位帐号Id</param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByBuilderUnitId(this IQueryable<DP_Project> query, Guid builderUnitId)
        {
            if (builderUnitId == Guid.Empty) return query;
            var id = builderUnitId.ToString();
            return query.Where(x => x.BuilderUnitIds.Contains(id));
        }

        /// <summary>
        /// 施工单位 Id 集合查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="builderUnitIdList">施工单位帐号 Id 集合</param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByBuilderUnitIdList(this IQueryable<DP_Project> query, List<string> builderUnitIdList)
        {
            if (builderUnitIdList == null || builderUnitIdList.Count == 0) return query;
            return query.Where(x => builderUnitIdList.Contains(x.BuilderUnitIds));
        }

        /// <summary>
        /// 查询区域帐号Id
        /// </summary>
        /// <param name="query"></param>
        /// <param name="areaId">区域帐号</param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByAreaId(this IQueryable<DP_Project> query, Guid areaId)
        {
            if (areaId == Guid.Empty) return query;
            return query.Where(x => x.AreaId == areaId);
        }

        /// <summary>
        /// 区域Id集合
        /// </summary>
        /// <param name="query"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByAreaIdList(this IQueryable<DP_Project> query, List<Guid> list)
        {
            if (list == null || list.Count == 0) return query;
            return query.Where(x => list.Contains(x.AreaId));
        }

        /// <summary>
        /// 是否 BIM
        /// </summary>
        /// <param name="query"></param>
        /// <param name="isBIM"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByIsBIM(this IQueryable<DP_Project> query, bool? isBIM)
        {
            if (!isBIM.HasValue) return query;
            return query.Where(x => x.IsBIM == isBIM.Value);
        }

        /// <summary>
        /// 查询工程
        /// </summary>
        /// <param name="query"></param>
        /// <param name="isChildPrject">是否工程</param>
        /// <returns></returns>
        public static IQueryable<DP_Project> WhereByIsChildParject(this IQueryable<DP_Project> query, bool? isChildPrject)
        {
            if (!isChildPrject.HasValue) return query;
            return query.Where(x => x.ParentId != Guid.Empty);
        }

        public static IQueryable<DP_Project> WhereByChildCodeList(this IQueryable<DP_Project> query, List<string> childCodeList)
        {
            if (childCodeList == null || childCodeList.Count == 0) return query;
            return query.Where(x => childCodeList.Contains(x.ChildCode));
        }
    }
}
