﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Ioc;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 单项申请记录扩展
    /// </summary>
    public static class SubitemReviewHistoryExtensions
    {

        /// <summary>
        /// 关键字查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReviewHistory> WhereByKeywords(this IQueryable<SubitemReviewHistory> query, string keywords)
        {
            if (string.IsNullOrWhiteSpace(keywords))
                return query;

            return query.Where(x => x.Name.Contains(keywords) || x.Code.Contains(keywords) || x.BuilderUnitNames.Contains(keywords) || x.AuditName.Contains(keywords));
        }

        /// <summary>
        /// 状态查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReviewHistory> WhereByStatus(this IQueryable<SubitemReviewHistory> query, AuditStatus status)
        {
            if (status == 0) return query;
            return query.Where(x => x.Status == status);
        }

        /// <summary>
        /// 类型查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReviewHistory> WhereByType(this IQueryable<SubitemReviewHistory> query, ApplyForType type)
        {
            if (type == 0) return query;
            return query.Where(x => x.Type == type);
        }

        /// <summary>
        /// 审核人Id筛选
        /// </summary>
        /// <param name="query"></param>
        /// <param name="auditId"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReviewHistory> WhereByAuditId(this IQueryable<SubitemReviewHistory> query, Guid auditId)
        {
            if (auditId == Guid.Empty) return query;
            return query.Where(x => x.AuditId == auditId);
        }

        /// <summary>
        /// 申请单位Id（施工单位/区县管理员） 筛选
        /// </summary>
        /// <param name="query"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReviewHistory> WhereByApplyForUnitId(this IQueryable<SubitemReviewHistory> query, Guid id)
        {
            if (id == Guid.Empty) return query;
            return query.Where(x => x.BuilderUnitIds.Contains(id.ToString()));
        }

        /// <summary>
        /// 项目Id筛选
        /// </summary>
        /// <param name="query"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReviewHistory> WhereByProjectId(this IQueryable<SubitemReviewHistory> query, Guid id)
        {
            if (id == Guid.Empty) return query;
            return query.Where(x => x.ProjectId == id);
        }
    }
}
