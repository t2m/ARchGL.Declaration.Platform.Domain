﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Ioc;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 帐号登录 Session 扩展
    /// </summary>
    public static class SessionExtensions
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="keywords">关键字</param>
        /// <returns></returns>
        public static IQueryable<Sys_Session> WhereByKeywords(this IQueryable<Sys_Session> query, string keywords)
        {
            if (string.IsNullOrWhiteSpace(keywords))
                return query;

            return query.Where(x => x.AccountName.Contains(keywords)
                                || x.Number.Contains(keywords)
                                || x.Token.Contains(keywords)
                                || x.Authorization.Contains(keywords));
        }

        /// <summary>
        /// 过期 Session 查询
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryable<Sys_Session> WhereByExpire(this IQueryable<Sys_Session> query)
        {
            return query.Where(x => DateTime.Now > x.ExpiredTime);
        }
    }
}
