﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Ioc;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 角色扩展
    /// </summary>
    public static class RoleExtensions
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public static IQueryable<Sys_Role> WhereByKeywords(this IQueryable<Sys_Role> query, string keywords)
        {
            if (string.IsNullOrWhiteSpace(keywords))
                return query;

            return query.Where(x => x.Name.Contains(keywords));
        }

        /// <summary>
        /// 状态查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static IQueryable<Sys_Role> WhereByStatus(this IQueryable<Sys_Role> query, int status)
        {
            if (status == 0)
                return query;

            return query.Where(x => x.Status == status);
        }

        /// <summary>
        /// 模块查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="idStr"></param>
        /// <returns></returns>
        public static IQueryable<Sys_Role> WhereByModules(this IQueryable<Sys_Role> query, string idStr)
        {
            if (string.IsNullOrWhiteSpace(idStr)) return query;

            return query.Where(x => idStr.Contains(x.Modules));
        }
    }
}
