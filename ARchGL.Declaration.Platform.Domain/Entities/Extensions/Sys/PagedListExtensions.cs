﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Ioc;
using TDF.Core.Models;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 操作项扩展
    /// </summary>
    public static class PagedListExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="pageIndex"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public static PagedList<T> ToPageResult1<T>(this IQueryable<T> query, int pageIndex, int totalRows)
        {
            if (pageIndex == 0) pageIndex = 1;
            else --pageIndex;
            return query.ToPageResult(pageIndex, totalRows);
        }
    }
}
