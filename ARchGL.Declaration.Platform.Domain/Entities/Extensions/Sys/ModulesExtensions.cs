﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Ioc;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 模块（菜单）扩展
    /// </summary>
    public static class ModulesExtensions
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public static IQueryable<Sys_Modules> WhereByKeywords(this IQueryable<Sys_Modules> query, string keywords)
        {
            if (string.IsNullOrWhiteSpace(keywords))
                return query;

            return query.Where(x => x.Name.Contains(keywords) || x.Code.Contains(keywords) || x.Url.Contains(keywords));
        }

        /// <summary>
        /// 状态查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static IQueryable<Sys_Modules> WhereByStatus(this IQueryable<Sys_Modules> query, int status)
        {
            if (status == 0) return query;

            return query.Where(x => x.Status == status);
        }

        /// <summary>
        /// 编码查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public static IQueryable<Sys_Modules> WhereByCode(this IQueryable<Sys_Modules> query, string code)
        {
            if (string.IsNullOrWhiteSpace(code)) return query;

            return query.Where(x => x.Code == code);
        }
    }
}
