﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Entity;
using TDF.Core.Event;
using TDF.Core.Ioc;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public static class EntityExtensions
    {
        /// <summary>
        /// 创建
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        public static void Create<T>(this T entity) where T : ICreationAudited
        {
            entity.Id = Guid.NewGuid();
            entity.CreatedTime = DateTime.Now;
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        public static void Modify<T>(this T entity) where T : IModificationAudited
        {
            entity.ModifiedTime = DateTime.Now;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        public static void Remove<T>(this T entity) where T : IDeleteAudited
        {
            entity.DeletedTime = DateTime.Now;
            entity.Deleted = true;
        }

        /// <summary>
        /// 创建人赋值并保存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        public static void CreateByOperator<T>(this T entity) where T : SystemEntity
        {
            var provider = Ioc.Resolve<IOperatorProvider>();
            if (provider != null)
            {
                var oper = Ioc.Resolve<IOperatorProvider>().GetCurrent();
                if (oper != null)
                {
                    entity.CreatorId = oper.Id;
                    entity.CreatorName = oper.UserName;
                }
            }
            entity.Create();
        }

        /// <summary>
        /// 编辑人赋值并保存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        public static void ModifyByOperator<T>(this T entity) where T : SystemEntity
        {
            var oper = Ioc.Resolve<IOperatorProvider>().GetCurrent();
            if (oper != null)
            {
                entity.ModifierId = oper.Id;
                entity.ModifierName = oper.UserName;
            }
            entity.Modify();
        }

        /// <summary>
        /// 编辑人赋值并删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        public static void RemoveByOperator<T>(this T entity) where T : SystemEntity
        {
            var oper = Ioc.Resolve<IOperatorProvider>().GetCurrent();
            if (oper != null)
            {
                entity.ModifierId = oper.Id;
                entity.ModifierName = oper.UserName;
            }
            entity.Remove();
        }

        /// <summary>
        /// 是否为空或状态已删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static bool IsNullOrDeleted<T>(this T entity) where T : IDeleteAudited
        {
            return entity == null || entity.Deleted;
        }

        /// <summary>
        /// 是否为空
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static bool IsNull<T>(this T entity)
        {
            return entity == null;
        }
    }
}
