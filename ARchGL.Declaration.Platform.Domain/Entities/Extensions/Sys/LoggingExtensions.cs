﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Ioc;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 帐号登录日志扩展
    /// </summary>
    public static class LoggingExtensions
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="keywords">关键字</param>
        /// <returns></returns>
        public static IQueryable<Sys_Logging> WhereByKeywords(this IQueryable<Sys_Logging> query, string keywords)
        {
            if (string.IsNullOrWhiteSpace(keywords))
                return query;

            return query.Where(x => x.AccountName.Contains(keywords)
                                || x.AccountNumber.Contains(keywords)
                                || x.IPAddress.Contains(keywords)
                                || x.UserAgent.Contains(keywords));
        }

        /// <summary>
        /// 帐号Id 查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="accountId">帐号Id</param>
        /// <returns></returns>
        public static IQueryable<Sys_Logging> WhereByAccountId(this IQueryable<Sys_Logging> query, Guid accountId)
        {
            if (accountId == Guid.Empty) return query;

            return query.Where(x => x.AccountId == accountId);
        }

        /// <summary>
        /// 帐号Id集合 查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="idList">帐号Id集合</param>
        /// <returns></returns>
        public static IQueryable<Sys_Logging> WhereByAccountIdList(this IQueryable<Sys_Logging> query, List<Guid> idList)
        {
            if (idList == null || idList.Count == 0) return query;

            return query.Where(x => idList.Contains(x.AccountId));
        }
    }
}
