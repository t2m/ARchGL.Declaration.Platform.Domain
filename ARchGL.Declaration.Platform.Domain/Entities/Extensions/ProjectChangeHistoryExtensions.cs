﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Ioc;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    public static class ProjectChangeHistoryExtensions
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Change_History> WhereByKeywords(this IQueryable<DP_Project_Change_History> query, string keywords)
        {
            if (string.IsNullOrWhiteSpace(keywords)) return query;
            return query.Where(x => x.Name.Contains(keywords) || x.Code.Contains(keywords) || x.AuditName.Contains(keywords) || x.Remark.Contains(keywords) ||
                                x.OperatorName.Contains(keywords) || x.Position.Contains(keywords) || x.Telephone.Contains(keywords) ||
                                x.OriginProjectNames.Contains(keywords) || x.TargetProjectNames.Contains(keywords));
        }

        /// <summary>
        /// 状态查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Change_History> WhereByStatus(this IQueryable<DP_Project_Change_History> query, AuditStatus status)
        {
            if (status == 0) return query;
            return query.Where(x => x.Status == status);
        }

        /// <summary>
        /// 不等于状态查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Change_History> WhereByNotStatus(this IQueryable<DP_Project_Change_History> query, AuditStatus status)
        {
            if (status == 0) return query;
            return query.Where(x => x.Status != status);
        }

        /// <summary>
        /// 状态集合查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Change_History> WhereByStatusList(this IQueryable<DP_Project_Change_History> query, List<AuditStatus> list)
        {
            if (list == null || list.Count == 0) return query;
            return query.Where(x => list.Contains(x.Status));
        }

        /// <summary>
        /// 变更类型查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="type">变更类型</param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Change_History> WhereByType(this IQueryable<DP_Project_Change_History> query, ChangeType type)
        {
            if (type == 0) return query;
            return query.Where(x => x.Type == type);
        }

        /// <summary>
        /// 审核人查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="auditId">审核人Id</param>
        /// <returns></returns> 
        public static IQueryable<DP_Project_Change_History> WhereByAuditId(this IQueryable<DP_Project_Change_History> query, Guid auditId)
        {
            if (auditId == Guid.Empty) return query;
            return query.Where(x => x.AuditId == auditId);
        }

        /// <summary>
        /// 项目 Id 查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="projectId">项目Id</param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Change_History> WhereByProjectId(this IQueryable<DP_Project_Change_History> query, Guid projectId)
        {
            if (projectId == Guid.Empty) return query;
            return query.Where(x => x.ProjectId == projectId);
        }

        /// <summary>
        /// 施工单位 Id 查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="builderUnitId">施工单位帐号Id</param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Change_History> WhereByBuilderUnitId(this IQueryable<DP_Project_Change_History> query, Guid builderUnitId)
        {
            if (builderUnitId == Guid.Empty) return query;
            var id = builderUnitId.ToString();
            return query.Where(x => x.BuilderUnitIds.Contains(id));
        }

        /// <summary>
        /// 项目 Id 集合查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="idList">项目 Id 集合</param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Change_History> WhereByProjectIdList(this IQueryable<DP_Project_Change_History> query, List<Guid> idList)
        {
            if (idList == null || idList.Count == 0) return query;
            return query.Where(x => idList.Contains(x.ProjectId));
        }

        /// <summary>
        /// 申请时间查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Change_History> WhereBySubmitTimeList(this IQueryable<DP_Project_Change_History> query, List<DateTime> list)
        {
            if (list == null || list.Count == 0) return query;
            return query.Where(x => list.Contains(x.SubmitTime));
        }

        /// <summary>
        /// 排除指定申请时间
        /// </summary>
        /// <param name="query"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Change_History> WhereBySubmitTimeNotInList(this IQueryable<DP_Project_Change_History> query, List<DateTime> list)
        {
            if (list == null || list.Count == 0) return query;
            return query.Where(x => !list.Contains(x.SubmitTime));
        }

        /// <summary>
        /// 审核时间查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Change_History> WhereByAuditTimeList(this IQueryable<DP_Project_Change_History> query, List<DateTime> list)
        {
            if (list == null || list.Count == 0) return query;
            return query.Where(x => list.Contains(x.AuditTime));
        }
    }
}
