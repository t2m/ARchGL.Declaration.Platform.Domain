﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Ioc;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 项目审核记录
    /// </summary>
    public static class ProjectReviewHistoryExtensions
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Review_History> WhereByKeywords(this IQueryable<DP_Project_Review_History> query, string keywords)
        {
            if (string.IsNullOrWhiteSpace(keywords)) return query;

            return query.Where(x => x.Name.Contains(keywords) || x.BuilderUnitNames.Contains(keywords) || x.ChildCode.Contains(keywords)
                    || x.Code.Contains(keywords) || x.AuditName.Contains(keywords) || x.OperatorName.Contains(keywords) || x.Position.Contains(keywords) ||
                    x.Telephone.Contains(keywords) || x.Remark.Contains(keywords));
        }

        /// <summary>
        /// 项目 Id 集合
        /// </summary>
        /// <param name="query"></param>
        /// <param name="idList">项目 Id 集合</param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Review_History> WhereByProjectIdList(this IQueryable<DP_Project_Review_History> query, List<Guid> idList)
        {
            if (idList == null || idList.Count == 0) return query;
            return query.Where(x => idList.Contains(x.ProjectId));
        }

        /// <summary>
        /// 项目 Id 查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="projectId">项目Id</param>
        /// <returns></returns> 
        public static IQueryable<DP_Project_Review_History> WhereByProjectId(this IQueryable<DP_Project_Review_History> query, Guid projectId)
        {
            if (projectId == Guid.Empty) return query;
            return query.Where(x => x.ProjectId == projectId);
        }

        /// <summary>
        /// 审核人 Id 查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="auditId">审核人Id</param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Review_History> WhereByAuditId(this IQueryable<DP_Project_Review_History> query, Guid auditId)
        {
            if (auditId == Guid.Empty) return query;
            return query.Where(x => x.AuditId == auditId);
        }

        /// <summary>
        /// 创建人
        /// </summary>
        /// <param name="query"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Review_History> WhereByCreatorId(this IQueryable<DP_Project_Review_History> query, Guid? id)
        {
            if (!id.HasValue || id == Guid.Empty) return query;
            return query.Where(x => x.CreatorId == id);
        }

        /// <summary>
        /// 施工单位帐号Id
        /// </summary>
        /// <param name="query"></param>
        /// <param name="builderUnitId">施工单位帐号Id</param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Review_History> WhereByBuilderUnitId(this IQueryable<DP_Project_Review_History> query, Guid builderUnitId)
        {
            if (builderUnitId == Guid.Empty) return query;
            var id = builderUnitId.ToString();
            return query.Where(x => x.BuilderUnitIds.Contains(id));
        }

        /// <summary>
        /// 状态查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="status">状态：1未报审，2审核中/待审核，3已打回，4已通过，5已撤回</param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Review_History> WhereByStatus(this IQueryable<DP_Project_Review_History> query, AuditStatus status)
        {
            if (status == 0) return query;
            return query.Where(x => x.Status == status);
        }

        /// <summary>
        /// 申请类型
        /// </summary>
        /// <param name="query"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Review_History> WhereByType(this IQueryable<DP_Project_Review_History> query, ReviewType type)
        {
            if (type == 0) return query;
            return query.Where(x => x.Type == type);
        }

        /// <summary>
        /// 申请类型集合
        /// </summary>
        /// <param name="query"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Review_History> WhereByTypeList(this IQueryable<DP_Project_Review_History> query, List<ReviewType> list)
        {
            if (list == null || list.Count == 0) return query;
            return query.Where(x => list.Contains(x.Type));
        }

        /// <summary>
        /// 一级分类Id
        /// </summary>
        /// <param name="query"></param>
        /// <param name="firstCategoryId">一级分类Id</param>
        /// <returns></returns>
        public static IQueryable<DP_Project_Review_History> WhereByFirstCategoryId(this IQueryable<DP_Project_Review_History> query, Guid firstCategoryId)
        {
            if (firstCategoryId == Guid.Empty) return query;
            return query.Where(x => x.FirstCategoryId == firstCategoryId);
        }
    }
}
