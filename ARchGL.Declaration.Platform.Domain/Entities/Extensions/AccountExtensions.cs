﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Ioc;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 帐号扩展
    /// </summary>
    public static class AccountExtensions
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public static IQueryable<Account> WhereByKeywords(this IQueryable<Account> query, string keywords)
        {
            if (string.IsNullOrWhiteSpace(keywords))
                return query;

            return query.Where(x => x.Name.Contains(keywords) || x.Number.Contains(keywords));
        }

        /// <summary>
        /// 状态查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static IQueryable<Account> WhereByStatus(this IQueryable<Account> query, int status)
        {
            if (status == 0) return query;
            return query.Where(x => x.Status == status);
        }

        /// <summary>
        /// 类型查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IQueryable<Account> WhereByType(this IQueryable<Account> query, AccountType type)
        {
            if (type == 0) return query;
            return query.Where(x => x.Type == type);
        }

        /// <summary>
        /// 帐号Id筛选
        /// </summary>
        /// <param name="query"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public static IQueryable<Account> WhereByAccountIds(this IQueryable<Account> query, string accountId)
        {
            if (string.IsNullOrWhiteSpace(accountId)) return query;
            return query.Where(x => x.AccountIds.Contains(accountId));
        }

        public static IQueryable<Account> WhereByAccountIds(this IQueryable<Account> query, List<string> list)
        {
            if (list == null || list.Count == 0) return query;
            return query.Where(x => list.Contains(x.AccountIds));
            //return (from p in a query).Except();
        }

        public static IQueryable<Account> WhereByAccountIdList(this IQueryable<Account> query, List<Guid> list)
        {
            if (list == null || list.Count == 0) return query;
            return query.Where(x => list.Contains(x.Id));
        }

        #region Private method

        ///// <summary>
        ///// 反转位枚举 中文
        ///// </summary>
        ///// <param name="serviceRankFlag"></param>
        ///// <returns></returns>
        //private static string Names(this AccountType serviceRankFlag)
        //{
        //    if (serviceRankFlag == AccountType.管理员)
        //        return "管理员";

        //    var remark = serviceRankFlag;
        //    var result = new List<String>();
        //    var max = AccountType.管理员;
        //    while (max > AccountType.项目帐号)
        //    {
        //        if (remark - max >= 0)
        //        {
        //            result.Add(max.Name());
        //            remark -= max;
        //        }
        //        max = (AccountType)((Int32)max / 2);
        //    }
        //    result.Reverse();
        //    return string.Join("，", result.Where(m => !string.IsNullOrEmpty(m)));
        //}

        #endregion
    }
}
