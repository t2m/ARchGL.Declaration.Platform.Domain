﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Ioc;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public static class PushProjectHistoryExtensions
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public static IQueryable<Push_Project_History> WhereByKeywords(this IQueryable<Push_Project_History> query, string keywords)
        {
            if (string.IsNullOrWhiteSpace(keywords))
                return query;

            return query.Where(x => x.Url.Contains(keywords) || x.Data.Contains(keywords) || x.Message.Contains(keywords));
        }

        /// <summary>
        /// 推送状态查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static IQueryable<Push_Project_History> WhereByStatus(this IQueryable<Push_Project_History> query, PushStatus status)
        {
            if (status == 0) return query;

            return query.Where(x => x.Status == status);
        }

        /// <summary>
        /// 数据类型查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="type">数据类型：1验收记录，2状态更新</param>
        /// <returns></returns>
        public static IQueryable<Push_Project_History> WhereByType(this IQueryable<Push_Project_History> query, PushType type)
        {
            if (type == 0) return query;
            return query.Where(x => x.Type == type);
        }
    }
}
