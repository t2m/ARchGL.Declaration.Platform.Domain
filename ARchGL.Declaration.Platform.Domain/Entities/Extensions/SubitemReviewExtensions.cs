﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Ioc;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    /// <summary>
    /// 单项申请记录扩展
    /// </summary>
    public static class SubitemReviewExtensions
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReview> WhereByKeywords(this IQueryable<SubitemReview> query, string keywords)
        {
            if (string.IsNullOrWhiteSpace(keywords))
                return query;

            return query.Where(x => x.Name.Contains(keywords) || x.Code.Contains(keywords) || x.BuilderUnitNames.Contains(keywords) || x.AuditName.Contains(keywords));
        }

        /// <summary>
        /// 小于创建时间
        /// </summary>
        /// <param name="query"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReview> WhereByCreatedTime(this IQueryable<SubitemReview> query, DateTime date)
        {
            if (date == DateTime.MinValue) return query;
            return query.Where(x => x.CreatedTime.Year == date.Year && x.CreatedTime.Month <= date.Month);
        }


        /// <summary>
        /// 状态查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReview> WhereByStatus(this IQueryable<SubitemReview> query, AuditStatus status)
        {
            if (status == 0) return query;
            return query.Where(x => x.Status == status);
        }

        /// <summary>
        /// 类型查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReview> WhereByType(this IQueryable<SubitemReview> query, ApplyForType type)
        {
            if (type == 0) return query;
            return query.Where(x => x.Type == type);
        }

        /// <summary>
        /// 审核人Id筛选
        /// </summary>
        /// <param name="query"></param>
        /// <param name="auditId"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReview> WhereByAuditId(this IQueryable<SubitemReview> query, Guid auditId)
        {
            if (auditId == Guid.Empty) return query;
            return query.Where(x => x.AuditId == auditId);
        }

        /// <summary>
        /// 申请单位Id（施工单位/区县管理员） 筛选
        /// </summary>
        /// <param name="query"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReview> WhereByApplyForUnitId(this IQueryable<SubitemReview> query, Guid id)
        {
            if (id == Guid.Empty) return query;
            return query.Where(x => x.BuilderUnitIds.Contains(id.ToString()));
        }

        /// <summary>
        /// 项目Id筛选
        /// </summary>
        /// <param name="query"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReview> WhereByProjectId(this IQueryable<SubitemReview> query, Guid id)
        {
            if (id == Guid.Empty) return query;
            return query.Where(x => x.ProjectId == id);
        }

        /// <summary>
        /// 项目Id集合筛选
        /// </summary>
        /// <param name="query"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static IQueryable<SubitemReview> WhereByProjectIdList(this IQueryable<SubitemReview> query, List<Guid> list)
        {
            if (list == null || list.Count == 0) return query;
            return query.Where(x => list.Contains(x.ProjectId));
        }
    }
}
