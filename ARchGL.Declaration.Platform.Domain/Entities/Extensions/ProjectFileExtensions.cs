﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Infrastructure;
using TDF.Core.Ioc;
using TDF.Core.Operator;

namespace ARchGL.Declaration.Platform.Domain.Entities
{
    public static class ProjectFileExtensions
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_File> WhereByKeywords(this IQueryable<DP_Project_File> query, string keywords)
        {
            if (string.IsNullOrWhiteSpace(keywords))
                return query;

            return query.Where(x => x.Name.Contains(keywords) || x.ShortName.Contains(keywords) || x.Path.Contains(keywords));
        }

        /// <summary>
        /// 状态查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_File> WhereByStatus(this IQueryable<DP_Project_File> query, AuditStatus status)
        {
            if (status == 0) return query;
            return query.Where(x => x.Status == status);
        }

        /// <summary>
        /// 文件删除状态
        /// </summary>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_File> WhereBySoftDeletedStatus(this IQueryable<DP_Project_File> query, SoftDeletedStatus status)
        {
            if (status == 0) return query;
            return query.Where(x => x.SoftDeleted == status);
        }

        /// <summary>
        /// 项目Id查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="projectId">项目Id</param>
        /// <returns></returns> 
        public static IQueryable<DP_Project_File> WhereByProjectId(this IQueryable<DP_Project_File> query, Guid projectId)
        {
            if (projectId == Guid.Empty) return query;
            return query.Where(x => x.ProjectId == projectId);
        }

        /// <summary>
        /// 项目Id集合查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="idList">项目Id集合</param>
        /// <returns></returns> 
        public static IQueryable<DP_Project_File> WhereByProjectIdList(this IQueryable<DP_Project_File> query, List<Guid> idList)
        {
            if (idList == null || idList.Count == 0) return query;

            return query.Where(x => idList.Contains(x.ProjectId));
        }

        /// <summary>
        /// 文件路径查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="path">文件相对路径</param>
        /// <returns></returns>
        public static IQueryable<DP_Project_File> WhereByPath(this IQueryable<DP_Project_File> query, string path)
        {
            if (string.IsNullOrWhiteSpace(path)) return query;

            return query.Where(x => x.Path == path);
        }

        /// <summary>
        /// 未上传文件查询
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_File> WhereByPathIsNull(this IQueryable<DP_Project_File> query)
        {
            return query.Where(x => string.IsNullOrWhiteSpace(x.Path));
        }

        /// <summary>
        /// 已上传文件查询
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryable<DP_Project_File> WhereByPathIsNotNull(this IQueryable<DP_Project_File> query)
        {
            return query.Where(x => string.IsNullOrWhiteSpace(x.Path));
        }

        #region 分类查询

        /// <summary>
        /// 一级分类Id查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="firstCategoryId">一级分类Id</param>
        /// <returns></returns> 
        public static IQueryable<DP_Project_File> WhereByFirstCategoryId(this IQueryable<DP_Project_File> query, Guid firstCategoryId)
        {
            if (firstCategoryId == Guid.Empty) return query;
            return query.Where(x => x.FirstCategoryId == firstCategoryId);
        }

        /// <summary>
        /// 二级分类Id查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="secondCategoryId">一级分类Id</param>
        /// <returns></returns> 
        public static IQueryable<DP_Project_File> WhereBySecondCategoryId(this IQueryable<DP_Project_File> query, Guid secondCategoryId)
        {
            if (secondCategoryId == Guid.Empty) return query;
            return query.Where(x => x.SecondCategoryId == secondCategoryId);
        }

        /// <summary>
        /// 三级分类Id查询
        /// </summary>
        /// <param name="query"></param>
        /// <param name="threeCategoryId">一级分类Id</param>
        /// <returns></returns> 
        public static IQueryable<DP_Project_File> WhereByThreeCategoryId(this IQueryable<DP_Project_File> query, Guid threeCategoryId)
        {
            if (threeCategoryId == Guid.Empty) return query;
            return query.Where(x => x.ThreeCategoryId == threeCategoryId);
        }

        #endregion
    }
}
