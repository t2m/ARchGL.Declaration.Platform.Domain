
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------


namespace ARchGL.Declaration.Platform.Domain.Entities
{

using System;
    using System.Collections.Generic;

    /// <summary>
    /// SystemEntity
    /// </summary>
    [Serializable]
    public abstract partial class SystemEntity : EntityBase
{

	/// <summary>
    /// 是否已删除
    /// </summary>
    public bool Deleted { get; set; }

	/// <summary>
    /// 删除时间
    /// </summary>
    public Nullable<System.DateTime> DeletedTime { get; set; }

	/// <summary>
    /// 创建人Id
    /// </summary>
    public Nullable<System.Guid> CreatorId { get; set; }

	/// <summary>
    /// 修改人Id
    /// </summary>
    public Nullable<System.Guid> ModifierId { get; set; }

	/// <summary>
    /// 修改人姓名
    /// </summary>
    public string ModifierName { get; set; }

	/// <summary>
    /// 修改时间
    /// </summary>
    public Nullable<System.DateTime> ModifiedTime { get; set; }

	/// <summary>
    /// 创建人姓名
    /// </summary>
    public string CreatorName { get; set; }

}

}
