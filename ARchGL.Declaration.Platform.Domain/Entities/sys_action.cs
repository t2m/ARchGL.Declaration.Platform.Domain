
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------


namespace ARchGL.Declaration.Platform.Domain.Entities
{

using System;
    using System.Collections.Generic;
    
/// <summary>
/// 操作
/// </summary>
public partial class Sys_Action : SystemEntity
{

	/// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; }

	/// <summary>
    /// 编码
    /// </summary>
    public string Code { get; set; }

	/// <summary>
    /// 状态：1启用、2禁用
    /// </summary>
    public int Status { get; set; }

}

}
