﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Event;

namespace ARchGL.Declaration.Platform.Domain.Events
{
    /// <summary>
    /// 分项状态更新
    /// </summary>
    public class ProjectStatusChangeEvent : IEvent
    {
        /// <summary>
        /// 工程项目编码
        /// </summary>
        public string projectCode { set; get; }
        /// <summary>
        /// 分部工程Id
        /// 验收系统分部工程编码
        /// </summary>
        public string partCode { set; get; }
        /// <summary>
        /// 验收状态
        /// 1:未上报,2:待审核,3:已打回,4:审核通过
        /// </summary>
        public string status { set; get; }

        public bool CancelBubble { get; set; }
        public int Order { get; set; }
        public bool Async { get; set; }
    }
}
