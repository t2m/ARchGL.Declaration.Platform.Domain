﻿using System;
using TDF.Core.Event;

namespace ARchGL.Declaration.Platform.Domain.Events
{
    /// <summary>
    /// 验收记录
    /// </summary>
    public class ProjectUploadFileEvent : IEvent
    {
        /// <summary>
        /// 工程项目编码
        /// </summary>
        public string projectCode { set; get; }
        /// <summary>
        ///验收系统分部工程编码
        ///具体值如下:
        ///P001: 地基与基础
        ///P002: 主体结构
        ///P003: 建筑节能
        ///P004: 竣工验收
        /// </summary>
        public string partCode { set; get; }
        /// <summary>
        /// 验收系统资料类型编码, 值如下:
        /// DT001: 观感质量检查记录
        /// DT002: 质量验收记录
        /// DT003: 安全和功能检验资料核查和主要功能抽查记录
        /// DT004: 质量控制质量核查记录
        /// </summary>
        public string docTypeCode { set; get; }
        /// <summary>
        /// 表号
        /// </summary>
        public string docNo { set; get; }
        /// <summary>
        /// 表格名称
        /// </summary>
        public string docName { set; get; }
        /// <summary>
        /// 文件URL
        /// </summary>
        public string fileUrl { set; get; }
        /// <summary>
        /// 记录提交时间
        /// 日期格式 "YYYY-MM-DD HH:mm:ss"
        /// </summary>
        public DateTime recordTime { set; get; }
        /// <summary>
        /// 验收系统数据id       
        /// 验收系统内此条数据的主键, 用于验证数据重复提交或更新的情况
        /// </summary>
        public string sourceId { set; get; }

        public bool CancelBubble { get; set; }
        public int Order { get; set; }
        public bool Async { get; set; }
    }
}
