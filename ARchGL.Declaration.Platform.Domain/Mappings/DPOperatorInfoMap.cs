﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class DPOperatorInfoMap : EntityTypeConfiguration<OperatorInfo>
    {
        public DPOperatorInfoMap()
        {
            Property(x => x.OperatorName).HasMaxLength(500);
            Property(x => x.Position).HasMaxLength(500);
            Property(x => x.Telephone).HasMaxLength(500);
        }
    }
}
