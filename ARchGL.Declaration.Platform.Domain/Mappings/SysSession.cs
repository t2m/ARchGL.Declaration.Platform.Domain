﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class SysSession : EntityTypeConfiguration<Sys_Session>
    {
        public SysSession()
        {
            Property(x => x.Number).IsRequired().HasMaxLength(20);
            Property(x => x.AccountName).IsRequired().HasMaxLength(256);
            Property(x => x.Authorization).HasMaxLength(1024);
            Property(x => x.Token).IsRequired().HasMaxLength(50);
            Property(x => x.Roles).HasMaxLength(1024);
        }
    }
}
