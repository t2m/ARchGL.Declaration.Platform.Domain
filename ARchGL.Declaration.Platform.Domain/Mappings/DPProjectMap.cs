﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class DPProjectMap : EntityTypeConfiguration<DP_Project>
    {
        public DPProjectMap()
        {
            Property(x => x.Name).IsRequired().HasMaxLength(256);
            Property(x => x.Code).IsRequired().HasMaxLength(22);
            Property(x => x.ChildCode).HasMaxLength(10);
            Property(x => x.Area).HasMaxLength(50);
            Property(x => x.AuditName).HasMaxLength(256);            

            Property(x => x.ConstructionUnit).HasMaxLength(256);
            Property(x => x.BuilderUnitIds).HasMaxLength(512);
            Property(x => x.BuilderUnitNames).HasMaxLength(512);
            Property(x => x.SupervisionUnit).HasMaxLength(256);
            Property(x => x.TestingUnit).HasMaxLength(256);
            Property(x => x.PremixedSupplier).HasMaxLength(256);
            Property(x => x.DesignUnit).HasMaxLength(256);
            Property(x => x.SurveyUnit).HasMaxLength(256);
            Property(x => x.Address).HasMaxLength(256);
            Property(x => x.Location).HasMaxLength(50);
            Property(x => x.Image).HasMaxLength(500);
            
            Property(x => x.OperatorName).HasMaxLength(50);
            Property(x => x.Position).HasMaxLength(50);
            Property(x => x.Telephone).HasMaxLength(30);
            Property(x => x.Remark).HasMaxLength(500);
        }
    }
}
