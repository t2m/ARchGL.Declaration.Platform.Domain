﻿using System.Data.Entity.ModelConfiguration;
using ARchGL.Declaration.Platform.Domain.Entities;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class AdminMap : BaseEntityTypeConfiguration<Admin>
    {
        public AdminMap()
        {
            Property(x => x.Account).IsRequired().HasMaxLength(50);
            Property(x => x.Password).IsRequired().HasMaxLength(50);
        }
    }
}
