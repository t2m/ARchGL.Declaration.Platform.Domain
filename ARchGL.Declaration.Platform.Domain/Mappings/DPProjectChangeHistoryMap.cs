﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class DPProjectChangeHistoryMap : EntityTypeConfiguration<DP_Project_Change_History>
    {
        public DPProjectChangeHistoryMap()
        {
            Property(x => x.AuditName).IsRequired().HasMaxLength(256);
            Property(x => x.Code).IsRequired().HasMaxLength(22);
            Property(x => x.ChildCode).HasMaxLength(10);
            Property(x => x.Name).HasMaxLength(256);
            Property(x => x.OriginProjectNames).HasMaxLength(2560);
            Property(x => x.OriginProjectCodes).HasMaxLength(512);
            Property(x => x.TargetProjectNames).HasMaxLength(2560);
            Property(x => x.TargetProjectCodes).HasMaxLength(512);
            Property(x => x.AuditName).HasMaxLength(256);
            Property(x => x.Remark).HasMaxLength(1024);

            Property(x => x.OperatorName).HasMaxLength(50);
            Property(x => x.Position).HasMaxLength(50);
            Property(x => x.Telephone).HasMaxLength(30);
            
            Property(x => x.ConstructionUnit).HasMaxLength(256);
            Property(x => x.BuilderUnitIds).HasMaxLength(512);
            Property(x => x.BuilderUnitNames).HasMaxLength(512);
            Property(x => x.SupervisionUnit).HasMaxLength(256);
            Property(x => x.TestingUnit).HasMaxLength(256);
            Property(x => x.PremixedSupplier).HasMaxLength(256);
            Property(x => x.DesignUnit).HasMaxLength(256);
            Property(x => x.SurveyUnit).HasMaxLength(256);

            Property(x => x.CreatorName).HasMaxLength(256);
            Property(x => x.ModifierName).HasMaxLength(256);
        }
    }
}
