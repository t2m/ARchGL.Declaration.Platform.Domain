﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class AccountMap : EntityTypeConfiguration<Account>
    {
        public AccountMap()
        {
            Property(x => x.Name).IsRequired().HasMaxLength(256);
            Property(x => x.Number).IsRequired().HasMaxLength(20);
            Property(x => x.PhoneNumber).HasMaxLength(20); 
            Property(x => x.Password).IsRequired().HasMaxLength(32);
            Property(x => x.AccountIds).HasMaxLength(1024);
            Property(x => x.Remark).HasMaxLength(500); 
            Property(x => x.Roles).HasMaxLength(1000);
        }
    }
}
