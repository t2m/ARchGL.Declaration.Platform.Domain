﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class SysLogging : EntityTypeConfiguration<Sys_Logging>
    {
        public SysLogging()
        {
            Property(x => x.AccountName).IsRequired().HasMaxLength(256);
            Property(x => x.AccountNumber).IsRequired().HasMaxLength(256); 
            Property(x => x.IPAddress).HasMaxLength(50);
            Property(x => x.UserAgent).HasMaxLength(512);
            Property(x => x.Descr).HasMaxLength(50);
        }
    }
}
