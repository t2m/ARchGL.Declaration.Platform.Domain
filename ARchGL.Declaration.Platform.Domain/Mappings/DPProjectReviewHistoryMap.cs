﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class DPProjectReviewHistoryMap : EntityTypeConfiguration<DP_Project_Review_History>
    {
        public DPProjectReviewHistoryMap()
        {
            Property(x => x.Name).IsRequired().HasMaxLength(256);
            Property(x => x.Code).IsRequired().HasMaxLength(22);
            Property(x => x.ChildCode).HasMaxLength(10);
            Property(x => x.BuilderUnitIds).HasMaxLength(256);
            Property(x => x.BuilderUnitNames).HasMaxLength(512);            
            Property(x => x.FirstCategoryName).HasMaxLength(256);
            Property(x => x.AuditName).HasMaxLength(256);
            Property(x => x.Remark).HasMaxLength(1024);

            Property(x => x.OperatorName).HasMaxLength(50);
            Property(x => x.Position).HasMaxLength(50);
            Property(x => x.Telephone).HasMaxLength(30);

            Property(x => x.CreatorName).HasMaxLength(256);
            Property(x => x.ModifierName).HasMaxLength(256);
        }
    }
}
