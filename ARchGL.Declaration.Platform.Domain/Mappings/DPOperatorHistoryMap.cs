﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class DPOperatorHistoryMap : EntityTypeConfiguration<OperatorHistory>
    {
        public DPOperatorHistoryMap()
        {
            Property(x => x.OperatorName).HasMaxLength(500);
            Property(x => x.Position).HasMaxLength(500);
            Property(x => x.Telephone).HasMaxLength(500);
            Property(x => x.Remark).HasMaxLength(500);
        }
    }
}
