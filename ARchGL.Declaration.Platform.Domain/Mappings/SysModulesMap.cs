﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class SysModulesMap : EntityTypeConfiguration<Sys_Modules>
    {
        public SysModulesMap()
        {
            Property(x => x.Name).IsRequired().HasMaxLength(100);
            Property(x => x.Code).IsRequired().HasMaxLength(50);
            Property(x => x.Url).IsRequired().HasMaxLength(100);
            Property(x => x.Actions).HasMaxLength(1000);

            Property(x => x.CreatorName).IsRequired().HasMaxLength(256);
            Property(x => x.ModifierName).IsRequired().HasMaxLength(256);
        }
    }
}
