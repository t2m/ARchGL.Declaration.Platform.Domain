﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class DPProjectFileMap : EntityTypeConfiguration<DP_Project_File>
    {
        public DPProjectFileMap()
        {
            Property(x => x.ShortName).HasMaxLength(256);
            Property(x => x.Name).HasMaxLength(256);            
            Property(x => x.Path).HasMaxLength(2048);
            Property(x => x.Extension).HasMaxLength(10);
            Property(x => x.CreatorName).HasMaxLength(256);
        }
    }
}
