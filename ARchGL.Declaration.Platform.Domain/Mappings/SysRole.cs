﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class SysRole : EntityTypeConfiguration<Sys_Role>
    {
        public SysRole()
        {
            Property(x => x.Name).IsRequired().HasMaxLength(100);
            Property(x => x.Modules).HasMaxLength(1000);

            Property(x => x.CreatorName).IsRequired().HasMaxLength(256);
            Property(x => x.ModifierName).IsRequired().HasMaxLength(256);
        }
    }
}
