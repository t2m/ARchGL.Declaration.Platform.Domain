﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class DPProjectCategoryMap : EntityTypeConfiguration<DP_Project_Category>
    {
        public DPProjectCategoryMap()
        {
            Property(x => x.Name).IsRequired().HasMaxLength(256);
        }
    }
}
