﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class DPSubitemReviewMap : EntityTypeConfiguration<SubitemReview>
    {
        public DPSubitemReviewMap()
        {
            Property(x => x.Name).IsRequired().HasMaxLength(256);
            Property(x => x.Code).IsRequired().HasMaxLength(22);

            Property(x => x.BuilderUnitNames).HasMaxLength(256);
            Property(x => x.AuditName).HasMaxLength(256);

            Property(x => x.Path).HasMaxLength(2048);
            Property(x => x.FileName).HasMaxLength(100);

            Property(x => x.CreatorName).HasMaxLength(256);

            Property(x => x.OperatorName).HasMaxLength(50);
            Property(x => x.Position).HasMaxLength(50);
            Property(x => x.Telephone).HasMaxLength(30);
            Property(x => x.Remark).HasMaxLength(500);
        }
    }
}
