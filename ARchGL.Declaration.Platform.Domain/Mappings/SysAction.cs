﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class SysAction : EntityTypeConfiguration<Sys_Action>
    {
        public SysAction()
        {
            Property(x => x.Name).IsRequired().HasMaxLength(100);
            Property(x => x.Code).IsRequired().HasMaxLength(50);

            Property(x => x.CreatorName).IsRequired().HasMaxLength(256);
            Property(x => x.ModifierName).IsRequired().HasMaxLength(256); 
        }
    }
}
