﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ARchGL.Declaration.Platform.Domain.Mappings
{
    public class PushProjectHistory : EntityTypeConfiguration<Push_Project_History>
    {
        public PushProjectHistory()
        {
            Property(x => x.Url).IsRequired().HasMaxLength(100);
            Property(x => x.Data).IsRequired().HasMaxLength(4096);
            Property(x => x.Message).HasMaxLength(4096);

            Property(x => x.CreatorName).IsRequired().HasMaxLength(256);
            Property(x => x.ModifierName).IsRequired().HasMaxLength(256); 
        }
    }
}
