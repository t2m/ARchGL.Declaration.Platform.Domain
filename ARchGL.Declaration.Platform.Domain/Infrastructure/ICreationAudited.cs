﻿using System;
using TDF.Core.Entity;

namespace ARchGL.Declaration.Platform.Domain.Infrastructure
{
    public interface ICreationAudited : IEntity
    {
        DateTime CreatedTime { get; set; }

        string CreatorName { get; set; }

        Guid? CreatorId { get; set; }
    }
}
