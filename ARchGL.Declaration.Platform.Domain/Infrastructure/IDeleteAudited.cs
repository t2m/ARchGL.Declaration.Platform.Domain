﻿using System;
using System.Linq;
using TDF.Core.Entity;

namespace ARchGL.Declaration.Platform.Domain.Infrastructure
{
    public interface IDeleteAudited : IEntity
    {
        bool Deleted { get; set; }

        DateTime? DeletedTime { get; set; }
    }

    public static class DeleteAuditedExtensions
    {
        public static IQueryable<T> WhereNotDelete<T>(this IQueryable<T> query) where T : class, IDeleteAudited
        {

            return query.Where(x => x.Deleted == false);
        }
    }
}
