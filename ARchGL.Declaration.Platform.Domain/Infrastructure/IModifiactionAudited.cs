﻿using System;
using TDF.Core.Entity;

namespace ARchGL.Declaration.Platform.Domain.Infrastructure
{
    public interface IModificationAudited : IEntity
    {
        DateTime? ModifiedTime { get; set; }

        Guid? ModifierId { get; set; }

        string ModifierName { get; set; }
    }
}
