﻿using System.Reflection;
using Autofac;
using Faurecia.WM.Service;
using TDF.Core.Ioc;
using TDF.Core.Reflection;

namespace Faurecia.WM.Web.Tests
{
    /// <summary>
    /// 
    /// </summary>
    public class ServiceDependencies : IDependencyRegistrar
    {
        public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            var assembly = Assembly.GetAssembly(typeof(IProductionService));
            builder.AutoRegisterByNamespace(
                new[] { "Faurecia.WM.Service" }, assembly,
                r => r.InstancePerLifetimeScope());
        }

        public int Order => 2;
    }
}