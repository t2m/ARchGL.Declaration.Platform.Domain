﻿using System.Reflection;
using Autofac;
using TDF.Core.Ioc;
using TDF.Core.Reflection;
using TDF.Data.EntityFramework.Repository;

namespace Faurecia.WM.Web.Tests
{
    /// <summary>
    /// 
    /// </summary>
    public class RepositoryDependencies : IDependencyRegistrar
    {
        public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
           
            builder.RegisterType<RepositoryBase>().As<IRepositoryBase>();
        }

        public int Order => 1;
    }
}