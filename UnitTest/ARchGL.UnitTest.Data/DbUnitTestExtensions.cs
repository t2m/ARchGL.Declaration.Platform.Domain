﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NDbUnit.Core.SqlClient;

namespace ARchGL.UnitTest.Data
{
    public static class DbUnitTestExtensions
    {
        public static void InitData(this IDbUnitTest test)
        {
            var binPath = AppDomain.CurrentDomain.BaseDirectory;
            var fullTestDataPath = binPath.TrimEnd("bin\\Debug".ToCharArray())
            +"\\" + TestSettings.TestDataPath;
            test.Database = new SqlDbUnitTest(test.ConnectionString);
            test.Database.ReadXmlSchema(binPath + @"\" + TestSettings.XmlSchemaFile);
            if (test.DataType == TestDataType.Empty)
            {
                test.Database.ReadXml(TestSettings.TestDataPath + "Empty.xml");
            }
            else if (test.DataType == TestDataType.Custom)
            {
                var customPath = fullTestDataPath + test.CustomDataPath;
                if (Directory.Exists(customPath))
                {
                    test.Database.ReadXmlFromFolder(customPath);
                }
                else
                {
                    test.Database.ReadXml(customPath);
                }
            }
            else
            {
                var folder = fullTestDataPath + test.DataType;
                test.Database.ReadXmlFromFolder(folder);
            }
        }
    }
}
