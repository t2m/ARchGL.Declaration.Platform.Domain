﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.UnitTest.Data
{
    public class ExportData
    {
        public void ExportDataMethod()
        {
            var binPath = AppDomain.CurrentDomain.BaseDirectory;
            var db = new NDbUnit.Core.SqlClient.SqlDbUnitTest(TestSettings.ConnectionString);
            db.ReadXmlSchema(binPath + @"\" + TestSettings.XmlSchemaFile);

            System.Data.DataSet ds = db.GetDataSetFromDb();
            var date = DateTime.Now.ToString("yyyy-MM-dd");
            var time = DateTime.Now.ToString("HH-mm-ss");
            var osUserName = Environment.UserName;
            var folderPath = binPath.TrimEnd("bin\\Debug".ToCharArray()) + "\\" + TestSettings.TestDataPath + "BackUp\\Export\\" + date + "_" + time + "_" + osUserName + "/";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            if (ds.Tables.Count == 0)
            {
                return;
            }
            else
            {
                for (var i = 0; i < ds.Tables.Count; i++)
                {
                    ds.Tables[i].WriteXml(folderPath + ds.Tables[i].TableName + ".xml");
                }
            }
        }
    }
}
