﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.UnitTest.Data
{
    public class TestSettings
    {
        private static string _connectionString;

        public static string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(_connectionString))
                {
                    _connectionString = ConfigurationManager.ConnectionStrings["TestDbContext4"].ConnectionString;
                }
                return _connectionString;
            }
            set { _connectionString = value; }
        }

        private static string _xmlSchemaFile;

        /// <summary>
        /// 数据集文件e.g:TDFSchema.xsd
        /// </summary>
        public static string XmlSchemaFile { get; set; } = "TDFSchema.xsd";

        /// <summary>
        /// 数据种子文件夹e.g:"TestData/"
        /// </summary>
        public static string TestDataPath { get; set; } = "TestData\\";
    }
}
