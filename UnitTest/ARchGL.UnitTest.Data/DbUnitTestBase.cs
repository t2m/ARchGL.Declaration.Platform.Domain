﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NDbUnit.Core;
using NDbUnit.Core.SqlClient;
using NUnit.Framework;
using TDF.Core.Ioc;
using TDF.Data.EntityFramework.DbContext;

namespace ARchGL.UnitTest.Data
{
    public abstract class DbUnitTestBase : IDbUnitTest
    {
        [SetUp]
        public void Setup()
        {
            Database.PerformDbOperation(DbOperationFlag.CleanInsertIdentity);
        }

        [OneTimeSetUp]
        public virtual void TestSetupFixture()
        {
            InitData();
        }

        public virtual void InitData()
        {
            DbUnitTestExtensions.InitData(this);
        }

        [OneTimeTearDown]
        public void RestoreData()
        {
            Database.PerformDbOperation(DbOperationFlag.CleanInsertIdentity);
        }

        protected AbstractDbContext NewDB()
        {
            return Ioc.Resolve<AbstractDbContext>();
        }

        public virtual TestDataType DataType
        {
            get { return TestDataType.Initials; }
        }

        public abstract string ConnectionString { get; set; }

        public virtual string ConnectionName { get; set; }
        
        public SqlDbUnitTest Database { get; set; }

        public virtual string CustomDataPath => null;
    }
}
