﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Operator;

namespace ARchGL.UnitTests.Services
{
    public class TestCacheOperatorProvider : IOperatorProvider
    {
        public T GetCurrent<T>() where T : OperatorModel
        {
            throw new NotImplementedException();
        }

        public OperatorModel GetCurrent()
        {
            return new OperatorModel()
            {
                Id = Guid.Parse("f760df2b-b183-4d78-bd1c-fa62956a1037"),
                UserName = "admin",
                IsSystem = true
            };
        }

        public void AddCurrent(OperatorModel operatorModel)
        {
            
        }

        public void AddCurrent<T>(T operatorModel) where T : OperatorModel, new()
        {
            throw new NotImplementedException();
        }

        public void RemoveCurrent()
        {
            
        }
    }
}
