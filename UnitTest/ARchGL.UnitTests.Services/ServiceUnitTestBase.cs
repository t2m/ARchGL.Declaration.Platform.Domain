﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using ARchGL.Declaration.Platform.Domain.Entities;
using ARchGL.Declaration.Platform.Repository;
using ARchGL.Declaration.Platform.Service.Dtos;
using ARchGL.Declaration.Platform.Service.Mapper;
using NUnit.Framework;
using TDF.Core.Configuration;
using TDF.Core.Ioc;
using TDF.Core.Log;
using TDF.Core.Module;
using TDF.Core.Operator;
using TDF.Data.EntityFramework.DbContext;
using TDF.Data.EntityFramework.Initializers;
using TDF.Data.EntityFramework.Migrations;
using ARchGL.UnitTest.Data;

namespace ARchGL.UnitTests.Services
{
    public abstract class ServiceUnitTestBase : DbUnitTestBase
    {
        public override string ConnectionName { get; set; }

        public sealed override string ConnectionString { get; set; }

        protected ServiceUnitTestBase()
        {
            ConnectionName = ConnName.TestDbContext4.ToString();

            ConnectionString = ConfigurationManager.ConnectionStrings[ConnectionName].ConnectionString;
            TestSettings.ConnectionString = ConnectionString;
            TestSettings.XmlSchemaFile = "ARchGLSchema.xsd";
        }

        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            InitializerContext.Instance.IocInitialize(c =>
            {
                c.Builder.RegisterType<MySqlDbContext>().As<AbstractDbContext>();
                c.Builder.RegisterType<TestCacheOperatorProvider>().As<IOperatorProvider>();
            })
            .DisableLog4Net()
            //根据对象名称自动映射如： Entity1 <=> Entity1Dto，DTO对象以dto结尾，并实现接口IDto
            .InitializeAutoMapper(new AutoMapperInitializer(Assembly.GetAssembly(typeof(Admin)),
                Assembly.GetAssembly(typeof(AdminDto))))
            .InitializeDatabase<MySqlDbContext>(c =>
            {
                c.ConnectionName = ConnectionName;
                c.DatabaseInitializer = new EmptyDatabaseInitializer<MySqlDbContext>();
            })
            .MouduleInitialize()
            .ExecuteInit();
        }
    }

    public enum ConnName
    {
        //本地数据库
        TestDbContextLocal = 0,

        //172.16.4.6 远程数据库
        TestDbContext4 = 1,
    }
}
