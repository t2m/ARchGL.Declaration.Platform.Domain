﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using ARchGL.Declaration.Platform.Domain.Entities;
using ARchGL.Declaration.Platform.Repository;
using ARchGL.Declaration.Platform.Service.Dtos;
using ARchGL.Declaration.Platform.Service.Mapper;
using NUnit.Framework;
using TDF.Core.Configuration;
using TDF.Core.Ioc;
using TDF.Core.Log;
using TDF.Core.Module;
using TDF.Data.EntityFramework.DbContext;
using TDF.Data.EntityFramework.Initializers;
using ARchGL.UnitTest.Data;

namespace ARchGL.UnitTests.Services
{
    [TestFixture(Category = "数据库工具测试类")]
    public class DbTest
    {
        [Test]
        public void Can_InitDb()
        {
            using (var db = Ioc.Resolve<AbstractDbContext>())
            {
                //db.Database.CreateIfNotExists();
                var admin = db.Set<Admin>().FirstOrDefault();
            }
        }

        [Test]
        public void ExportXmlData()
        {
            new ExportData().ExportDataMethod();
        }

        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            var connectionName = ConnName.TestDbContextLocal.ToString();

            TestSettings.ConnectionString = ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            TestSettings.XmlSchemaFile = "ARchGLSchema.xsd";
            InitializerContext.Instance.IocInitialize(c =>
            {
                c.Builder.RegisterType<MySqlDbContext>().As<AbstractDbContext>();
            })
            .DisableLog4Net()
            //根据对象名称自动映射如： Entity1 <=> Entity1Dto，DTO对象以dto结尾，并实现接口IDto
            .InitializeAutoMapper(new AutoMapperInitializer(Assembly.GetAssembly(typeof(Admin)),
                Assembly.GetAssembly(typeof(AdminDto))))
            .InitializeDatabase<MySqlDbContext>(c =>
            {
                c.ConnectionName = connectionName;
                c.DatabaseInitializer = new MigrateDatabaseToLatestVersion<MySqlDbContext, Configuration<MySqlDbContext>>();
            })
            .MouduleInitialize()
            .ExecuteInit();
        }
    }

    public class Configuration<T> : DbMigrationsConfiguration<T> where T : System.Data.Entity.DbContext
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(T context)
        {
            new DbSeedContainer().Seed(context);
            //DbSeed.Seed(context);
        }
    }
}
