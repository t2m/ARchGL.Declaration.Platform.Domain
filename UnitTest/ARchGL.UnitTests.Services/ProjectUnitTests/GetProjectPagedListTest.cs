﻿using ARchGL.Declaration.Platform.Service;
using ARchGL.Declaration.Platform.Service.Dtos;
using ARchGL.UnitTests.Services;
using ARchGL.UnitTest;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Ioc;

namespace ARchGL.UnitTests.Services.ProjectUnitTests
{
    [TestFixture(Category = "获取项目-分页数据集合")]
    public class GetProjectPagedListTest : ServiceUnitTestBase
    {
        public override string ConnectionName => ConnName.TestDbContext4.ToString();

        private IProjectService ProjectService;

        public override TestDataType DataType => TestDataType.Custom;

        public override string CustomDataPath => "Initials.xiaoping.liu";

        [SetUp]
        public void SetUp()
        {
            ProjectService = Ioc.Resolve<IProjectService>();
        }

        [Test]
        public void 获取项目分页数据集合()
        {
            #region Mocks

            var criteria = new ProjectCriteria
            {
                AuditId = Guid.Parse("3ae37820-da81-11e8-a2c2-5254003c2d54")//系统管理员
            };

            #endregion

            #region Execute

            var pagedList = ProjectService.GetProjectPagedList(criteria);

            #endregion

            #region Assert

            Assert.GreaterOrEqual(pagedList.Rows.Count, 1);            

            #endregion
        }
    }
}
