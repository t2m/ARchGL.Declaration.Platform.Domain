﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ARchGL.Declaration.Platform.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARchGL.Declaration.Platform.Service.Dtos;
using ARchGL.UnitTests.ServicesTests;
using TDF.Core.Ioc;

namespace ARchGL.UnitTests.ServicesTests
{
    [TestClass()]
    public class ProjectServiceTests : ServiceUnitTestBase
    {
        private IProjectService ProjectService;

        public ProjectServiceTests()
        {
            ProjectService = Ioc.Resolve<IProjectService>();

            //ProjectService = new ProjectService();
        }

        [TestMethod()]
        public void GetProjectPagedListTest()
        {
            //Assert.AreEqual(1, 1);
            //return;
            #region Mocks

            var criteria = new ProjectCriteria
            {
                AuditId = Guid.Parse("3ae37820-da81-11e8-a2c2-5254003c2d54")//系统管理员
            };

            #endregion

            #region Execute

            var pagedList = ProjectService.GetProjectPagedList(criteria);

            #endregion

            #region Assert

            Assert.AreEqual(pagedList.Rows.Count, 1);

            #endregion
        }
    }
}