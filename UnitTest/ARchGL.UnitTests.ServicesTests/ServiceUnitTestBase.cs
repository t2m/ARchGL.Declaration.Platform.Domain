﻿using ARchGL.Declaration.Platform.Domain.Entities;
using ARchGL.Declaration.Platform.Service.Dtos;
using ARchGL.Declaration.Platform.Service.Mapper;
using Autofac;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Configuration;
using TDF.Core.Ioc;
using TDF.Core.Log;
using TDF.Core.Module;
using TDF.Core.Operator;
using TDF.Data.EntityFramework.DbContext;
using TDF.Data.EntityFramework.Initializers;

namespace ARchGL.UnitTests.ServicesTests
{
    public class ServiceUnitTestBase
    {
        public string ConnectionName { get; set; }
        public string ConnectionString { get; set; }

        public ServiceUnitTestBase()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["TestDbContext4"].ConnectionString;

            InitializerContext.Instance.IocInitialize(c =>
            {
                c.Builder.RegisterType<MySqlDbContext>().As<AbstractDbContext>();
                c.Builder.RegisterType<TestCacheOperatorProvider>().As<IOperatorProvider>();
            })
            .DisableLog4Net()
            //根据对象名称自动映射如： Entity1 <=> Entity1Dto，DTO对象以dto结尾，并实现接口IDto
            .InitializeAutoMapper(new AutoMapperInitializer(Assembly.GetAssembly(typeof(Admin)),
                Assembly.GetAssembly(typeof(AdminDto))))
            .InitializeDatabase<MySqlDbContext>(c =>
            {
                c.ConnectionName = ConnectionName;
                c.DatabaseInitializer = new EmptyDatabaseInitializer<DefaultDbContext>();
            })
            .MouduleInitialize()
            .ExecuteInit();
        }

        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            InitializerContext.Instance.IocInitialize(c =>
            {
                c.Builder.RegisterType<MySqlDbContext>().As<AbstractDbContext>();
                c.Builder.RegisterType<TestCacheOperatorProvider>().As<IOperatorProvider>();
            })
            .DisableLog4Net()
            //根据对象名称自动映射如： Entity1 <=> Entity1Dto，DTO对象以dto结尾，并实现接口IDto
            .InitializeAutoMapper(new AutoMapperInitializer(Assembly.GetAssembly(typeof(Admin)),
                Assembly.GetAssembly(typeof(AdminDto))))
            .InitializeDatabase<MySqlDbContext>(c =>
            {
                c.ConnectionName = ConnectionName;
                c.DatabaseInitializer = new EmptyDatabaseInitializer<DefaultDbContext>();
            })
            .MouduleInitialize()
            .ExecuteInit();
        }
    }
}
