﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDF.Core.Operator;

namespace ARchGL.UnitTests.ServicesTests
{
    public class TestCacheOperatorProvider : IOperatorProvider
    {
        public T GetCurrent<T>() where T : OperatorModel
        {
            throw new NotImplementedException();
        }

        public OperatorModel GetCurrent()
        {
            return new OperatorModel()
            {
                Id = Guid.Parse("3ae37820-da81-11e8-a2c2-5254003c2d54"),
                UserName = "admin",
                IsSystem = true
            };
        }

        public void AddCurrent(OperatorModel operatorModel)
        {

        }

        public void AddCurrent<T>(T operatorModel) where T : OperatorModel, new()
        {
            throw new NotImplementedException();
        }

        public void RemoveCurrent()
        {

        }
    }
}
