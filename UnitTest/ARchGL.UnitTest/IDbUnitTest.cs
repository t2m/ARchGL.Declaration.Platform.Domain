﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NDbUnit.Core.SqlClient;

namespace ARchGL.UnitTest
{
    public interface IDbUnitTest
    {
        TestDataType DataType { get; }
        string ConnectionString { get; }
        SqlDbUnitTest Database { get; set; }
        string CustomDataPath { get; }
    }
}
