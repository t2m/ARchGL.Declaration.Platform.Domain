﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NDbUnit.Core.SqlClient;

namespace ARchGL.UnitTest
{
    public static class SqlDbUnitTestExtensions
    {
        public static void ReadXmlFromFolder(this SqlDbUnitTest test, string folderPath)
        {
            var xmls = Directory.GetFiles(folderPath);
            if (xmls.Length == 0)
            {
                return;
            }
            test.ReadXml(xmls[0]);
            for (var i = 1; i < xmls.Length; i++)
            {
                test.AppendXml(xmls[i]);
            }
        }
    }
}
