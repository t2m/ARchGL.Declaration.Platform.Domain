﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARchGL.UnitTest
{
    public enum TestDataType
    {
        Empty = 1,
        Initials = 2,
        Deploys = 3,
        Custom = 4
    }
}
