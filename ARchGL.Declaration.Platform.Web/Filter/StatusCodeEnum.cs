﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Filter
{
    public enum StatusCodeEnum
    {
        [Text("请求(或处理)成功")]
        Success = 200, //请求(或处理)成功

        [Text("内部请求出错")]
        Error = 500, //内部请求出错

        [Text("未授权标识")]
        Unauthorized = 401,//未授权标识

        [Text("请求参数不完整或不正确")]
        ParameterError = 400,//请求参数不完整或不正确

        [Text("请求TOKEN失效")]
        TokenInvalid = 403,//请求TOKEN失效

        [Text("HTTP请求类型不合法")]
        HttpMehtodError = 405,//HTTP请求类型不合法

        [Text("HTTP请求不合法,请求参数可能被篡改")]
        HttpRequestError = 406,//HTTP请求不合法

        [Text("该URL已经失效")]
        URLExpireError = 407,//HTTP请求不合法
    }

    /// <summary>
    /// 
    /// </summary>
    public static class EnumExtension
    {
        private static Dictionary<string, Dictionary<string, string>> enumCache;

        private static Dictionary<string, Dictionary<string, string>> EnumCache
        {
            get
            {
                if (enumCache == null)
                {
                    enumCache = new Dictionary<string, Dictionary<string, string>>();
                }
                return enumCache;
            }
            set { enumCache = value; }
        }

        public static string GetEnumText(this Enum en)
        {
            string enString = string.Empty;
            if (null == en) return enString;
            var type = en.GetType();
            enString = en.ToString();
            if (!EnumCache.ContainsKey(type.FullName))
            {
                var fields = type.GetFields();
                Dictionary<string, string> temp = new Dictionary<string, string>();
                foreach (var item in fields)
                {
                    var attrs = item.GetCustomAttributes(typeof(TextAttribute), false);
                    if (attrs.Length == 1)
                    {
                        var v = ((TextAttribute)attrs[0]).Value;
                        temp.Add(item.Name, v);
                    }
                }
                EnumCache.Add(type.FullName, temp);
            }
            if (EnumCache[type.FullName].ContainsKey(enString))
            {
                return EnumCache[type.FullName][enString];
            }
            return enString;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class TextAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public TextAttribute(string value)
        {
            Value = value;
        }

        public string Value { get; set; }
    }
}