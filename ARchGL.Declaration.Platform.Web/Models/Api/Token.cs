﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Models.Api
{
    /// <summary>
    /// 令牌基类
    /// </summary>
    public class TokenBase
    {
        /// <summary>
        /// 微信 Openid
        /// </summary>
        public string Openid { get; set; }

        /// <summary>
        /// 用户对应签名Token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Token过期时间
        /// </summary>
        public virtual DateTime ExpireTime { get; set; }

        /// <summary>
        /// Token过期时间戳
        /// </summary>
        public long ExpireTimeStamp { get; set; }
    }

    /// <summary>
    /// 回话令牌
    /// </summary>
    public class Token : TokenBase
    {
        /// <summary>
        /// Token过期时间
        /// </summary>
        public sealed override DateTime ExpireTime { get; set; }
    }
}
