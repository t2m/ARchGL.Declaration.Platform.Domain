﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using log4net;
using TDF.Core.Ioc;
using TDF.Core.Log;
using TDF.Core.Models;
using TDF.Web.Authentication.Attributes.WebApi;
using TDF.Web.Authentication.Models;
using TDF.Web.Authentication.Services;

namespace ARchGL.Declaration.Platform.Web.Controllers
{
    /// <summary>
    /// Api父类
    /// </summary>
    public abstract class ApiControllerBase : ApiController
    {
        /// <summary>
        /// 日志
        /// </summary>
        public ILog Log => LogFactory.GetLogger(GetType());

        private IdentityInfo _currentIdentityInfo;

        /// <summary>
        /// 
        /// </summary>
        protected ApiControllerBase()
        {

        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="filterContext"></param>
        //protected override void OnActionExecuting(ActionExecutingContext filterContext)
        //{

        //    base.OnActionExecuting(filterContext);
        //}

        [NonAction]
        protected virtual ApiResult Fail(string message = "")
        {
            var result = new ApiResult();
            result.Message = message;
            result.Fail();
            return result;
        }

        [NonAction]
        protected virtual ApiResult Success(string message, object data)
        {
            var result = new ApiResult<object>();
            result.Value = data;
            result.Message = message;
            result.Succeed();
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="count">其它总数</param>
        /// <param name="data">Rows</param>
        /// <returns></returns>
        [NonAction]
        protected virtual ApiResult Success(int count, object data = null)
        {
            var result = new ApiResultCount<object>
            {
                Value = data,
                Count = count,
                Success = true,
            };
            return result;
        }

        [NonAction]
        protected virtual ApiResult Success(object data = null)
        {
            var result = new ApiResult<object>();
            result.Value = data;
            result.Message = string.Empty;
            result.Succeed();
            return result;
        }

        [NonAction]
        protected virtual ApiResult Error(string message)
        {
            var result = new ApiResult();
            result.Message = message;
            result.Succeed();
            result.Code = -1;
            return result;
        }
    }
}