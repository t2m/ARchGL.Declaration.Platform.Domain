﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ARchGL.Declaration.Platform.Service;
using TDF.Core.Ioc;
using TDF.Web.Attributes.Mvc;
using TDF.Web.Authentication.Attributes.Mvc;
using TDF.Web.Authentication.Models;

namespace ARchGL.Declaration.Platform.Web.Controllers
{
    /// <summary>
    /// 登录相关
    /// </summary>
    [AllowAnonymous]
    public class LoginController : AdminControllerBase
    {
        /// <summary>
        /// 系统服务
        /// </summary>
        private readonly ISystemService SystemService;

        /// <summary>
        /// 初始化服务
        /// </summary>
        public LoginController(ISystemService systemService)
        {
            SystemService = systemService;
        }
        
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ModelValidation]
        public ActionResult Login(LoginModel loginModel)
        {
            SystemService.Login(loginModel.UserName.Trim(), loginModel.Password);
            return Success();
        }

        /// <summary>
        /// 退出登录
        /// </summary>
        /// <returns></returns>
        [HandlerLogin]
        public ActionResult LoginOut()
        {
            SystemService.Logout(string.Empty);
            return Success();
        }
    }
}