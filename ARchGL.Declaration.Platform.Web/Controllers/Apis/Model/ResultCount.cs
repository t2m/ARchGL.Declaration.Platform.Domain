﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TDF.Core.Models;

namespace ARchGL.Declaration.Platform.Web.Controllers
{
    public class ApiResultCount<T>: ApiResult
    {
        public ApiResultCount() { }
        /// <summary>
        /// 返回记录
        /// </summary>
        public T Value { get; set; }
        /// <summary>
        /// 返回总数（可以是任何总数）
        /// </summary>
        public int Count { get; set; }
    }
}