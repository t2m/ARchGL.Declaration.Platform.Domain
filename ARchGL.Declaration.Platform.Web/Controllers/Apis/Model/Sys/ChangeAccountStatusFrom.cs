﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 修改帐号状态参数
    /// </summary>
    public class ChangeAccountStatusFrom : BaseFrom
    {
        /// <summary>
        /// 帐号Id
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}