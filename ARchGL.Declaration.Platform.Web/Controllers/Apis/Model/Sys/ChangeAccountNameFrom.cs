﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 修改帐号名称参数
    /// </summary>
    public class ChangeAccountNameFrom : BaseFrom
    {
        /// <summary>
        /// 帐号Id
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}