﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 重置密码
    /// </summary>
    public class ResetPasswordFrom : BaseFrom
    {
        /// <summary>
        /// 帐号Id
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// 帐号
        /// </summary>
        public string Number { get; set; }
    }
}