﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 帐号信息
    /// </summary>
    public class AccountFrom : BaseFrom
    {
        /// <summary>
        /// 名称（审核，企业，项目名称）
        /// </summary>
        public string Name { get; set; }

        ///// <summary>
        ///// 密码，初始密码和 Number 一致，所以不需要密码
        ///// </summary>
        //public string Password { get; set; }

        /// <summary>
        /// 帐号（审核、企业、项目帐号）
        /// </summary>
        public string Number { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 岗位
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// 类型：1区县、2企业、4项目、8质监员
        /// </summary>
        public AccountType Type { get; set; }

        /// <summary>
        /// 状态：1启用、2禁用
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 区域Id
        /// </summary>
        public Guid AreaId { get; set; }
    }
}