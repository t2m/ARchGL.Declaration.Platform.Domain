﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 修改密码参数
    /// </summary>
    public class ChangePasswordFrom : BaseFrom
    {
        /// <summary>
        /// 帐号Id
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
    }
}