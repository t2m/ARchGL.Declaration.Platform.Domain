﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 退出-条件
    /// </summary>
    public class LogoutFrom
    {
        /// <summary>
        /// 登录令牌
        /// </summary>
        public string LoginToken { get; set; }     
    }
}