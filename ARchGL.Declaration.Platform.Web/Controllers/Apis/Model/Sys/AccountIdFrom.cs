﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 查询帐号-参数
    /// </summary>
    public class AccountIdFrom
    {
        /// <summary>
        /// 帐号Id
        /// </summary>
        public Guid AccountId { get; set; }
    }
}
