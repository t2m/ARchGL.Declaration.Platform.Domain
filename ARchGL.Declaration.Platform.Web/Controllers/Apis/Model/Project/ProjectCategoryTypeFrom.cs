﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 创建项目-参数
    /// </summary>
    public class ProjectCategoryTypeFrom
    {
        /// <summary>
        /// 类型：
        /// 1.房屋建筑工程
        /// 2.城市道路工程
        /// 3.城市桥梁工程
        /// 4.城市隧道工程
        /// 5.城镇给排水构筑物及管道工程
        /// 6.市政工程边坡及档护结构工程
        /// 7.城镇污水处理厂工程
        /// </summary>
        public int Type { get; set; }
    }
}
