﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 单项申报
    /// </summary>
    public class SubitemReviewFrom : SubitemReviewHistoryFrom
    {
        /// <summary>
        /// 数据标识
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 审核状态：true已通过，false已打回
        /// </summary>
        public bool Status { get; set; }
    }

    /// <summary>
    /// 单项申报记录
    /// </summary>
    public class SubitemReviewHistoryFrom : OperatorBaseFrom
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 申请类型：1.BIM认定、2元素表报
        /// </summary>
        public ApplyForType Type { get; set; }

        /// <summary>
        /// 文件Id（来自文件系统）
        /// </summary>
        public Guid FileId { get; set; }

        /// <summary>
        /// 申请文件
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件相对路径，多个用“|”分隔
        /// </summary>
        public string Path { get; set; }
    }
}