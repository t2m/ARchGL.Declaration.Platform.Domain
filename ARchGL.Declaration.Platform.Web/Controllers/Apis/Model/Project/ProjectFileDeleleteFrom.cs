﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 删除项目文件-参数
    /// </summary>
    public class ProjectFileDeleleteFrom : BaseFrom
    {
        /// <summary>
        /// 项目文件Id
        /// </summary>
        public Guid ProjectFileId { get; set; }
    }
}
