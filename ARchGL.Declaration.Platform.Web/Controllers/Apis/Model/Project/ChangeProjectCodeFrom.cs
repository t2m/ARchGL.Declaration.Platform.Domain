﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 修改工程编码参数
    /// </summary>
    public class ChangeProjectCodeFrom : BaseFrom
    {
        /// <summary>
        /// 工程Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 工程编号
        /// </summary>
        public string ChildCode { get; set; }
    }
}