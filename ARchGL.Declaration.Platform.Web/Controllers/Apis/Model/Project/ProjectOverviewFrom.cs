﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 项目概况-条件
    /// </summary>
    public class ProjectOverviewFrom : BaseFrom
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public string ProjectId { get; set; }

        /// <summary>
        /// 帐号Id
        /// </summary>
        public string AccountId { get; set; }      
    }
}
