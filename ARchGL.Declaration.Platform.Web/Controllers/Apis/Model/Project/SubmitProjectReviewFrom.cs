﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 撤回项目审核
    /// </summary>
    public class RecallProjectReviewProjectFrom : OperatorBaseFrom
    {
        /// <summary>
        /// 审核记录标识
        /// </summary>
        public Guid Id { get; set; }
    }

    /// <summary>
    /// 提交工程审核
    /// </summary>
    public class SubmitProjectReviewFrom : OperatorBaseFrom
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 一级Id
        /// </summary>
        public Guid FirstCategoryId { get; set; }
    }
}