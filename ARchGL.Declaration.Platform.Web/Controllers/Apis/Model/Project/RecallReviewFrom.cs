﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 撤回-参数
    /// </summary>
    public class RecallReviewFrom : OperatorBaseFrom
    {
        /// <summary>
        /// 记录标识
        /// </summary>
        public Guid Id { get; set; }
    }
}
