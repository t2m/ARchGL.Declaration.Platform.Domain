﻿using System;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 创建项目-参数
    /// </summary>
    public class ProjectCreateFrom : OperatorBaseFrom
    {
        /// <summary>
        /// 父级项目Id
        /// </summary>
        public Guid? ParentId { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 项目编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 施工单位
        /// </summary>
        public string BuilderUnitIds { get; set; }

        /// <summary>
        /// 是否 BIM（Building information model）  
        /// </summary>
        public bool IsBIM { get; set; }

        /// <summary>
        /// 审核人Id（质监员）
        /// </summary>
        public Guid AuditId { get; set; }

        /// <summary>
        /// 类型：
        /// 1.房屋建筑工程
        /// 2.城市道路工程
        /// 3.城市桥梁工程
        /// 4.城市隧道工程
        /// 5.城镇给排水构筑物及管道工程
        /// 6.市政工程边坡及档护结构工程
        /// 7.城镇污水处理厂工程
        /// </summary>
        public int Type { get; set; }
    }
}
