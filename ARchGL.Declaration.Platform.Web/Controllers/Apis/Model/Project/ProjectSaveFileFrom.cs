﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 保存项目文件-参数
    /// </summary>
    public class ProjectSaveFileFrom : BaseFrom
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 三级分类Id
        /// </summary>
        public Guid ThreeCategoryId { get; set; }

        /// <summary>
        /// 项目文件Id（来自文件系统）
        /// </summary>
        public Guid FileId { get; set; }

        /// <summary>
        /// 文件名全称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 网络路径
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 文件大小，保存时统一转换为B，显示时根据需求转换对应单位
        /// </summary>
        public int Size { get; set; }
    }
}
