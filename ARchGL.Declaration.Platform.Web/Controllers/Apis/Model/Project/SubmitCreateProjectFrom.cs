﻿using System;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 创建、修改、删除项目-参数
    /// </summary>
    public class SubmitCreateProjectFrom : OperatorBaseFrom
    {
        /// <summary>
        /// 项目标识
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 项目编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 施工单位
        /// </summary>
        public string BuilderUnitIds { get; set; }

        /// <summary>
        /// 是否 BIM（Building information model）  
        /// </summary>
        public bool IsBIM { get; set; }

        /// <summary>
        /// 质监员标识
        /// </summary>
        public Guid ManageId { get; set; }
    }
}
