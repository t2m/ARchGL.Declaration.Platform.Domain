﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 查询项目-参数
    /// </summary>
    public class SubmitProjectChangeReviewFrom : OperatorBaseFrom
    {
        /// <summary>
        /// 工程Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 审核人Id
        /// </summary>
        public Guid AuditId { get; set; }

        /// <summary>
        /// 变更类型：1分解，2合并
        /// </summary>
        public ChangeType Type { get; set; }
        
        /// <summary>
        /// 变更前数量
        /// </summary>
        public int Origin { get; set; }

        /// <summary>
        /// 变更后数量
        /// </summary>
        public int Target { get; set; }

        /// <summary>
        /// 原工程名称集合，多个使用“,”分隔
        /// </summary>
        public List<string> OriginProjectNames { get; set; }

        /// <summary>
        /// 原工程编码集合，多个使用“,”分隔
        /// </summary>
        public string OriginProjectCodes { get; set; }

        /// <summary>
        /// 目标工程名称集合，多个使用“,”分隔
        /// </summary>
        public List<string> TargetProjectNames { get; set; }

        /// <summary>
        /// 目标工程编码集合，多个使用“,”分隔
        /// </summary>
        public string TargetProjectCodes { get; set; }
    }
}
