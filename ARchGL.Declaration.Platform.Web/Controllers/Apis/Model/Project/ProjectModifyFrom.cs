﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 编辑项目-参数
    /// </summary>
    public class ProjectModifyFrom : BaseFrom
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 建设单位
        /// </summary>
        public string ConstructionUnit { get; set; }

        /// <summary>
        /// 建设单位Id
        /// </summary>
        public System.Guid ConstructionUnitId { get; set; }

        /// <summary>
        /// 施工单位
        /// </summary>
        public string BuilderUnit { get; set; }

        /// <summary>
        /// 施工单位 Id（企业）
        /// </summary>
        public System.Guid BuilderUnitId { get; set; }

        /// <summary>
        /// 监理单位
        /// </summary>
        public string SupervisionUnit { get; set; }

        /// <summary>
        /// 监理单位Id
        /// </summary>
        public System.Guid SupervisionUnitId { get; set; }

        /// <summary>
        /// 检测单位
        /// </summary>
        public string TestingUnit { get; set; }

        /// <summary>
        /// 检测单位Id
        /// </summary>
        public System.Guid TestingUnitId { get; set; }

        /// <summary>
        /// 预混供应商
        /// </summary>
        public string PremixedSupplier { get; set; }

        /// <summary>
        /// 预混供应商Id
        /// </summary>
        public System.Guid PremixedSupplierId { get; set; }

        /// <summary>
        /// 设计单位Id
        /// </summary>
        public string DesignUnit { get; set; }

        /// <summary>
        /// 设计单位Id
        /// </summary>
        public System.Guid DesignUnitId { get; set; }

        /// <summary>
        /// 勘测单位
        /// </summary>
        public string SurveyUnit { get; set; }

        /// <summary>
        /// 勘测单位Id
        /// </summary>
        public System.Guid SurveyUnitId { get; set; }

        /// <summary>
        /// 项目地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 定位（存储 X坐标，Y坐标）
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// 概览图
        /// </summary>
        public string Image { get; set; }
    }
}
