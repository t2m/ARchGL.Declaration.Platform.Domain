﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 创建项目-参数
    /// </summary>
    public class ProjectSaveUploadImageFrom : BaseFrom
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        public string Path { get; set; }
    }
}
