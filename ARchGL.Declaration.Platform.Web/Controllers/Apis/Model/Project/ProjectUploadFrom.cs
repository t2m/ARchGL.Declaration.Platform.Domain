﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 上传文件-条件
    /// </summary>
    public class ProjectUploadFrom
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid ProjectId { get; set; }

    }
}
