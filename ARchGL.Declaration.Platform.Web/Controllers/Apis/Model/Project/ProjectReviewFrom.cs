﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    public class ProjectReviewProjectFrom : ProjectReviewFrom
    {
        /// <summary>
        /// 数据标识
        /// </summary>
        public Guid Id { get; set; }
    }

    /// <summary>
    /// 审核项目-参数
    /// </summary>
    public class ProjectReviewFrom : OperatorBaseFrom
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 一级分类Id
        /// </summary>
        public Guid CagegoryId { get; set; }

        /// <summary>
        /// 审核人Id
        /// </summary>
        public Guid AuditId { get; set; }

        /// <summary>
        /// 审核状态：true 通过、 false 打回
        /// </summary>
        public bool Status { get; set; }
    }
}
