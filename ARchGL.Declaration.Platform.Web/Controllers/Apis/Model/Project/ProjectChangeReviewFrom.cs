﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 提交项目变更审核
    /// </summary>
    public class ProjectChangeReviewFrom : OperatorBaseFrom
    {
        /// <summary>
        /// 审核记录Id
        /// </summary>
        public Guid Id { get; set; }

        ///// <summary>
        ///// 项目Id
        ///// </summary>
        //public Guid ProjectId { get; set; }

        /// <summary>
        /// 审核人Id
        /// </summary>
        public Guid AuditId { get; set; }

        /// <summary>
        /// 审核状态：true 通过、 false 打回
        /// </summary>
        public bool Status { get; set; }
    }
}