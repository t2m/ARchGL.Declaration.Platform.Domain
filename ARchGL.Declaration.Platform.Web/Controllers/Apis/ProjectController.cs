﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using ARchGL.Declaration.Platform.Domain.Entities;
using ARchGL.Declaration.Platform.Service;
using ARchGL.Declaration.Platform.Service.Dtos;
using Newtonsoft.Json;
using TDF.Core.Configuration;
using TDF.Core.Exceptions;
using TDF.Core.Models;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 项目相关接口
    /// </summary>
    public partial class ProjectController : ApiControllerBase
    {
        #region 注入服务

        /// <summary>
        /// 管理员接口
        /// </summary>
        public readonly IAdminService AdminService;
        /// <summary>
        /// 系统服务
        /// </summary>
        private readonly ISystemService SystemService;
        /// <summary>
        /// 项目服务
        /// </summary>
        private readonly IProjectService ProjectService;

        /// <summary>
        /// 初始化服务
        /// </summary>
        public ProjectController(IAdminService adminService, ISystemService systemService, IProjectService projectService)
        {
            AdminService = adminService;
            SystemService = systemService;
            ProjectService = projectService;
        }

        #endregion

        #region 项目

        /// <summary>
        /// 编辑工程
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult ModifyProject([FromBody]ProjectModifyFrom from)
        {
            var dto = from.Merge<ProjectModifyFrom, ProjectDto>();
            ProjectService.InitLoginToken(from.LoginToken);
            #region Backup code，临时改为用户自己输入单位名称
            //var accountIdList = new List<Guid>
            //{
            //    dto.ConstructionUnitId,
            //    dto.BuilderUnitId,
            //    dto.SupervisionUnitId,
            //    dto.TestingUnitId,
            //    dto.PremixedSupplierId
            //};
            //var accountList = SystemService.GetAccountList(new AccountCriteria { IdList = accountIdList });

            ////建设单位
            //var constructionUnit = accountList.FirstOrDefault(x => x.Id == dto.ConstructionUnitId);
            //if (constructionUnit != null) dto.ConstructionUnit = constructionUnit.Name;

            ////施工单位
            //var builderUnit = accountList.FirstOrDefault(x => x.Id == dto.BuilderUnitId);
            //if (builderUnit != null) dto.BuilderUnit = builderUnit.Name;

            ////监理单位
            //var supervisionUnit = accountList.FirstOrDefault(x => x.Id == dto.SupervisionUnitId);
            //if (supervisionUnit != null) dto.SupervisionUnit = supervisionUnit.Name;

            ////检测单位
            //var testingUnit = accountList.FirstOrDefault(x => x.Id == dto.TestingUnitId);
            //if (testingUnit != null) dto.TestingUnit = testingUnit.Name;

            ////预混供应商
            //var premixedSupplier = accountList.FirstOrDefault(x => x.Id == dto.PremixedSupplierId);
            //if (premixedSupplier != null) dto.PremixedSupplier = premixedSupplier.Name;
            #endregion
            ProjectService.ModifyProject(dto);
            return Success();
        }

        /// <summary>
        /// 提交创建项目申请
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult SubmitCreateProjectReview([FromBody]SubmitCreateProjectFrom from)
        {
            var dto = from.Merge<SubmitCreateProjectFrom, ProjectReviewHistoryDto>();
            ProjectService.InitLoginToken(from.LoginToken);
            ProjectService.SubmitCreateProjectReview(dto, from.ManageId);
            return Success();
        }

        /// <summary>
        /// 提交修改项目申请
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult SubmitModifyProjectReview([FromBody]SubmitCreateProjectFrom from)
        {
            var dto = from.Merge<SubmitCreateProjectFrom, ProjectReviewHistoryDto>();
            ProjectService.InitLoginToken(from.LoginToken);
            ProjectService.SubmitModifyProjectReview(dto);
            return Success();
        }

        /// <summary>
        /// 提交删除项目申请
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult SubmitRemoveProjectReview([FromBody]SubmitCreateProjectFrom from)
        {
            var dto = from.Merge<SubmitCreateProjectFrom, ProjectReviewHistoryDto>();
            ProjectService.InitLoginToken(from.LoginToken);
            ProjectService.SubmitRemoveProjectReview(dto);
            return Success();
        }

        /// <summary>
        /// 审核 创建、修改、删除 项目
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult ProjectReviewProject([FromBody]ProjectReviewProjectFrom from)
        {
            var dto = from.Merge<ProjectReviewProjectFrom, ProjectReviewHistoryDto>();
            ProjectService.InitLoginToken(from.LoginToken);
            ProjectService.ProjectReview(dto, from.Status);
            return Success();
        }

        /// <summary>
        /// 撤回 创建、修改、删除 项目审核
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult RecallProjectReviewProject([FromBody]RecallProjectReviewProjectFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            var dto = from.Merge<RecallProjectReviewProjectFrom, ProjectReviewHistoryDto>();
            ProjectService.RecallProjectReview(dto);
            return Success();
        }

        /// <summary>
        /// 修改项目/工程名称
        /// </summary>
        /// <param name="from">项目信息</param>
        /// <returns></returns>
        public ApiResult ChangeProjectName([FromBody]ChangeProjectNameFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            ProjectService.ChangeProjectName(from.ProjectId, from.Name);
            return Success(true);
        }

        /// <summary>
        /// 修改工程编号
        /// </summary>
        /// <param name="from">工程信息</param>
        /// <returns></returns>
        public ApiResult ChangeProjectChildCode([FromBody]ChangeProjectCodeFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            if (!ProjectService.CheckProjectFirstCategoryStatus(from.ProjectId))
                throw new KnownException("当前状态不能修改，请稍后重试");
            ProjectService.ChangeProjectChildCode(from.ProjectId, from.ChildCode);
            return Success(true);
        }

        /// <summary>
        /// 上传项目设计图片，除文件外还有：ProjectId
        /// </summary>
        /// <returns></returns>
        [Obsolete("对接文件服务后，该方法不再使用")]
        public ApiResult ProjectUploadImage()
        {
            var request = HttpContext.Current.Request;
            Guid.TryParse(request.Params["ProjectId"], out Guid projectId);
            var path = ProjectService.ProjectUploadImage(projectId);
            return Success(path);
        }

        /// <summary>
        /// 保存项目设计图
        /// </summary>
        /// <param name="from">项目信息</param>
        /// <returns></returns>
        public ApiResult ProjectSaveUploadImage([FromBody]ProjectSaveUploadImageFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            ProjectService.ProjectSaveUploadImage(from.ProjectId, from.Path);
            return Success();
        }

        /// <summary>
        /// 获取项目-分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns>JSON</returns>
        [HttpPost]
        public ApiResult QueryProjectPagedList([FromBody]ProjectCriteria criteria)
        {
            var account = new AccountDto();
            if (criteria.AuditId != Guid.Empty)//
                account = SystemService.GetAccountById(criteria.AuditId);

            if (account.Type == Domain.Entities.AccountType.管理员)
                criteria.AuditId = Guid.Empty;
            else if (account.Type == Domain.Entities.AccountType.区县帐号)
            {
                criteria.AreaId = criteria.AuditId;
                criteria.AuditId = Guid.Empty;
            }
            else if (account.Type == Domain.Entities.AccountType.企业帐号)
            {
                criteria.BuilderUnitId = criteria.AuditId;
                criteria.AuditId = Guid.Empty;
            }
            
            //criteria.ParentId = null;//只查询项目
            var list = ProjectService.GetProjectPagedList(criteria);
            var pagedList = new PagedList<ProjectReviewDto>();
            #region 图片前缀赋值
            //var virtualDirectory = new Configs().GetValue("VirtualDirectory");
            //foreach (var item in list.Rows)
            //{
            //    if (!string.IsNullOrWhiteSpace(item.Image))
            //        item.Image = item.Image = virtualDirectory + item.Image;
            //}
            #endregion            
            //获取项目下工程信息
            var projectIdList = list.Rows.Select(x => x.Id).ToList();
            var projectChildList = ProjectService.GetProjectPagedList(new ProjectCriteria { ParentIdList = projectIdList, PageIndex = 1, PageSize = 1000 });

            //获取项目 BIM、元素报表申报状态
            var subitemList = ProjectService.GetSubitemReviewPagedList(new SubitemReviewHistoryCriteria { ProjectIdList = projectIdList, PageIndex = 1, PageSize = 1000 });

            foreach (var item in list.Rows)
            {
                item.ProjectCount = projectChildList.Rows.Count(x => x.ParentId == item.Id);

                var entity = item.Merge<ProjectDto, ProjectReviewDto>();

                entity.BIMStatus = subitemList.Rows.GetStatus(ApplyForType.BIM认定, item.Id);
                entity.ElementStatus = subitemList.Rows.GetStatus(ApplyForType.元素表报, item.Id);

                pagedList.Rows.Add(entity);
            }
            pagedList.PageIndex = list.PageIndex;
            pagedList.PageSize = list.PageSize;
            pagedList.TotalCount = list.TotalCount;
            pagedList.TotalPages = list.TotalPages;
            return Success(pagedList);
        }

        /// <summary>
        /// 获取项目工程信息-分页数据集合
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ApiResult QueryProjectChildPagedList([FromBody]ProjectCriteria criteria)
        {
            var list = ProjectService.GetProjectPagedList(criteria);
            //if (list.Rows.Count == 0)
            //{
            //    //criteria.ProjectId = criteria.ParentId.Value;
            //    criteria.ParentId = null;
            //    list = ProjectService.GetProjectPagedList(criteria);
            //}
            return Success(list);
        }

        /// <summary>
        /// 获取可以操作 分解/合并 的工程信息
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ApiResult QueryProjectChildList([FromBody]ProjectCriteria criteria)
        {
            var result = ProjectService.GetProjectPagedList(criteria);

            var idList = result.Rows.Select(x => x.Id).ToList();
            var fList = ProjectService.GetProjectFirstCategoryOverview(new ProjectOverviewCriteria
            {
                PageIndex = 1,
                PageSize = 100,
                ProjectIdList = idList,
                IsChildProject = true,
                ChangeType = ChangeType.正常
            }).Rows;

            result.Rows = result.Rows.OrderBy(x => x.ChildCode).ToList();
            var projectList = result.Rows.ToList();

            foreach (var item in projectList)
            {
                var project = result.Rows.FirstOrDefault(x => x.Id == item.Id);
                var f = fList.Where(x => x.Id == item.Id).FirstOrDefault();
                if (f.IsNull()) continue;
                if (f.FirstCategoryStatus1 == AuditStatus.待审核 ||
                    f.FirstCategoryStatus2 == AuditStatus.待审核 ||
                    f.FirstCategoryStatus3 == AuditStatus.待审核 ||
                    f.FirstCategoryStatus4 == AuditStatus.待审核 ||

                    f.FirstCategoryStatus1 == AuditStatus.已完成 ||
                    f.FirstCategoryStatus2 == AuditStatus.已完成 ||
                    f.FirstCategoryStatus3 == AuditStatus.已完成 ||
                    f.FirstCategoryStatus4 == AuditStatus.已完成)
                    result.Rows.Remove(project);
            }

            return Success(result);
        }

        /// <summary>
        /// 获取项目下最大工程编码
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns></returns>
        public ApiResult QueryProjectMaxCode([FromBody]ProjectIdFrom criteria)
        {
            var entity = ProjectService.GetProjectMaxChildCode(criteria.ProjectId);
            if (entity != null)
            {
                int.TryParse(entity.ChildCode, out int code);
                return Success(code);
            }
            return Success(0);
        }

        /// <summary>
        /// 根据项目Id获取项目信息
        /// </summary>
        /// <param name="criteria">项目</param>
        /// <returns></returns>
        public ApiResult QueryProjectInfoById([FromBody]ProjectIdFrom criteria)
        {
            var project = ProjectService.GetProjectById(criteria.ProjectId);
            var entity = project.Merge<ProjectDto, ProjectInfoDto>();

            var fList = ProjectService.GetProjectFirstCategoryOverview(new ProjectOverviewCriteria
            {
                PageIndex = 1,
                PageSize = 10,
                ProjectIdList = new List<Guid> { criteria.ProjectId },
                IsChildProject = true,
                ChangeType = ChangeType.正常
            }).Rows;

            var f = fList.Where(x => x.Id == criteria.ProjectId).FirstOrDefault();
            if (!f.IsNull())
            {
                entity.FirstCategoryStatus1 = f.FirstCategoryStatus1;
                entity.FirstCategoryStatus2 = f.FirstCategoryStatus2;
                entity.FirstCategoryStatus3 = f.FirstCategoryStatus3;
                entity.FirstCategoryStatus4 = f.FirstCategoryStatus4;
            }

            return Success(entity);
        }

        /// <summary>
        /// 获取分类工程下 一二级分类 概况（分类为主）
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns>JSON</returns>
        public ApiResult QueryProjectCategoryFirstCategoryOverview([FromBody]ProjectOverviewCriteria criteria)
        {
            var list = ProjectService.GetProjectCategoryFirstCategoryOverview(criteria);
            return Success(list);
        }

        /// <summary>
        /// 获取工程下 一级分类概况（工程为主）
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns>JSON</returns>
        public ApiResult QueryProjectFirstCategoryOverview([FromBody]ProjectOverviewCriteria criteria)
        {
            var account = new AccountDto();
            if (criteria.AuditId != Guid.Empty)//
                account = SystemService.GetAccountById(criteria.AuditId);

            criteria.ChangeType = Domain.Entities.ChangeType.正常;
            if (account != null && account.Type == Domain.Entities.AccountType.企业帐号)
            {//施工单位（企业）帐号
                criteria.BuilderUnitId = criteria.AuditId;
                criteria.AuditId = Guid.Empty;
                if (criteria.ParentId == Guid.Empty) criteria.ParentId = null;
                criteria.IsChildProject = true;
            }
            else if (account != null && account.Type == Domain.Entities.AccountType.管理员)
            {
                criteria.AuditId = Guid.Empty;
                if (criteria.ParentId == Guid.Empty) criteria.ParentId = null;
                criteria.IsChildProject = true;
            }
            else if (account != null && account.Type == Domain.Entities.AccountType.区县帐号)
            {
                criteria.IsChildProject = true;

                if (criteria.ParentId == Guid.Empty) criteria.ParentId = null;
            }
            else if (criteria.ParentId.HasValue) criteria.AuditId = Guid.Empty;//有ParentId的情况下，清空 AuditId

            var list = ProjectService.GetProjectFirstCategoryOverview(criteria);
            return Success(list);
        }

        #endregion

        #region 项目文件

        /// <summary>
        /// 上传项目文件，除文件外还有：ProjectId，ThreeCategoryId
        /// </summary>
        /// <returns></returns>
        [Obsolete("对接文件服务后，该方法不再使用")]
        public ApiResult ProjectUploadFile()
        {
            var request = HttpContext.Current.Request;
            Guid.TryParse(request.Params["ProjectId"], out Guid projectId);
            Guid.TryParse(request.Params["ThreeCategoryId"], out Guid threeCategoryId);

            var reslut = ProjectService.ProjectUploadFile(projectId, threeCategoryId);
            var dto = new ProjectFileDto();
            if (reslut != null)
            {
                dto.Path = reslut.Path;
                dto.Size = reslut.Size;
                dto.Extension = reslut.Extension;
                dto.ProjectId = projectId;
                dto.ThreeCategoryId = threeCategoryId;//三级分类Id
                ProjectService.CreateProjectFile(dto);
            }
            var virtualDirectory = new Configs().GetValue("VirtualDirectory");
            return Success(virtualDirectory + dto.Path);
        }

        /// <summary>
        /// 保存工程文件信息
        /// </summary>
        /// <param name="from">文件信息</param>
        /// <returns></returns>
        public ApiResult ProjectSaveFileInfo([FromBody]ProjectSaveFileFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            if (string.IsNullOrWhiteSpace(from.Path))
                throw new KnownException("文件路径不能为空");

            var extension = string.Empty;
            if (from.Path.IndexOf('.') != -1)
                extension = from.Path.Substring(from.Path.LastIndexOf('.'));

            var dto = from.Merge<ProjectSaveFileFrom, ProjectFileDto>();
            dto.Extension = extension;
            if (dto.Name.LastIndexOf('.') != -1)
                dto.Name = dto.Name.Substring(0, from.Name.LastIndexOf('.'));

            try
            {
                ProjectService.CreateProjectFile(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Success();
        }

        /// <summary>
        /// 删除项目文件
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult ProjectDeleteFile([FromBody]ProjectFileDeleleteFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            var reslut = ProjectService.DeleteFile(from.ProjectFileId);
            return Success(reslut);
        }

        /// <summary>
        /// 获取项目文件-分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns>JSON</returns>
        [HttpPost]
        public ApiResult QueryProjectFilePagedList([FromBody]ProjectFileCriteria criteria)
        {
            var list = ProjectService.GetProjectFilePagedList(criteria);
            return Success(list);
        }

        #endregion

        #region 项目分类

        /// <summary>
        /// 获取项目分类-分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns>JSON</returns>
        public ApiResult QueryProjectCategoryPagedList([FromBody]ProjectCategoryCriteria criteria)
        {
            var list = ProjectService.GetProjectCategoryPagedList(criteria);
            return Success(list);
        }

        /// <summary>
        /// 根据分类获取二级分类集合
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult QueryProjectCategoryByType([FromBody]ProjectCategoryTypeFrom from)
        {
            var list = ProjectService.GetProjectCategoryByType(from.Type);
            return Success(list);
        }

        #endregion

        #region 审核记录

        /// <summary>
        /// 提交审核（工程）
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult SubmitProjectReview([FromBody]SubmitProjectReviewFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            var dto = from.Merge<SubmitProjectReviewFrom, ProjectReviewHistoryDto>();

            //if(!ProjectService.GetProjectFilePagedList(new ProjectFileCriteria { ProjectId = dto.ProjectId, FirstCategoryId = dto.FirstCategoryId, PageIndex = 1, PageSize = 1 }).Rows.Any())
            //    throw new KnownException("请先上传文件");

            ProjectService.SubmitProjectReview(dto, from.FirstCategoryId);
            return Success();
        }

        /// <summary>
        /// 撤回审核（工程）
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult RecallProjectReview([FromBody]SubmitProjectReviewFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            var dto = from.Merge<SubmitProjectReviewFrom, ProjectReviewHistoryDto>();
            ProjectService.RecallProjectReview(dto, from.FirstCategoryId);
            return Success();
        }

        /// <summary>
        /// 审核项目（工程）
        /// </summary>
        /// <param name="from">审核结果</param>
        /// <returns></returns>
        public ApiResult ProjectReview([FromBody]ProjectReviewFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            var dto = from.Merge<ProjectReviewFrom, ProjectDto>();
            dto.Id = from.ProjectId;
            ProjectService.ProjectReview(dto, from.CagegoryId, from.Status);
            return Success();
        }

        /// <summary>
        /// 重审工程
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult ResetProjectStatus([FromBody]SubmitProjectReviewFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            var dto = from.Merge<SubmitProjectReviewFrom, ProjectReviewHistoryDto>();
            ProjectService.ResetProjectStatus(dto, from.FirstCategoryId);
            return Success();
        }

        /// <summary>
        /// 获取工程审核记录分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns></returns>
        public ApiResult QueryProjectReviewHistoryPagedList([FromBody]ProjectReviewHistoryCriteria criteria)
        {
            var account = SystemService.GetAccountById(criteria.AuditId);
            if (account == null || account.Type == Domain.Entities.AccountType.管理员)
                criteria.AuditId = Guid.Empty;

            if (account != null && account.Type == Domain.Entities.AccountType.企业帐号)
            {//施工单位（企业）帐号
                criteria.BuilderUnitId = criteria.AuditId;
                criteria.AuditId = Guid.Empty;
            }
            else if (account != null && account.Type == Domain.Entities.AccountType.区县帐号)
            {
                criteria.CreatorId = criteria.AuditId;
                criteria.AuditId = Guid.Empty;
            }

            if (criteria.ProjectId == Guid.Empty)
                criteria.TypeList = new List<ReviewType> { ReviewType.创建项目, ReviewType.修改项目, ReviewType.删除项目 };

            var list = ProjectService.GetProjectReviewHistoryPagedList(criteria);
            return Success(list);
        }

        #endregion

        #region 变更记录

        /// <summary>
        /// 提交变更申请（工程）
        /// </summary>
        /// <param name="from">变更信息</param>
        /// <returns></returns>
        public ApiResult SubmitProjectChangeReview([FromBody]SubmitProjectChangeReviewFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            var project = ProjectService.GetProjectById(from.ProjectId);
            if (project == null) throw new KnownException("项目不存在");

            var dto = from.Merge<SubmitProjectChangeReviewFrom, ProjectChangeHistoryDto>();
            dto.OriginProjectNames = JsonConvert.SerializeObject(from.OriginProjectNames);
            if (dto.OriginProjectNames == "null") dto.OriginProjectNames = string.Empty;
            dto.TargetProjectNames = JsonConvert.SerializeObject(from.TargetProjectNames);
            if (dto.TargetProjectNames == "null") dto.TargetProjectNames = string.Empty;
            ProjectService.SubmitProjectChangeReview(dto);
            return Success();
        }

        /// <summary>
        /// 撤回变更审核（工程）
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult RecallProjectChangeReview([FromBody]RecallReviewFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            ProjectService.RecallProjectChangeReview(from.Id);
            return Success();
        }

        /// <summary>
        /// 审核变更记录（工程）
        /// </summary>
        /// <param name="from">变更信息</param>
        /// <returns></returns>
        public ApiResult ProjectChangeReview([FromBody]ProjectChangeReviewFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            var dto = from.Merge<ProjectChangeReviewFrom, ProjectChangeHistoryDto>();
            try
            {
                ProjectService.ProjectChangeReview(dto, from.Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Success();
        }

        /// <summary>
        /// 获取项目变更记录-分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns></returns>
        public ApiResult QueryProjectChangeHistory([FromBody]ProjectChangeHistoryCriteria criteria)
        {
            if (criteria.AuditId != Guid.Empty)
            {
                var account = SystemService.GetAccountById(criteria.AuditId);
                if (account == null || account.Type == Domain.Entities.AccountType.管理员)
                    criteria.AuditId = Guid.Empty;
                else if (account != null && account.Type == Domain.Entities.AccountType.企业帐号)
                {//施工单位（企业）帐号
                    criteria.BuilderUnitId = criteria.AuditId;
                    criteria.AuditId = Guid.Empty;
                }
                else if (criteria.ProjectId != Guid.Empty)
                {
                    criteria.AuditId = Guid.Empty;
                }
            }

            var result = ProjectService.GetProjectChangeHistorySubmitTimePagedList(criteria);

            return Success(result.Item1, result.Item2);
        }

        #endregion

        #region 单项申请记录

        /// <summary>
        /// 这是分割线
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        [Obsolete()]
        public ApiResult 单项申报API分割线______________________________________________________________________([FromBody]SubitemReviewFrom from)
        {
            return Success();
        }

        /// <summary>
        /// 创建或更新单项申报（重传申请表也调用该方法）
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult CreateOrUpdateSubitemReview([FromBody]SubitemReviewFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            if (from.Type == 0) throw new KnownException("申请类型[Type]为必填参数");
            var dto = from.Merge<SubitemReviewFrom, SubitemReviewDto>();
            var id = ProjectService.CreateOrUpdateSubitemReview(dto);
            return Success(id);
        }

        /// <summary>
        /// 撤回单项申报审核记录
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult RecallSubitemReviewHistory([FromBody]SubitemReviewFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            var dto = from.Merge<SubitemReviewFrom, SubitemReviewHistoryDto>();
            ProjectService.RecallSubitemReviewHistory(dto);
            return Success();
        }

        /// <summary>
        /// 审核单项申报审核记录
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult ReviewSubitemReviewHistory([FromBody]SubitemReviewFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            var dto = from.Merge<SubitemReviewFrom, SubitemReviewHistoryDto>();
            dto.Status = from.Status ? AuditStatus.已完成 : AuditStatus.被打回;
            ProjectService.ReviewSubitemReviewHistory(dto);
            return Success();
        }

        /// <summary>
        /// 重审单项申报审核记录
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult ResetSubitemReviewHistory([FromBody]SubitemReviewFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            var dto = from.Merge<SubitemReviewFrom, SubitemReviewHistoryDto>();
            ProjectService.ResetSubitemReviewHistory(dto);
            return Success();
        }

        /// <summary>
        /// 获取单项申报审核-分页数据集合
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ApiResult QuerySubitemReviewPagedList([FromBody]SubitemReviewHistoryCriteria criteria)
        {
            var list = ProjectService.GetSubitemReviewPagedList(criteria);
            return Success(list);
        }

        /// <summary>
        /// 获取单项申报审核记录-分页数据集合
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ApiResult QuerySubitemReviewHistoryPagedList([FromBody]SubitemReviewHistoryCriteria criteria)
        {
            var list = ProjectService.GetSubitemReviewHistoryPagedList(criteria);
            return Success(list);
        }

        #endregion

        #region 操作人记录相关

        /// <summary>
        /// 获取操作人信息
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public ApiResult QueryOperatorInfo([FromBody]BaseFrom from)
        {
            ProjectService.InitLoginToken(from.LoginToken);
            var result = ProjectService.GetOperatorInfoByAccountId();
            return Success(result);
        }

        #endregion

        /// <summary>
        /// 获取系统当前时间
        /// </summary>
        /// <returns>JSON</returns>
        [HttpGet]
        public ApiResult GetTime()
        {
            return Success(string.Empty, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        }
    }
}
