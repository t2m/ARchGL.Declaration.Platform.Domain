﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using ARchGL.Declaration.Platform.Service;
using ARchGL.Declaration.Platform.Service.Dtos;
using ARchGL.Declaration.Platform.Web.Models;
using ARchGL.Declaration.Platform.Web.Models.Api;
using TDF.Core.Configuration;
using TDF.Core.Models;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 推送数据相关接口
    /// </summary>
    public class PushController : ApiControllerBase
    {
        /// <summary>
        /// 管理员接口
        /// </summary>
        public readonly IAdminService AdminService;
        /// <summary>
        /// 系统服务
        /// </summary>
        private readonly ISystemService SystemService;
        /// <summary>
        /// 项目服务
        /// </summary>
        private readonly IProjectService ProjectService;
        /// <summary>
        /// 推送服务
        /// </summary>
        private readonly IPushService PushService;

        /// <summary>
        /// 初始化服务
        /// </summary>
        public PushController(IAdminService adminService, ISystemService systemService, IProjectService projectService, IPushService pushService)
        {
            AdminService = adminService;
            SystemService = systemService;
            ProjectService = projectService;
            PushService = pushService;
        }

        /// <summary>
        /// 获取推送-分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns>JSON</returns>
        [HttpPost]
        public ApiResult QueryProjectPagedList([FromBody]PushProjectHistoryCriteria criteria)
        {
            var result = PushService.GetPushPagedList(criteria);
            return Success(result.Item1, result.Item2);
        }

        /// <summary>
        /// 获取系统当前时间
        /// </summary>
        /// <returns>JSON</returns>
        [HttpGet]
        public ApiResult GetTime()
        {
            return Success(string.Empty, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        }
    }
}