﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARchGL.Declaration.Platform.Domain.Entities;
using TDF.Core.Exceptions;
using TDF.Core.Models;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 项目相关接口
    /// </summary>
    public partial class ProjectController 
    {
        public string rootId = "0EC6A0D1-8158-4834-95AB-C8A57D7E5";
        public Guid emptyGuid = Guid.Parse("00000000-0000-0000-0000-000000000000");
        public List<DP_Project_Category> list = new List<DP_Project_Category>();
        
        /// <summary>
        /// 初始化分类数据
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public ApiResult InitCategory(string password)
        {
            if (password != "!@#123qwe") throw new KnownException("密码不正确，请联系管理员");

            var asdfasdf = ProjectService.GetProjectCategoryById(Guid.Parse("0EC6A0D1-8158-4834-95AB-C8A57D7E5001"));
            if (!asdfasdf.IsNull()) throw new KnownException("分类已初始化");

            #region 一级分类

            CreateCategory(emptyGuid, "路基", 1, 1, 2);
            CreateCategory(emptyGuid, "垫层与基层", 1, 2, 2);
            CreateCategory(emptyGuid, "面层", 1, 3, 2);
            CreateCategory(emptyGuid, "挡护结构", 1, 4, 2);
            CreateCategory(emptyGuid, "安全防护设施", 1, 5, 2);
            CreateCategory(emptyGuid, "单位工程预验收", 1, 6, 2);

            CreateCategory(emptyGuid, "地基与基础", 1, 1, 3);
            CreateCategory(emptyGuid, "下部结构", 1, 2, 3);
            CreateCategory(emptyGuid, "上部结构", 1, 3, 3);
            CreateCategory(emptyGuid, "桥面系与附属结构", 1, 4, 3);
            CreateCategory(emptyGuid, "单位工程预验收", 1, 5, 3);

            CreateCategory(emptyGuid, "洞口及明洞工程", 1, 1, 4);
            CreateCategory(emptyGuid, "隧道掘进及初支工程", 1, 2, 4);
            CreateCategory(emptyGuid, "隧道防排水及二衬工程", 1, 3, 4);
            CreateCategory(emptyGuid, "隧道总体及附属工程", 1, 4, 4);
            CreateCategory(emptyGuid, "单位工程预验收", 1, 5, 4);

            CreateCategory(emptyGuid, "地基与基础", 1, 1, 5);
            CreateCategory(emptyGuid, "池体结构", 1, 2, 5);
            CreateCategory(emptyGuid, "管道结构", 1, 3, 5);
            CreateCategory(emptyGuid, "附属工程", 1, 4, 5);
            CreateCategory(emptyGuid, "单位工程预验收", 1, 5, 5);

            CreateCategory(emptyGuid, "地基与基础", 1, 1, 6);
            CreateCategory(emptyGuid, "墙体", 1, 2, 6);
            CreateCategory(emptyGuid, "附属工程", 1, 3, 6);
            CreateCategory(emptyGuid, "单位工程预验收", 1, 4, 6);

            CreateCategory(emptyGuid, "地基与基础", 1, 1, 7);
            CreateCategory(emptyGuid, "主体工程", 1, 2, 7);
            CreateCategory(emptyGuid, "安装工程", 1, 3, 7);
            CreateCategory(emptyGuid, "单位工程预验收", 1, 4, 7);

            #endregion

            #region 1.房屋建筑工程

            #endregion

            #region 2.城市道路工程

            var levelList = list.Where(x => x.Type == 2 && x.Level == 1).OrderBy(x => x.Sort).ToList();

            var category = CreateCategory(levelList[0].Id, "质量验收记录", 2, 1, 2);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[1].Id, "质量验收记录", 2, 2, 2);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[2].Id, "质量验收记录", 2, 3, 2);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[3].Id, "质量验收记录", 2, 4, 2);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[4].Id, "质量验收记录", 2, 5, 2);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[5].Id, "质量验收记录", 2, 6, 2);
            AddProjectCategorySecondChildren(category, "渝市政验收-8", "单位（子单位）工程质量竣工验收记录");

            category = CreateCategory(levelList[5].Id, "质量控制资料核查记录", 2, 7, 2);
            AddProjectCategorySecondChildren(category, "渝市政验收-9", "单位（子单位）工程质量控制资料核查记录");

            category = CreateCategory(levelList[5].Id, "安全和功能检验资料核查和主要功能抽查记录", 2, 8, 2);
            AddProjectCategorySecondChildren(category, "渝市政验收-10", "单位（子单位）工程安全和功能检验资料核查和主要功能抽查记录");

            category = CreateCategory(levelList[5].Id, "观感质量检查记录", 2, 9, 2);
            AddProjectCategorySecondChildren(category, "渝市政验收-11", "单位（子单位）工程观感质量检查记录");

            #endregion

            #region 3.城市桥梁工程

            levelList = list.Where(x => x.Type == 3 && x.Level == 1).OrderBy(x => x.Sort).ToList();
            category = CreateCategory(levelList[0].Id, "质量验收记录", 2, 1, 3);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[1].Id, "质量验收记录", 2, 2, 3);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[2].Id, "质量验收记录", 2, 3, 3);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[3].Id, "质量验收记录", 2, 4, 3);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[4].Id, "质量验收记录", 2, 5, 3);
            AddProjectCategorySecondChildren(category, "渝市政验收-8", "单位（子单位）工程质量竣工验收记录");

            category = CreateCategory(levelList[4].Id, "质量控制资料核查记录", 2, 6, 3);
            AddProjectCategorySecondChildren(category, "渝市政验收-9", "单位（子单位）工程质量控制资料核查记录");

            category = CreateCategory(levelList[4].Id, "安全和功能检验资料核查和主要功能抽查记录", 2, 7, 3);
            AddProjectCategorySecondChildren(category, "渝市政验收-10", "单位（子单位）工程安全和功能检验资料核查和主要功能抽查记录");

            category = CreateCategory(levelList[4].Id, "观感质量检查记录", 2, 8, 3);
            AddProjectCategorySecondChildren(category, "渝市政验收-11", "单位（子单位）工程观感质量检查记录");

            #endregion

            #region 4.城市隧道工程

            levelList = list.Where(x => x.Type == 4 && x.Level == 1).OrderBy(x => x.Sort).ToList();
            category = CreateCategory(levelList[0].Id, "质量验收记录", 2, 1, 4);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[1].Id, "质量验收记录", 2, 2, 4);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[2].Id, "质量验收记录", 2, 3, 4);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[3].Id, "质量验收记录", 2, 4, 4);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[4].Id, "质量验收记录", 2, 5, 4);
            AddProjectCategorySecondChildren(category, "渝市政验收-8", "单位（子单位）工程质量竣工验收记录");

            category = CreateCategory(levelList[4].Id, "质量控制资料核查记录", 2, 6, 4);
            AddProjectCategorySecondChildren(category, "渝市政验收-9", "单位（子单位）工程质量控制资料核查记录");

            category = CreateCategory(levelList[4].Id, "安全和功能检验资料核查和主要功能抽查记录", 2, 7, 4);
            AddProjectCategorySecondChildren(category, "渝市政验收-10", "单位（子单位）工程安全和功能检验资料核查和主要功能抽查记录");

            category = CreateCategory(levelList[4].Id, "观感质量检查记录", 2, 8, 4);
            AddProjectCategorySecondChildren(category, "渝市政验收-11", "单位（子单位）工程观感质量检查记录");

            #endregion

            #region 5.城镇给排水构筑物及管道工程

            levelList = list.Where(x => x.Type == 5 && x.Level == 1).OrderBy(x => x.Sort).ToList();
            category = CreateCategory(levelList[0].Id, "质量验收记录", 2, 1, 5);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[1].Id, "质量验收记录", 2, 2, 5);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[2].Id, "质量验收记录", 2, 3, 5);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[3].Id, "质量验收记录", 2, 4, 5);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[4].Id, "质量验收记录", 2, 5, 5);
            AddProjectCategorySecondChildren(category, "渝市政验收-8", "单位（子单位）工程质量竣工验收记录");

            category = CreateCategory(levelList[4].Id, "质量控制资料核查记录", 2, 6, 5);
            AddProjectCategorySecondChildren(category, "渝市政验收-9", "单位（子单位）工程质量控制资料核查记录");

            category = CreateCategory(levelList[4].Id, "安全和功能检验资料核查和主要功能抽查记录", 2, 7, 5);
            AddProjectCategorySecondChildren(category, "渝市政验收-10", "单位（子单位）工程安全和功能检验资料核查和主要功能抽查记录");

            category = CreateCategory(levelList[4].Id, "观感质量检查记录", 2, 8, 5);
            AddProjectCategorySecondChildren(category, "渝市政验收-11", "单位（子单位）工程观感质量检查记录");

            #endregion

            #region 6.市政工程边坡及档护结构工程

            levelList = list.Where(x => x.Type == 6 && x.Level == 1).OrderBy(x => x.Sort).ToList();
            category = CreateCategory(levelList[0].Id, "质量验收记录", 2, 1, 6);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[1].Id, "质量验收记录", 2, 2, 6);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[2].Id, "质量验收记录", 2, 3, 6);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[3].Id, "质量验收记录", 2, 4, 6);
            AddProjectCategorySecondChildren(category, "渝市政验收-8", "单位（子单位）工程质量竣工验收记录");

            category = CreateCategory(levelList[3].Id, "质量控制资料核查记录", 2, 5, 6);
            AddProjectCategorySecondChildren(category, "渝市政验收-9", "单位（子单位）工程质量控制资料核查记录");

            category = CreateCategory(levelList[3].Id, "安全和功能检验资料核查和主要功能抽查记录", 2, 6, 6);
            AddProjectCategorySecondChildren(category, "渝市政验收-10", "单位（子单位）工程安全和功能检验资料核查和主要功能抽查记录");

            category = CreateCategory(levelList[3].Id, "观感质量检查记录", 2, 7, 6);
            AddProjectCategorySecondChildren(category, "渝市政验收-11", "单位（子单位）工程观感质量检查记录");

            #endregion

            #region 7.城镇污水处理厂工程

            levelList = list.Where(x => x.Type == 7 && x.Level == 1).OrderBy(x => x.Sort).ToList();
            category = CreateCategory(levelList[0].Id, "质量验收记录", 2, 1, 7);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[1].Id, "质量验收记录", 2, 2, 7);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[2].Id, "质量验收记录", 2, 3, 7);
            AddProjectCategorySecondChildren(category, "渝市政验收-12", "                                     分部工程质量验收记录");
            AddProjectCategorySecondChildren(category, "渝市政验收-13", "                                   子分部工程质量验收记录");

            category = CreateCategory(levelList[3].Id, "质量验收记录", 2, 4, 7);
            AddProjectCategorySecondChildren(category, "渝市政验收-8", "单位（子单位）工程质量竣工验收记录");

            category = CreateCategory(levelList[3].Id, "质量控制资料核查记录", 2, 5, 7);
            AddProjectCategorySecondChildren(category, "渝市政验收-9", "单位（子单位）工程质量控制资料核查记录");

            category = CreateCategory(levelList[3].Id, "安全和功能检验资料核查和主要功能抽查记录", 2, 6, 7);
            AddProjectCategorySecondChildren(category, "渝市政验收-10", "单位（子单位）工程安全和功能检验资料核查和主要功能抽查记录");

            category = CreateCategory(levelList[3].Id, "观感质量检查记录", 2, 7, 7);
            AddProjectCategorySecondChildren(category, "渝市政验收-11", "单位（子单位）工程观感质量检查记录");

            #endregion

            ProjectService.InitProjectCategory(list);

            return Success();
        }

        public int count = 1;
        ///一级二级分类
        private DP_Project_Category CreateCategory(Guid parentId, string name, int level, int sort, int type)
        {
            var entity = new DP_Project_Category
            {
                Id = Guid.Parse(rootId + (count++.ToString().PadLeft(3, '0'))),
                ParentId = parentId,
                Name = name,
                Level = level,
                Sort = sort,
                Type = type,
                CreatedTime = DateTime.Parse("2018-12-21")
            };
            list.Add(entity);
            return entity;
        }

        //三级、四级分类
        private void AddProjectCategorySecondChildren(DP_Project_Category category, string threeName, string fourName)
        {
            var projectCategory = CreateCategory(category.Id, threeName, 3, 1, category.Type);
            list.Add(projectCategory);//三级
            list.Add(CreateCategory(projectCategory.Id, fourName.Replace(" ", "_"), 4, 1, category.Type));//四级
        }
    }
}
