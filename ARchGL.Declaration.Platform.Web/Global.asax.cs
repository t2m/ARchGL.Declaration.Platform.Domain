﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Web.Optimization;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using ARchGL.Declaration.Platform.Domain.Entities;
using ARchGL.Declaration.Platform.Service.Dtos;
using ARchGL.Declaration.Platform.Service.Mapper;
using TDF.Core.Configuration;
using TDF.Core.Ioc;
using TDF.Core.Log;
using TDF.Core.Module;
using TDF.Core.Operator;
using TDF.Data.EntityFramework.DbContext;
using TDF.Data.EntityFramework.Initializers;
using TDF.Data.EntityFramework.Migrations;
using TDF.Web.Authentication.Services.Implements;
using TDF.Web.Infrastructure;
using ARchGL.Declaration.Platform.Repository;
using ARchGL.Declaration.Platform.Service;

namespace ARchGL.Declaration.Platform.Web
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            var db = new DbSeedContainer();

            // 在应用程序启动时运行的代码
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            InitializerContext.Instance.IocInitialize(c =>
            {
                //这里可以做依赖注入
                //c.Builder.RegisterType<Class>().As<Interface>.InstancePerLifetimeScope();
                //c.Builder.RegisterType<DefaultDbContext>().As<AbstractDbContext>().InstancePerLifetimeScope();
                c.Builder.RegisterType<MySqlDbContext>().As<AbstractDbContext>().InstancePerDependency();
                c.Builder.RegisterType<CacheOperatorProvider>().As<IOperatorProvider>();
                c.Builder.RegisterControllers(Assembly.GetExecutingAssembly());
                c.Builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
                c.SetResolver = x =>
                {
                    GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(x);
                    DependencyResolver.SetResolver(new AutofacDependencyResolver(x));
                };

            }, new WebAppTypeFinder())
               .EnableLog4Net(c =>
               {
                   c.ConfigPath = HttpContext.Current.Server.MapPath("/Configs/log4net.config");
               })
               //根据对象名称自动映射如： Entity1 <=> Entity1Dto，DTO对象以dto结尾，并实现接口IDto
               .InitializeAutoMapper(new AutoMapperInitializer(Assembly.GetAssembly(typeof(Admin)),
                   Assembly.GetAssembly(typeof(AdminDto))))
                .InitializeDatabase<MySqlDbContext>(c =>
                //.InitializeDatabase<DefaultDbContext>(c =>
                {
                    c.ConnectionName = "MySqlDatabase";
                    //c.DatabaseInitializer = new CollegeDropCreateDatabaseIfModelChanges<DefaultDbContext>();//第一次创建数据库用此方法
                    //c.DatabaseInitializer = new EmptyDatabaseInitializer<DefaultDbContext>();//正式环境用此方法
                    //数据自动迁移模式，数据库会根据领域模型变化而自动更新表，适合开发中使用的模式。
                    //c.DatabaseInitializer = new MigrateDatabaseToLatestVersion<DefaultDbContext, Configuration>();
                    if (Configs.Instance.EnvironmentType < EnvironmentType.Formal)
                    {
                        //数据自动迁移模式，数据库会根据领域模型变化而自动更新表，适合开发中使用的模式。
                        //c.DatabaseInitializer = new MigrateDatabaseToLatestVersion<DefaultDbContext, Configuration>();
                        c.DatabaseInitializer = new MigrateDatabaseToLatestVersion<MySqlDbContext, MySqlConfiguration>();
                    }
                    else
                    {
                        //生产环境用此方法
                        c.DatabaseInitializer = new EmptyDatabaseInitializer<DefaultDbContext>();
                    }
                })
               .MouduleInitialize()
               .ExecuteInit();


            var autoTaskService = Ioc.Resolve<IAutoTaskService>();
            autoTaskService.StartTask();//启动推送数据检查任务
        }

        void Application_End(object sender, EventArgs e)
        {

        }
    }
}