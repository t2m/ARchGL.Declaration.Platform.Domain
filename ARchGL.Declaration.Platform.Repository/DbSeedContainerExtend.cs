﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace ARchGL.Declaration.Platform.Repository
{
    public partial class DbSeedContainer
    {
        /// <summary>
        /// 添加审核帐号
        /// </summary>
        /// <param name="dbContext"></param>
        public void AddAccount(DbContext dbContext)
        {
            //批量初始化数据
            AddAccounts(dbContext, accountList, "aa705634-29cf-46dc-b7a5-84b9b3c4d567", "重庆市", "500000", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "ba7c1e25-d786-4119-9cb5-cfd1ce0b0e89", "万州区", "500001", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "c0be1724-744c-4f6a-97ca-96b6342d839d", "黔江区", "500002", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "fd36f882-7d1a-403a-9df1-c760c313f394", "涪陵区", "500003", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "a2b11d33-f027-4fa7-99bf-096a5d05a9a7", "渝中区", "500004", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "9defaec1-e5c8-4b53-9f18-35ccb1df3d59", "大渡口区", "500005", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "ec00c583-0e8c-4dbf-b107-d267a9fc1ce2", "江北区", "500006", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "354e3d70-6356-42e3-9912-c040b834d1d4", "沙坪坝区", "500007", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "2eede11c-1843-45f3-bb78-262010adfba1", "九龙坡区", "500008", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "cb490469-66e9-4716-afed-60a1946bb9e3", "南岸区", "500009", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "908dc527-7755-4f3f-92c6-e481d90e9d2d", "北碚区", "500010", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "e1e41c64-6a85-4629-8ccb-e2e4af805b52", "渝北区", "500011", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "7052674d-c05c-4316-b9ed-3809e748c660", "巴南区", "500012", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "7f3d76ee-894d-4d17-bf3d-2f69704e7959", "长寿区", "500013", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "f8aac356-2ce3-4e61-8723-e8627806eddd", "江津区", "500014", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "bf5b2ffd-721a-468c-96b0-2afd2ea5cb0d", "合川区", "500015", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "9b778056-4d22-4488-a6d6-d80b927492e3", "永川区", "500016", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "f87709a3-fb55-4522-ae20-70125807f6a1", "南川区", "500017", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "b4c301e1-aab8-4d98-ace2-4312907c13fd", "綦江区", "500018", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "2e65f938-649e-46aa-9d1a-18ee39889391", "大足区", "500019", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "eb4955a7-13c6-4b9a-80ca-d4a2d622bd98", "璧山区", "500020", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "f74b14c2-2064-475f-9d42-d764d31de277", "铜梁区", "500021", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "3b0c472d-04d5-4f9a-9930-f7fabe956966", "潼南区", "500022", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "33784f73-37f8-471e-a6d4-8d9d7923c953", "荣昌区", "500023", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "d58e4523-7fdd-421b-a6ce-f1c339604b24", "开州区", "500024", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "8dfe88d9-f8be-4add-a729-8c339fb95394", "梁平区", "500025", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "f144154b-1a79-4990-90d0-755d4ea5e1c4", "武隆区", "500026", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "f4a10c82-86cc-4953-b6af-a2a9cf81f3c3", "城口县", "500027", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "feaa0d4f-0387-415e-b3ce-0bada03e0dfb", "丰都县", "500028", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "e7b4841e-29c7-4a1b-8e24-130f660b2636", "垫江县", "500029", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "4b713adc-71f4-4120-bc30-5b7c25aa27b0", "忠县", "500030", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "5fe60c8a-8209-4255-ad2b-a6f10150c5fa", "云阳县", "500031", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "17a250d6-9d41-4b4d-a244-e53ab7ce62d1", "奉节县", "500032", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "43d3c4cc-1056-4329-a85b-d52219f91342", "巫山县", "500033", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "049340eb-58c8-45fd-9992-ff033410e341", "巫溪县", "500034", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "c9077131-1e8c-4cb9-a6df-bbf68f82971e", "石柱县", "500035", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "d3338ec3-5030-4945-9029-a754960c1787", "秀山县", "500036", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "14ca8d41-1d0c-4ce3-bab3-b3f2beef544a", "酉阳县", "500037", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "a35faa09-c20e-4ccd-bdbc-3e96f1e4417f", "彭水县", "500038", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "117c1aa7-c9bc-4c47-ad3f-653608fdca15", "两江新区", "500039", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "4a4dac13-8846-4853-b63d-894a890c8a36", "经开区", "500040", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "a7b9bcd4-d044-4a6d-8ec4-24e4a77f1b80", "高新区", "500041", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "8a964c4f-fbb0-41e4-a230-418ebf8138f7", "万盛经开区", "500042", AccountType.区县帐号);
            AddAccounts(dbContext, accountList, "7ed8674e-932d-4041-a0c6-12a99ff1a660", "双桥经开区", "500043", AccountType.区县帐号);
        }

        /// <summary>
        /// 添加企业账号
        /// </summary>
        public void AddProjectAccount(DbContext dbContext)
        {
            AddAccounts(dbContext, accountList, "0197da37-4fe2-40bf-92fe-5d42d857e287", "中建八局第三建设有限公司", "91320100134891128H", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "01aeabfa-7e8c-4006-b688-5d4acc34831a", "四川大诚建筑工程有限公司", "91510000560738089M", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "037e22b1-2654-4c99-ab5b-899516fd0c9d", "江苏仪征苏中建设有限公司", "913210817307396915", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "05090c9e-f24f-4d48-a60e-8430274ec905", "重庆建工第七建筑工程有限责任公司", "915000002030377780", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "0512d623-9dfb-4ed5-91a0-8fb74b6c4a16", "中铁十七局集团有限公司", "911400001100708439", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "079f4a1d-b320-40e1-842a-ef34da631f38", "重庆市庆东建筑工程公司", "91500230208753984C", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "08748421-9edb-4884-9f94-4443f9a3f252", "重庆教育建设(集团)有限公司", "91500000203645116A", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "0b4d146e-d77c-4c65-85cc-35b3f6cecc3b", "中铁二十二局集团有限公司", "9111000071092227XH", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "0cdda195-f3a5-4376-8312-3dc2ca031ac5", "中铁建设集团有限公司", "9111000010228709XY", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "0e5d14e5-b7df-46f9-9f35-67a2954b88af", "重庆金晟源建设工程有限公司", "91500101207934249H", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "0ea4c755-a59b-4107-9119-33042c8cb8bc", "南部县锦兴建筑安装工程有限公司", "91511321584213383R", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "0f088b7c-04af-4080-91b0-9a04e34dbaf0", "重庆崇景建设有限公司", "915001017116844455", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "105787a7-4286-4e3a-9762-f752dd6d4d8f", "上海建工一建集团有限公司", "913101151324008074", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "10805aa9-a094-4274-b862-ec3618228ce2", "重庆长坪建设集团有限公司", "915001132034066685", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "169d0e2b-0963-4c4a-9790-e6b0f19839c7", "重庆吉隆建筑有限公司", "91500118203791076F", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "16c1cf78-0958-430b-9c36-305313b566bf", "浙江新东方建设集团有限公司", "9133072777825022XY", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "194250c7-a8f6-4e00-b844-922f7875df96", "重庆诚业建筑工程有限公司", "91500107666403682M", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "1aae05a8-3a48-48fd-bff8-7cbe3975a7aa", "重庆松龙建筑(集团)有限公司", "915001122035067490", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "1bcb46fc-459a-417a-9be2-70fc1128c148", "重庆市茂森建筑工程有限公司", "91500224203705246R", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "1c678213-39ec-4157-8126-54cca223dce8", "中国建筑一局(集团)有限公司", "91110000101107173B", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "1d0f18a0-bb54-462f-a006-70a4e0f7349f", "湖南众诚建设工程有限公司", "91430000707239613F", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "1d57c549-0249-4e20-9d6d-242387f18833", "重庆博达建设集团股份有限公司", "91500000203592894J", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "1e42693b-3616-408f-9ea5-ef59009a1397", "重庆渝建实业集团股份有限公司", "91500000203888388W", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "216dfd94-52ec-436e-9543-e2859e5acfd3", "正威科技集团有限公司", "91321204141366786E", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "222ebce6-7355-48e3-a5d3-8ebb037f07fe", "福建省荔隆建设工程有限公司", "91350300671935165J", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "247b5b85-602e-426e-b34c-e803b46ba335", "中建一局集团建设发展有限公司", "91110000101715726A", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "24ac0af3-9672-4280-b426-92cd96278b79", "中冶建设高新工程技术有限责任公司", "91110000100023357T", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "24d8750b-cc36-4596-a274-2ba329f0e85c", "中建新疆建工（集团）有限公司", "9165000022859700XU", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "26eca04f-c433-4627-bc5e-797f706b14a8", "重庆跃城建筑工程有限公司", "915002265842852727", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "275537e1-68b2-45e3-979b-fe19de87b1e4", "中天建设集团有限公司", "91330783147520019P", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "276d5ccd-3a39-4e2f-a629-dbea39a25db2", "中国建筑第六工程局有限公司", "911201161030636028", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "27d0f82a-86cc-4139-8de0-74acdb5d65bf", "重庆市渝北区海华建筑工程有限公司", "91500112203540058R", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "27fa6c8c-868f-4150-bfbd-5e1ea0817cf8", "中冶天工集团有限公司", "91120118789363043U", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "290e0bd6-b601-4be3-b47b-b32de9984cc7", "重庆山海建设(集团)有限公司", "915001032028648861", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "29996dd5-86fe-4bd8-aa07-801e1b3a0385", "重庆益欣荣建设工程有限公司", "915002437717917369", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "2a4c88bd-ecc3-48e1-bf4d-91e7b4d49207", "中东建设集团有限公司", "91350521768564006U", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "2a893b8a-fd7c-4c62-aff3-1d3bebe61166", "重庆庆华建设工程有限公司", "91500109753062492U", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "2af544e1-36ad-4353-95a5-06cb5bb035e5", "华建利安建设集团有限公司", "91510000749633694W", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "2af71b98-89a4-4d18-886b-4d7109974443", "重庆万港工程建设有限公司", "91500101711685747K", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "2afe914f-8e88-45da-a87d-1955e0e00118", "南通德胜建筑工程有限公司", "91320684138791968Q", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "2ca9283f-fbd1-4b35-994d-e7047040544f", "重庆厦宇建筑工程有限公司", "91500118073660106L", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "2d0f5e00-bb98-4ec2-95aa-9b3ec09a1262", "中国华西企业股份有限公司", "91510000201808890B", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "2d5cc682-4ab8-405f-a464-3e943e424721", "重庆达康建筑工程有限公司", "91500107203139790L", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "2da1d3ee-f75a-4924-9b27-439d0bdb3a41", "重庆中科建设(集团)有限公司", "91500102709427140L", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "2ef8e605-870d-4cb9-8c2a-10bfbebd0938", "重庆华硕建设有限公司", "915001152034048303", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "3129478f-acc2-4600-a1ba-9e1ed3399c37", "重庆市阜泰建设（集团）有限公司", "915001162035865740", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "31720c35-0e8e-4466-911a-565fbb98e9c2", "中建四局第五建筑工程有限公司", "91522701216250420B", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "31ea594e-7df2-43a2-aa75-5b15338f59ee", "重庆南洋建筑安装工程有限公司", "915001132034257973", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "32c40b01-8ac6-46b5-9e28-3f7530813600", "重庆佳宇建设（集团）有限公司", "91500107203114390A", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "32e42192-851b-4a5c-bc50-642aa7dfbba0", "江西润财建设有限公司", "91361100561082712Q", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "34519915-8b49-4bb8-8285-ad5b9a11bf64", "福建众诚建设工程有限公司", "913507001570228818", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "367d7526-0a2b-4c72-9362-a3d8eaa3d186", "中建欣立建设发展集团股份有限公司", "915116816841895878", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "3843d8d0-ef7f-4809-b982-65f4cc75de92", "重庆市万州区地方建筑有限公司", "915001012079017115", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "38513882-9909-4df7-8ebc-1b648c2f40f9", "山河建设集团有限公司", "91421121272000266B", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "3d75761b-56f6-4c96-b361-bd64aeaddab4", "福建宏盛建设集团有限公司", "91350100611325517C", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "3e0c0b2d-764e-47c6-b91c-e7ce2d31fcb0", "重庆腾鲁建筑安装工程有限公司", "91500105450448910H", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "3e6a1d19-a086-4231-ac63-f5c0aff37ce8", "四川住总建设有限公司", "91510521204864994Q", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "3eadd52a-c095-46b1-80d4-6643af942084", "重庆市万州建筑工程总公司", "915001017116015079", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "4080cf4b-a087-493a-b122-c8371d23047a", "重庆城鹏建筑工程有限公司", "9150010820315005XF", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "43a9253c-ca37-4240-968d-307dece27496", "上海宝冶集团有限公司", "91310000746502808A", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "450c7abf-40bf-4feb-bd08-e806385f0bc7", "重庆利安人居建设工程有限公司", "91500118305024927M", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "45610f97-fb83-4b22-bc2a-f2002445ca03", "中铁十一局集团有限公司", "91420000179315087R", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "46685fc4-a2f8-4942-8548-72f67f87be98", "重庆渝发建设有限公司", "91500115203399701L", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "48057976-f9c9-4d4c-a402-83c5c82a81fe", "浙江新东阳建设集团有限公司", "91330783147581401A", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "48b52bae-ef5d-4a5c-9c10-790afc6c7e3a", "南通华新建工集团有限公司", "913206217037355485", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "4949e603-c8bd-43b8-a429-1494fa57ff64", "四川航天建筑工程有限公司", "91510112210450739B", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "49c1bd81-af99-4442-b70a-89c283f629fa", "中交第二航务工程局有限公司", "914201001776853910", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "4ab371c4-31e1-4ca9-9cf7-afe8c7079762", "重庆慧河建设有限公司", "91500107771776659A", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "4bf8c27a-8757-4051-beb2-6954a64f44ed", "中国十九冶集团有限公司", "91510400204350723Y", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "4d79e301-5dab-4ab0-8a40-4cb596adcec4", "重庆康悦建设（集团）有限公司", "91500222203459823F", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "4e23f0b9-9c7d-4ac4-9344-ae645b1dd436", "中亿丰建设集团股份有限公司", "91320500137690962B", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "4ea8fdae-4d37-4922-ab95-d36c4f064efa", "中交一航局第四工程有限公司", "911201161036225156", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "4ee954c6-392c-480b-ad10-52040d701e49", "中铁建工集团有限公司", "91110000710921189P", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "4f6df011-0ac3-44d7-957e-4af37bf3c473", "重庆第六建设有限责任公司", "915000002028326067", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "50602e69-2542-476d-b383-8fcccdee040f", "重庆鹏威建筑工程有限公司", "9150010857344416XK", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "51a10c3d-02fb-4101-ad03-66c453d034f7", "湖北信义兄弟建设集团有限公司", "91420116558449739P", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "544c3b36-3018-4c2b-ba82-3bf71db3f217", "中亚建业建设工程有限公司", "91511402207304853R", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "544c941a-0c0a-4aa8-b4db-559b4bc3a416", "重庆渝能建筑安装工程有限公司", "9150010820292907XR", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "571db377-e0eb-4d4f-832d-c9d800c2f314", "重庆对外建设（集团）有限公司", "91500000202803864J", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "582b12aa-bc80-462c-a546-a1d9f8b74b53", "中建隧道建设有限公司", "91500113MA5U7U4M9E", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "585d5bcf-6059-437a-97cf-b0998838aa97", "重庆钢铁集团建设工程有限公司", "915000002028768602", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "588b8101-e61c-482f-9f3f-c52a2cad0027", "中建一局集团第五建筑有限公司", "91110000101638302P", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "589fa178-41ee-4fe5-b511-67e8f4e0f87b", "中铁二十三局集团有限公司", "91510100740338242L", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "59953b9e-f9ee-4098-a883-c54c94ed3d4d", "四川域高建筑工程有限公司", "9151000068417596XN", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "5b2ca500-2a32-4a84-b620-f35e8297c828", "中国核工业二四建设有限公司", "91510000621600455J", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "5b7386be-38bf-4f64-9228-c55ae0e8de17", "重庆先锋建筑工程有限公司", "915001137094571042", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "5c432919-8921-47e6-ab3f-cda6c3da7e17", "重庆创设建筑工程有限公司", "9150011266088805XJ", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "5c9baf2e-a42c-4eed-9971-960efdcc442f", "衢州市政园林股份有限公司", "91330800609808544L", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "5cc3ea3c-60dc-4d31-9106-256a8a9524af", "重庆市银峡建筑工程集团有限公司", "915001012079140610", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "5d7faad7-f1cc-4640-9eda-fccb95689278", "重庆市万州水电建筑工程有限公司", "91500101207912963T", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "5dc80105-4d40-4956-82b0-801c3565dc5f", "浙江欣捷建设有限公司", "913302001448768377", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "6398cba0-2428-432a-bf88-c6912038b48f", "重庆三峰卡万塔环境产业有限公司", "91500104202981978N", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "64273614-6ae1-45c1-a650-eaa1dbd2681e", "重庆光鸿建筑工程有限公司", "91500108060538419U", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "655f11dc-b4d9-48fc-8d17-165f9854034b", "重庆渝鸿建筑工程（集团）有限公司", "915001056220060368", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "65d6e1c6-0bf3-4b8d-bcc5-803123fce002", "重庆桥强建筑工程有限公司", "91500242MA5UR91H0U", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "66d02ab5-107f-4902-9e0a-eac177d6cd49", "中铁大桥局集团第八工程有限公司", "91500109573405793Y", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "68b9e657-754f-4642-a7b1-6c42ee90e3cc", "重庆城建控股（集团）有限责任公司", "91500000709441516W", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "68f923cf-0b2a-4310-bb3e-896ba706be7c", "天元建设集团有限公司", "913713001682510225", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "6a494d3f-40d9-48d0-8306-939a6dffd840", "中铁十二局集团建筑安装工程有限公司", "91140000111071637N", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "6a997b8e-ff82-4b4a-a05f-e6b101d2e3c8", "中交一航局第二工程有限公司", "913702001635708411", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "6ac6f789-1076-4a4a-83cf-6a9c9236b935", "重庆建工第二市政工程有限责任公司", "91500000202801455F", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "6b5e6758-88e3-4c29-bd1f-bcd3b2c6b649", "重庆坤飞建设（集团）有限公司", "9150010920320015XF", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "6b802965-c2b0-4f96-880e-541e7780a376", "南通四建集团有限公司", "913206121387235782", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "6ba78e69-8b96-4271-841b-b3419d7979d1", "重庆展宇建筑工程有限公司", "91500102561607291C", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "6bc920f7-7e57-4dce-b580-2ee354862b04", "浙江精工钢结构集团有限公司", "9133062171252825XY", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "6f87f712-6f6b-4d11-a782-4797ad55c14d", "龙盘建设工程集团有限公司", "91510300204053518T", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "6ff32a3a-8802-43e2-82f7-77f5be237f9f", "重庆建工第九建设有限公司", "91500000203145189X", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "7032ee0b-6b54-4cf8-85ff-dc034ef949bf", "贵州建工集团第五建筑工程有限责任公司", "91520000214410777M", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "71314278-4c63-4621-96b1-59881f885525", "中建三局第三建设工程有限责任公司", "914201001776930413", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "7148a831-086a-48ea-a5f8-522a8cd8c755", "重庆建工第四建设有限责任公司", "91500000202800890K", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "7299260b-880a-4c80-8cbd-3536227dfa0b", "中国建筑一局（集团）有限公司", "91110000101107173B", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "72e1208b-55d5-42cc-82b2-6b070fb12df3", "重庆北部双龙建设(集团)有限公司", "91500112622100436A", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "72f20602-3f68-4e85-b7dd-48e6d4474221", "太原市政建设集团有限公司", "911401001100834259", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "76c6e29b-1588-4f7d-9687-bf4244a5f0c7", "十九冶成都建设有限公司", "91510100562038799X", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "785def62-f6fe-4a35-9dbc-cb8e585e9e14", "重庆市巴南建设（集团）有限公司", "91500113203436859R", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "7a9ce85b-40cf-4aa5-a04c-e2bdc7793c90", "重庆市辰河建筑工程有限公司", "91500233208153735T", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "7ab36022-858c-41fe-afc9-9678889f26f0", "重庆市华东建筑集团有限公司", "91500228753052710C", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "7b0bfe6f-a735-483f-b75a-a431501f6046", "重庆建工第二建设有限公司", "915000002028051812", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "7de3e0d5-b2f6-40f7-9762-b5eb52c7df08", "重庆嘉逊建筑营造工程有限公司", "91500000778457538G", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "7e3d3ff6-33ad-4018-81eb-50abe9ac3e8b", "中铁二十四局集团有限公司", "913100001322024481", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "7f623193-8cd8-472e-9936-44d4d0e4e78d", "中国建筑第二工程局有限公司", "91110000100024296D", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "7f6ad9f0-724e-4f50-96cf-e02f60e62240", "重庆市开州区渝东建筑工程有限公司", "91500234208109988P", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "7f755880-c430-47fc-bb67-dba8eb8890d3", "嘉兴大源建筑工程有限公司", "91330402683141389T", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "807a9da3-621b-4584-b30f-ece57fa1fafe", "中国建筑第五工程局有限公司", "91430000183764483Y", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "8108a8f8-9312-42da-aeba-14e1ff16dd02", "上海绿地建设（集团）有限公司", "913100001331263364", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "833e01a9-e272-4599-b200-23465180c93f", "中国二十冶集团有限公司", "91310000739759277B", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "83466abd-fb30-47e3-9aef-f921e9f7359d", "四川省第三建筑工程公司", "915100002018080067", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "835140a7-fa4c-4c9a-9079-083d75e4c1f8", "重庆万美建设工程有限公司", "91500102208557377K", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "84fb5bae-000c-40e0-8c07-411e16eb42f3", "重庆祥瑞建筑安装工程有限公司", "915001167453400690", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "84fdc14f-86fa-4557-bb41-5efd3de6fe0e", "中建四局第三建筑工程有限公司", "91520302214780129Y", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "856cf954-92a9-4e8c-8b82-7ee2cfc721bd", "广州天富建设工程监理有限公司", "91440101734893848B", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "85bb5dbb-f21c-4e98-b897-b748ea66000d", "业兴实业集团有限公司", "91500112203526977P", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "86b0b794-2b07-405a-9b07-d91a25d21dde", "重庆万泰建设（集团）有限公司", "91500112784219544A", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "86ce174d-3e7c-45d6-af45-ed5776c90a73", "重庆市基础工程有限公司", "9150000020282565XH", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "879907cb-42e9-45ef-8b8f-5d2a9443042e", "重庆大江建设工程集团有限公司", "915001056220734903", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "8a84a836-a591-4766-8c72-6708a9a7a046", "中电建建筑集团有限公司", "911100001011159077", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "8a84aebb-8696-4f2b-9f80-bbda2533ab31", "重庆昌林建筑工程有限公司", "91500000668945637C", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "8b51ecbb-5b37-4b2d-925a-16104ee9654f", "重庆龙润建筑安装工程有限公司", "91500000793536752C", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "8f5b3ef0-bce9-4a9c-8305-3439fa2389eb", "中冶建工集团有限公司", "91500000795854690R", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "8f9724f9-c2ee-4052-bc93-9d1e2e04b4fa", "中铁城建集团第一工程有限公司", "911400000870963902", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "8fdd4d37-f283-48b3-bec4-da6c77996ab3", "重庆一品建设集团有限公司", "91500113203404742P", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "8ff9fd6a-4b89-4cb7-8b15-383f0ed766bc", "中国铁建港航局集团有限公司", "914404005796688347", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "92efa489-c79f-41e0-b170-8d90349e493f", "中建二局第一建筑工程有限公司", "91110000104341301L", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "92f67f6f-db14-4429-90c9-0b933dcffb75", "贵州仓达建筑有限公司", "915205223373883976", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "93b55e3d-36dc-46eb-a72a-ba2f947b765e", "湖南北新天际建设工程有限公司", "914300006985786391", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "93dfac7e-4a91-4d49-b51a-b063f105a0d8", "北京城建北方集团有限公司", "9111000073555651X9", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "9531cb47-dbfd-4469-85fb-7e049ae9b6b6", "北京城建九建设工程有限公司", "91110108722601000X", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "960a5a9f-0832-490f-933a-cc99c14b7b0e", "重庆单轨交通工程有限责任公司", "915000007784693958", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "969013d0-32c7-4fa3-9aa6-016a2b1865a7", "重庆建工第一市政工程有限责任公司", "91500000202800874X", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "97297861-e2e3-4d85-b8e3-d34e0926afac", "重庆倍广建筑工程有限公司", "91500109588032053B", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "98d67963-2a18-4adc-bf45-b6de85d17bb0", "重庆建工第三建设有限责任公司", "915000007339743120", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "9925d595-797c-4e8d-bfa2-56495fa741f2", "国基建设集团有限公司", "911400007159997781", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "99a600e6-7dea-4aea-b99d-e8a23d8d70b8", "重庆宏宇建设工程(集团)有限公司", "915002436608590130", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "99e61ccb-bb02-4d6f-89a5-5d1ca7e0a1c3", "江西省城建建设集团有限公司", "91360983733939357R", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "9a0881d6-05d0-4490-afb3-75d9bc59bd3f", "四川省第四建筑工程公司", "91510600205100611N", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "9ad9582f-b0cc-4a98-82c0-08bfe1e1bfaa", "重庆渝圣建筑工程有限公司", "915001132034143856", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "9b53e0bd-0bda-47a0-bb62-e2f9268bc589", "重庆中航建设（集团）有限公司", "915002232036970047", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "9c553d39-4a7a-4d52-b3dd-6bc9f207abc4", "西双版纳云海建筑工程有限公司", "91532800080409024X", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "9da6967b-cc16-49e0-8377-422f5554aa4d", "中建五局第三建设有限公司", "91430100183853582A", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "9f8b8c3b-4b8e-4d81-bd91-fd9a62216672", "四川永存建筑工程有限公司", "91510000633164363L", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "a0cc8733-2847-4bf9-85d5-376dde0d1abf", "山西恒业建筑安装有限公司", "91141100746033626W", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "a194c9e5-97fe-40a8-82b4-9507ef073d6e", "北京中铁隧建筑有限公司", "911100008011269490", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "a4192eb5-729c-4623-b162-b434fedc1aaa", "中建三局第一建设工程有限责任公司", "914201007483157744", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "a4c2cd5b-2fb3-4c6e-a04b-faacbbb6c68f", "自贡华兴建筑安装工程有限公司", "91510300204053518T", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "a598cb28-24a3-4e24-b609-93009de7f383", "中国核工业中原建设有限公司", "911100001000124711", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "a60a1d8e-99d1-4734-b1a2-ed968760c087", "中欧国际建工集团有限公司", "91511402621133375W", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "a83a5037-ff18-4068-b433-a1f5f0a4315b", "重庆市渝北木鱼石建筑工程有限公司", "915001127339876234", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "a845a788-a96c-4b25-9932-1c752eb73090", "重庆黔程建设（集团）有限公司", "91500114213901275F", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "a98e5f3f-b12b-4ad7-8712-12dcb39c1962", "重庆建工第八建设有限责任公司", "91500000203137250B", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "aa1bd4a4-ab9c-4a86-8681-91cfc550fdc8", "江苏省建工集团有限公司", "913200001347521875", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "abcbc7fd-e2a8-4c09-8ef6-70b29579f852", "重庆一建建设集团有限公司", "915000002028150726", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "ac2017db-5b32-4d37-b7ce-ff992af4b979", "重庆飞洋控股集团宝宇建筑工程有限公司", "91500236736587780W", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "ac4cc814-631d-45db-8d9d-c89260ed03ac", "重庆项毅建设（集团）有限公司", "9150010967104897X2", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "ac6f42a0-779c-45d6-89c7-2750ceea9ed6", "四川公路桥梁建设集团有限公司", "9151000020181190XN", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "ad0897b4-3bd8-4512-bd3f-652e4cc1bd16", "重庆桂佳建筑工程有限公司", "915002317935449124", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "ad2d452a-7bb9-4b12-9c3b-92ad96a24c87", "中国建筑第八工程局有限公司", "9131000063126503X1", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "ae9d1cca-c12f-4040-88a0-bdae91a7c388", "中建八局第四建设有限公司", "9137021271370434XD", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "aff5f949-9909-47bf-9a9b-2fdd5e844b89", "重庆覃家岗建设（集团）有限公司", "91500106203099717F", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b064747c-e0bf-4621-a83c-87be068409ff", "北京建工四建工程建设有限公司", "91110000101510712K", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b0ae47c3-dcb9-413c-ab2e-e9d5d232194b", "中国建筑股份有限公司", "911100007109351850", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b29c75e5-c0f2-4926-b32e-e34e7ec0587f", "福建省晓沃建设工程有限公司", "91350122154700712W", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b31b3fc7-1dc0-4aa7-9919-fee8ad06152a", "重庆锦庆建筑工程有限公司", "91500110203906048J", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b3773b13-d2b9-43e4-a3f7-6b1bed4e2feb", "重庆海博建设有限公司", "91500112203510350T", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b3fbf9db-5def-4366-a320-035a9e94aa88", "重庆市兴远建筑工程有限公司", "91500227203905942M", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b63ae829-1513-466e-b787-07153cfdcc53", "重庆市渝海建设（集团）有限公司", "915001017093776733", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b6c9399c-6e54-4746-89dc-6963fb2a7960", "中交二航局第二工程有限公司", "91500000778492034J", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b6f1af85-b515-4c2d-b7da-8626ab03186d", "富利建设集团有限公司", "91440101190527857P", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b7c429a0-5b9a-4b21-88e9-2344733ba94c", "泰兴一建建设集团有限公司", "91321283745563684C", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b804fbcc-7372-4198-91e9-99fc989f68c0", "重庆亿瑞建设工程有限公司", "915001126221017891", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b82bb245-9355-444d-8d46-e9e0bf301b04", "广州天力建筑工程有限公司", "91440101190498128T", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b880b95c-ef3d-4a9f-bea9-89fe9054a9f5", "中核华辰建筑工程有限公司", "91610000661156408T", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b9551331-2df3-4ed9-9249-f821edcf0b81", "中国建筑第八工程局有限公司", "9131000063126503X1", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "b9f678ce-a853-432c-8bee-991adba85262", "重庆市渝泽建筑工程有限公司", "915001182037507761", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "ba955159-6e18-4848-8e58-25e970e43c36", "海天建设集团有限公司", "91330783785663700R", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "baabb873-3c82-4ab9-9c64-b399904108ea", "重庆腾宇建设工程有限公司", "91500109739828762U", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "bebabfae-7faf-4663-95a7-0a6be38bd7db", "中建三局集团有限公司", "91420000757013137P", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "c2adb616-6a20-4284-978a-953f95261c7c", "重庆致辉建筑工程有限公司", "91500106668918719G", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "c34865bd-f956-4b39-93e4-7e28cb7b3aed", "重庆鹏帆建设集团有限公司", "91500113203408583G", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "c348bfd5-1233-491e-aa68-d5b76a7df22a", "重庆市垫江县建筑有限公司", "915002312086533341", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "c382fb91-2840-47e8-9bfe-536b68e7b6dd", "重庆建工第十一建筑工程有限责任公司", "915001072031018215", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "c3c3a152-5a8d-4821-ac7b-edfd0d386fa8", "重庆光宇建设开发（集团）有限公司", "915001152033974580", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "c4062244-50d7-4e85-b504-504ba3fb4b90", "重庆市爆破工程建设有限责任公司", "91500000202883649J", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "c6fb301b-9273-4c08-93b1-d11ac9bdd48b", "重庆市佳诺建筑工程有限公司", "91500115203353877R", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "c7243232-e5aa-415e-8476-724a9cd5e7b6", "成都建工第八建筑工程有限公司(原名：成都市第八建筑工程公司)", "91510100201912401J", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "cae2d210-1f8a-44a6-85a0-6137ea5476d9", "重庆中环建设有限公司", "9150000020289820XH", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "d08eae9d-62eb-470d-ae63-e60fb4ebc7fc", "中铁十一局集团第五工程有限公司", "91500000202805587B", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "d1ad370a-e384-4c64-aaae-2da20620f036", "华润建筑有限公司", "911101017109278399", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "d2720987-240a-46c5-bc7f-e299e925ce51", "中建二局第三建筑工程有限公司", "9111000063370987XW", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "d3c8377f-9c83-4a72-a7a4-5862a0f1f892", "中铁隧道股份有限公司", "9141000017292850XF", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "d3caea1f-1257-482b-8445-9bf6002246f4", "中铁二十五局集团有限公司", "9144000019043049X8", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "d545c8f8-50e4-4790-b803-9081cbb61fc4", "西南建工集团有限公司", "915116007118166988", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "d61622fb-a171-46ad-8d71-c4887d754ea7", "浙江天勤建设有限公司", "913306217368750966", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "d6c19bcf-35a8-49e8-b354-898971b49035", "福建省中兴建设发展有限公司", "91350521768557105C", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "d7c5dc3a-3cf8-4c9b-81a3-dd72d8a821d8", "四川时宇建设工程有限公司", "91510000595089285M", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "d83e5716-367b-45cc-a157-51abd2eb79e6", "重庆建工住宅建设有限公司", "915000002028009114", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "d8cf6e10-9615-4924-b114-285c6cb95e04", "中建三局第二建设工程有限责任公司", "91420100177739097A", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "d8ea0340-b111-4ca8-a5f0-53e10bdd78c5", "重庆紫东建设工程（集团）有限公司", "915001092032313197", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "d99ab770-4982-4619-8316-d09fa4b13802", "重庆万州兴涛建筑工程有限公司", "91500101207912074U", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "da9eda22-7ae7-426d-838d-2c636673bdbf", "重庆诚业建筑工程有限公司", "91500107666403682M", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "dcadeb12-262a-479d-99cc-e69c966a9476", "重庆太白建设（集团）有限公司", "91500101711601021C", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "de27262c-87fe-4eb4-a208-92652788f361", "重庆新科建设工程有限公司", "915000002035362864", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "deec71fa-83e2-43f5-a3c6-d4b9e2bb291c", "福建省惠东建筑工程有限公司", "91350521156220149Q", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "e0b52b8f-3b45-4b75-95b5-95b83f46783d", "重庆灯塔建筑工程有限公司", "91500109203200088U", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "e0f22306-a3bb-428e-af23-24a4d0b9eb9c", "重庆华力建设有限公司", "915001032028344687", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "e1a4f19e-5ee7-40e4-a4ce-8d056ed3066d", "陕西建工集团有限公司", "91610000220521879Y", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "e1a86961-b724-4601-9ad3-63438ccc40b0", "山东天诚市政公路工程有限公司", "91370200164324095C", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "e20ddea9-4a07-47cb-a518-8b072f85055e", "重庆浩龙建设集团有限公司", "91500112750085744T", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "e2c69068-a0f5-4450-a218-ade951a6648a", "重庆光宇建设开发(集团）有限公司", "915001152033974580", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "e3f05820-a814-4b22-8f94-85610c362564", "重庆建工集团股份有限公司", "915000002028257485", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "e494ac81-da17-4b69-883c-0a045b7a0e12", "重庆中通建筑实业有限公司", "91500107203046316P", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "e86dee73-89cc-447f-b9fd-6cf7c6894248", "重庆荣达建设（集团）有限公司", "91500105203041718K", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "ea7a504c-567a-4b9d-9642-fd528df200f9", "中兴建设有限公司", "91321283737840047C", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "eb068097-3ed9-467e-be32-6edc4c4ceeaf", "重庆宏鑫建设集团有限公司", "915001177093882418", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "eb0c5338-9bc2-47f3-9314-049e9f44b981", "重庆中伦建筑工程有限公司", "91500112622194380R", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "eccf8b2f-ff56-494a-8c2b-793b07bbf881", "广东蕉岭建筑工程集团有限公司", "91441427196671139Q", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "eec997b3-f1c7-4f86-9580-d5ca761ae4a7", "重庆建工市政交通工程有限责任公司", "915000002028014715", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "ef7d3eef-4678-44f4-a334-ab78cdecd954", "中铁隧道集团一处有限公司", "915000006733598862", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "f0a036f2-f865-441c-ac3c-f937a3d1bf6d", "中铁二局集团有限公司（原名：中铁二局工程有限公司）", "91510100MA61RKR7X3", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "f142a7d9-c16b-4d12-8a3b-982daba602bc", "重庆拓达建设（集团）有限公司", "91500108768876288X", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "f2aa9c0a-ef66-4f44-8a1c-fe33c5afe979", "北京城建七建设工程有限公司", "91110000101639946H", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "f35201a3-6e0f-40cb-a9d7-0151897bc5af", "攀枝花公路桥梁工程有限公司", "91510400204355137E", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "f35eea60-6725-4042-b276-35898d3b55ea", "重庆欣耀建设工程有限公司", "915000007659339370", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "f4406b1f-ff93-43e1-bb4f-d28c324c6a44", "重庆两江建筑工程有限公司", "91500234621193706E", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "f44598df-a979-44ea-a8da-fc7b0c90e816", "北京纽约建设发展有限公司", "911100006635560265", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "f49214e9-4f49-46c6-9fe6-8ca9afe80d42", "中国建筑土木建设有限公司", "91110000100007314T", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "f61463a8-9b9f-4c0f-9244-e41e7cebb4ba", "重庆耀文建设（集团）有限公司", "915001122029003492", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "f79ca537-d3c7-4e71-b38d-e744ba127202", "杭州萧宏建设环境集团有限公司", "91330108253934178H", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "f8436269-db19-49d2-8b25-5558d812fd20", "中国航空技术国际工程有限公司", "911100001000010001", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "f8795836-29d5-4cd2-b278-adc4303c8744", "重庆大禹宁河建筑工程有限公司", "91500237092410946X", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "fb6a70ac-ac14-41c5-bb7c-48dd9ceaad8c", "重庆市南城建筑工程有限公司", "91500113203447988E", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "fb6f9270-1bb4-49a0-9d6f-3990004176fb", "四川飞腾建筑工程有限公司", "915117247208860494", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "fb90f33b-d636-4ac9-86f1-179071afa819", "广西建工集团第五建筑工程有限责任公司", "91450200198614605J", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "fbc556ce-b1a1-413e-a752-6e73d3a3540a", "远海建工（集团）有限公司", "9150000071160023XL", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "fc56bb08-4edd-47aa-bb84-0b1223d23144", "中国电建集团重庆工程有限公司（原名：重庆电力建设总公司）", "91500108202801156A", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "fedc4860-2b7b-41ed-9bb4-8c4777670ae8", "重庆华姿建设集团有限公司", "91500106203050024F", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "feffe3a1-42b2-48a3-aa8d-fb38a65acb26", "中国建筑第七工程局有限公司", "91410000169954619U", AccountType.企业帐号);
            AddAccounts(dbContext, accountList, "ff869034-27a6-48a9-84f9-d4be07d96435", "重庆中科建设（集团）有限公司", "91500102709427140L", AccountType.企业帐号);

        }

        /// <summary>
        /// 添加项目
        /// </summary>
        /// <param name="dbContext"></param>
        public void AddProject(DbContext dbContext)
        {
            #region 添加项目信息

            AddProjects(dbContext, accountList, "34f584e0-efc4-490e-a48b-8f612044695b", "0944fc61-9ff8-4772-882e-b5ff7547bc30", "华商·悦江府一期一标段（1#、2#、9#、10#、11#、12#、13#、14#、15#、16#、17#）及对应车库", "50003920181008527", "自贡华兴建筑安装工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "ecd96cf2-70ad-46aa-ad05-518e92b66ae3", "45b51864-af6d-4ddc-a0e7-f26ad43f3e30", "重庆涪陵万达广场", "50000320181018051", "中国建筑一局(集团)有限公司", "涪陵区", false);

            AddProjects(dbContext, accountList, "64a49dd3-5b32-444f-864c-58ca89c0d134", "311d1624-5058-4102-93e4-ff304b495702", "快速路三纵线红岩村嘉陵江大桥至五台山立交建设工程（二标段）", "50000020181017413", "中国建筑第六工程局有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "10492cbb-e496-454f-a1b3-6b0554243c49", "686e5093-b7f1-4caa-93e4-8685311d7dc2", "快速路三纵线红岩村嘉陵江大桥至五台山立交建设工程（一标段）", "50000020181017421", "中国建筑第八工程局有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "a93e5e37-10a9-414b-9dcc-ce8d05a719ef", "1b8402e5-41f0-4e99-9e05-b319401b3f0d", "快速路一横线歇马蔡家段工程歇马隧道东西干道工程PPP项目", "50000020181017435", "中冶建工集团有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "816aaa34-278a-4006-81fa-0cc15fd262db", "651e8666-5c60-4289-9731-dddf9411f8cd", "重庆市档案馆新馆项目主体及安装工程", "50000020181017461", "重庆建工第三建设有限责任公司", "重庆市", false);
            AddProjects(dbContext, accountList, "60d827bc-485f-4e39-93d2-7c8c4c7a644c", "0526e88b-d8c1-49db-af25-fc5fed7b4728", "重庆第二师范学院综合实验实训大楼建设工程", "50000020181017489", "重庆建工第二建设有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "3c67099a-c557-4180-bf0b-c76aee3f5cd6", "3db377b8-6d14-4e75-ba25-ee1fbb7eca60", "卫生学校青杠迁建项目图书馆、办公、教学综合楼", "50000020181017493", "重庆中航建设（集团）有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "8386201b-bc4e-4eb9-8f78-070e50ebffad", "298b8019-1006-41d3-990f-e417b14e1371", "甘悦大道渝北段工程", "50000020181017502", "攀枝花公路桥梁工程有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "1fa12252-1766-49e9-a9b6-25afa08ab409", "21d9775a-7381-421a-b366-9c0211381ef9", "新溉路二期工程（一标段）", "50000020181017508", "重庆建工市政交通工程有限责任公司", "重庆市", false);
            AddProjects(dbContext, accountList, "9b2a5e9a-f644-42d7-a35f-c21d8de08d40", "fd42fea6-9150-4c1b-af45-0bd8c0ab63dc", "新溉路二期工程（二标段）", "50000020181017509", "重庆市基础工程有限公司", "重庆市", false);
            //AddProjects(dbContext,accountList,"9f3ec80d-12a0-4f9c-beb4-dbc7f8f827ab","9de5b6bf-beb9-4097-8a13-eaf6be4a0911","龙湖沙坪坝枢纽项目一期工程","50000020181017561","中国建筑第八工程局有限公司、重庆诚业建筑工程有限公司","重庆市", true);
            AddProjects(dbContext, accountList, "9f3ec80d-12a0-4f9c-beb4-dbc7f8f827ab", "9de5b6bf-beb9-4097-8a13-eaf6be4a0911", "龙湖沙坪坝枢纽项目一期工程", "50000020181017561", "中国建筑第八工程局有限公司", "重庆市", true);
            AddProjects(dbContext, accountList, "e3ef462b-a0b6-42f5-84b6-74d254a8e1d1", "34064dfd-599a-4ca6-a5fe-65d7cec29f4a", "重庆轨道交通六号线支线二期一标", "50000020181019001", "中铁十七局集团有限公司", "重庆市", true);
            AddProjects(dbContext, accountList, "85b90006-c259-4d97-b98b-88c2930f3db7", "a0fea4f8-850c-4436-b4d4-7ea4e34ee01f", "重庆市曾家岩嘉陵江大桥桥梁工程", "50000020181019002", "中交二航局第二工程有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "70595e3e-73ba-49ca-8e16-bdf5c985f001", "7f9dae8f-3041-47ed-8e0f-66b647b31b94", "重庆市曾家岩嘉陵江大桥隧道工程", "50000020181019003", "中铁隧道集团一处有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "09ef2d26-2144-4933-8cfa-3486a7c406ba", "23fe7425-1845-41a4-aec9-ba62f44eeff8", "重庆市洛碛垃圾焚烧发电厂项目", "50000020181019004", "重庆三峰卡万塔环境产业有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "18c88810-62c0-4d24-80b3-834a320bbecd", "acda818c-08fb-43cd-af1b-c04e36fb2102", "重庆市轨道交通五号线一期工程土建5107标", "50000020181019005", "北京中铁隧建筑有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "a338f921-4e91-44aa-a891-769b9d3616eb", "c1855dbb-10b0-4f12-b5b6-d656f5cb2cfa", "重庆市轨道交通十号线二期南纪门长江轨道专用桥", "50000020181019006", "中铁大桥局集团第八工程有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "114195df-162b-4a6d-9b5e-b3c2e3dc1d88", "2322c2ea-f1e6-4455-827d-179e33a1a51b", "重庆轨道交通九号线一期工程土建一标", "50000020181019007", "中国建筑第五工程局有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "dbe9a19b-aae7-457a-8fee-cc4e44740e81", "1b5e11c1-b1e9-4188-a7ad-735e265ae671", "重庆轨道交通九号线一期工程土建三标", "50000020181019008", "中建隧道建设有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "5eccdb89-ab95-4aca-a149-c56d6589c537", "01fc275f-297a-4f21-8f37-3a9cfaf9f5f0", "重庆轨道交通九号线一期工程土建二标", "50000020181019009", "中建隧道建设有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "2849dc73-a9ba-494b-a9ef-d44123a45b16", "a918ac99-c9ad-46b3-9127-a8311dc893ae", "重庆轨道交通九号线一期工程土建五标", "50000020181019010", "中建三局集团有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "e2d9444d-fb24-4d35-a772-d0ede083f767", "ee65bd86-8912-42a6-ac50-f14ef020e6c8", "重庆轨道交通九号线一期工程土建四标", "50000020181019011", "中建隧道建设有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "ae6724a1-8090-4cb0-9800-e21d063bcd4b", "ae3d8a65-3ca2-4bc7-9720-5dc3fb73d266", "鸡冠石污水处理厂提标改造工程（场平、边坡治理、排洪沟改造设计及施工工程和土建及安装）", "50000020181019013", "重庆建工集团股份有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "ae952c0a-faa4-497f-8ae7-f66b85a85911", "f610150a-541c-4e94-ba7f-a96272fdb5eb", "重庆轨道交通环线马家岩停车场土建及安装工程", "50000020181019014", "重庆建工集团股份有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "1eb50a10-a0b3-4441-a002-0bdf1d7f0f07", "7f646cb3-26d5-454d-8a33-07ebfa366f02", "重庆轨道交通六号线支线二期草家湾车辆段工程", "50000020181019015", "重庆单轨交通工程有限责任公司", "重庆市", false);
            AddProjects(dbContext, accountList, "b5f4aa9b-67b3-4aa9-9947-6100dd2bfeef", "10de0e99-332a-4b13-82b9-cabba1b5d5c0", "重庆市市郊铁路（轨道延长线）尖顶坡至璧山段工程", "50000020181019016", "中铁二十四局集团有限公司", "重庆市", false);
            AddProjects(dbContext, accountList, "fd0dbc15-d93e-420a-a48f-b5ff5e62a33f", "29acfeb4-f5a7-48a5-8fb1-ea2b9ac5da4c", "博翠江岸小区1#、6#至14#楼、门岗", "50000120181015005", "南部县锦兴建筑安装工程有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "01de38ac-c9a0-44e6-884b-cbba024e9717", "e7f8fe00-6d4d-41f9-888a-7050123257bb", "金科观澜(二期)高层G-9、G-16至G18号楼；洋房D-38号楼；商业S-5A、5B号楼；岗亭B-3及对应车库及附属工程", "50000120181015025", "重庆建工住宅建设有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "1c9c8ac0-adab-4124-9503-7bee32a4fae8", "8cfcd34a-19a0-43ed-96af-a6168a49878c", "南滨上院B区二标段", "50000120181015040", "重庆市渝海建设（集团）有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "e288e363-7a22-43ba-a909-00dcaf3bc827", "0f9c4026-00a2-4aac-a264-550ab6d6431c", "南滨上院B区三标段", "50000120181015041", "重庆万港工程建设有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "f125dc5a-6720-4b76-9243-8f3a86b04a0a", "852dd5f2-bcd9-4b88-acd8-ea4c7d211b76", "南滨上院B区一标段", "50000120181015044", "重庆市万州建筑工程总公司", "万州区", false);
            AddProjects(dbContext, accountList, "5bec9d57-4125-4511-8742-f0f501154006", "691b9c36-c59b-47e6-868e-b503204ee536", "南滨上院C区一标段", "50000120181015044", "重庆金晟源建设工程有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "b43463a8-d311-4a16-bf31-188d187dcd40", "7ca31cb9-f550-4659-9838-7b0a2d648df7", "书香华庭(2#、3#楼)", "50000120181015071", "重庆太白建设（集团）有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "9ea980e3-ae67-47d4-a237-62aa60624f9d", "7056e519-4d22-4b06-b6f2-9e20a2915128", "万萃城一期（A组团1号楼-17号楼）", "50000120181015093", "重庆欣耀建设工程有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "85b0eb37-5f93-41a1-b20d-c660541d1877", "ed70f89c-a835-4f29-9c89-7b10b1a553d9", "天生天城二期（B1、B2、B8、B9号楼）", "50000120181015094", "重庆市万州建筑工程总公司", "万州区", false);
            AddProjects(dbContext, accountList, "61895b77-d7e7-48cc-8117-9ef37ae067c4", "f2629a82-25f0-493a-af7e-3091b01a7085", "万萃城一期B组团项目1、5、6号楼基础及对应地下车库基础工程", "50000120181015095", "上海绿地建设（集团）有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "95534500-6e83-42b9-9b1a-07a9f7626320", "dfbdc9d1-a311-4ce7-9021-b57aa68eba6f", "万山国际三期3#车库B区", "50000120181015103", "重庆万州兴涛建筑工程有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "70b44e26-1db6-4dd6-9ca0-676d0b0868ba", "84d1c49d-3365-42af-824f-fab4579dce45", "万山国际三期33#楼、34#楼", "50000120181015105", "重庆市万州水电建筑工程有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "161a1a14-86aa-4d52-a5de-36f5e7558d89", "49606172-7393-4ddd-96d2-9288fd3cf61a", "万州二中江南校区（第二次）", "50000120181015115", "重庆金晟源建设工程有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "a87304af-5723-4d00-a073-61782050c297", "bc2646c5-9fcc-48c1-bd0a-b843a837bf7a", "万州经开区高峰园创新孵化基地建设工程", "50000120181015137", "重庆教育建设(集团)有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "d54623a6-68ea-4420-a266-37c66a9b21a2", "6e8a76bb-9095-40b8-91fd-1e4ab5c78a30", "万州区旧城功能恢复连接道建设工程-高梁收费站连接道三标段", "50000120181015177", "重庆建工第一市政工程有限责任公司", "万州区", false);
            AddProjects(dbContext, accountList, "31e3de52-257a-45a3-b8f4-54ff29e99db1", "f76d6bc2-6ff1-4538-a35d-89e9ac1009fa", "万州区长岭新城区主干道工程——香山大道部分市政工程（二期）", "50000120181015224", "重庆市渝海建设（集团）有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "4c48160f-be49-48b4-b0f1-07816deee5d5", "669c0e4a-6f57-4df0-86ca-b8aeaa4d9dda", "御澜府一期13、15、16号楼及中庭车库", "50000120181015263", "重庆市万州水电建筑工程有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "d0e12512-a53c-4ee0-a5b5-c6f529c9b2e0", "c459688e-4fa9-47a4-a607-a45b7c565e00", "重报·万州中心二期工程（5#、7#～13#楼及相应车库工程）", "50000120181015272", "重庆市万州建筑工程总公司", "万州区", false);
            AddProjects(dbContext, accountList, "2366eaa7-c97b-41cb-b05b-94d057de4e48", "a5f84389-b3c5-45bd-b247-fa6849de1832", "重庆市万州三峡文化艺术中心一期大剧院", "50000120181015298", "浙江精工钢结构集团有限公司", "万州区", false);
            AddProjects(dbContext, accountList, "2e14a73f-6e06-4b03-831f-ebfbf4ed0c0d", "7bcddcf2-f0b0-4d75-9b04-ae79f465e394", "万丽·城市客厅旅游度假综合体一号地块1期B组团10#、13#、15#楼及地下车库项目", "50000220181015021", "重庆坤飞建设（集团）有限公司", "黔江区", false);
            AddProjects(dbContext, accountList, "bafdfa15-61aa-4edd-aadf-53d98f6ffac1", "8ca9a838-f01f-4b98-b45f-c7c7140cf261", "中科·中央公园城二组团", "50000220181015022", "重庆中科建设(集团)有限公司", "黔江区", false);
            AddProjects(dbContext, accountList, "be3a57a8-4691-420f-bfdf-d157eee9e3dc", "ebf3a89b-2671-4e95-b930-70a10a414cca", "黔江碧桂园三期三标段工程（102#、103#、105#、106#及车库）", "50000220181015025", "重庆渝发建设有限公司", "黔江区", false);
            AddProjects(dbContext, accountList, "4a6078b0-47bf-4e73-b43b-2eceb8f023ab", "05a43296-2420-45bc-97e8-7fc2f944137b", "金科.集美郡项目1-25号楼及26号车库工程", "50000320181018020", "重庆建工第四建设有限责任公司", "涪陵区", false);
            AddProjects(dbContext, accountList, "e905dd87-3ff2-48a1-a5be-6cd1870a7c70", "0740f53a-6914-4c79-a191-849d08e7f533", "涪陵新区中央商务区公寓楼工程（5#楼）和涪陵区新城区商务广场（生化池）打包项目", "50000320181018200", "重庆中科建设(集团)有限公司", "涪陵区", false);
            AddProjects(dbContext, accountList, "2f46aa7e-80d0-46f2-8a46-7fa06abc9647", "821a342d-f054-4bcd-bb3a-b10c3047e21a", "金科·中央公园城二期31-46号楼、86号门岗、对应83号楼B区车库及边坡附属工程", "50000320181018201", "中国建筑土木建设有限公司", "涪陵区", false);
            AddProjects(dbContext, accountList, "98bb5b0f-369b-4f36-9f6f-f1aa9c492d26", "c59a8463-2381-459c-b363-9b773cacfc30", "贵博·瀚林学府（1-1号楼、1-2号楼、2号楼至7号楼、8-1号楼、8-2号楼、8-3号楼、1号车库至4号车库）", "50000320181018207", "重庆展宇建筑工程有限公司", "涪陵区", false);
            AddProjects(dbContext, accountList, "eac69645-a854-444d-9f41-9379af14c941", "e48fa118-2690-4ef0-a160-2d1c68a8eab7", "重庆复地金融中心", "50000420181008001", "中国建筑一局（集团）有限公司", "渝中区", false);
            AddProjects(dbContext, accountList, "783c9c04-b29a-4f5b-823c-d1c0952edb2e", "ff6c3bd4-dee5-413e-8034-deccd8375c30", "重庆罗宾森广场项目T4#楼承包工程（含塔楼下的商业及车库）", "50000420181008004", "中兴建设有限公司", "渝中区", false);
            AddProjects(dbContext, accountList, "81460faf-e951-4d00-a4f8-587f1b3046e7", "cbe9cd32-3158-4c91-8c09-6e2427d8388b", "重庆中心一期", "50000420181008005", "中冶天工集团有限公司", "渝中区", false);
            AddProjects(dbContext, accountList, "d6a648ed-4723-471a-9dd2-f6c2d5720ac8", "83d5ab4d-66a4-4fd1-b3d2-61b8dad06edb", "财信·渝中城三期（1栋及地下车库）", "50000420181010014", "重庆建工住宅建设有限公司", "渝中区", false);
            AddProjects(dbContext, accountList, "e76cf003-3761-4484-b35f-2cc817f39421", "f4f4e018-05fe-47a8-9a69-8d1988a8f801", "和泓·江山国际三期8、9号楼、S1商业及地下车库", "50000420181011016", "重庆海博建设有限公司", "渝中区", false);
            AddProjects(dbContext, accountList, "69e53dbd-520c-41d1-97ca-40bb88f1565c", "8bfb71ef-9c19-4c99-ab02-ee17331ef934", "重庆来福士广场T2塔楼", "50000420181011017", "中国建筑第八工程局有限公司", "渝中区", true);
            AddProjects(dbContext, accountList, "efbf4ca9-df06-4767-a80b-ca14e2c43cc0", "b2071ddc-897f-4e57-9aaf-9780c3239fab", "万科.黄花园项目", "50000420181011019", "重庆渝发建设有限公司", "渝中区", false);
            AddProjects(dbContext, accountList, "baa0fc0b-b8f8-43db-a55a-eafe03891c15", "6c20b5c4-4764-4e60-891b-9e9008051ddd", "重庆塔项目", "50000420181011021", "中铁建工集团有限公司", "渝中区", false);
            AddProjects(dbContext, accountList, "07aa26e4-71f3-4ee4-aaff-519e954b9a8b", "99c016b7-31aa-4168-9676-413f74c5a121", "重庆来福士广场项目观景天桥", "50000420181016037", "中建三局集团有限公司", "渝中区", true);
            AddProjects(dbContext, accountList, "9ff93104-ff5e-49f6-94f5-5464c1b1888a", "1e24b788-bd06-444b-bf1a-f471ea97b065", "重庆融创·凯旋路项目南区三期5#地块（4#楼及裙楼）", "50000420181016038", "重庆万泰建设（集团）有限公司", "渝中区", false);
            AddProjects(dbContext, accountList, "7857fbbf-aa80-4f4b-9297-644c43ba4c3b", "2a6b658f-c4d6-4bf4-98f3-74538a1bb55e", "化龙桥片区B5/3、B10/3地块", "50000420181017045", "重庆万泰建设（集团）有限公司", "渝中区", false);
            AddProjects(dbContext, accountList, "f7702cc7-3597-49e7-8ec5-55f364ece979", "f3f70349-2e07-4957-9a16-37bdce8d47c5", "化龙桥片区B14-3地块项目", "50000420181017047", "重庆城鹏建筑工程有限公司", "渝中区", false);
            AddProjects(dbContext, accountList, "d30316e3-c3c4-4266-a688-2d60edb064f1", "b4234650-a3e0-4020-bb16-6fdb681487d4", "化龙桥B11超高层二、三期", "50000420181017048", "中国建筑股份有限公司", "渝中区", false);
            AddProjects(dbContext, accountList, "486b6804-9ba3-4a88-ba04-9c035244742e", "31fe831e-b946-4e45-baa2-eeda1c001bcc", "光控朝天门中心二期", "50000420181018060", "中建三局集团有限公司", "渝中区", false);
            AddProjects(dbContext, accountList, "617ce676-5241-48ad-87dd-4b7053b1dfce", "22dc083d-d6c3-4b99-9a27-a1383df53994", "雷家坡立交", "50000420181018073", "山东天诚市政公路工程有限公司", "渝中区", false);
            AddProjects(dbContext, accountList, "37dea3b3-50a9-4dfb-a0c8-ba64accadb3e", "f87c600a-7eec-46b4-bd68-9e5533fd4646", "解放碑地下环道三期", "50000420181019077", "中国建筑第八工程局有限公司", "渝中区", false);
            AddProjects(dbContext, accountList, "f10bb6c9-7968-4b49-a36e-4d087fed6084", "35d91d0e-e3b8-48bc-a5c0-f2b81010cf7c", "金科星辰三期（一批次）", "50000520181014008", "重庆建工住宅建设有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "70ed28c4-32bd-400a-80ca-30c422ff03d9", "1d6ac622-5c3b-43db-8010-872e03fcaa6f", "重庆大渡口万达广场", "50000520181015016", "中国建筑一局(集团)有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "67b15872-6bd4-4e39-9e44-0fcb3cf029c3", "b393dfe0-c625-41c7-b7e5-6bfb6b03adca", "新郭伏路南段（新郭伏路K6+280-K8+874.875段）", "50000520181015031", "重庆城建控股（集团）有限责任公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "0eda22a2-35f5-446c-940f-6296521c6962", "d8ba41dd-1359-47b3-9977-7a1755e5ca2b", "朵力•迎宾大道F6-2地块二标段（B5#、B6#、B7#、B8#、B9#、B10#及部分地下室）、幼儿园（三标段）工程施工", "50000520181015039", "重庆钢铁集团建设工程有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "6cdace62-bb6c-4922-9414-69a4719ab214", "7ac4b355-10df-4f01-8776-5fd333ec12a7", "金科博翠长江项目", "50000520181015040", "中建三局第二建设工程有限责任公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "bad9bc43-17ed-4513-b6b4-1a0a638967da", "3d24152f-ca26-43f1-ba8b-05a0e878f31b", "中洲重庆大渡口P2-14号地块项目一标段总承包工程", "50000520181015041", "中国建筑第二工程局有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "43823653-4b02-46be-b111-63ac631786ee", "6a51809c-6702-44f8-b504-ccdd24d02f30", "中洲重庆大渡口P2-14号地块项目二标段总承包工程", "50000520181015042", "中建二局第一建筑工程有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "35b3a180-3bef-4098-a322-c6d3d9af6c31", "8e683f2f-e36b-441a-82e6-d91495604cd0", "金科•集美阳光M37-1地块（一期、二期）", "50000520181015048", "中建三局集团有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "3418bc8a-03c3-42e6-8554-9888325b6520", "2e411926-f1fc-4600-a506-c608ad334cd0", "佳兆业滨江新城（H15-6/04号地块）（3、4、5、6#楼及部分车库）项目", "50000520181015049", "中天建设集团有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "0339a3df-e8b4-4630-acff-9b39f2aaa6df", "6ba91e8e-d9d1-485c-9083-eb3adbbeae06", "金地大渡口项目一期（中梁山组团L22地块)(1、2、3、7、8、9、14、15号楼、门卫房及1号地下车库）", "50000520181015050", "中天建设集团有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "dabd660f-f163-41bf-9d97-ef5adf5075d7", "fd48c0d2-9cc3-446b-9294-ce1e85df4d03", "恒大麓山湖I57-1地块（暂定名）", "50000520181015051", "重庆嘉逊建筑营造工程有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "77363848-d0ca-48cb-925b-aeb12e348a3c", "b11d07e5-982a-4b88-bef4-3a166eca56aa", "荣盛城-观麟郡I11-2地块（7#-12#楼、D13地下车库）、I15-1地块（1#-6#楼、S3#楼、S5#楼、S6#楼、D7地下车库）", "50000520181015052", "中冶建工集团有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "dcfedca7-0b94-4d21-b2c7-55888592fe89", "ffccd226-880e-482c-afd4-18eabbf0adf0", "金科星辰三期（二批次）", "50000520181015053", "重庆建工住宅建设有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "b3f43c5c-5f08-4d31-958c-5f1e4dbf1353", "4750fae1-495b-45e7-a836-a0a1a7452587", "联发西城首府大渡口组团N23-3-1、N23-3-2地块（1#-20#楼、26#楼、39#楼、地下车库）", "50000520181015058", "上海宝冶集团有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "975f1954-9ed7-45a6-a5f9-8940819440c5", "a70813ab-c297-4ba7-96c8-e642957c89ab", "林语春风M01-4、M02-2地块（暂定名）", "50000520181015060", "重庆鹏帆建设集团有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "99cf5d27-7afc-42e6-a59f-2627f2ca9984", "84c0d410-7d33-4e2a-a53d-4e2b78ef59cb", "金地大渡口项目一期（中梁山组团L22地块)(4、5、6、10、11、12、13号楼、幼儿园、门卫房及2号地下车库）", "50000520181015063", "浙江欣捷建设有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "e26a4b3f-4ca7-48ed-b1cb-837ae78f3558", "7444554c-bea6-475e-8627-5176ef1be5e7", "华润万象汇二期（1#、2#、3#、2-T1#、2-T2#及地下车库）（调整）工程", "50000520181015064", "中亿丰建设集团股份有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "5d7d8c2c-8066-4aa2-83d2-22a4dd251811", "5cbd2c89-0def-4789-892e-bbf34a9e6d80", "金地大渡口项目一期（大渡口组团N32-1地块）", "50000520181015065", "重庆万泰建设（集团）有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "06f54a8f-3948-494a-be1a-35b548b36db5", "c0a7087c-4c52-440d-a476-a6d679af5cb3", "朵力•迎宾大道F6-2地块四标段（B1#、B2#、B3#及部分地下室）工程施工（第二次）", "50000520181015066", "重庆钢铁集团建设工程有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "d871569b-8318-4b2a-b39a-13dc7862f586", "bff4084b-2550-43a9-a46a-43085677094e", "融科滕王阁3#（H15-14-1/04）地块（暂定名）（一期）（1.2.3号楼）", "50000520181015067", "重庆第六建设有限责任公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "363f098b-7344-4f7d-a3d2-be8e31b7c9b4", "e4518b82-1103-4761-8a68-dec2af262cbc", "融科滕王阁3#（H15-14-1/04）地块（暂定名）（一期）（5号楼）", "50000520181015068", "重庆光宇建设开发(集团）有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "ef920e8e-8e1e-48ba-93e2-5252c4126eab", "388c7e5d-8234-40c5-8dfd-be99e5f0dd68", "佳兆业滨江新城（H15-7-1/04号地块A区）（6-17#楼及部分车库）项目", "50000520181016080", "浙江新东方建设集团有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "b8c485c8-853d-45e7-aedc-acaca07ca480", "6b296a48-1e87-4b61-bf01-f7a584e5baf7", "恒大麓山湖I58-1地块", "50000520181016088", "中建五局第三建设有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "d19c2d8b-86ba-4dbb-bfcc-4e77ed193825", "4e7c2e33-e7b4-4547-ac28-8743dcd2e133", "天安江畔珑园二期", "50000520181016090", "中国建筑一局(集团)有限公司", "大渡口区", false);
            AddProjects(dbContext, accountList, "8110c638-323a-4af6-8372-a111b810bf4f", "2ec489ed-b0b8-406c-8afc-4bcf1b661085", "中冶铁山坪F-2/02地块项目施工总承包工程", "50000620181017001", "上海宝冶集团有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "0a8f5a1c-d3d3-467b-9b36-c1fce6f5dbfb", "31b0b0cf-02e7-4b28-a441-3352a0d344d2", "华融现代城项目一期施工招标", "50000620181017002", "福建省惠东建筑工程有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "a084d623-5f71-465e-af6f-c76a03d556c0", "48ac32f7-e09b-4432-ac3f-6cdc778200c2", "华融现代城项目二期（施工招标）", "50000620181017003", "重庆松龙建筑(集团)有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "9b2a7677-4d3a-49f3-b4b1-77ca4e327ef9", "6f8a3e5d-6e92-4c7c-9bc3-c3beab696c24", "御龙天峰项目（原信和北滨路项目）三A期12-T16号楼、S3、S4商业及D02#车库", "50000620181017004", "重庆渝发建设有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "d1e956c1-f849-4c8e-b253-6f94ec91b0c8", "80f830bd-b51f-4469-a394-9a5f829ddbf1", "御龙天峰项目（原信和北滨路项目）四期H01、H02号楼、S5商业及D03#车库工程", "50000620181017005", "中建五局第三建设有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "c1166c1d-03a6-4986-b1db-1671d443a868", "279ed3ce-ad70-4dd5-9e0c-61609d7757cd", "旭辉江北区E14项目", "50000620181017006", "重庆佳宇建设（集团）有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "7cabc77d-a191-4e13-b55f-b869c46d0e17", "9416130e-1c18-4c01-b7cc-c3f5fd4746e3", "洺悦府", "50000620181017007", "中电建建筑集团有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "3e5d10b2-0d76-4272-bb93-699561c1e844", "0ae00c65-cf72-4637-9c7e-578726b60629", "港城小时代", "50000620181017008", "业兴实业集团有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "80d72d59-b991-4cbc-81e9-34fc0643e60c", "1049c621-efc6-45f8-95af-5410fb3c12c2", "漫山居", "50000620181017009", "重庆亿瑞建设工程有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "1437014e-aa4a-4be8-a32a-1bd0e242ce51", "ac29a482-0e36-447f-8241-4eb7c30f45ed", "绿地.海外滩二期2、3标段工程（重新招标）", "50000620181017010", "重庆市渝泽建筑工程有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "f585dd03-1bb5-49eb-a889-d5201031e71a", "d39d1bfd-aac2-46b9-8431-d8ec09e8a37b", "绿地.海外滩四期三组团E区（9、10、13号楼、14-2号楼及地下车库）工程", "50000620181017011", "重庆市渝北区海华建筑工程有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "5138a8a0-0184-4c8c-b558-309e9665426d", "75ccd386-a29c-4ffd-ba2b-157ee6bb575b", "观音桥F地块", "50000620181017012", "重庆城鹏建筑工程有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "2fa5d859-b5ff-4c7e-9e3a-b49840991125", "5be37c8e-553e-4441-8cf8-37313c561a6a", "重庆市蜀都中学校改扩建工程（主体及部分配套施工）", "50000620181017013", "重庆建工第八建设有限责任公司", "江北区", false);
            AddProjects(dbContext, accountList, "a48886be-1e0a-4792-961e-65417ee89bd7", "a8d45a42-8f14-45b6-9d98-c0c98a09a930", "重庆科技金融中心L01-4-02地块3#楼、6#楼及地下车库建设工程", "50000620181017014", "中欧国际建工集团有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "c223db2f-437f-4e1c-b39b-bd5caa39ab1f", "1bb63879-7116-47eb-81d0-e2f9bc5b4b36", "重庆航天职业技术学院教学综合大楼", "50000620181017015", "四川航天建筑工程有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "615e78d7-2c64-47dd-813e-68911f7cd79f", "23fda5bb-a172-4972-b517-5e28b5acec05", "金融街.融景城B1（021-3/02）地块项目三期工程", "50000620181017016", "重庆浩龙建设集团有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "e9cb28c5-42a6-4f55-b522-96f2c5dfb63a", "812f80ec-cd8b-4737-a5bf-bd51c69373b7", "龙湖·春森彼岸五期T2-1、T3-3、5#车库", "50000620181017017", "成都建工第八建筑工程有限公司(原名：成都市第八建筑工程公司)", "江北区", false);
            AddProjects(dbContext, accountList, "98934939-ece1-40b0-90a2-7b7544935f9f", "35d80c43-6cb6-4d6a-a7a0-c9ac9ad3d24e", "龙湖观音桥项目二期一组团工程（6#、S4#及地下车库）", "50000620181017018", "重庆建工第三建设有限责任公司", "江北区", false);
            AddProjects(dbContext, accountList, "f3144250-b33a-4c01-a638-15be8260ad60", "6d96752f-3282-4d45-9b87-16ddad60fa80", "武江路立交", "50000620181017019", "重庆建工集团股份有限公司", "江北区", false);
            AddProjects(dbContext, accountList, "4edf020c-cedd-4352-ab21-4fe66e4d2937", "80e9d489-4731-424f-99f4-0abae4e782e3", "港城西路延伸段工程", "50000620181017020", "重庆建工市政交通工程有限责任公司", "江北区", false);
            AddProjects(dbContext, accountList, "d3c06a8b-d0b8-46d6-823e-aa266a4eaf6c", "fdb2ff26-d9e4-4d57-9d90-c33d979eacae", "金阳.大学城第一农场一期五组团（89#、91#）", "50000720181001010", "四川飞腾建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "c5b1dd15-8b15-4e3a-8266-50c8e7d77713", "f8b2ceff-f309-43a0-b833-8dcf5d24e5c0", "重庆万达城酒店群L74-1/01地块一期项目", "50000720181003118", "中国建筑一局（集团）有限公司", "沙坪坝区", true);
            AddProjects(dbContext, accountList, "bee3230e-347c-4bde-9fc6-86b7f88f8564", "a0133bd5-f12a-4466-9e02-7bdec915d40b", "重庆万达文化旅游城A-08（L77-1/01）地块项目", "50000720181004030", "中国建筑第八工程局有限公司", "沙坪坝区", true);
            AddProjects(dbContext, accountList, "dadc9ad0-540b-4d04-8a95-bee09f5eb49c", "ff298419-902e-4e92-a56f-9c85460dc0f6", "万科沙坪坝区沙坪坝组团B分区B12/02地块项目1#、4#、5#主体及1#、4#、5#装饰工程", "50000720181004063", "重庆万泰建设（集团）有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "6df7c375-ae06-4b62-8e38-84f432a3b13d", "cb7b4211-30b4-4630-9194-0cf22c453590", "万科沙坪坝区沙坪坝组团B分区B12/02地块项目2#、3#、6#主体工程、车库基础及2#、3#、6#装饰工程", "50000720181004064", "四川时宇建设工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "4af6f280-a621-4da2-a63f-2932272399a9", "c4b5bfb1-e245-4ee2-9abc-2dc5567d0f10", "金科西永天宸L48-2/03地块3-1、3-2、3-5~3-9#楼及对应商业及对应3-15#车库、岗亭3-13、3-14#楼", "50000720181004064", "重庆建工住宅建设有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "e8affaae-71fa-4ad0-a4f1-ac6ed3c59b19", "6070dca4-242c-4703-83c2-3264468a6e00", "金融街.融府项目双碑组团E分区E52-1-1/04地块一期", "50000720181004067", "上海建工一建集团有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "8cb4cf02-2739-411a-9ab6-7fd384d8505e", "e4a77809-0096-4424-998a-dcc773e9cddc", "恒大未来城1#地块（1-9#楼、地下车库、门岗、含室内新装修）", "50000720181007109", "中兴建设有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "9c9f8b15-9ac1-493d-83bc-c8bf889a5784", "29ca82d0-80de-4165-ac9c-9316d80c946a", "西永组团W13-1地块（龙湖科技学院项目1号地块5组团）3#楼～8#楼、15#楼～18#楼、28#楼-1、29#楼-4、29#楼-1及地下车库", "50000720181007110", "重庆先锋建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "33c7aeb1-28b1-4ed6-b2b4-bbc11f1962ec", "c89a4cd0-ee36-4e7a-8db2-d63d3dcde169", "西永组团W14-1地块（龙湖科技学院项目2号地块2组团）1～2号楼、12～22号楼、34～41号楼、门卫、部分设备用房及车库", "50000720181009012", "重庆先锋建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "513a5e87-9cb1-4792-933a-1400578f00fc", "768b969e-5079-427e-b7db-9b83027b9040", "金科西永一期L29-1/03、L29-2/03地块", "50000720181009014", "重庆海博建设有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "5100ed5e-f370-4ed2-bcc5-508d3533bd89", "f43a4103-7b8b-4291-a848-3d161c7bfe97", "重庆科技学院产教融合实验实训实习基地工程", "50000720181009016", "中冶建工集团有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "12998445-fa3a-4141-930f-f8f6c40e8de0", "c91ba18c-7c1a-4da8-b61f-fa00cf29e10d", "富力城二期2R组团二期", "50000720181009019", "广州天力建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "03366576-9548-4676-b7ae-963267520433", "26827574-3362-4ce9-aa43-530b4cc69d2c", "西永组团L22-1/05地块(龙湖西永核心区项目)1#楼、15#～25#楼、S1#、S20#～S22#楼及地下车库", "50000720181009039", "重庆诚业建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "201e9324-b0ac-4f54-8740-0365e61b74c9", "4d62ceef-8d94-41ae-bc5c-a88685d66a13", "融汇泉景D区7号楼及商业和地下车库（部分）", "50000720181009041", "重庆市佳诺建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "5ba93d57-0b7d-4fa5-916f-98d5b68dd0aa", "0397dbd9-2c8b-4678-a75d-b9729a162411", "渝开发·格莱美城一组团总承包", "50000720181009129", "重庆建工第三建设有限责任公司", "沙坪坝区", true);
            AddProjects(dbContext, accountList, "cfb8aaff-4af3-4234-9da5-fb3c9f8415fa", "f98bf0f5-862d-4bf1-837d-d74c11aa583c", "西永组团L21-1-1/05地块（龙湖西永核心区项目）1#～12#楼、13#楼（幼儿园）、S1#～S3#楼、S9#楼及地下车库", "5000072018100938", "重庆先锋建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "c32ff975-880f-4b02-9a85-5bbcef1093cb", "5ae0c3f5-4a19-4772-9c88-09f82ec90efd", "美的金科郡（30#、31#、39#-41#、51#-52#楼及地下车库）", "50000720181010125", "国基建设集团有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "8f0a669e-e528-4689-88a0-8ee3a2b7bb84", "f302d729-470f-4919-963a-ce387563e8cd", "金阳.大学城第一农场一期四组团（39#-69#）、一期五组团（88#、90#）", "50000720181010127", "四川飞腾建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "89e1ece0-0296-4982-bc75-8f0470cc6b01", "fe3ad91b-c61e-461a-8233-d158c3e838e3", "美的金科郡（1-12号楼及地下车库）", "50000720181011103", "中建二局第三建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "a6e133a3-dce2-4b37-b8a9-13a2ccf33aa5", "893f6733-7125-4d0a-8272-915f86e593c9", "融创朗裕特钢项目E38-7-1/04地块（二期）", "50000720181011104", "重庆市巴南建设（集团）有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "3731b8f9-06e8-4754-b925-4d093c02d0e6", "18706f3c-f534-412c-8d85-592a53d63f7b", "重庆万达文化旅游城万达茂（L68/05）地块项目", "50000720181011133", "中国建筑第八工程局有限公司", "沙坪坝区", true);
            AddProjects(dbContext, accountList, "e04eab38-c0ff-48f5-b34e-ae8350952178", "62aa7d17-ffdd-4445-9f2e-2b484b327ed7", "南方新城（一期）C11-1地块G、E组团.公园华府5号楼及其对应车库", "50000720181012023", "重庆拓达建设（集团）有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "c33325b8-91d6-4f98-9a31-feaf493611a9", "72e313de-ffc6-4ff3-9ed5-c86883d52cbe", "重庆铁路口岸公共物流仓储项目工程总承包（EPC）", "50000720181012024", "重庆建工集团股份有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "0381849d-51e1-4f86-bee0-389876942ead", "910f4212-3501-4aa1-a287-93f925191f85", "首创天阅嘉陵项目一组团（1-8号楼、20、21号楼、001#地下车库、门卫室）", "50000720181012028", "中国华西企业股份有限公司", "沙坪坝区", true);
            AddProjects(dbContext, accountList, "d1da99ec-6e31-4624-97b1-d440ccc0f5fb", "29c0b19e-82ff-4557-8e91-f4c1c040a297", "重庆万达文化旅游城B-01-1(L81-101)及B-01-2(L81-201)地块项目", "50000720181013007", "中国建筑第八工程局有限公司", "沙坪坝区", true);
            AddProjects(dbContext, accountList, "d3facf01-b671-499c-b10d-148037b06b15", "12a13c66-88a1-4938-9504-f18543dda413", "富力城二期2R组团（A1号楼、车库）", "50000720181013112", "广州天富建设工程监理有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "b3247156-0784-4386-8134-d9cad1c4256c", "96b549c4-0748-450b-9768-c1bb3797a892", "西永组团Ah01-Ah24标准分区道路工程", "50000720181014025", "中国十九冶集团有限公司", "沙坪坝区", true);
            AddProjects(dbContext, accountList, "d38edbea-59e2-419f-84ac-37f8d7b5da2c", "73d332d3-0e9c-41de-a2a1-201ec39707c0", "美的金科郡(42#-50# 门岗4、地下车库）", "50000720181015078", "中建二局第三建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "4d85c199-8e78-4dad-8f7b-f79aee39c02f", "461d07de-7295-45e7-a5fc-99b353bc92b4", "融汇泉景D区二期8、9号楼及部分商业和地下车库部分", "50000720181015106", "重庆市佳诺建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "82e2ad2b-1dc9-4cd3-b2cb-6d939c0f861b", "51d68c9a-580c-4684-bb5f-17883ec14cab", "西永组团W13-1地块（龙湖科技学院项目4组团1标段A区）1号楼、11号楼、13号楼、21-25号楼、28#-2号楼、28#-3-A号楼、29#-3号楼及地下车库", "50000720181016084", "重庆先锋建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "4cfdb094-2a38-4198-9f49-d87a7ed0812a", "5a04f151-cb9e-44ff-8883-65cb0c1c923d", "重庆沙坪坝西永L23号地块项目（L23-4/04、L23-5/04、L23-6/04号地块）", "50000720181016088", "重庆万泰建设（集团）有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "9901af0f-c059-47cb-9546-4a1090683065", "89ccb4e1-143c-4af3-80db-a33a4a97b187", "重庆沙坪坝西永L23号地块项目（L23-1/04、L23-2/04、L23-3/04号地块）", "50000720181017089", "重庆诚业建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "811249cc-9df4-4ad3-ad44-1c8d769c710e", "a00a20d1-0822-4dc2-aa6c-cc2861aadb4e", "西永组团L22-1/05地块(龙湖西永核心区项目)2#～14#楼、S2#、S4#～S7#楼及地下车库", "50000720181021050", "重庆先锋建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "83314153-c6e5-462f-bd45-e7a1b4782643", "4f7b9ebe-969e-4f1c-ac2a-33f99b6dd562", "富力城二期2F组团一期（A1-A6号楼、北区地下车库）", "50000720181021051", "广州天力建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "bacd4e85-0472-43ca-b400-9083efead83d", "83c12697-bed0-4cb9-a13b-60e39ea74d44", "重庆大学城中央公园暨城市功能项目（重庆.熙街)四期总承包工程", "50000720181021056", "重庆建工集团股份有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "90819ade-bbf9-442c-867f-0668286170ef", "d4815d40-e11c-4b37-a6fd-8b70a4f889fb", "龙湖西永微电园项目（L30-1地块）1#～3#楼、S1～S4#楼及地下车库", "50000720181021094", "重庆建工第三建设有限责任公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "dcabdfc6-aad2-403e-aeb1-5932aef3074e", "6e8029fc-adc8-46b0-a319-ee389d2f31cd", "富力城二期2R组团（A2-A7号楼、S2-S4号楼、车库）", "50000720181021097", "广州天力建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "12152758-143c-4f2c-a29c-6d7f1394f146", "d9d31570-e2e7-4354-8b1d-3135d95f2e9d", "金科西永二期L29-3/03、L29-4/03地块", "50000720181023044", "重庆建工住宅建设有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "694d6f97-6eba-4826-b43c-4c293cedd436", "2e53d735-18bc-42e4-9d32-34494e710065", "协信.天骄名城四期（二组团）", "50000720181029132", "中建八局第三建设有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "88f06d36-0de4-4d67-882f-9ed6fe00f9c8", "477dcfc9-16ec-4f1d-885d-4dbd0f9ea35e", "美的金科郡(13#~26#楼及地下车库、幼儿园)", "50000720181029137", "中建二局第三建筑工程有限公司", "沙坪坝区", false);
            AddProjects(dbContext, accountList, "b063cbf5-342d-4902-afd5-c8029f6ac180", "70b35142-9f59-4c7d-b789-e501eeffa720", "云飞九龙香山S-1商业、Z-3、Z-4、Z-5、Z-6、幼儿园、地下车库工程、车库屋面及边坡支护", "50000820181015009", "重庆庆华建设工程有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "355545cc-1948-495a-94bd-6a270bb5556e", "cced717c-f9b8-49a9-b130-3c0600195ec9", "重庆中迪广场一期6#楼标段、8#楼塔楼标段工程", "50000820181015010", "中国建筑一局(集团)有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "2fdd2268-5f38-496f-9924-85236d3a53bc", "a39cec9d-e7f5-433f-8b7f-059af97765bb", "卓越中寰（1#、2#、3#楼、地下室及边坡支护）", "50000820181015013", "中建三局第一建设工程有限责任公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "d52035d1-82a1-4ffb-b49f-9cf61c513537", "51205aa7-61f6-40ce-a379-134207364906", "中建龙玺台", "50000820181015021", "中建五局第三建设有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "8ad69650-e2d2-4faa-924e-18f629b04fd7", "b329a542-ccdf-4595-bd6d-d341d1add300", "北大资源燕南五地块（大渡口C12-1/06、C14-1/04）（包括5-1#楼A栋、5-1#B栋、5-2#楼、5-3#楼、5-1#商业裙房、岗亭及地下车库）", "50000820181015027", "重庆万泰建设（集团）有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "49c9ee54-981a-429b-9d09-b2af0ca1a6fb", "91e8e2a8-0a9e-4385-af31-e4f2fcd341a7", "北京城建华岩住宅小区二期19-38#楼、1号地下车库B区及临街商业、物管用房及边坡支护", "50000820181015029", "北京城建七建设工程有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "65cd646f-68d0-436a-b4e3-15f272828058", "a1b3d59b-b5d5-453a-8ba6-45669ee06fe0", "云鼎栖山1号院（1-11#楼及车库）", "50000820181015035", "重庆宏宇建设工程(集团)有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "051d8bbf-71d6-4139-b2b9-bb3273dfbed3", "6d75c45c-1282-4c36-a6a1-b7ca6dcef7da", "昊华小区一期（1-1#~1-5#、1-S1#、1-S2#、独立风井、001#地下车库及基坑支护）", "50000820181015040", "中国华西企业股份有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "5a5b0f0e-2d6d-4ba1-97cb-59d4ea7e1857", "8382cf2c-532e-4349-80e7-fed2e7eb6ea5", "西海岸（C36-2/02地块）17#、18#、21#、22#、25#、26#楼及地下室一区（17#、18#、21#、22#、25#、26#楼对应部分）", "50000820181015041", "中国华西企业股份有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "c07f1baf-1a35-4bc2-ac85-259911c8aaca", "029780df-5a5c-4587-8710-5ebc8e59a6ce", "重庆壹本科工城（A/B）标准厂房（B-1、B-2及汽车坡道（B区））", "50000820181015065", "重庆中科建设(集团)有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "ded56926-3431-4c47-82a6-0935ea6e6b67", "1ca0fac0-f65c-4fe5-86a2-15287d4cf2c2", "九龙坡区精神卫生中心迁建项目", "50000820181015079", "重庆建工第四建设有限责任公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "208288a5-c976-4ee1-b564-ed26d0dd1ab5", "05f5ceee-b52c-44c2-87e9-a576258c2e02", "中昂别院1#、2#、4#、5#、19A#（1#、2#、4#、5#对应地下车库）及边坡支护", "50000820181015081", "北京纽约建设发展有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "f60ec145-a97b-4e21-8583-e70af50d1a0d", "b6be7bee-98b8-4da2-b5aa-5e5daba3c42e", "盘龙广场及基坑支护工程", "50000820181015091", "重庆佳宇建设（集团）有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "f46026ff-ae92-4e4a-a5dc-fe08873ad9ef", "f6c97893-6ea8-44d7-92c2-9c1585c399a4", "保利爱尚里二期B组团（B1、B2号楼、B001号地下车库及边坡支护）", "50000820181015092", "富利建设集团有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "82e815cf-f95d-4c6e-b9f6-4e07537f703c", "d321b961-0dcf-472c-83e7-a1f367dd7e92", "柏景西雅图（B5-1塔楼、B5-1裙房、B5-2塔楼、B5-2裙房、一期车库）", "50000820181015093", "南通四建集团有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "3a6e60c7-00d2-4198-bf32-e71eab6f233d", "daf0d06d-94d5-4417-8f1e-07e7112e11e4", "九龙坡区九龙镇蟠龙小学扩建项目", "50000820181015096", "重庆建工住宅建设有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "d64ff27f-1ef3-44e2-9f3e-fbd20838da7e", "47109c4c-0805-46f6-a928-fcccf78444e9", "华岩隧道西延伸段项目二标段", "50000820181015101", "重庆建工集团股份有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "c1aa51f9-1ed7-4abc-841f-89b41975a84f", "a8c6fb5a-3851-4b24-8e7e-35fe0a3e31a8", "华岩隧道西延伸段项目五标段", "50000820181015102", "重庆市爆破工程建设有限责任公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "53720cbb-d856-46a1-ad2b-a0d467da5f7e", "c20c045d-0a3f-4014-8e93-bdfe2e27a65c", "嘉华大桥南延伸段三期南段工程", "50000820181015103", "重庆建工第一市政工程有限责任公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "02aacabd-15ec-40ba-adc7-0aabee5ff9a4", "d4c81873-c877-47b6-8052-8519901ebfcd", "快速路二纵线华岩至跳蹬段工程（一标段）施工", "50000820181015104", "中冶建工集团有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "7cf32c76-99e2-4e72-8960-abee48727124", "85b919b0-6835-4514-997e-c1e3e6567bd5", "快速路二纵线华岩至跳蹬段工程（二标段）施工", "50000820181015105", "重庆建工第三建设有限责任公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "548ca290-de01-4dbf-86ad-fc9b9d8d5bf7", "78569d0e-0bb7-4fea-9e68-b8a4d2d9eb71", "五洲文化创业中心", "50000820181015106", "重庆对外建设（集团）有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "c20ec24a-2acc-41fc-a415-3ed74397597f", "11473bdb-5350-40ec-be53-a7111206381a", "恒大林溪郡三组团", "50000820181015107", "中兴建设有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "5a75a1bc-8a33-465a-a420-eb06c7759aa7", "1bd966c7-47d7-49b2-8bc5-4c2288908756", "恒大林溪郡五组团", "50000820181015108", "中兴建设有限公司", "九龙坡区", false);
            AddProjects(dbContext, accountList, "dbf502dd-219b-4f99-a509-e8fd88a5601d", "e6307193-a556-4843-8ddc-cabf1051df8c", "西郊路27号排危改造项目", "50000820181015109", "重庆建工第三建设有限责任公司", "九龙坡区", true);
            AddProjects(dbContext, accountList, "b64f0aa9-01d2-4aa2-af38-e4764a73d47a", "7d5fb113-baac-4372-a0b6-c62ad4aa140f", "中交漫山三期", "50000920181008008", "中交第二航务工程局有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "b0757db7-acd6-4048-b199-3878eb739f25", "eb11c167-379a-48f9-b9bc-3836cc75c82b", "茶园组团B分区项目（B44-1、B48-3、B44-2）一期总包工程（标段二）", "50000920181008017", "重庆荣达建设（集团）有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "ba14a4b8-2265-4658-a8be-4286c47ad0d8", "f33e0f19-edcb-415d-a088-8586bfc5cfb2", "茶园组团B分区项目（B44-1、B48-3、B44-2）一期总包工程（标段一）", "50000920181008018", "重庆市辰河建筑工程有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "efc3470a-59a8-479e-b7ff-2670a3a7626d", "6c5d92e1-ec0a-467d-a187-1cecbeabc58a", "南岸区M组团（金域学府）翰林二期C期项目", "50000920181008021", "重庆城鹏建筑工程有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "4f64b68c-8b7f-44e8-a6cb-01b9043d6ee6", "0c966da2-f40f-4946-b699-827b002d23e8", "三江花园", "50000920181008022", "重庆建工第二建设有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "800b1be2-a6cd-409e-a37d-9f06f2fa39fe", "fbc7e6ad-0f35-4952-9cf4-0626389df0c8", "国家文物保护综合服务设施项目", "50000920181008023", "中建欣立建设发展集团股份有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "289ddc48-eb81-4513-ba37-50895acacfb5", "c5476f04-4f27-4991-a515-0066a265ecbd", "金辉城三期一标段二区2#楼、3#楼、14-2、14-3、50#楼(门卫房和电梯）及部分地下车库", "50000920181008030", "福建宏盛建设集团有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "a6c89068-877c-44bc-b19a-9894c811229e", "7607073c-079a-4cbf-bb47-852151c5109c", "君临南山项目（1#-32#楼、1#-4#车库）", "50000920181008045", "江苏仪征苏中建设有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "f05bb66e-237d-498e-b1d8-d63b366eddac", "ed3ac254-b474-4def-9a42-14a48edd34a8", "美心长江一期A7-1地块（一标段）", "50000920181008048", "重庆市南城建筑工程有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "14203307-3a3d-47f6-9fd1-8183f70883f0", "2bbf44b2-3c81-4eaa-b7be-b093d57e891b", "茶园新城区K标准分区住宅20组团2期二标段", "50000920181008049", "山西恒业建筑安装有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "860b2e6b-8b73-4534-833b-0b9dc5ba9f55", "de10b657-8942-4e3d-82ec-83782ec8463c", "保利二塘项目一期C组团", "50000920181008050", "重庆第六建设有限责任公司", "南岸区", false);
            AddProjects(dbContext, accountList, "66a89863-eace-4021-9235-86df7891359e", "2ab840f4-57fb-4509-9850-5e3f5cdfd320", "茶园新城区K标准分区住宅20组团2期一标段", "50000920181008051", "重庆庆华建设工程有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "825064bd-f6b7-48f6-bb68-369c60be7e68", "8bcb8fcc-8528-4111-b3f0-1afb62bc3dc3", "保利二塘项目二期B2组团", "50000920181008057", "中建三局第三建设工程有限责任公司", "南岸区", false);
            AddProjects(dbContext, accountList, "8826dd28-44c7-4f5a-822e-f27deb8eee23", "faf7714f-4e1c-45ab-9902-858a9d6b7611", "南坪组团M分区M10-3-2/06号宗地项目", "50000920181008058", "华建利安建设集团有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "9e2da290-2bae-47bc-8976-7800349aa0f4", "6390538b-f9ee-4433-ba1c-8405a1e7ed7c", "奥园越时代一期三标段", "50000920181008073", "重庆拓达建设（集团）有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "1964f9e1-70a0-4f39-b560-f8d76af60c63", "3f5866c1-a991-4dd7-8f84-03222cd29afe", "至元成方弹子石项目二期(A17-1/05地块)", "50000920181008085", "重庆诚业建筑工程有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "2d92fae4-86ec-4ad1-8db1-55fd2c395dde", "5ef718d1-0e38-4af8-9ca8-01f27b4e35d6", "天盈首原二期(天盈海峡路项目1.5.6.7#楼)", "50000920181008086", "重庆诚业建筑工程有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "ee4c4f64-173e-4452-a61f-df46e5e74d68", "2d098364-2f88-4c97-807f-848ce7f6e13a", "鸿笙苑（秀苑华俊）（BCD）", "50000920181008088", "中国电建集团重庆工程有限公司（原名：重庆电力建设总公司）", "南岸区", false);
            AddProjects(dbContext, accountList, "3060da30-1a29-4b86-98ad-920d74345c3c", "67ee811d-4217-4954-8234-cbe0b2735d8d", "茶园堃运项目一期（B35-1/03地块）（17#楼-21#楼及附属商业、部分车库）（D-24轴-D-42轴/E-J轴-F-P轴）", "50000920181008096", "重庆光宇建设开发（集团）有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "fd078a45-fb6a-4745-beb4-0c1935597baf", "e2f39d7a-2573-4226-8326-0c6d48a34c99", "渝信阳光居住小区二期（局部设计方案修改）工程", "50000920181008098", "重庆渝圣建筑工程有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "a2e64600-d1dd-4425-94d1-10ad6dc2d978", "262ae12b-7f2d-4c98-9cf2-6dff2e9998fa", "重庆金隅大成南山郡(9号地块)", "50000920181008099", "重庆渝发建设有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "ef90a16e-691b-4d76-80a8-6e441fdf08fe", "fece40bd-50be-47ce-ad57-f409dc13508a", "重庆金隅大成新都会项目(A23-2/07地块)", "50000920181008101", "北京建工四建工程建设有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "8f469f49-0be7-4d57-81ed-54cd6cbabf6d", "a4043aff-d737-4b0b-8fc7-a0f2d2c71995", "金地·首创弹子石项目", "50000920181008102", "浙江欣捷建设有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "5e6f229b-e6d2-41c7-b39e-97c137688e9c", "09948c83-c504-426a-9d29-78aff52cb42f", "长嘉汇小区（G3-4/02地块F1组团）II标段", "50000920181008111", "重庆诚业建筑工程有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "1e666acb-38d9-43e4-a6df-54f3f3260720", "a21bea22-932d-432d-8426-6d8b8cbea91a", "长嘉汇小区（G3-4/02地块F1组团）I标段", "50000920181008112", "重庆华硕建设有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "fa9a4acf-3ba9-4b82-bd36-f81c65504a41", "50871260-944e-4f90-935f-d758c47af723", "长嘉汇小区A组团1#楼", "50000920181008113", "正威科技集团有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "8f9640ce-0b04-4e44-b7a2-e041914cfcc6", "c16e4ce2-6e51-4800-9f6f-d962f85327f8", "荣盛华府三期(茶园新区A74-1/02地块)", "50000920181008127", "重庆嘉逊建筑营造工程有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "c9f1575e-414b-4ac2-a85a-fea9a84c2291", "f540b421-0602-4368-96f4-380a6deb9193", "重庆金隅大成南山郡（南岸区黄桷垭组团B、C分区宗地）8号地块", "50000920181008132", "重庆市阜泰建设（集团）有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "2b380c8e-60ae-44bd-8010-42e8389b0862", "b3ac17a7-7038-42f2-bfed-27bf3aff5fdb", "保利茶园项目（一期）", "50000920181008133", "海天建设集团有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "47bcf06e-7c22-4d83-ac9d-c54f8d720d32", "92cb1212-9a7c-43ec-b3ac-6ab6dbe611cb", "保利二塘项目二期B1组团二标段一区", "50000920181017186", "海天建设集团有限公司", "南岸区", false);
            AddProjects(dbContext, accountList, "3bce02b4-6f2b-4a87-af5e-24e18d739d81", "99a36398-40bd-495b-81a2-9c3cbd60a7c4", "金科蔡家M分区项目M35-1/05、M44-1/04地块一标段土建及普通水电安装工程", "50001020181016026", "中建八局第三建设有限公司", "北碚区", false);
            AddProjects(dbContext, accountList, "c3a238c6-282d-4d63-831c-e494fab2d56d", "73873d9d-ce81-4ac5-9fd7-dbcc3f4baed0", "嘉运大道一号住宅小区项目（N01-02/03地块1至24号楼、30号楼、36号楼、43至47号楼及48号（48-1）地下车库）", "50001020181016028", "重庆建工第三建设有限责任公司", "北碚区", false);
            AddProjects(dbContext, accountList, "15e474db-0dca-4820-b7f2-447a36eb3937", "34a3a0fc-2015-457e-860b-5e238cf8a9a0", "金科蔡家P分区M23-02/03地块三标段", "50001020181016034", "重庆先锋建筑工程有限公司", "北碚区", false);
            AddProjects(dbContext, accountList, "d694219f-5aad-4744-abd5-f62aa78bd156", "634ae122-6e9d-4ab1-b1f1-10959b6eaa17", "北碚区歇马农转非安置房工程", "50001020181016039", "中冶建工集团有限公司", "北碚区", true);
            AddProjects(dbContext, accountList, "cdd6dc5b-a433-4e2d-808b-74ac49d72a20", "05a6bc64-e6dd-44cd-b623-b6484081c316", "嘉运大道一号住宅小区项目（N01-06地块44至68#楼及D3#、、D4#地下车库）", "50001020181016045", "重庆建工第三建设有限责任公司", "北碚区", false);
            AddProjects(dbContext, accountList, "5be00dc6-44b2-4936-a405-3204dea2c943", "d45853e7-5ce6-4b41-976c-7017343735fd", "华润·琨瑜府（1、2栋，5-31栋，2#地下车库）", "50001020181016047", "西南建工集团有限公司", "北碚区", false);
            AddProjects(dbContext, accountList, "3a4f5a6a-4a42-43bb-95b0-24981ce12f5e", "0cc10892-2184-410e-bdd7-615efdb8a9e9", "万科蔡家N分区项目", "50001020181016052", "重庆中通建筑实业有限公司", "北碚区", true);
            AddProjects(dbContext, accountList, "62dac3f3-e0a9-4b57-b11f-6393c44024ec", "fbfef550-329e-4d2f-b7cc-38a9b0f0e800", "万科蔡家项目（M04-01-1/04、M04-01-02/04地块）", "50001020181016060", "重庆中通建筑实业有限公司", "北碚区", false);
            AddProjects(dbContext, accountList, "690144ee-2451-4205-af54-8a2329979b66", "a723e65c-4c2a-4a63-860f-371a980d7529", "华润·琨瑜府（32-45栋、垃圾站、1#地下室）", "50001020181016063", "四川省第四建筑工程公司", "北碚区", false);
            AddProjects(dbContext, accountList, "e25d9166-43b0-45f4-98b9-4d71c7d7e040", "8208c3f8-75c7-43b3-966d-08c718e74934", "金科蔡家M50/04地块项目华硕标段", "50001020181016066", "重庆华硕建设有限公司", "北碚区", false);
            AddProjects(dbContext, accountList, "a51290f9-a1e8-4ae8-9ffe-f412f4595aea", "04bcd121-510c-42e6-ac36-555045e7b884", "中共重庆市北碚区委党校校舍项目", "50001020181016082", "重庆建工第三建设有限责任公司", "北碚区", true);
            AddProjects(dbContext, accountList, "f490c43a-a0e5-42d7-8b9e-85b80e41e2b5", "9024897b-4b1b-4956-989b-36170b047b20", "嘉运大道一号住宅小区项目N01-06/03地块10-17、40-43#、113#楼、岗亭及D1-5、D5#地下车库", "50001020181016086", "重庆建工第三建设有限责任公司", "北碚区", false);
            AddProjects(dbContext, accountList, "fb31b864-08b8-4c77-9097-5b1b12b0027a", "443008d5-e5b4-4cfb-9d0c-d542903bd81a", "中交·锦悦（Q04-4/02地块一标段2号楼、3号楼、4号楼、34号楼、地下车库）", "50001220181018001", "中交第二航务工程局有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "064f0c8c-7a16-4aff-97c8-833c0a3c00b8", "4472042e-1c74-4914-a448-83b419582a6f", "中交·锦悦（Q04-4/02地块三标段9号楼、10号楼、12-33号楼、地下车库）", "50001220181018002", "中交一航局第四工程有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "4c3d0e6f-87bc-4e46-917d-2dc9b97ff086", "79357e1b-2d68-48ff-a300-bb0a8f95a893", "中交·锦悦（Q04-4/02地块二标段1号楼、5号楼、6号楼、7号楼、8号楼、11号楼、地下车库）", "50001220181018003", "中交一航局第二工程有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "23ab8c5a-98ce-4ee9-b9e2-204c620a183b", "30812a73-0a45-4298-ba68-24466e07ef12", "中建·瑜和城（一期）", "50001220181018004", "中建五局第三建设有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "baa88685-bca2-45f5-908b-5c3328337abe", "04a5e1e9-f7f3-42dc-b1c7-4b4b6e11aa99", "中建·瑜和城（三期）施工", "50001220181018005", "中建五局第三建设有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "e92d5d90-2962-470f-b934-26be027937cb", "5a085a84-cb90-467e-be32-bbcf5f45d4bb", "保利云禧项目Q21-5号地块（1#楼至8#楼、13#楼、14#楼、S1#楼及地下车库A轴线到AF轴线向上2米）", "50001220181018006", "福建众诚建设工程有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "31cfdf16-7f62-4ecc-b701-b8eb93d09920", "62bd8fdf-189f-454b-acff-350876a5de98", "华宇龙湾二期（M4-4地块）(1#、2#、3#、4#及5#车库）", "50001220181018007", "重庆华姿建设集团有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "42da0f12-34c8-4303-948c-6248014f9ecb", "6cb0e1f5-c2bf-46c4-8ca3-87a54fdf7ce3", "华熙LIVE·重庆鱼洞（巴南区体育中心综合整治）项目——住宅（1#楼-3#楼、7#楼、9#楼、地下车库及设备用房（1-1轴交1-17轴交1-b轴至1-L轴交2-1轴至7-2轴））", "50001220181018008", "中建一局集团建设发展有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "b4d4d791-3112-46bc-af16-1abe2fc9e778", "14fa1ee4-997f-4d9b-b156-eb15f6e20882", "华熙LIVE·重庆鱼洞（巴南区体育中心综合整治）项目——住宅（4#楼-6#楼、8#楼、10#楼、11#楼、地上人防设施及其他、地下车库及设备用房（1-17轴交1-30轴交1-b轴至1-A轴交7-2轴至9-9轴）", "50001220181018009", "重庆华力建设有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "cf39081c-da58-4bba-a379-d6c40951b1c2", "7f002999-bbea-46d5-ab9e-db89af575495", "华远·海蓝城（G26-1/03地块）22#-37#和地下车库", "50001220181018010", "中国建筑第二工程局有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "cb759464-c955-4caf-bc42-3f76722352ac", "41a846a8-32ef-48e6-bddb-4007e7e9005c", "协信·星澜汇四期S9-2/03地块2-1#至2-15#楼及地下室、S15-3/03号地块5-1#至5-9#楼及地下室", "50001220181018011", "中建新疆建工（集团）有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "dd3b825f-d838-4242-b003-01858b2f5a9c", "bdf2dfcc-f143-4f9f-bf07-c7bbc9cb4345", "协信汽车城(D11-1/04地块）（18#、地下车库）", "50001220181018012", "重庆建工第七建筑工程有限责任公司", "巴南区", false);
            AddProjects(dbContext, accountList, "f4587f45-66a1-413c-a73a-6424e919d18f", "8f363a80-8188-4403-9ff5-84abf260849b", "启迪协信·敬澜山（一期）（1#楼、2#楼、7#~12#楼、15#~18#楼、29#楼、31#~38#楼、S1#~S3#楼、D1车库（1轴~11轴/V轴~A轴+11轴~26轴/N轴~V轴+26轴~32轴", "50001220181018013", "重庆建工住宅建设有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "bc5991e0-5d1f-4462-bb74-d0e8563ebfe3", "bee9ea81-7be6-4c8a-951d-2da09a4057dc", "恒大新城V6-1地块（4#至10#楼、59#楼、D区车库）", "50001220181018014", "中建五局第三建设有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "19c9a20d-78d6-4263-9b6b-aa23a6b3f609", "a95f07ba-361d-4779-8195-b35213b2f6b3", "洺悦城一期（4#楼-6#楼、12#楼、15#楼-21#楼、D1-1#地下车库）", "50001220181018015", "中电建建筑集团有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "ad807a8b-4aac-494a-a5ae-39c3efb42732", "9bf9fb72-4ccf-4643-a789-f86c7d7f2cc6", "界石组团N分区N12/02地块（1#楼、11#楼、18#-27#楼、2#车库（2-4轴~2-22轴部分））", "50001220181018016", "重庆光鸿建筑工程有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "de1954eb-b7c5-4e81-b698-822edd0fac7a", "688ec7f1-eb5c-43a1-9e95-42f3bd8ed1ba", "碧桂园·渝南首府（三期）21-1#、21-2#、22#至25#楼、地下车库）", "50001220181018017", "中天建设集团有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "7213a29b-2b8b-4adf-8714-22a795239be7", "d223b933-1bf2-43d1-b02d-4adf00385f9e", "联发·龙洲湾1号一期C组团（1-C-1#楼~1-C-5#楼、1-C-3-2#、1-C-4-2#、1#地下车库（二期））", "50001220181018018", "中建三局第一建设工程有限责任公司", "巴南区", false);
            AddProjects(dbContext, accountList, "7fdf8f6d-c485-4c65-b4c4-5893d9c3f96d", "815a3b8a-1f32-4795-98bd-4b5adc17de67", "荣盛滨江华府二期（L2-1、L1-3地块）（L2-1地块：15#至20#、D2地下车库29-45交A-AB轴，32-45交AB-HB轴）", "50001220181018019", "重庆建工第八建设有限责任公司", "巴南区", false);
            AddProjects(dbContext, accountList, "e0c37753-e919-4f7d-b693-5f174761c840", "d9e0e254-0fb2-41bc-aa52-cf1ebf02a305", "荣盛锦绣南山Q21-1地块（1#-3#楼、S1#-S3#商业及D15#地下室（1~25轴交A~E轴、25~33轴交E~G轴）", "50001220181018020", "中冶建工集团有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "7cb91045-3d6f-47e5-9a62-99bfdaedd969", "872cecf4-59b4-407e-bf80-de2989913e1c", "蓝光水岸公园（1#、3#、4#、9#至16#楼、P1#、P3#地下车库）", "50001220181018021", "重庆拓达建设（集团）有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "623d0518-3149-471d-afd4-e2b6550c24a5", "509aa216-230f-4e2e-b194-24199924c3c7", "蓝光水岸公园（2#、5#至8#楼、P2#地下车库）", "50001220181018022", "重庆华力建设有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "194cab37-d0ac-4ca6-9a45-f4e66eae2cfd", "795885dd-7619-41b5-bd91-5c0cafbd3c1b", "蓝光美渝森林（三期）【8#-12#楼、2#地下车库部分（2-4至1-34轴交1-A至1-P轴、3-5至1-34轴交1-A至1-P轴)】", "50001220181018023", "中建四局第五建筑工程有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "505cbed9-175a-44a9-8f30-81d96da1a2d0", "4e469127-dca0-4f90-a102-d00de1e409af", "融信龙洲府（S1、S2、1-19号楼及C区、D区地下车库）", "50001220181018024", "福建省荔隆建设工程有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "4bfc8e58-c6c1-4a82-b51d-d998a92f32c1", "eaecc8d8-f0cd-4dc3-9d8c-8afb84987c41", "融信龙洲府（S3、S4、20-46号楼及A区、B区地下车库）", "50001220181018025", "福建省荔隆建设工程有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "1460f794-4c31-45c9-a79a-5ecf0d7f6f74", "65da8c20-bc80-4568-9882-ba77cb655204", "融创·金裕三期（M20/2地块）四标段（20-G5#楼、20-G6#楼、20-Y7#楼、20-Y8#楼、20-Y11#楼至20-Y15#楼、岗亭3及002#地下车库（1~24轴线部分））", "50001220181018026", "重庆光宇建设开发(集团）有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "610518c6-69c6-4062-a04c-5f0de8e25a06", "e807df10-521f-4a9f-8248-69f58954f3ca", "融创·金裕五期-1期（M21-1/03地块）（1#、2#、3#、4#、5#、6#、7#、8#楼（地下车库及设备用房）", "50001220181018027", "重庆华硕建设有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "60b09be7-f55c-4e56-ae37-72548e64162c", "1c09a085-188b-45b6-bc52-1d67c763b128", "融汇半岛十五期（1#楼、2#楼、3#楼、商业及车库）", "50001220181018028", "四川省第三建筑工程公司", "巴南区", false);
            AddProjects(dbContext, accountList, "04a8dec6-3460-47e4-8035-dc0f2beed4e6", "b552699b-2d9c-489a-b12a-929027a3dfa7", "金碧辉公司66#地块（G21-3/03地块）（4~8#楼、S2~S4#及1-A~3-C/1-1~1-25轴车库）", "50001220181018029", "重庆万泰建设（集团）有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "f551b289-0852-4184-8dfb-27010a5decdb", "aacbbc7e-2319-45c3-aab9-f1cf069bd891", "金碧雅居·鹿角项目（N16-1/02、N16-3/02、N17/02、N18/02号地块）1#-14#楼、22#楼、21#楼（车库一期）", "50001220181018030", "重庆海博建设有限公司", "巴南区", false);
            AddProjects(dbContext, accountList, "35b5e44f-ef0d-44d5-926d-d76ad2aa02f4", "7b392535-afa5-4053-a026-20f0a2127594", "长寿经开区科技创新园一期工程设计采购施工总承包", "50001320181015023", "重庆钢铁集团建设工程有限公司", "长寿区", false);
            AddProjects(dbContext, accountList, "5c95ea17-b57a-4568-8bbd-fee36529979d", "fe8e7aa7-43fc-44b0-960e-87298fa6408b", "重庆长寿长江二桥工程", "50001320181015039", "中国铁建港航局集团有限公司", "长寿区", true);
            AddProjects(dbContext, accountList, "6627ad41-bb17-4417-b2ec-d30c6199dd7b", "f626d869-5b79-4747-93b5-55de0d2a62e8", "金科集美郡一标段", "50001420181008017", "南部县锦兴建筑安装工程有限公司", "江津区", false);
            AddProjects(dbContext, accountList, "8ca1d3ee-0325-45c7-8d5d-58718b59908d", "58312f2a-a493-4b18-91b6-3e9267229b0b", "恒大金碧天下三期695-701#、704-705#楼", "50001420181008028", "重庆建工第三建设有限责任公司", "江津区", false);
            AddProjects(dbContext, accountList, "4265d212-1aba-42ab-86f6-bba4f3b82ed8", "2410506b-6797-475e-b223-3c65a346c625", "中骏·云景台", "50001420181008029", "重庆华力建设有限公司", "江津区", false);
            AddProjects(dbContext, accountList, "a1c595f2-d8d3-4912-b7dc-ad70370051cd", "445db32c-f93d-4bad-8b7c-8e6475a83cdc", "恒大国际文化城A区A17-A22#楼及地下车库", "50001420181008036", "重庆建工第三建设有限责任公司", "江津区", false);
            AddProjects(dbContext, accountList, "08817a15-5d20-4c19-83e5-5a19df204b0a", "de2551de-f0a8-484a-838f-627a1a1ca9dd", "恒大国际文化城A区A13-A16#楼及地下车库", "50001420181008040", "重庆庆华建设工程有限公司", "江津区", false);
            AddProjects(dbContext, accountList, "8ba794f5-dfb1-427d-98e8-33f9810ebff5", "ec3d9cf0-6531-4533-ac98-72fc4a46a552", "中梁首府项目一标段（1-10#、20-21#、26-27#、35-39#楼及对应地下车库）", "50001420181008052", "中国建筑第七工程局有限公司", "江津区", false);
            AddProjects(dbContext, accountList, "7d3a4a08-aeef-450f-a4f5-e5f1b1e878e3", "62f6622c-01ff-4fe8-8798-11f4afb1c5ed", "华远江和墅一期（1-46#楼、73#楼、车库A-D）", "50001420181008067", "南通华新建工集团有限公司", "江津区", false);
            AddProjects(dbContext, accountList, "1791be6a-9c01-47ec-ba81-2f7f62bbec97", "07a15587-92b1-4c21-8b9a-ef57beb4391e", "城海滨江春城二期（3-4#楼、9-10#楼、14-15#楼、18#商业裙房及对应地下车库）", "50001420181008080", "重庆拓达建设（集团）有限公司", "江津区", false);
            AddProjects(dbContext, accountList, "cc8adfc2-9894-49fb-b5e6-609ba83f13ff", "d373dbf5-f129-490c-b5d8-9d3543b89864", "江津区第二人民医院扩建工程（含住院综合楼、儿科综合大楼）", "50001420181008122", "重庆建工第七建筑工程有限责任公司", "江津区", false);
            AddProjects(dbContext, accountList, "34f96c4f-0d37-4819-86ba-ef7a0e5564e1", "6877e45f-1839-4f7b-895f-2f844a531ffc", "御景江城三期（3-6#、8-9#及部分地下车库）", "50001420181008134", "重庆一品建设集团有限公司", "江津区", false);
            AddProjects(dbContext, accountList, "5cf7816a-f688-4537-a383-14c1748e0635", "39ff3e72-ea18-404b-8d9a-4df9eb6903b3", "北新•御龙湾I期一街区", "50001520181018063", "湖南北新天际建设工程有限公司", "合川区", false);
            AddProjects(dbContext, accountList, "92a2d908-b1e5-4a45-9d76-f0155ac08408", "2dbe24af-b2ea-4728-b636-a4450d638fc6", "重庆合川花滩医院一期工程", "50001520181018075", "天元建设集团有限公司", "合川区", false);
            AddProjects(dbContext, accountList, "171580a4-0f6b-4df7-bea8-5c8dae66a6f1", "c101b07f-07bc-449d-b163-2c809b0fa05b", "鹏润悦秀御江府（一期)", "50001520181018082", "贵州建工集团第五建筑工程有限责任公司", "合川区", false);
            AddProjects(dbContext, accountList, "f00088e8-2608-492f-80af-103f44000de8", "a2917586-0064-4433-ac01-325a74fb2546", "碧桂园•智慧家一期", "50001520181018096", "中亚建业建设工程有限公司", "合川区", false);
            AddProjects(dbContext, accountList, "9a0b6963-a9c1-4aec-b623-e528d827bc7a", "8e6e2e50-1744-4644-a7f8-c00d7be02edc", "中船华尚城一期工程", "50001620181008019", "中国核工业二四建设有限公司", "永川区", false);
            AddProjects(dbContext, accountList, "986ab916-eb63-433d-aa88-572b25b46e14", "a749f208-4409-4b8a-becb-91d41b3e67dd", "永川长岛汇7-10号楼及地下车库", "50001620181008036", "重庆建工第八建设有限责任公司", "永川区", false);
            AddProjects(dbContext, accountList, "3f189875-199c-476f-ae1d-4ce1866279bb", "284ed6d4-46c7-44e6-a28d-06321c370f8f", "凰城御府三期一组团33、35、36、39、40#楼及地下车库", "50001620181008059", "重庆利安人居建设工程有限公司", "永川区", false);
            AddProjects(dbContext, accountList, "4aaca824-6b42-4b63-bc43-8a0fd03e83d9", "d0309a42-772d-47fa-9e2b-f5885bbaf6ca", "万达B1B2地块Y20、Y27-Y31、S8-2、S9、D3B车库", "50001620181008060", "中国建筑一局（集团）有限公司", "永川区", false);
            AddProjects(dbContext, accountList, "a5a1bee8-1f52-4542-b3a8-4f3f374bd7eb", "ff764c65-8a4a-4925-8810-8abf38687f6a", "凌云阁项目二期组团建设工程（7-11#、1#商业及地下车库）", "50001620181008073", "重庆建工第九建设有限公司", "永川区", false);
            AddProjects(dbContext, accountList, "cc69fbbd-148b-4213-93cc-1ff726c3d722", "89920eb4-e4f6-465d-81bc-382b9a8e0a72", "置铖荣华府1、2、9-12、16-20#楼及地下车库", "50001620181008076", "重庆吉隆建筑有限公司", "永川区", false);
            AddProjects(dbContext, accountList, "6bf3e1e1-56da-497a-b52f-46e45d625f9c", "6935a547-8e59-4e2d-87fd-ac03d6ea1378", "金科.集美天宸一期（二标段）2、6、8、9、16、17#大门及地下车库", "50001620181008077", "重庆大江建设工程集团有限公司", "永川区", false);
            AddProjects(dbContext, accountList, "35a3afdb-44fb-4745-8077-3ca0180633be", "ae6ad799-80fb-47d9-bdbe-daa011e882ca", "昕晖.香缇时光D组团6-15号楼、1-5#商业", "50001620181008078", "重庆鹏威建筑工程有限公司", "永川区", false);
            AddProjects(dbContext, accountList, "bb18004e-7399-436b-b422-8b3d51e2c0a6", "57a92f18-b2e0-4e21-8558-7309cdae7ce8", "天悦府9-17#、17-1#、15-1#、10-1#门岗、部分地下车库及设备用房建设工程", "50001620181008107", "山河建设集团有限公司", "永川区", false);
            AddProjects(dbContext, accountList, "18c33f7e-dc70-4d41-b384-7bde2a6e0288", "e5f8263d-776f-4246-be1e-fa39a202d217", "礼悦东方（1-3#、6#、7#、9-10#、12#、13#、15#、16#、18-27#、30#、S2-S4及地下车库）", "50001620181008113", "重庆建工第九建设有限公司", "永川区", false);
            AddProjects(dbContext, accountList, "6847123e-eff3-4575-9d6c-59a79cb1c076", "57f8eaa4-35d0-48f0-b8d0-3d52b12afe77", "永川服务外包E区工程", "50001620181008116", "重庆建工住宅建设有限公司", "永川区", false);
            AddProjects(dbContext, accountList, "73fc5b8a-a362-4bf9-83c7-f6c5caea6cf6", "5b785e99-a4e6-4e3f-bfc6-277c56f5d0d0", "协润.凤凰世纪城三期14-18#楼及车库A区", "50001620181016096", "重庆腾宇建设工程有限公司", "永川区", false);
            AddProjects(dbContext, accountList, "bcb5a494-d0ee-45a1-b191-730fbeb2e2cd", "ae46666a-083a-4300-a69b-f7b541106b3d", "金科.集美天宸一期（1、7、10-15#楼及相应地下车库、门卫）和二期（1、2、10#、1-2#门岗、警卫室）建设工程", "50001620181016164", "重庆大江建设工程集团有限公司", "永川区", false);
            AddProjects(dbContext, accountList, "9443a690-12ea-4205-8c01-ab5af8b352e2", "3a52b202-2b57-4491-8b3f-d8f802f8f6a9", "鼓楼新天地房地产开发项目", "50001720181008001", "四川省第三建筑工程公司", "南川区", false);
            AddProjects(dbContext, accountList, "fab47e11-9c43-4a88-8fab-083911c7b604", "dcc3d358-fa45-47db-a4b9-cc079da87c40", "南川区凤江晓月农转非安置还房建设工程", "50001720181008002", "重庆建工住宅建设有限公司", "南川区", false);
            AddProjects(dbContext, accountList, "3a262dcd-5d5e-477b-8a85-7541a16ab396", "68aa22fc-1664-47e4-b70a-c143c74cccc6", "天馥城一期二标段", "50001720181008003", "重庆崇景建设有限公司", "南川区", false);
            AddProjects(dbContext, accountList, "3daf80a4-286c-4d81-981c-650401424f27", "8040764f-057f-4ceb-8df0-21d93b3618c9", "天馥城一期一标段", "50001720181008086", "重庆建工住宅建设有限公司", "南川区", false);
            AddProjects(dbContext, accountList, "922e923e-c0a3-47b6-a06b-76c4574db7f5", "9ab741ae-c53d-4dac-b782-9e83a272e3e3", "綦江区城市交通综合体二标段", "50001820181008061", "重庆拓达建设（集团）有限公司", "綦江区", false);
            AddProjects(dbContext, accountList, "0552ad6b-1ac3-4f7c-9ec3-913a40543959", "a48bb48e-9b44-4048-9c21-269f4698eb8b", "綦江红星国际小区B区6#楼", "50001820181010117", "重庆中科建设(集团)有限公司", "綦江区", false);
            AddProjects(dbContext, accountList, "8523e7f5-bdf6-4c36-bb82-cf11048dd676", "d9ca858c-b480-41bc-ab9d-67e1f19a5189", "重庆计华办公楼", "50001820181010123", "重庆市渝北区海华建筑工程有限公司", "綦江区", false);
            AddProjects(dbContext, accountList, "35fb5425-df42-40e8-8071-352e15b36c74", "6eb72942-3ee1-4cbc-ae47-9c2e3c663718", "康德城市花园二期（一标段）", "50001820181010124", "重庆锦庆建筑工程有限公司", "綦江区", false);
            AddProjects(dbContext, accountList, "b20ec499-9539-46c1-810e-3c41d20fe8c9", "520a8579-abd7-4bd6-8018-e65d42b32d4b", "东部新城金凤-新城商贸", "50001820181010135", "重庆教育建设(集团)有限公司", "綦江区", false);
            AddProjects(dbContext, accountList, "97cef1d7-ed42-4cfe-b71c-a842f75c772c", "1ba1b549-7314-4fc9-bfda-260845beb971", "奥园金澜湾一期", "50001820181010139", "广东蕉岭建筑工程集团有限公司", "綦江区", false);
            AddProjects(dbContext, accountList, "eda4ef88-89fb-42d4-ae4c-0cf744ed4a2e", "e80986c1-2c8d-4f5c-a138-83b39bc7b0e0", "阳光城翡丽云邸", "50001820181010155", "中国建筑第二工程局有限公司", "綦江区", false);
            AddProjects(dbContext, accountList, "a1ab13bc-c0aa-47aa-8971-c58cdd2446c6", "3bd5aabb-7801-4112-88a7-ef6740106176", "大足金科中央公园城2地块二期一标段7#一13#楼、M2、Y1#楼及对应部分D1#车库", "50001920181009076", "中国建筑土木建设有限公司", "大足区", true);
            AddProjects(dbContext, accountList, "51a0232f-0e38-40b5-abee-1b6fb736c44d", "ee252d76-065b-42e8-9478-21051123b6c3", "重庆大足柏林广场4.5.6.7号楼及三期地下车库", "50001920181010153", "重庆建工住宅建设有限公司", "大足区", true);
            AddProjects(dbContext, accountList, "5aed03fc-84b6-440b-902e-ac64fb52979c", "d832514e-c9dd-4098-bc48-18e6cdf94e1a", "金科·博翠天悦（1#~29#楼、S1#~S3#楼、D1地下车库）", "50002020181008018", "中国建筑第二工程局有限公司", "璧山区", false);
            AddProjects(dbContext, accountList, "04159bd7-5d8c-45c6-8dd3-6f7df9caddc0", "ec039c33-77b1-42c1-8eb1-bcdbe567ac5e", "交投·香漫溪岸（四期6#、7#及五期13#、14#、20#、21#楼、D1车库）", "50002020181008034", "中铁十一局集团第五工程有限公司", "璧山区", false);
            AddProjects(dbContext, accountList, "09746b10-6d13-40ac-848f-10c0dea05d19", "e58aa755-3294-4a80-939a-8d39c51a2147", "鸥鹏·凤凰城一期三组团19-22#楼、I标段车库工程", "50002020181008035", "泰兴一建建设集团有限公司", "璧山区", false);
            AddProjects(dbContext, accountList, "d4f33253-f1d7-48c2-8ca0-f38dc6b68596", "2b0583a1-4b2a-454a-8acf-e2ba53e94272", "中瓯·璧河名都7#、12#楼", "50002020181008037", "中国建筑第二工程局有限公司", "璧山区", false);
            AddProjects(dbContext, accountList, "3c867a4a-485c-440d-8086-2d33f1bfc77a", "8ceb3f4e-828a-4f0f-beb2-a476fc397268", "中建·璧山印象（一期）（A区）（2#、3#、18#楼）", "50002020181008044", "中建五局第三建设有限公司", "璧山区", false);
            AddProjects(dbContext, accountList, "e3f823e6-4344-477c-9778-8c19d3c978c1", "5ce33a87-7e01-4736-95a7-e86fa2184522", "璧山区两山丽苑定向经济适用房项目", "50002020181008066", "重庆中科建设（集团）有限公司", "璧山区", false);
            AddProjects(dbContext, accountList, "4e973030-3cf0-459b-a8b5-d39c26efbe65", "e3f48b1b-f82c-463e-929b-ade827ed5cb1", "嘉利世纪广场", "50002120181018006", "重庆市茂森建筑工程有限公司", "铜梁县", false);
            AddProjects(dbContext, accountList, "9df74a83-99b6-4500-897b-aa80b48e60cc", "efbe0999-8daa-4642-ba67-ea956b9852ad", "泽京·龙樾府2、7至12号楼及一期车库", "50002120181018037", "西双版纳云海建筑工程有限公司", "铜梁县", false);
            AddProjects(dbContext, accountList, "312e908f-497c-4d23-9e3f-7adad10b8bcc", "b4e43e76-fddd-464b-a8a0-f8c6e9770390", "曜阳·龙泉香榭工程一期（1#、6-8#、13-16#、36#楼，6-8#楼地下车库）", "50002120181018040", "贵州仓达建筑有限公司", "铜梁县", false);
            AddProjects(dbContext, accountList, "d36bdba8-dc56-4818-aa8d-332ea2dd89fa", "c381a197-2e19-41c2-b970-346d076118c3", "台湾中小企业园生活配套服务区“博悦.悦城”（二期10#、11#楼）", "50002120181018068", "重庆灯塔建筑工程有限公司", "铜梁县", false);
            AddProjects(dbContext, accountList, "e5bfd2db-3f8b-419c-b782-4d4e729c2c10", "8e3d82c5-b879-41ae-bbfe-924c7ce11c53", "工业园区南区标准厂房一标段", "50002220181017001", "中冶建工集团有限公司", "潼南区", false);
            AddProjects(dbContext, accountList, "0b7ffb2c-b5ca-45d0-a43e-a66fe47fd355", "9d512b82-3d8e-4490-8501-bad33970d67c", "重庆市潼南区中医院创建“三甲”医院建设项目", "50002220181017004", "重庆建工第三建设有限责任公司", "潼南区", false);
            AddProjects(dbContext, accountList, "c0c3c1a2-9053-4922-88db-3feb97934acd", "53a658ca-3345-456c-9644-bfcb118818d0", "荣府.天一城", "50002320181008004", "十九冶成都建设有限公司", "荣昌区", false);
            AddProjects(dbContext, accountList, "7ecc57cd-9a0b-4e74-a0b3-d82ebdbd82cd", "a0395d9e-d3df-4cb0-ac27-e430ea61ce99", "金科世界城二期34#-39#、46#、49#、50#楼及44#部分车库", "50002320181008014", "重庆黔程建设（集团）有限公司", "荣昌区", false);
            AddProjects(dbContext, accountList, "591aa377-a756-4882-8634-d34878ada2f9", "806f230b-8df7-48fc-9754-30e270810242", "昌宁苑", "50002320181008036", "重庆跃城建筑工程有限公司", "荣昌区", false);
            AddProjects(dbContext, accountList, "de409f65-fc2f-4478-985f-0b4842e08c61", "0cf35d6e-cc3d-4036-a5d5-73c6c3d0927f", "万开周家坝－浦里快速通道工程一标段", "50002420181006024", "中铁二十五局集团有限公司", "开州区", false);
            AddProjects(dbContext, accountList, "481f5cd2-277a-48a5-a3d7-10d6bc9f141a", "7a6b550a-626a-41ea-8534-ad040e1da48a", "万开周家坝－浦里快速通道工程二标段（K5+300-ZK11+594）施工", "50002420181006025", "中铁十一局集团有限公司", "开州区", false);
            AddProjects(dbContext, accountList, "b28a7269-2753-4ed3-b2aa-aa51db81ee3e", "18c82bd0-3a41-4d0c-87b2-fbb740c18db5", "开县大丘邻里中心", "50002420181007000", "重庆城建控股（集团）有限责任公司", "开州区", false);
            AddProjects(dbContext, accountList, "3b0a9b16-e1f9-4942-a95e-4b1ba77be0e7", "2951be4b-1115-4326-8ec1-84ee3acea8c9", "金科云玺台三期", "50002420181007100", "中建八局第三建设有限公司", "开州区", false);
            AddProjects(dbContext, accountList, "59f84c95-d12b-472a-9044-47c7f5eb6583", "0a1a9ecf-4f72-45d3-8e12-e2fea55e6370", "开县北部新区基础设施建设工程-滨湖北路丰太段", "50002420181008038", "重庆建工市政交通工程有限责任公司", "开州区", false);
            AddProjects(dbContext, accountList, "7f0fa85e-4e96-4533-9866-566ad14ef970", "cee434a5-8ff2-4b4f-9e71-76f7b9e0fdb4", "巨源·南泊湾一期一标段（1、5、12、13号楼、S2商业及地下车库）", "50002520181017039", "重庆市华东建筑集团有限公司", "梁平区", false);
            AddProjects(dbContext, accountList, "d12afd00-1150-44f8-ad5b-573d2ff8394a", "876a43cc-ea32-446e-b766-4ced1ec16e04", "立城·香韵华府1.2.3.4.7.8.9.10栋、地下车库、小区门岗", "50002520181017042", "重庆厦宇建筑工程有限公司", "梁平区", false);
            AddProjects(dbContext, accountList, "4ba003c7-4fc7-468d-91f7-c00d6b012037", "238b2781-634d-4a09-9e8c-5fce9668f99c", "天誉一期", "50002520181017084", "山河建设集团有限公司", "梁平区", false);
            AddProjects(dbContext, accountList, "f1c743c9-bac6-40b1-bc70-c05535826655", "f5564d87-fbef-4921-a303-94face4096ee", "梧桐郡1#、2#、3#、4#、5#、6#、7#、8#、门禁、车库", "50002520181017087", "重庆鹏威建筑工程有限公司", "梁平区", false);
            AddProjects(dbContext, accountList, "ef460d33-85b9-46a0-9376-65af462fdb41", "97e4b7df-1a48-4a34-99df-2e95c98c3048", "武隆.江上明珠1.2.3.8号楼", "50002620181017020", "中核华辰建筑工程有限公司", "武隆区", false);
            AddProjects(dbContext, accountList, "1dda29c9-6e20-4447-a72d-5cae421287b5", "9bb62848-f09a-47a5-afdf-d6f7cc5c51d5", "武隆县职业教育中心改扩建项目", "50002620181017033", "重庆建工第八建设有限责任公司", "武隆区", true);
            AddProjects(dbContext, accountList, "6bf6ca68-aad1-4db2-af82-cecd356720f0", "e2735fd3-b71d-48f1-8360-24091e357750", "佳合锦绣城一期工程", "50002720181008002", "重庆覃家岗建设（集团）有限公司", "城口县", false);
            AddProjects(dbContext, accountList, "863cf7dd-cfb7-48e0-a8e1-8f0bcd72b25c", "2cd1d07f-a7c5-4fbb-a1fb-206e69004e80", "美都香榭二期", "50002720181008013", "重庆慧河建设有限公司", "城口县", false);
            AddProjects(dbContext, accountList, "84e3a4dd-cd8a-45d4-9ba9-7e91f8c282d8", "b6935e7b-42e1-4db8-98dd-0aa02b0cb7ec", "永缘·新宸壹號4号楼、5号楼、10号楼", "50002720181008015", "重庆倍广建筑工程有限公司", "城口县", false);
            AddProjects(dbContext, accountList, "86492e30-38b5-4713-973b-3bd5085f8e5b", "ccfb6514-9872-4a9e-8f0b-9eea1cbe159e", "丰都县峡南溪安置房A、B、C地块和三合街道瓜草湾拆迁安置房工程", "50002820181012005", "广西建工集团第五建筑工程有限责任公司", "丰都县", false);
            AddProjects(dbContext, accountList, "194f5e5a-e02a-4367-81a6-b2e64e03fbb4", "3ac999f2-c4c6-4818-82be-1052e37f81d6", "丰都金科黄金海岸4期（B1~B4、B14~B19-2、B31~B32号楼及车库）", "50002820181012023", "重庆中通建筑实业有限公司", "丰都县", false);
            AddProjects(dbContext, accountList, "82080d69-ed2e-450b-8d42-6f4054b849f1", "96ab24cc-8ad5-4d69-b00d-999559ce1776", "久桓城三期（F区）", "50002820181012026", "重庆市兴远建筑工程有限公司", "丰都县", false);
            AddProjects(dbContext, accountList, "38e48fa5-dda3-4d1b-a0f6-7ad1dc6dd6c2", "9351f8bc-abb3-4d9c-b30b-cffa8b914302", "帝景江山（4#、7#、8#、10#及车库）", "50002820181012032", "重庆长坪建设集团有限公司", "丰都县", false);
            AddProjects(dbContext, accountList, "63a2b75e-2979-4da6-bdcf-d0e1fe4a85fe", "185aea33-fb15-4aaa-bc5b-02138dbd462e", "恒安·金色星城（A1-A9#楼及A区地下车库）", "50002820181012034", "重庆市庆东建筑工程公司", "丰都县", false);
            AddProjects(dbContext, accountList, "ad76a06b-60f2-4986-ad06-fabb2bda29fa", "0cc0b644-776e-4ec1-986f-661ad4a860a1", "贵博·东方明珠一期（2#、3#、4#、5#、6#楼及地下车库）", "50002820181012037", "重庆展宇建筑工程有限公司", "丰都县", false);
            AddProjects(dbContext, accountList, "4a66681a-0441-44b5-9337-2016e239c506", "30d83b0a-de74-440f-abfb-ab5b219ddc8e", "龙城华府C区（三期）二组团一标段", "50002820181012044", "重庆北部双龙建设(集团)有限公司", "丰都县", false);
            AddProjects(dbContext, accountList, "198002c1-9a2a-4d88-ab23-5d3e6e92ccd0", "121659bb-73f5-4d1e-8251-a7ac8d623b1f", "丹湖时代城（一期）1#-3#、8#-10#、16#、17#楼", "50002920181016008", "重庆市垫江县建筑有限公司", "垫江县", false);
            AddProjects(dbContext, accountList, "71c7d719-2caa-4af4-9d18-3a37fe870903", "f5a6c1ab-7cec-4e37-86d0-357cf2872f71", "丹香御府商住小区建设项目总承包工程（二期二标段）", "50002920181016030", "中建四局第五建筑工程有限公司", "垫江县", false);
            AddProjects(dbContext, accountList, "106caab7-cf4d-4e17-916d-6f36d84dd74f", "d26bb952-9d36-4d36-a8d5-27c8f7c0bab6", "丹香御府商住小区建设项目（二期一标段）", "50002920181016071", "中建四局第三建筑工程有限公司", "垫江县", false);
            AddProjects(dbContext, accountList, "256c9143-31b1-4c5c-8dc9-56982c31eba6", "6bde1e0b-e022-42cd-8b80-d0630ef3d9a0", "忠县忠州中学校迁建工程暨县体育中心", "50003020181018080", "中国二十冶集团有限公司", "忠县", false);
            AddProjects(dbContext, accountList, "d0820645-9336-4510-aabf-cf86290337be", "24f5076b-5efd-4e81-8907-5cf18aa331bf", "忠州大剧场及附属设施项目[EPC 设计、采购、施工]", "50003020181018096", "中冶建工集团有限公司", "忠县", false);
            AddProjects(dbContext, accountList, "b35ffe4a-4f5a-4673-a062-80512751a0a6", "9e66f860-c34b-4c56-bb27-f15ba27c9ee6", "云阳金科世界城一期七标段（洋房D16-30，D35，D36高层G17-19，商业s1a.s1b.s2-s4.s5a.s5b.s6a.s6b.岗亭B1b2b6及对应车库土建及普通水电安装工程）", "50003120181010074", "中建八局第三建设有限公司", "云阳县", false);
            AddProjects(dbContext, accountList, "a7939873-dcdc-4939-8ad7-bec188019408", "7569eae7-c497-4fe9-a9ea-ccdf80427706", "逸合。两江未来城8区项目31-33#、35-36#、48-53#楼及车库", "50003120181010102", "重庆建工住宅建设有限公司", "云阳县", false);
            AddProjects(dbContext, accountList, "f354e059-9f00-4c79-b256-1053a11de4e0", "e550d7c8-20a6-4c0f-b74f-ac51fdee0b88", "飞洋·阳光佳苑（A1、A2、A6、A7、B5、B6、S1、S2、S5）", "50003220181008002", "重庆飞洋控股集团宝宇建筑工程有限公司", "奉节县", false);
            AddProjects(dbContext, accountList, "3fe8d6eb-9c63-48b0-b5d0-66e4e220feb3", "7f4c009d-6cfc-4fb5-95fe-730148c7dbd1", "飞洋·阳光佳苑二标段（A3、A4、A5、B1、B2、B3、B4、S3、S4、S6、S7、幼儿园）", "50003220181008012", "重庆飞洋控股集团宝宇建筑工程有限公司", "奉节县", false);
            AddProjects(dbContext, accountList, "10795e65-8c7f-4e1f-9a2d-621b98236960", "b56f35c2-ef11-4276-918b-09dddf0475e1", "滨湖上院B区B1、B2、B3、B4、B7、B8、B9、B10、B11、B12栋", "50003220181008123", "重庆市银峡建筑工程集团有限公司", "奉节县", false);
            AddProjects(dbContext, accountList, "d0517559-f694-4ff0-8a3e-06a3751af92a", "686364bf-9563-4f13-9dbb-ddd897e1ae14", "滨湖上院A区车库及商业、A15、A16栋", "50003220181008126", "重庆市银峡建筑工程集团有限公司", "奉节县", false);
            AddProjects(dbContext, accountList, "a32d6b99-2657-4391-ae59-6156b7f8f41e", "2f6821db-9319-498d-a20c-5aeb93162519", "中昂·新天地（10-07地块）（1#-6#楼及地下车库）", "50003320181008014", "北京纽约建设发展有限公司", "巫山县", false);
            AddProjects(dbContext, accountList, "1f5324f4-a60f-4f78-ad7d-bff254b31f02", "fda4f239-ca18-4ab0-8dfe-ce694b84d5f9", "亿丰国际商贸城一期", "50003320181008022", "重庆大禹宁河建筑工程有限公司", "巫山县", false);
            AddProjects(dbContext, accountList, "b9f6ad48-f6cf-41d6-9466-09a8f319cf48", "a2c3ae2d-9352-4352-9f09-e4a8bdfc2481", "巫溪县畔山悦府住宅小区", "50003420181018008", "重庆渝鸿建筑工程（集团）有限公司", "巫溪县", false);
            AddProjects(dbContext, accountList, "84223c7b-bb3c-4966-b320-684097f8da3e", "81cc06e3-d0b5-4ca9-99aa-83d4c1c693b1", "碧桂园.翡翠郡二期南区28 --37号楼及（地下车库）", "50003420181018022", "中天建设集团有限公司", "巫溪县", false);
            AddProjects(dbContext, accountList, "101d054d-cffa-4ead-9ae2-93c4bb0d7364", "97f46718-7bec-4e5f-8ba1-6e2089f121ac", "业宇峰.寰宇世家8#楼及周边车库、10#楼、12#楼", "50003520181018001", "重庆建工第十一建筑工程有限责任公司", "石柱县", false);
            AddProjects(dbContext, accountList, "f9c79ebb-0fda-4d0d-ae80-8e5688ca75b6", "2ba1c175-d4c7-40b8-bc34-bf4a04712e1f", "碧桂园天麓府一期（1-3#、5-7#楼及2#车库、8-13#、15-21#楼及3#车库）", "50003520181018002", "重庆市巴南建设（集团）有限公司", "石柱县", false);
            AddProjects(dbContext, accountList, "4b8252a5-f021-4683-a07b-89356bae0235", "d1ebf8dd-03c6-4ca3-8ce6-e3220de3cca9", "财信城二期II标段1#、2#楼", "50003520181018003", "重庆桂佳建筑工程有限公司", "石柱县", false);
            AddProjects(dbContext, accountList, "299e7b3d-629b-4ffc-9c07-96dade809b0e", "1802f057-fb33-4544-a432-6ba6ab5a42c8", "黄金水岸一期4#楼", "50003520181018004", "中冶建设高新工程技术有限责任公司", "石柱县", false);
            AddProjects(dbContext, accountList, "089936a2-2d4e-4870-888c-11df78e75c76", "8753f4f6-0f48-491c-8f6f-9be01f409810", "秀山县丹凤华庭建设工程", "50003620181005026", "中冶建工集团有限公司", "秀山县", false);
            AddProjects(dbContext, accountList, "3dcf8f89-84f1-44db-b2d4-382f1bd4e32d", "7e438a84-7174-41e1-b076-97039d2bc851", "秀山凤凰嘉苑二期建设工程", "50003620181007001", "重庆万美建设工程有限公司", "秀山县", false);
            AddProjects(dbContext, accountList, "09cbcb15-7146-4e4c-b3cb-84c1617ae70b", "c7f5c2d7-6da6-413c-ae26-6dc013b16d6b", "兴源·黄杨郡二期二标段", "50003620181007027", "湖南众诚建设工程有限公司", "秀山县", false);
            AddProjects(dbContext, accountList, "4a73a4ad-096d-4562-87c7-0ca196043af5", "fd909ad8-741a-4cd7-adc8-76a7d4dd363f", "酉阳县城投·伴山华府房地产开发项目（一标段）", "50003720181011017", "重庆康悦建设（集团）有限公司", "酉阳县", false);
            AddProjects(dbContext, accountList, "ba9fc98c-434b-43ec-9353-5780532baa5b", "a3335cf1-0177-4018-b550-a2520144a1d8", "酉阳县城投·伴山华府房地产开发项目（二标段）", "50003720181011018", "重庆项毅建设（集团）有限公司", "酉阳县", false);
            AddProjects(dbContext, accountList, "f7f70561-4820-460f-8473-d1ff01f9cb76", "bea44b92-9f50-4f07-bc11-1b4902a3b4a1", "酉阳县城投·伴山华府房地产开发项目（三标段）", "50003720181011019", "江西省城建建设集团有限公司", "酉阳县", false);
            AddProjects(dbContext, accountList, "66333e1e-9c73-480f-bf20-ccd8f55a13db", "887ed279-10fd-49de-8fbc-c23d6016d3d2", "酉阳土家族苗族自治县中医院迁建项目", "50003720181011024", "中冶建工集团有限公司", "酉阳县", false);
            AddProjects(dbContext, accountList, "cd717c9f-229d-4cac-bebe-51382292b464", "5fe8cd78-905d-4baf-bd2f-9c1b16cbc2b4", "鑫沃世纪城（二期）工程", "50003820181010019", "重庆紫东建设工程（集团）有限公司", "彭水县", false);
            AddProjects(dbContext, accountList, "44b883ea-76b4-4a66-badc-69167681e169", "1c838943-d696-47d6-90a4-9f78415e6084", "彭水新领域（二期）工程", "50003820181018060", "重庆建工第九建设有限公司", "彭水县", false);
            AddProjects(dbContext, accountList, "ff43aa40-508a-4d20-9274-5afc3856741d", "e8518591-d5e1-4e3f-85c5-e98dd87aefb5", "重庆照母山G4-2/02、G4-3/02号地块项目三期二组团1-2#楼、9#楼及部分10#地下车库（轴交：12轴-1轴纵", "50003920181008053", "重庆建工集团股份有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "4f9be9b5-55ab-49d3-a880-ea7db32a2d7d", "8ab8de09-c250-4b03-a705-cfa9487245e2", "绿地·保税中心二期二组团二标段（4-A办公、4-B办公、4-商业及地下车库）", "50003920181008165", "重庆市渝北区海华建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "636be2a6-e1d8-4929-bd29-435b7a77c3ae", "63646226-bcd5-47fa-8f95-28f9d1f0ac77", "重庆涉外商务区B区二期", "50003920181008184", "重庆教育建设(集团)有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "65e1ed34-df94-40a1-866b-8dd40296bf3f", "cb370af8-23ba-4e2f-8274-398b6192f5cf", "重庆儿童医疗中心综合楼", "50003920181008189", "重庆建工第三建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "ef62839b-a6f5-4fcf-9adc-cf751914dc47", "5806f530-5bab-4e0e-8369-060490a42b43", "恒大照母山项目三期（1-6号楼、12-18号楼及一号车库）", "50003920181008257", "重庆渝发建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "69df11a4-69b1-4b99-b7d9-aaff678735b7", "1c315c51-b761-4c8f-90f9-c0b85fb0dca1", "中国摩（重庆）项目一期I54/02号地块工程二标段（5#、8-10#、14#楼及相应车库）", "50003920181008261", "中建八局第三建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "0bf4a9d2-a34e-4a97-bc74-503c47d351aa", "df2a0382-8da4-4330-b875-84f94df10237", "北大资源博雅东二期一批次（含3-27#至3-30#楼、3-40#至3-41#楼及对应地下车库）", "50003920181008276", "重庆万泰建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "7282ebc3-5b23-4703-b377-b45bb258864f", "a4b521f8-16f1-4ff7-8dfc-6fa80ce9865e", "重庆龙湖怡置新大竹林项目一期（022-7号地块C组团）1号楼、2号楼、6-1b号楼、6-2号楼、地下车库(1~19轴/A~L轴）", "50003920181008281", "重庆诚业建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "17d97642-11f4-4d76-ae63-82bbd35451de", "6f516268-1ccf-45b0-8ce7-57083bbe97fb", "重庆龙湖怡置新大竹林项目一期（022-7号地块C组团）3号楼、4号楼、5号楼、8号楼、地下车库（19~40轴/A~L轴）", "50003920181008282", "重庆拓达建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "a3c07c8b-3425-456b-9b8e-e6234c15ec8f", "3b9601c4-f80b-40b6-8d05-0b1bde47747e", "两江总部智慧生态城吉田总部二标段（1-8#楼、11-18#楼）", "50003920181008296", "重庆市万州区地方建筑有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "2f2619a8-826c-46b1-a2e6-725af2f041e6", "af130eec-dbfa-4b37-979e-edf7c5784648", "北部新区G5G6地块项目D区", "50003920181008304", "重庆建工第四建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "9d8188aa-423e-4d45-99ff-abc658ee02bc", "fb8ac922-2997-4953-aae0-c32e78cb13f6", "保利溉澜溪项目G11/02号宗地项目三期（二标段）", "50003920181008306", "中建四局第三建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "5ab4c597-67b6-4bf4-b247-615473c91cc3", "d21bd0f5-8097-4970-86e4-44982e2248c4", "重庆市人民医院", "50003920181008307", "重庆建工第三建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "99839735-7108-4a66-bb82-79753f364d2f", "fe531234-8d63-4622-8489-8854cec61e4d", "恒大照母山项目六期（山水城38-42号楼、IV、V、VI号车库）（含边坡支护以及室内精装修）", "50003920181008332", "中兴建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "c6ed3856-4794-4941-ab6f-6493d65d99ec", "24a8cdbc-32a5-4407-bee1-36218b537a1b", "金科天元道（二期）020-6、020-8号地块一标段", "50003920181008335", "重庆华硕建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "55c10903-7a60-4c9d-87f6-804fe686fd7c", "e90453b0-306f-4a6b-9ea3-1b4c787d27cc", "棕榈泉悦来项目一、二期（B1-B9号楼、B10-1号楼、B10-2号楼、B11地下室）", "50003920181008338", "重庆拓达建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "79f04fce-54b3-4306-a22a-bb157f0ff737", "e02a8da9-46d4-4fe5-b1ea-d01fb4ec0ec8", "万科溉澜溪二期（4-1#、5-1#~5-5#楼）", "50003920181008344", "重庆渝发建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "566c8352-7f99-4594-919e-a0737ab00533", "2a5a0136-85b2-4780-ae46-9f3dc751d895", "重庆怡置北郡·黄桷水库B14-2/02、B14-4/02及B14-8/02号地块项目（B区75~87#楼、4A高区车库）", "50003920181008345", "四川域高建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "200bc1bc-3c3b-4190-9b18-13261121b6fa", "0dd6324e-357c-4815-a60e-45c9105e5620", "太阳座", "50003920181008347", "重庆建工集团股份有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "683338ce-5de0-448a-b21e-ea50a47c07a8", "1fbdc04f-8bd5-416f-992a-893480dbb6e8", "重庆龙湖创佑九曲河项目（一期2组团F06-1地块）1、5~7、12~15、17-1、17-2#楼及19#部分（地下车库）", "50003920181008351", "重庆拓达建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "cdfa9ecc-4c5e-4b0e-900a-93f170bcd72d", "f806535a-a76d-4ce3-820a-2dbb73e72b76", "首地悦来项目（一期四组团）", "50003920181008355", "中铁建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "f0919dbd-0e0e-4766-9770-cb3cdd6d7dfe", "d9c627eb-6915-4459-abaf-639092b3efe7", "金科·空港城（南区A110-1/03地块二期2-1#至2-3#、B2#至B8#楼及D2部分车库）", "50003920181008360", "重庆华硕建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "af6f5edb-2a3f-4d3f-b92a-bfd711d28114", "8c59909f-e752-41c5-ad79-7c8434ef2eca", "龙湖礼嘉新项目A62-4/03地块（5~10#、11-2#地下车库、12#门卫）", "50003920181008361", "重庆先锋建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "44aac899-fecc-443d-920c-f27e7cb05860", "673a8b8c-2e5e-430b-86eb-ba9d4984fb5d", "旭原创展大竹林项目08-07/02地块（1-3号、9-14号楼住宅、S1号楼商业及部分地下车库）", "50003920181008363", "山河建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "78cfdc7c-8bcd-4a9e-8d11-6a1d3285712c", "5fec9527-82b7-401e-870e-fd21e4e257f0", "旭原创展大竹林项目08-07/02地块（4-8号、15-16号楼住宅、S2、S3号楼商业及部分地下车库）", "50003920181008368", "山河建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "f3c7cdb5-8633-4f86-a93d-b32358f2ddb8", "e2e41fcb-ad91-4e88-925f-daf8eb6d852d", "金科天元道（二期）020-6、020-8号地块二标段", "50003920181008370", "重庆华硕建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "8689e066-fcdc-4458-92ba-b2a4785651b1", "39f013e4-faa0-4a02-8de6-77b2a4af9dde", "恒大世纪城38#、75-83#、87#、90-91#、93-96#", "50003920181008371", "中兴建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "85399464-148d-45fc-938b-8497f06b4741", "db90f28d-290f-4256-bac8-11a3d6639fa3", "金科九曲河项目（F03-11-1/05）A-1#至A-13#、A-16#至A-19#、A-S1#、A-S2#及A区部分车库", "50003920181008372", "重庆建工住宅建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d1fc8dac-a595-41a7-b65b-788f193a63ed", "04bbc8c1-e6f5-4419-b7d8-eb049f1ba932", "金科九曲河项目（F03-11-1/05）A-14#、A-15#、A-20#至A-46#及A区部分车库", "50003920181008373", "重庆祥瑞建筑安装工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "291a9a85-3de7-4853-9844-952c391281c4", "e47b8c4f-78a1-499b-a25e-3b0a33d7f9e5", "重庆龙湖创安照母山项目G17/05号地块（一期二组团一批次）11-14、16、29-39、65号楼及部分地下车库", "50003920181008377", "重庆先锋建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "7dd4be75-25ce-4bf4-9f33-cc30a2d3cfdc", "326f0914-eae0-464a-aea9-46c9b566be58", "重庆龙湖创安照母山项目G17/05号地块（一期二组团一批次）15、17-22、24-28、40、64、67号楼及部分地下车库", "50003920181008378", "重庆嘉逊建筑营造工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d74569f6-3ada-4b00-82e4-bf4c365af532", "9988127c-e7bb-4e3f-986f-4379e6edacc7", "力帆红星广场二期工程C10组团（C10、C11号楼）", "50003920181008385", "中建三局第一建设工程有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "ad53a58b-2dbc-47e4-88b7-c504a6cf8d0b", "fdd7f08e-038a-48e1-8dbf-a4dc3b9c106e", "重庆龙湖怡置新大竹林项目（二期三组团）1、5、6-14号楼和A、D区商业、部分地下车库及边坡支护", "50003920181008389", "重庆诚业建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "09828c1c-e4ad-40ea-af18-cebe227adcab", "d877810f-6790-46a9-a595-b636da08c750", "重庆龙湖怡置新大竹林项目（二期三组团）2、3、4号楼和B、C区商业、部分地下车库及边坡支护", "50003920181008390", "重庆华硕建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "01d66ebf-8cfb-4064-a167-2ea87893cbde", "8b6f728d-7891-4532-ba43-fb1f2b6a3b0a", "葛洲坝融创悦来项目三期5、7号地（D16-4/08、D18-3/09地块）5号地块", "50003920181008391", "重庆新科建设工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "350c28ad-44c6-4cc3-8ecf-d72ae50be983", "98490c05-8ae8-45b2-92f6-2e98b6ed9682", "北大资源·悦来二期（一标段）（含65-1#商业、66#、67#、68#、69#、70#楼及对应车库、配套用房）", "50003920181008393", "重庆万泰建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "0338c455-d475-4d09-ab32-f56f91806e91", "02808582-8ad9-4cac-aaa4-f6fd9f94fcd9", "悦来会展中心北辰项目一期（C05-1/07号地块（1号楼）、C09-2/06号地块、C18-6/06号地块）施工总承包", "50003920181008396", "中建五局第三建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "a9381ce7-b70f-4e40-b3c3-bf713fd4d256", "29407644-9b42-47a8-91b1-5eff221a2cd3", "江北嘴金融城4号工程", "50003920181008397", "中冶建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "1e1689f0-0a9c-46a0-84d0-e8de1a46738b", "c61419a5-61f3-48e4-a4aa-b781b6275d75", "远洋九曲河项目（1#-5#、7#、8#、49#-64#、地下车库、设备用房）", "50003920181008401", "四川永存建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "b3f69614-20ee-4289-88d4-c92b14b08c6e", "3ff77397-57f9-4b70-8c2b-bc16d812ab63", "高科江澜一期总承包", "50003920181008402", "重庆建工第三建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "b45deaf7-81ce-459b-8f3e-a7f6b0c3eeaa", "1d76131d-aaae-4a89-ad38-80cc8522db3f", "重庆华侨城项目（地产一期多层B21-1/05地块11#—24#楼、B22-1/05地块64#—132#楼）二标段施工总承包招标", "50003920181008415", "中国建筑第六工程局有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "0185b6d4-1d99-46a8-8e18-2b24bb3d7f8d", "01a54f60-7d44-485c-a76c-6f5cbaa07f7f", "金科九曲河项目（F03-10/05）一期（B-2至B-7#、B-10至B-12#、B-S4#、B-S5#楼及B区部分车库）", "50003920181008417", "重庆祥瑞建筑安装工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d634f431-2ca0-4ce4-b3c4-f5a631d21176", "893fa0c2-86dd-4957-b00d-65bf238d671e", "渝高·观澜A组团一期（1#楼、4#楼、5#楼、6#楼、11#楼-15#楼及相应地下车库）", "50003920181008418", "重庆益欣荣建设工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "52e6fe38-bdd2-4af0-bc9d-178b749178cc", "2c696f4b-c4e2-4b95-a39a-90d4b530c24c", "金科天元道（二期）020-7、020-9号地块二标段", "50003920181008419", "中建五局第三建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "a46f7f95-70bd-4cad-8e4a-5c59f61894dd", "a0d716b8-a2e9-4415-850c-3dd13a7dbeaa", "旭原创展大竹林项目（08-01-4/03）地块项目一期二期一标段", "50003920181008422", "重庆万泰建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "b62015cd-3c24-4a06-bf19-07388c9ee085", "d012e01a-a374-4032-8058-6a991666655b", "旭原创展大竹林项目（08-01-4/03）地块项目一期二期二标段", "50003920181008423", "重庆嘉逊建筑营造工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "3093830f-11e9-487f-be7c-9836a6156b8c", "a5aa9e79-2ca7-4af3-8e83-cda455543a7b", "旭原创展大竹林项目（08-01-4/03）地块项目一期二期三标段", "50003920181008424", "重庆拓达建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "c101f5a3-89ad-4420-816b-b7598f21edd8", "82a2d03e-f695-4dc0-8205-64600838df0c", "重庆龙湖创安照母山项目（G16号地块）1-10号楼、19号楼、26号楼、S1号商业及部分地下车库", "50003920181008436", "重庆先锋建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "06c2b39d-c7f7-49e7-92e8-ec53e7517202", "6b5583af-7b7a-4f3b-9dfc-fe686d2332c5", "重庆龙湖创安照母山项目（G16号地块）11-18号楼、20-25号楼、S2号商业及部分地下车库", "50003920181008437", "重庆嘉逊建筑营造工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "f8c63d69-f0c6-46b1-99b7-bec185e658d9", "6c613c30-243d-4ce8-a037-8bdf9bf6fcaf", "重庆龙湖怡置新大竹林项目（二期一组团A区、B区）1-95号楼、物管用房、地下车库剩余部分", "50003920181008438", "四川域高建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d79c4d01-e8d9-4d8f-8343-91564c14d8c9", "c6fd606d-1fa6-4a1b-9293-2709e0b9858a", "金海岸", "50003920181008439", "重庆中伦建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "97e7ca49-71fd-4cc9-9238-2fb1f2c93973", "d98f5571-02df-44cd-acff-51ced2900947", "北部新区B标准分区B18-1/03号宗地C区（五期）", "50003920181008440", "重庆对外建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "681eb615-e87c-4760-b9f4-5114fb03871d", "786b7580-2792-4c8d-93f9-1bdd8ff6d815", "葛洲坝融创悦来项目建设工程三期（D19-3）三标段（8-4#、8-5#、8-S2#、8-S3#、门岗及地下车库）", "50003920181008445", "重庆创设建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "8b15552c-833a-4512-9fa6-1fbe8335e142", "a29ac179-aad3-486b-b00a-11d421a8eb2c", "绿地·保税中心四期一标段（1、7、8号楼住宅、8-S#商业及地下车库）", "50003920181008450", "重庆华硕建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "26bc3673-d7f9-4ba2-aa2e-f53c180d5dc0", "284ecebe-8fcf-4f75-ae15-6959caa811f2", "葛洲坝融创悦来项目建设工程三期（D19-3)一标段（8-6#、8-7#、8-S4#、门岗及地下车库）", "50003920181008451", "重庆万泰建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "1df7ffeb-f0db-406e-9bef-1f4e5e76821d", "8534c2e6-f7ba-4552-a22d-c0c379df18e8", "葛洲坝融创悦来项目建设工程三期（D19-3)二标段（8-2#、8-3#、8-S2#、门岗及地下车库）", "50003920181008452", "重庆创设建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "421ca36e-d9fd-433d-9a08-2aa4980b156b", "0eaad54f-5528-422a-b0de-091dd8411cf7", "金科亿达·重庆两江健康科技新城二期二部分（E8-1/01地块北区）", "50003920181008453", "四川住总建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "eac1cc07-e91d-404e-a019-dfc3eebcf86a", "6f0d7d59-8387-49ef-9c5c-550dad01b1f8", "北部·湖霞郡住宅小区开发项目二期建设工程", "50003920181008455", "重庆两江建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "521c25e7-cf0c-4d35-8118-6200d1d4e41b", "b80e2099-0499-4111-b6ad-c36397148528", "恒大照母山项目六期（11-14号楼）", "50003920181008456", "中兴建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "6a3fde61-a077-4d06-bf98-23f16d08a1d2", "1f7a38eb-320b-49de-87d5-ffa5f38190fe", "中国西部黄金珠宝国际交易结算中心", "50003920181008460", "重庆建工第四建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "0a7fad76-e296-4907-b3b5-641f364a659b", "b4d03bbb-050a-459e-bd86-4af84f3c8255", "远洋九曲河项目（18-43#、2#门岗及相应地下车库）", "50003920181008466", "四川永存建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "dbdb66a7-59e0-4458-a977-76aa8910f007", "552cf4ca-ee39-4d45-808f-e7ae4d978e62", "中交·中央公园项目（C104-1/02、C105-1/03、C107-1/02）一标段施工总承包工程一期", "50003920181008470", "重庆渝发建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "be651cb1-9f24-4be5-8bdf-58262515db9b", "5130f8dc-781e-4bbf-aaed-8a46223248fb", "中交·中央公园项目（C104-1/02、C105-1/03、C107-1/02）二标段施工总承包工程一期", "50003920181008471", "重庆万泰建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "397ec8f4-4d7a-45bf-a93c-3f2ac6e6b5a9", "3b2e8e7c-b7ac-4d7e-acb3-83155a078a7d", "金科天元道（二期）O20-7、O20-9号地块一标段", "50003920181008472", "中建五局第三建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "240c11c5-078a-4061-b3b7-df90584a5259", "eb869153-43c2-48fa-a772-db0bc6ca500c", "远洋九曲河项目（6#楼、9-17#楼、44-48#楼、65#及相应地下车库）", "50003920181008473", "四川永存建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "31cf49a7-8e1b-4ff3-a03e-3e61555ab285", "cdbf753b-f71c-4782-9245-ae62d0fd256d", "两江新区悦来组团C分区望江府一期（C50/05地块）", "50003920181008477", "中天建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "aa15cee6-fbdb-4d4d-a1f0-9dfa821161a3", "3f191741-8b0f-4493-9e1a-0ea94e302259", "中国摩（重庆）项目一期、二期（二期I53/02号地块）一标段", "50003920181008479", "中建八局第三建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "5be28599-2e0b-41bb-ac44-537ff59c514e", "bd475eb8-8bd0-4f63-93bd-36cf747c32f9", "桥达·漫生活街区（5#楼、6#楼、7#地下车库）", "50003920181008480", "重庆桥强建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "987b9da8-f166-4b32-869c-a0c736c84e14", "7f8def54-1ecf-4095-808a-cd6a5edff630", "龙湖礼嘉核心区项目（A58-1/05地块）8-10号楼、14-15号楼、19号楼、21号楼部分商业、22号楼商业、26号楼部分地下车库", "50003920181008482", "重庆教育建设(集团)有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "802b710e-aadc-4606-884b-6fdfa354a966", "e89f45c3-0f11-4fd3-89a7-c22a2bb69ef3", "龙湖礼嘉核心区项目（A58-1/05地块）11-13号楼、20号楼、21号楼部分商业、24号楼幼儿园、25号楼地下车库及26号楼部分地下车库", "50003920181008483", "重庆诚业建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d26e5a5c-3cd3-460d-b367-f1d252a3f967", "09326459-d17c-42d1-b1c2-76d206298735", "首地悦来项目（重庆首地人和街小学校）", "50003920181008490", "中建五局第三建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "43cff751-9374-428d-9e3a-af30ffbf0db9", "ee1aaeea-f1ef-43f4-8f72-c6d96e8c44c6", "北部新区B标准分区B14-7-1/04号地块（一期）1-5、16-19、43、54号楼及对应车库", "50003920181008492", "重庆中通建筑实业有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "ef897b94-3321-4e1d-9eff-c75f3184206b", "da0531b6-3af5-411d-9a07-d0311433dca7", "绿地·保税中心四期一组团二标段（2#、3#、2-S#商业、3-S#商业、部分地下车库）工程", "50003920181008495", "重庆欣耀建设工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "95346cf8-0549-4374-b34c-d4aeb67f83a5", "dd39ca96-e2cb-4d1c-a90a-3e6c049b866e", "中央铭著（C80-1/02）", "50003920181008496", "福建宏盛建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "6f982f14-c363-448e-aaa0-b890b101b0f0", "45221b2e-71e0-4fe1-8fd8-5bda2f6eb826", "寸滩项目A-34-1-16号地块一期（标段一）（1#、2#及其地下室）", "50003920181008497", "中铁城建集团第一工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "a8263e21-7ae8-4ad7-9c57-ef21797ebc7a", "eb24e459-d291-4305-9bd8-e2911854cf90", "寸滩项目A-34-1-16号地块一期（标段二）（3#及其地下室）", "50003920181008498", "中铁二十三局集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "7f4d1570-fafe-4fa0-8886-bfd1205a2683", "f05e9ec2-fb34-45b5-8a0b-102096d1866e", "万科城四期G26-6地块项目", "50003920181008502", "重庆渝发建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "a16b9e02-9996-4cf8-abcd-f2fd9d2dbab8", "f8878702-aefe-43f3-95e4-26184d529097", "盛泰礼嘉项目（A29-3号地块）7、9、11号楼及部分地下车库", "50003920181008505", "重庆华姿建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "568e7a84-a99a-4e0c-bc25-9fafb03079c2", "cf5d081e-03af-4b6c-911d-070ba589e7e1", "金科御临河（一期）H33-4/01地块（4-1#至4-15#楼、D4地下车库）", "50003920181008506", "重庆建工住宅建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "ce380c32-d5fc-4cb5-a80c-8bd9085a35d1", "00566373-034d-4277-ba32-60e77ec78b57", "金科御临河（一期）H32-3/01地块（3-1#至3-14#楼、D3地下车库）、H39-2/01地块（5-1#、5-2#楼）", "50003920181008507", "中建八局第三建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "11916bef-e9fd-41e7-ac60-360b89e585da", "d4b6177e-338b-42da-a63a-312ac6a07250", "恒大世纪城B1-1/03地块100#~105#、III号车库", "50003920181008508", "中兴建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "65ef206d-c670-485f-a188-03bb49414f86", "57ffbbf4-fef3-47c3-b09c-0dd79a4394ff", "龙兴组团J标准分区J46-1/01、J47-1/01号地块项目（一期J47-1/01号地块）二标段", "50003920181008510", "重庆华硕建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "9e6ed9e6-b448-4029-a2b9-3fe7cadce597", "09eed80e-e296-4f80-a9b4-d443f431bca9", "金瓯.理工国际项目二期工程", "50003920181008514", "重庆达康建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "79cd78bc-5863-4faf-8803-db6819e7800a", "096b5e4b-bcab-4650-ad0c-658809ad8583", "重庆市人和中学校", "50003920181008516", "重庆建工第四建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "e53a1c4f-3068-49ad-83eb-7b7179543807", "04892f7d-8830-41e2-b9df-b361b34f9226", "盛泰礼嘉项目（A37-5-1号地块1-12、物管用房及地下车库）", "50003920181008517", "山河建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "f17d8fe6-5278-453c-8db9-98325b786f97", "c45c0de6-7b55-4ee7-a74e-13c598c85f42", "旭原创展大竹林项目（08-04-1/02、08-04-2/02号地块）（北区1-13号楼及对应车库）", "50003920181008519", "重庆嘉逊建筑营造工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "23f9977d-5893-43e9-9748-3d44963ceea9", "dd583040-8d0c-4755-9626-9709b17e8659", "重庆龙湖创佑九曲河项目（二期1组团F05-8/05号）一批次", "50003920181008520", "重庆教育建设(集团)有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "1ce6bd72-4343-4497-9a05-1c9d0137c16f", "f3423d82-22ab-4146-b207-0885aea7908c", "金科天元道（一期）023-1号地块1-24栋及地下车库", "50003920181008524", "重庆建工住宅建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "3ee1c57d-bb93-4668-a43c-c38940e90fbc", "9b01cc8d-7abd-4bce-9c69-c2803cf169fd", "金科九曲河项目（F03-10/05）一、二期（B-1#、B-8#、B-9#、B-S1至B-S3#、B-29#、B-30#、B-32#、B-33#、B-36#至B-39#、B-S7#至B-S9#及B区部分地下车库）", "50003920181008525", "重庆佳宇建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "c446c65c-bb4c-4afc-9b64-c525fca3078d", "95a20b94-e879-4aa7-8921-c0c830d7e261", "金科九曲河项目（F03-10/05）二期（B-13#至B-17#、B-21#至B-28#、B-31#、B-34#、B-35#、B-S6#、B-S10#及B区部分地下车库）", "50003920181008526", "重庆龙润建筑安装工程有限公司", "两江新区", false);

            AddProjects(dbContext, accountList, "14feb4b7-321e-43bc-82f6-1c62b08ef190", "28a9c7e7-3a69-4b15-86ad-7a789e1bb0b7", "龙湖礼嘉新项目四期A63-2地块1#~4#、S3、部分地下车库", "50003920181008531", "重庆建工第三建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "339cca87-ee66-47d5-9649-afb7eda81b05", "c980b858-86cb-4ed2-b075-05abacc40762", "华润·公园九里（C65-3/04）二标段", "50003920181008533", "华润建筑有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "18cd2141-b78a-45c5-ae47-51499dc37bbd", "d04e0f1b-c04f-436e-a2cc-304e784a94ff", "渝高·观澜A组团（一期）2号楼、3号楼、7-8号楼、16-22号楼、24-25号楼、27号楼及相应地下车库及二期", "50003920181008534", "江苏省建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "74d5423f-8c8c-4ec3-b278-1ed36b85aaa7", "b83615ec-103d-4c47-b791-5e91cc3fdc2e", "北京城建·龙樾生态城（C30-2/06地块）二标段", "50003920181008535", "北京城建九建设工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "1a96ad2a-61ac-4e85-a9b2-2b64284cc44f", "e2b7a5b9-17de-441e-ba05-7b538f950e8d", "中国摩（重庆）项目二期I53/02号地块三标段（18、19、26-37号楼及相应地下车库）", "50003920181008538", "中建八局第三建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "7eeeefa3-79a7-4940-97f1-ff1f725166a8", "78905243-f924-4165-b0ff-505359e7d86a", "重庆华侨城项目（地产一期高层B区）B1-B7", "50003920181008539", "中冶建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "3ddce472-8c1f-475c-992c-41bb86bbf8a7", "9fd6666e-8353-4511-9847-e9c2830af4ef", "两江新区悦来组团C分区望江府一期（C48/05地块）A区二标段（1-2#、4-9#、12-16#、门岗M1#及27-1#、27-2#部分地下车库）", "50003920181008543", "重庆新科建设工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d6a0df63-6b70-4bec-824b-2ba4f9bbd721", "f257d837-8edf-4046-b384-94649b6267e8", "鲁能·星城外滩5#地块1#-10#楼、S1#、S2#楼及附属车库建设工程", "50003920181008544", "中国建筑第八工程局有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "4a024883-a2a8-4658-8bc1-392381fd1ba6", "3c984759-a61b-47a0-8807-5d21632b1716", "国瑞·两江城（一期）3、5、6、10、11、12、32、32-1、33、33-1号楼及地下一组团车库", "50003920181008545", "重庆渝建实业集团股份有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "45520d80-403d-4fce-ac0d-9ef2805f1491", "3d56e5a1-db03-4820-8138-3dd0f8158802", "两江新区悦来组团C分区望江府一期（C48/05地块）A区10#-11#、17#-26#及27-1#、27-2#部分地下车库", "50003920181008547", "中国核工业中原建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "b22634d6-d9e7-46bf-85ee-aa2439c4a577", "2f4baa29-fd10-49a6-beb9-8f8f0f368e4a", "晟景台项目1#、2#、13#、1号车库、农贸市场部分项目", "50003920181008548", "重庆一建建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "f6284e5e-72d0-4a76-90a8-7a8bafc3a1a5", "2977a90f-9d7d-4130-b85c-669843151fea", "重庆华侨城项目地产二期高层A69-11/06号地块", "50003920181008549", "中国建筑第二工程局有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "f863cb06-a57b-44b2-9c90-58b5c67f254d", "272213f8-f2a6-4845-be48-d5eae668c0bf", "南极·凤麟苑（下沉式商业广场）", "50003920181008550", "重庆山海建设(集团)有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "50b2d4b4-1929-4a90-b4cc-8b9f0e55c3c1", "8b368a31-db76-449f-912f-76341b18ca0b", "金弓动力综合发展园", "50003920181008553", "重庆耀文建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "2c39e7c9-a098-488b-a1c4-198e882b18f0", "6b6087f6-9411-43d5-ba07-5ae84071d603", "民生两江新区综合物流基地项目（F30-1/02号地块）", "50003920181008560", "重庆建工第三建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "142fa3bf-e4f9-45aa-b7cf-e26eb9f895b2", "f70e6b72-553e-4f60-a664-23d0631c123f", "北部新区B标准分区B14-7-1/04号地块（一期）44、51、57、58号楼及对应地下车库", "50003920181008563", "中天建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d5078c61-b49d-4b99-9a8a-54ba8ee8782f", "cf7c0109-93fc-42db-8b7d-880f9107472b", "华御澜湾二期（F01-9、F01-6号地块第一批次）", "50003920181008565", "重庆华姿建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "34e15db6-eb48-4f7f-81bc-ab2c1619958c", "4165f096-c3d3-4a2e-9e96-3ccef0fb7d13", "龙湖礼嘉新项目四期A63-2地块5#~8#、地下车库", "50003920181008567", "重庆诚业建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "5327250f-97e7-4255-ba1b-ef8938ef2bce", "2ab58fdd-fba5-4507-a063-4ab11f559511", "大竹林G标准分区G20B项目II标段", "50003920181008568", "重庆诚业建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "2014730d-875b-41ce-ab04-f9a4471e308e", "f2bb40be-9e93-4bfc-bb72-b666bbfc3989", "北部新区B标准分区B14-7-1/04地块一期（碧桂园中俊·天玺）6-12#、14#、20-32#及对应地下车库、大门", "50003920181008570", "重庆中通建筑实业有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "f1dca988-adf6-4739-861c-601fb8079036", "70503d8c-7dc3-4044-a285-3559c900c7b9", "金科亿达·重庆两江健康科技新城（E3-1地块6#、7#楼）", "50003920181008571", "重庆华硕建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "727f4410-b2d8-4ef5-8dfd-6c442f62816b", "2c9b39f3-2fe8-4a84-86d9-7fdcf8c267c1", "龙湖礼嘉核心区项目（A57-2/05地块一批次）", "50003920181008578", "重庆诚业建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "3769ee0b-e48a-43f1-bef8-3ca72b046f31", "af2c8275-9efe-4f70-b265-ce0b813b9a1f", "盛泰礼嘉项目（A29-3号地块）1-6号楼及部分车库", "50003920181008579", "山河建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "82ec8175-10b2-4076-bdf4-d4cc9e7ec39f", "58563f25-a138-4e53-b6a1-ed0dd8e828e4", "北部新区B标准分区B14-7-1/04号地块一期(碧桂园中俊·天玺）33-42、45-50、52、53号楼及对应车库", "50003920181008580", "中天建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "c8e70a5f-e165-4494-96ad-2a7c01a750c1", "77102184-f810-42cb-8495-55f6ea7be883", "万科溉澜溪项目（G12-1/02地块三期10#~13#楼及001#车库部分）工程", "50003920181008581", "重庆渝发建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "43eb2af8-4438-40ad-b07b-9611a32742fe", "e4db60a7-99f0-4d66-85b6-9b429fa93deb", "北大资源·悦来二期（二标段）（含57#-65#、71#-74#楼及对应地下车库）", "50003920181008592", "重庆万泰建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "3c478d86-4b8a-4852-9500-36ba6d130f47", "99225ffc-4a4e-4236-87b9-bd139922872b", "旭原创展大竹林项目（08-02/02号地块）1-4、9-17、49-51号楼及对应车库", "50003920181008596", "山河建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "94ef69c3-f2bc-4dee-9e82-44c7796e1c31", "d9c2742e-dacc-4920-aa2a-2abf89fd8ddf", "旭原创展大竹林项目（08-02/02号地块5-8、18-48、52号楼及对应车库）", "50003920181008597", "山河建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "4aa067eb-640d-4112-805b-50751162a412", "c119d879-0a29-4148-a235-9e360965152c", "重庆华侨城置地项目B20-4/05地块一标段", "50003920181008599", "中国建筑第二工程局有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "99f48534-7985-450e-91be-380af086fd89", "c8e7ffe2-0164-49e0-ad49-f2ce56b18699", "重庆华侨城置地项目B20-4/05地块二标段", "50003920181008600", "正威科技集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "85cdcfc0-4738-4a62-8f95-0890c67b9873", "4bcfd9ff-13a7-4ef0-a6e9-aeb99777e895", "鸳鸯公交站场上盖物业综合开发项目", "50003920181008600", "重庆建工第九建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "aaa1e93c-09a2-423e-ad47-f33f8fd50030", "71ce10d7-93be-4663-a6f7-d5ba5ad26841", "北大资源·博雅东二期二批次", "50003920181008601", "中建一局集团第五建筑有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "017febd9-9de5-46a2-a075-eeb141dd2f41", "091b823b-7207-49a2-96a9-4e7810fabd6e", "格力龙盛总部经济区商业一期（P22-2/02地块）", "50003920181008602", "上海建工一建集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "444ef822-7fe7-401e-b502-71314077e7c5", "0450ce86-a7c6-493c-9f99-ab6cfc210de9", "旭原创展大竹林项目（08-04-1/02、08-04-2/02号地块）南区1-7号楼及车库", "50003920181008604", "重庆嘉逊建筑营造工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "c54bfd94-478d-4c3e-8f16-a3bac104d002", "d3dc766d-adf3-435c-8a16-bec9ba99483f", "华商悦江府（D04-1-1/07地块）二标段（3#、4#、5#、6#、7#、8#、18#、19#楼）及对应车库", "50003920181008607", "自贡华兴建筑安装工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "9bc7ec29-c8e3-4bf5-8b36-4d11bcf6718b", "71431567-09fe-42a8-b373-0ea36b21d543", "金科御临河（一期）H40-1/01地块（6-1#至6-16#楼、D6地下车库）", "50003920181008612", "中建八局第三建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "099febd3-00a1-438f-95c1-ecd99ff067ea", "10025524-4e2e-4263-8e87-50653b2f7964", "悦来会展中心北辰项目二期（C05-3/07地块、C18-1/07号地块）施工总承包", "50003920181008613", "中建五局第三建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "ca087b1f-72c7-474e-bcdb-171e596d4d5b", "4e2e8d03-e409-47d5-9874-5f947f06ade1", "金州苑三期", "50003920181008614", "重庆建工住宅建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "1401ac34-710c-4107-ad66-3977b755214c", "03027608-4c71-4222-9a54-4c1a8dc72a9e", "龙兴组团J标准分区J46-1/01、J47-1/01号地块项目（二期J46-1/01号地块）二标段", "50003920181008617", "重庆市巴南建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "c53b673d-76c0-416a-81ea-c3951046a325", "849eea0f-0cdf-49bc-a931-ea49882b8495", "重庆华侨城置地项目B20-4/05地块三标段", "50003920181008619", "陕西建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "7ac8e08e-72e4-4e1b-8c4f-21c4b4b57509", "4157b9d1-d3ee-43f9-b22e-daec4826f1c6", "北京城建·龙樾生态城（C30-2/06地块）一标段", "50003920181008621", "北京城建北方集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d6657429-57e6-45e5-84d9-7d9bf2c0c81a", "e2450e79-b059-438d-9ba3-011cb711b345", "保利溉澜溪项目G08-4地块、G11/02号宗地项目（G08-4号地块）（二标段）C3号楼、C4号楼、C5号楼、C-2号楼对应车库工程", "50003920181008624", "海天建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "561d31f5-549b-49d5-83b1-8d49c628c259", "e8232c70-b17b-435e-9005-8134f6a1a5ca", "重庆怡置北郡·黄桷水库B14-2/02、B14-4/02及B14-8/02号地块项目（C区96~111号楼、6号车库）", "50003920181008627", "四川域高建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d6cc5450-8c39-4ace-9a8e-446c471c05b0", "0de356c8-5081-4dbb-880c-6cff06219b02", "重庆华侨城项目（B21-2/05号地块）", "50003920181008629", "正威科技集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "6b11e3a0-d1da-4491-ab65-5ae6a9cf530e", "79b65367-a067-4706-a9d0-dd8e43063f65", "万科溉澜溪项目（G12-1/02地块三期二标段7#楼及对应001#车库部分）工程", "50003920181008631", "重庆渝发建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "8e3f4100-a28c-42e5-8945-5255f32ac84e", "0dc4f8d5-923e-4c8d-a819-081118e2b27d", "重庆龙湖创佑九曲河项目（二期2组团F05-16地块）1#~18#楼、M1#岗及地下车库", "50003920181008635", "重庆建工第三建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "bf60b1a1-31c4-49c4-a33a-330296a60714", "58da4ba3-7974-491c-afed-e7587e05b59a", "保利溉澜溪项目G08-4/02、G11/02号宗地项目（G08-4号地块）一标段 C1号楼、C-1号楼、C2号楼及对应车库工程", "50003920181008636", "海天建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "6b92f16d-beb4-474c-9b87-1da38e5d7bf6", "18a51a14-6d53-4557-82b8-e8a2af26c8d1", "中交·中央公园项目C108地块总承包一期工程（1、3、4、7、8、9、11、12、13号楼及地下室）", "50003920181008637", "中国建筑第二工程局有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "974211ff-21de-44fa-8854-08e2684adfd7", "6be58727-6e30-40c1-8314-ff7affd056c4", "两江新区悦来组团C分区望江府（C58-1/06地块）一标段（1-4#及部分地下车库）", "50003920181008638", "重庆市巴南建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "fdd187aa-0a5a-47e4-98fa-2a44696ac5fd", "1a7cee98-6057-4d94-b321-d337ba78a1b8", "两江新区悦来组团C分区望江府（C58-1/06地块）二标段（5-9#、门卫1、门卫2及部分地下车库）", "50003920181008639", "重庆市巴南建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "8811df35-a08c-4869-9948-ac0498f7d2a3", "f9ee5ce8-6c9e-4a02-ab65-17e5caa7ad76", "两江御园项目（F13-1、F14-1号地块（一期））G01#-G30#", "50003920181008641", "重庆建工第三建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "a6c50e88-1518-4a66-8b59-4fa48877e91a", "94c3ed51-3594-4259-9020-c33001da7f8e", "两江御园项目（F13-1、F14-1号地块（一期））FD01#、F01#-F41#", "50003920181008642", "重庆教育建设(集团)有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "a8f4ea23-157d-478f-a951-8384621713e6", "c9917ea3-65ac-4c3f-92d2-b250a63bbca6", "华夏航空培训中心（学校）项目一期工程", "50003920181008644", "重庆中科建设(集团)有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "7b6ea2b3-6ed4-4133-8078-c2364961287c", "9d38165b-c488-4e32-8de4-fcef48ca9f2d", "葛洲坝融创悦来项目建设工程四期10-1、10-2地块（D17-4、D17-5地块）8#-12#、门岗2、D2#车库", "50003920181008647", "重庆创设建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "1a895f97-3f63-4596-8e06-a2dfde60741d", "98a0c832-114c-424b-a5f8-19a53f61fdd9", "重庆北部新区金州初级中学校工程", "50003920181008648", "重庆建工第九建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "90b4b725-dc7e-44f2-8aff-b32a08611c77", "eeb0656c-be0a-4b6c-988e-9f3b15262777", "葛洲坝融创悦来项目建设工程四期10-1、10-2地块（D17-4、D17-5地块）1#-7#、门岗1、D1#车库", "5000392018100865", "重庆创设建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "1c76dba8-432d-4d15-8000-954d927dc5c2", "6c2a2f96-cd02-4a8c-b611-e7ce9df4b39a", "大地工谷标准厂房（二期）37#-40#、 45#-46#、 50-54#、 57#、 58# 及地下车库", "50003920181008654", "重庆宏鑫建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "2ee68628-ed4f-4c0f-a577-1ac658015047", "d4598db4-e704-4e10-a034-ad1faea42cd4", "力帆红星广场工程B2组团（B3-2、B3-3、B3-6、B3-8、B3-11、B3-14、B4-1~B4-12、B5-9~B5-15、B6-1~B6-10、部分车库）和力帆红星广场二期工程C13组团（C13、C14、C15、地下部分）", "50003920181008656", "中国建筑第七工程局有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "63fe84c2-77a5-4a06-a95a-d1e7e1992bc2", "5a1506c7-c4b2-48a7-aa6c-6899d03ce268", "水土污水处理厂二期扩建工程土建及设备采购安装调试工程", "50003920181008662", "中冶建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "02e0c61c-227f-4e89-8ccf-35f58cbc6ded", "7869696a-db68-4a81-a926-5c7369fa5129", "联东U谷.重庆两江新区国际企业港二期5号地二批次（1#、6#-7#、12#-15#）", "50003920181008664", "重庆慧河建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "e1928563-d1c9-4ad3-a2de-8df14a17f3a1", "cdf24d6f-957e-40f6-954a-49367146e995", "金科亿达·重庆两江健康科技新城（E5-1/01地块）", "50003920181008666", "重庆海博建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "46429ccf-a1db-448f-8dba-24cb04109150", "712d3f39-13fe-4512-9fae-af84a710806b", "两江国际新能源汽车创新创业孵化园（定制厂房六、定制厂房七、门卫房1-4及地下车库）", "50003920181008669", "重庆建工第三建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "21cc97b2-2113-4b05-9c5d-bee31205de11", "8c6ee83d-9a33-4889-bd54-33ae3b46a649", "中交·中央公园项目（C106地块）1-1、1-2、1-3、2、3、4、5、6、7、8、9号楼、地下车库及设备用房", "50003920181008673", "中建八局第四建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "b0264104-cfdd-4f20-bf10-333c40b01dcb", "0f3c0b7b-ba2f-42da-99d9-5b17bde00253", "绿地·保税中心四期二组团5#、6#、地下车库", "50003920181008679", "重庆华硕建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "165541b5-e9d2-46e2-bda3-08b98b35afc3", "eeccdaa9-9c6e-4eed-a123-ce8e53f6c5cf", "联发·山水谣（三期）5-9、32号楼", "50003920181008680", "重庆大江建设工程集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "56b3b658-473a-4d1c-80dc-5b7ec78bdb87", "34f93894-6c94-4c6e-ba11-7e9aa3358482", "高科江澜二期一组团", "50003920181008682", "中冶建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "cf7927b5-414a-4fc3-b980-4483ee40cebb", "eeb44ce2-a6d3-48f5-af7b-ec3b2c59b5cb", "龙兴组团H分区H46-1/01、H47-2/01、H48-2/01、H50-1/01、H51-2/01、H52-2/01号地块项目（H46-1/01地块）", "50003920181008684", "中建四局第三建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "a554127b-0d90-4fa1-8c35-a76a2405f11b", "ce172c1e-0de9-4912-868c-6f5985cc57f4", "金科御临河二期一批次H45-1/01地块（8-1#至8-3#楼、D8地下车库）、H45-4/01地块（9-1#至9-14#楼、9-S1#楼、9-S2#楼、D9地下车库）", "50003920181008685", "重庆建工住宅建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "fcda3535-423c-4013-a856-48eec0e8781b", "4901af61-0c6b-4386-8f52-1ddc9465ac73", "葛洲坝融创悦来项目建设工程D分区D16-1号地块", "50003920181008686", "重庆市巴南建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "dcbd68f5-4043-479f-b37b-0df085b45e8f", "4e384225-c49b-4888-9a6c-fc588f23c6b2", "碧桂园龙兴国际生态城（H34-1/02地块一期洋房)", "50003920181008694", "浙江新东阳建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "5815e82e-30a3-4c52-ab5c-f1e20e27ccd2", "e0fc5180-7ac7-41a0-b749-2abd274e2d7d", "天蓬樾府（C51-3/01）1#、2#、7#、8#、9#、10#及对应车库建设工程", "50003920181008702", "重庆桥强建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "73132c3f-6cd2-4e02-96e7-f6ada9c16c04", "7c01090f-cfe9-4368-9d21-969cf7d18a11", "寸滩项目A-34-1-16号地块二期（15#楼）", "50003920181008707", "中铁二十三局集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "6125af1f-029f-4a92-836d-8292aeb12a25", "9d2e6a24-a44f-4400-9151-8a9a0638f093", "雅居乐富春山居一期2号地块（B1#-B5#、B9#部分车库）", "50003920181008708", "山河建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d547a819-6b88-4371-aa00-967d31ab754c", "e0e55da4-c8cf-4741-9095-459eec4f5d0c", "龙湖龙兴项目（H60-1地块）、（H61-1地块）", "50003920181008709", "重庆拓达建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "526c29db-415d-49c2-9628-caf5daed09a9", "4fa2c944-8622-422e-862a-d6d0d677bcf4", "龙湖龙兴项目（H66-1地块一期）", "50003920181008710", "重庆华硕建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "cead35e4-fd08-46e5-ba55-029d6908b0c5", "b14442dd-0efd-477c-8d31-c205cc48b56e", "中国摩（重庆）项目I49-5/03、I50-1/03、I51/03号宗地（I51/03地块）（一标段B2-B6、B8-B14、S3、S4、B2#地下车库）", "50003920181008711", "嘉兴大源建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "fd5460e5-0517-4050-a7f1-d3055641b1dd", "e46067c3-e767-40e5-8853-00ada1b49797", "中国摩（重庆）项目I49-5/03、I50-1/03、I51/03号宗地（I51/03地块）（二标段B1、B7、B15、A8、A10~A12、S1、S2、S5及相应A1#地下车库、南、北门厅）", "50003920181008712", "嘉兴大源建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "014d5f2b-aca1-4195-b168-40cde1709613", "26d86138-6da1-4006-bc94-a4a694f70030", "鸳鸯派出所业务用房新建项目", "50003920181008713", "福建省中兴建设发展有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "bb4a4016-0edc-4cdb-891a-3ee3a457d483", "b8846f5b-19c2-4042-af24-01511be8b634", "两江御园项目（F12-1/03地块一期）（H03#- H04#、H16#- H17及相应车库）、（F12-1/03地块二期）（HS01#、HS02#、H01#、H02#、H18#、地下车库）", "50003920181008715", "重庆教育建设(集团)有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "7ca2da85-1cb4-439c-8442-bacb9838fdff", "dea8e492-efbd-4af8-a050-d78617d027e6", "两江曲院风荷项目（C04-1/01地块）、（C05-1/01地块S01#楼）", "50003920181008717", "重庆建工第三建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "5ce45826-dbfd-47a9-8100-26cc7585a4f6", "85140fdf-3cb6-415f-956d-4e3984a336f7", "盛泰礼嘉项目A37-2号地块", "50003920181008718", "重庆华姿建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "14bf255a-f2b9-43a0-9905-f1777ef82f68", "bb9c2039-5074-4c3e-8405-2215f4fdecff", "华润·公园九里（C69-1/02）", "50003920181008720", "华润建筑有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "3e24816a-0b5a-44b0-a31b-35e11ac5b9b6", "c6dc504c-b75f-4925-8e3c-c844d8ba7f68", "万科园博园项目（一期）（C28-2-3/03地块）", "50003920181008722", "四川大诚建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "2f4b8ec6-6845-44ec-98d7-129b54e2a701", "c1af64cb-e801-4aa3-988f-8d2478775da8", "北部新区B标准分区B14-7-1/04号地块（二期）", "50003920181008723", "重庆中通建筑实业有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "f41a68f3-1753-48f9-a2bd-1ab4568aef2b", "15447177-c03a-4399-8e70-c14b50bd2d50", "金科天元道项目商业地块（二期）", "50003920181008724", "重庆建工住宅建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "1f08c063-ce80-47ba-a6f3-3daef1a0bf49", "f232e17f-b029-45d2-b167-0d4af7b78374", "龙湖礼嘉工程三期28号楼", "50003920181008726", "重庆教育建设(集团)有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "16d3bbfb-b669-4af6-aed7-47bc83349537", "7763067e-acdb-4d1f-98b4-db8578f88642", "重庆龙湖怡置新大竹林项目（二期二组团A区）8~18号楼、22~25号楼、28~31号楼、41~42号楼及部分1号地下车库（A区）", "50003920181008727", "南通德胜建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "184b2a30-de51-45e3-8ca3-97213117ec40", "46202a64-98f2-49c7-89be-addaa1e44d5a", "重庆龙湖怡置新大竹林项目（二期三组团）2号楼、B区商业、部分C区商业及部分地下车库的剩余部分", "50003920181008728", "重庆诚业建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "350090de-8f88-404c-957b-5dca6069e275", "17b51522-41a8-47c2-85a5-1fb90c9ac1b9", "中央铭著（C77-1/03）", "50003920181008730", "福建宏盛建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "495d5091-211d-4f3e-9602-7858eb5e410b", "688cff29-8ca7-442e-848c-57465c337ab3", "雅居乐·常乐府（A21-4-1/06地块）2-14#—2-24#楼及地下车库", "50003920181008732", "重庆拓达建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d98af18a-3581-4951-b98d-495b5a55f2c8", "0ef63a18-d183-4f20-90d6-f399a757ecb4", "雅居乐富春山居一期2号地块（B6-B8#，部分B9#车库）", "50003920181008740", "中欧国际建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "01e3a5fa-4516-44a5-bdaa-a355ba89c32a", "f13b0318-a149-426f-b9de-175539c36af1", "重庆两江新区龙兴住宅项目（H83-1/01、H85-1/01号地块）", "50003920181008743", "重庆建工第三建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "fb5b54aa-a17b-4a4c-8c2a-8e82eb85c633", "02627e5c-c3a8-42b1-a191-9c27c603544c", "重庆龙湖创鑫新照母山项目", "50003920181008744", "重庆拓达建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "f41e45d6-eccc-4d5a-b999-98105d5233ba", "c5944358-c900-42a3-8791-dec95c48a1ad", "重庆两江新区礼嘉组团A标准分区A09-4-1/07号宗地项目（1~11#楼、S1~S5#楼及地下车库D2-U轴至D3-AP轴交D3-1轴至D3-31轴）", "50003920181008747", "中国建筑第八工程局有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "4dd3c2ae-fe45-49be-ab89-6ffac39b88b3", "d75f8951-9893-4238-9acf-d55070ebcf62", "重庆两江新区礼嘉组团A标准分区A09-4-1/07号宗地项目（12#~27#楼及对应地下车库）", "50003920181008748", "中国建筑第八工程局有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "0badb1d4-4f23-4a02-a2ad-eaacb977fcad", "753abc02-9f1f-4e09-ab80-9146a51e34bb", "礼悦江山（1-5、8-10、12、13、24#楼及地下车库）", "50003920181008749", "福建省晓沃建设工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d6bd40da-75e7-4dce-a63d-71ec7358f0cf", "ac0f80c7-724c-4228-8895-3258e1e8e524", "万科园博园项目（一期）（C28-2-1/03号地块、C28-2-4/03号地块）", "50003920181008750", "重庆城鹏建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "6cec6d90-5293-471d-b4e2-499729f3df7b", "904c3d6c-2f54-40b2-adac-946f88370a12", "中交·中央公园项目（C110、C111地块）2-1~2-4号楼、2-11~2-15号楼及对应地下车库", "50003920181008752", "重庆昌林建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "9a6ec203-b60b-4df6-9623-8dee7fe449cc", "83cad6b6-4664-4771-b5d2-7a1603b39c9e", "中交·中央公园项目（C110、C111地块）2-5~2-10号楼及对应地下车库（2-16号楼）", "50003920181008753", "重庆昌林建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "bf1bb58b-8e9e-4eed-a8c1-dab5535960c3", "473a936e-9c6b-45e6-9dca-0fcc8a286367", "中交•中央公园项目（C110、C111地块）3-3~3-4号楼及对应地下车库（3-5号楼）", "50003920181008754", "中建八局第四建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "fd3f84c2-eca9-4236-a4ba-afde154c21f8", "d6d8867e-21a6-4c95-8c3d-7ed280ff8e83", "盛泰礼嘉项目（A39-1号地块）", "50003920181008757", "重庆华姿建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "96c3712e-a087-40fd-9d69-c90fcf115863", "02ed0699-3454-4b9e-904c-1e3aff2f719d", "两江新区悦来组团C分区望江府项目（C41/05地块）", "50003920181008759", "中国核工业中原建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "ec01afe3-2992-4046-a764-bd603bdbcb53", "e576ef0e-6db3-41ea-a0f4-6cdeb81cabf3", "万科园博园项目（一期）（C30-1-1/03地块）", "50003920181008765", "四川大诚建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "21267ddf-34d3-4c41-8621-a35dc6af9001", "a6af880b-8e34-4c9d-85cf-354322706df2", "礼悦江山（11-1,11-2,14~16,17-1,17-2,18,19,20-1,20-2,21-1,21-2,22,25,26#楼及地下车库）", "50003920181008766", "福建省晓沃建设工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "97adde62-bb81-4b7e-aa9a-a655f0fcd8a9", "e77a36a5-1be9-43e9-8ebd-9452386ab7e8", "格力两江商住（P25-1/01地块）", "50003920181008769", "上海建工一建集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "dd4f1492-e83b-4bd2-a9b8-b92b50542a9d", "0593c01f-ab37-40e3-98b5-cd60642072d7", "格力两江商住（P26-1/01号地块）", "50003920181008770", "上海建工一建集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d8a67075-5860-402d-b257-1bfd8dbe38cd", "8c150988-43dd-4c0a-8702-18c09e11796b", "龙湖龙兴项目（H65-1地块)", "50003920181008782", "重庆华硕建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "ef49cf90-318e-4d64-88f0-d164237ac684", "859fc2e8-f50a-43c7-8745-cb6822b942f5", "北部新区经开园A23号（晟景台）6#、7#楼", "50003920181008785", "重庆一建建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "0485a5a9-7d0b-40fb-9ddc-c17e4c2dc6e2", "e3b253bd-328f-4e57-bff3-ba67304d6895", "重庆龙湖创安照母山项目（G13-4-2地块）", "50003920181008786", "重庆先锋建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "504d5662-1b8b-4214-b928-2c45b9dcdced", "9417aa12-b9e6-4e20-99e8-b7baa7228715", "中航两江体育公园（E07-01/01）", "50003920181008802", "中国航空技术国际工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "6c8cfeca-76f9-4997-b8c0-80c6f2be5e5f", "3926a3ee-aef0-48b8-b1a1-1fcb60d1e562", "北部新区古木峰立交工程F匝道", "50003920181008875", "中铁隧道股份有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "3c83444b-cb11-4101-8249-3d3fce64f698", "ea9debdd-cdad-4baa-a579-1a7809b781de", "新南立交工程", "50003920181008903", "重庆建工住宅建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "518170d1-7f8d-45e1-af07-89dfc6f13576", "6c750f5a-34f5-4d4e-ac9f-5d965ab221bd", "北部新区礼嘉商务中心区L42路西段道路及配套", "50003920181008907", "重庆建工第四建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "a168fd53-b4bf-4d2a-9e4b-4f34a8c6f7ea", "2d575a47-d1bd-4212-9ac7-48878c13f075", "北部新区金开大道（金州大道节点）改造、人行天桥", "50003920181008911", "重庆城建控股（集团）有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "464b58b4-8c02-4e27-b116-54b229358a09", "7bd65789-57d8-4a40-b2e6-b8807a5b2f32", "观音桥组团A标准分区A34-1等地块道路及综合管网工程（二期）", "50003920181008924", "重庆建工第一市政工程有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "8a71b019-b430-4c33-86de-5c85e94dcbc5", "c382f998-5591-47c3-94de-483a48ae7ae3", "高新园大竹林组团A区市政道路工程（横一路K0+200-K0+620段）、北部新区大竹林组团A区纵九路道路及配套（纵九路K-1+860-K0+000段）", "50003920181008926", "衢州市政园林股份有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "79a38d20-c3f0-4d77-9370-1c6dea1d119d", "bd9c8cd6-d619-4961-84dd-a1923c96a47c", "重庆市巴蜀初级中学校", "50003920181009045", "重庆教育建设(集团)有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "e6231681-a7df-4842-b825-cf3e26e78670", "3e230af4-ed18-47e2-8942-919606f17d2f", "重庆际华目的地中心项目（启迪乔波冰雪世界）", "50003920181009113", "中冶建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "57f2b31a-c836-417f-9b7c-63e3d6914daf", "4ae36fb5-f7b3-4486-8d68-ca6d5e24ef10", "重庆北部新区重光小学校工程", "50003920181009132", "重庆博达建设集团股份有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "17b93955-6a83-4822-9c85-a0a17e3e902f", "21489b54-947b-49cc-a03f-e305ad6cddc8", "和合家园D组团工程", "50003920181009139", "重庆建工第三建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "27054717-44e7-4a97-9a26-da2d92bb1f26", "6cfcd038-b143-4c13-8c02-1dbcbb86ddaa", "重庆保税港区空港皓月小区公租房项目工程（二标）", "50003920181009141", "重庆对外建设（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "6e917310-d01e-447c-be3a-beb3b663e757", "2a48ba36-b100-47dc-9e51-d06ac91a87e1", "重庆保税港区空港皓月小区公租房项目（一标）", "50003920181009142", "重庆建工集团股份有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "4722609d-7b6b-4b7b-b378-cb1b4096fd65", "b1925a0d-4696-4e41-9163-cf546a270a98", "H5-1路道路及配套工程（二标）", "50003920181009152", "太原市政建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "f9ee9cd6-c044-41b9-a224-f85be31d2275", "d17fe5fc-b8d0-4584-ae4c-70217cf60adb", "重庆北部新区金童初级中学工程", "50003920181009161", "中冶建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "387d2f3d-5b22-4845-b0f1-2208478e35f3", "bd623d7e-0255-4ddb-8cca-95a3617c0368", "江北嘴公司溉澜溪广场项目——主体工程、溉澜溪广场西侧道路工程", "50003920181009162", "重庆建工第九建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "9e016287-8deb-491d-b08c-88f5c1d67645", "d3aa0257-4f22-4dab-a603-e6820be942bb", "两江中心（环球欢乐世界）", "50003920181009163", "中国建筑第七工程局有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "486394c2-6474-4e59-84b8-0a60b8703c3e", "6cdf69e6-d6c9-4db1-91cd-ce7e06bafe81", "寨子路三期工程", "50003920181009164", "重庆城建控股（集团）有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "a3adea2f-9715-487d-8f18-4086870d26b6", "650fcf1a-b3dd-4bd6-a12f-e83b61de85ef", "L9路西段道路及配套工程", "50003920181009165", "杭州萧宏建设环境集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "c26421c6-aab6-46fb-aae2-6896b3d290e7", "ce2dc645-e3e6-446f-9957-6950c3aaae67", "曾家岩北延伸穿越内环新增通道工程（二标段）", "50003920181009172", "四川公路桥梁建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "8a3621d6-775d-459d-974f-029cd4ffe5eb", "fef6b0bd-fc69-4317-985d-2c4559465893", "曾家岩北延伸穿越内环新增通道工程（三标段）", "50003920181009173", "重庆建工第二市政工程有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "91ecd2aa-1624-43e7-bd75-1a21454b27c9", "8297fba3-a5be-40fd-9c50-6eb81181b0c3", "礼悦路道路及配套工程", "50003920181009182", "重庆中环建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "708b2639-4aab-4aa5-bbfd-44d2ddff1d07", "a6e835c1-c416-4e4e-a16d-40cde63b48b4", "龙兴隧道（三标段）", "50003920181009183", "中冶建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "c6c8b1c5-ef18-4ce0-858f-3e35d04889cc", "3eb3ef37-d57e-4646-b0e5-028c83565dd4", "蔡家大桥南引道（金山寺立交）工程", "50003920181009185", "中冶建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "887311c5-7744-4b8b-a94b-45a02580a931", "41ce8373-0fab-47e1-ad58-fb5aad79ad45", "两江·中迪广场（原重庆两江创新科技园项目部分）（C35-1-1/05地块）", "50003920181009187", "中国建筑一局(集团)有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "c64e7bc6-f8d4-4af2-8e43-16cf4b582396", "4cb9f8b8-dcbb-427f-84e5-ec491a4e497b", "龙兴隧道项目（一标段）", "50003920181009190", "中铁十一局集团第五工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "2416ff7a-d34e-46e8-a670-7d232d670e29", "e4ddec89-b2ff-443e-8ed5-98501750f20a", "重庆康菲动力科技有限公司年产20万台高性能发动机零部件项目（一期）-物流中心", "50003920181009200", "重庆致辉建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "a61b235b-228c-4c8c-95bd-3b2e79c10de0", "54050c2a-77fa-42f2-bf98-f5e4627519fc", "中德（水土）智能制造产业园厂房总承包工程", "50003920181009225", "重庆建工住宅建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "412ad441-b245-4ebf-87f7-9cf20252127a", "64ee79c8-b348-4fa2-8e1f-6aa133768592", "远海建工集团技术研发中心", "50003920181009227", "远海建工（集团）有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "27f74337-34d7-40c8-b4e8-5c19d9ed5755", "126e67f1-324e-4ad9-9ca0-2f997068c929", "重庆祥胜物流有限公司新建标准厂房项目", "50003920181009253", "重庆市开州区渝东建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "9aefc9c9-ff65-4d68-a63c-eb49f2f5d6cf", "25613786-2c39-4670-bd14-3ed5e6246258", "水木云天平基土石方工程及基坑边坡支护工程", "50003920181009291", "重庆市渝北木鱼石建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "745bfeea-a4a2-4c97-b82d-65addb4b3d2e", "266f466e-636d-46a7-bbdb-3da8d2c1430d", "碧桂园龙兴国际生态城（H34-1/02地块一期别墅和二期)", "50003920181010001", "浙江新东阳建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "e73c717e-66fe-498c-b647-c2674d9d1336", "f3354722-f727-4f90-b0df-c4c6fd17a7c8", "中交·中央公园项目（C109地块）", "50003920181010002", "中国建筑第二工程局有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "1490ab2d-41e9-49b9-8b14-12a37371d006", "4e52709b-1de8-4a5e-a2dd-eb8ab5fba051", "两江曲院风荷项目（C03-1/01地块）、（C07-1/01地块）", "50003920181010005", "中冶建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "6c1af5c5-feea-42cb-8bc1-44cce4059a64", "e2c34e73-01b7-40ae-9361-f99d8c396582", "重庆雅居乐富春山居（一期8号地块工程C85-1/05地块）G3栋、G4栋、G9-G18栋、G19部分车库）", "50003920181016005", "中欧国际建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "73f85f02-53cc-4378-ac16-c57c34d9bf14", "d78b0220-d27c-45e9-9bb7-029c0516f9c1", "融创中航项目（E15-01/01号地块）二标段", "50003920181019002", "重庆大江建设工程集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "a063fde8-05c8-4961-8983-ff0151e6aa94", "3e980306-0f7d-480a-a9b9-71734de9458e", "两江新区G11-5/03公共停车楼项目", "50003920181022001", "重庆建工第八建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "fb40c543-cbea-423c-afe3-d474037843e8", "4ea9430e-304e-4fe0-8047-ba265fd3f13d", "两江龙兴体育城", "50003920181022002", "中东建设集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "e618e204-3768-40bf-ae88-7c706be0a94c", "05b61b6f-d379-4add-8546-fb6430b875fb", "中国摩（重庆）项目I49-5/03、I50-1/03、I51/03号宗地（I51/03地块）（三标段A1~A7、A9、S6、S7及相应A1#地下车库）", "50003920181022003", "嘉兴大源建筑工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "ad0f1ff3-654b-48bd-9bfa-9372d6cc7934", "0f25595c-6123-407a-95d1-43347dff66a1", "中航两江体育公园（E04-02/01）", "50003920181022004", "中铁二局集团有限公司（原名：中铁二局工程有限公司）", "两江新区", false);
            AddProjects(dbContext, accountList, "863f8e66-723f-4e9a-ae75-29e1483013ae", "ff731dc1-80b9-4b17-ad9a-41cd6063a328", "中航两江体育公园（E16-01/01)", "50003920181022005", "中国航空技术国际工程有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "a50b776e-3073-4581-826c-f686416b1120", "c3c439a4-2a21-4198-b572-f5c9de8ccebd", "北部新区人和组团O标准分区O13（部分）号地块（渝兴·星座天地）", "50003920181022006", "江西润财建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d112ec9f-e2be-4cc6-8b9b-977e05c3f1a0", "1c5efb76-5090-4bab-85cd-5891b21664eb", "华商悦江府（D04-4/5地块）1#、4#、5#、6#楼、6#楼商业及对应车库", "50003920181022007", "龙盘建设工程集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "ebdf812c-6449-4a37-af26-a26e3b6fe6a4", "f1fd4f27-e8ad-421c-8ab7-95fcaf9f43fd", "华润.公园九里（C81-1/03）", "50003920181022008", "西南建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "880725a3-dcd2-47e0-937a-a8fc9a56cff0", "389c3025-6ef5-46a0-b635-9f69825ec870", "恒大御府（B17-2号地块一期）（14#-44#、75#-79#及地下车库、门卫一）、恒大御府（B17-2号地块二期）（59#-60#、63#-66#及地下车库B区）", "50003920181022009", "重庆建工第三建设有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "29e0e74e-76e9-4fdf-a6e2-15210789287e", "b033fe2f-7c20-4e74-a91d-ca66dc9f8b9e", "恒大江北嘴项目（G26-1/05地块项目一期）", "50003920181022010", "中建五局第三建设有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "5069ae73-fa94-4dd8-b392-a589bde57992", "f1e45e08-4234-4174-80dc-0edd89301b7e", "重庆水土嘉陵江大桥工程施工", "50003920181022011", "重庆城建控股（集团）有限责任公司", "两江新区", false);
            AddProjects(dbContext, accountList, "b1f0698f-6305-41c1-9efd-47468a8b71dd", "c8e142a4-4c1a-46a2-abda-b9d18fa3e9c3", "金科天元道（一期）商业1、2号楼及车库", "50003920181022012", "中国建筑第二工程局有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "2e651e2e-020f-4714-8960-c7429f8886ed", "407ee17e-828c-4b80-b100-9d830eff47dd", "龙兴组团H分区H48-2/01号地块项目", "50003920181022013", "中建三局集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "d071c150-5a52-4f76-a894-1aff6248b77b", "c90dc22a-dfd2-46a2-b48c-e02ed764aebd", "龙盛路（两江大道）南北延长段-石河立交工程", "50003920181022014", "中冶建工集团有限公司", "两江新区", false);
            AddProjects(dbContext, accountList, "b5b7f8cf-7262-4416-b0c3-03df912e58c4", "4ec6f366-219d-4ce3-8bf5-bc09d415eae8", "渝能•长悦府施工总承包工程（第二次）", "50004020181008007", "重庆渝能建筑安装工程有限公司", "经开区", false);
            AddProjects(dbContext, accountList, "c3ac445a-7b61-4886-a8c7-4dd3d2e4dcb0", "89101cc9-b66c-4a3a-a126-9f60cf169a44", "和坤商城", "50004020181008009", "重庆南洋建筑安装工程有限公司", "经开区", false);
            AddProjects(dbContext, accountList, "0d841db8-ec3e-44e5-b0cf-ff86561d73d9", "5db624b2-beab-4108-bc7e-1339cb8bc499", "恒大同景国际城博达地块项目", "50004020181008011", "重庆万泰建设（集团）有限公司", "经开区", false);
            AddProjects(dbContext, accountList, "4d6802e8-0bf6-4db3-b544-9480fb2a06a0", "22e1ad40-e65a-4b85-8029-0c401aa08475", "恒大滨河左岸项目（7-12号楼、车库、大门）", "50004020181008013", "重庆渝发建设有限公司", "经开区", false);
            AddProjects(dbContext, accountList, "1bf84bbf-07d8-4b41-9b09-ad48d5f977d9", "c4b7741a-726d-42a0-aaf7-ddc782536113", "重庆九州通医药物流中心（三期）", "50004020181008015", "湖北信义兄弟建设集团有限公司", "经开区", false);
            AddProjects(dbContext, accountList, "6253d6b9-8d96-4f31-b91c-fb0b150ee18d", "5e5f2b87-5f7f-4ff5-90dc-28901234c2e8", "昕晖.依山郡", "50004020181008018", "重庆鹏威建筑工程有限公司", "经开区", false);
            AddProjects(dbContext, accountList, "ce0fb5cd-73c5-4c0c-bbc7-005ca555734d", "89af1af2-a675-4b3b-b605-7d7ba8e72467", "碧和原茶园项目二期", "50004020181008019", "山河建设集团有限公司", "经开区", false);
            AddProjects(dbContext, accountList, "97fed051-df2e-42e7-8376-672544ac86e4", "5162045a-8f21-4457-9d35-336682175ace", "金科.博翠园F13-3/01地块", "50004020181008020", "重庆海博建设有限公司", "经开区", false);
            AddProjects(dbContext, accountList, "90763288-c001-4712-b677-f553c540f19d", "a5dfb490-b941-4cdc-b372-5186f7ed7e49", "金科.博翠园F13-4/02地块", "50004020181008021", "重庆海博建设有限公司", "经开区", false);
            AddProjects(dbContext, accountList, "5a0ccc5e-e3fe-41bd-9598-b939b7f843a6", "aaaf8fc5-1a83-429c-907f-f0eac7e4019a", "金科.博翠园F13-1/02地块", "50004020181008022", "山河建设集团有限公司", "经开区", false);
            AddProjects(dbContext, accountList, "e106a1e6-b84d-42f9-aae8-3f129e54213e", "cd3000de-c4b2-4e48-8b39-8ebb9ffd6719", "金科.博翠园F13-8/02地块", "50004020181008023", "山河建设集团有限公司", "经开区", false);
            AddProjects(dbContext, accountList, "720c09a2-ad64-4613-ab08-f2267be39f18", "14c16650-dc66-41d0-8f5f-c9d4a77109b4", "茶园组团J分区J1-2-1/03地块", "50004020181008024", "四川域高建筑工程有限公司", "经开区", false);
            AddProjects(dbContext, accountList, "a29f8783-6347-407d-b921-a5ac4c570f21", "a522f3fd-bd87-444d-8245-2061ca0c9386", "领安重庆东港物流园", "50004020181008026", "浙江天勤建设有限公司", "经开区", false);
            AddProjects(dbContext, accountList, "e4fecae3-cbf3-4ac7-985e-ffead2c7a5b0", "47b6aed4-8e24-4e5d-910c-fcd707e2c806", "经开区C-D连接桥工程", "50004020181008027", "重庆城建控股（集团）有限责任公司", "经开区", false);
            AddProjects(dbContext, accountList, "0111b859-6f31-439e-b91c-535d2cd40e2d", "51496d82-b11e-4adf-9b95-4c1a3215559e", "重庆永翔现代物流产业园一期", "50004020181008028", "浙江天勤建设有限公司", "经开区", false);
            AddProjects(dbContext, accountList, "f4f2b717-b0d0-443e-afb1-b9d10a7f0bd3", "3e094c0c-2a05-422d-bb5b-e9db85a43bc0", "万盛经济技术开发区城市综合管廊工程（一期）鱼田堡组团", "50004120181010016", "重庆建工第二建设有限公司", "万盛经开区", false);
            AddProjects(dbContext, accountList, "fbcaf656-b4f9-47ff-95d9-0c5c82c74258", "3a99b305-4d7a-4c54-b1d4-961da4fd75e5", "滨江壹号.首座", "50004120181010038", "四川省第三建筑工程公司", "万盛经开区", false);
            AddProjects(dbContext, accountList, "b2768c7f-7741-4d83-980f-bf4c1779e5b5", "85f7a0c6-5bf5-4617-a847-ec6e5c426545", "融堃▪彩云里", "50004120181016012", "业兴实业集团有限公司", "高新区", false);
            AddProjects(dbContext, accountList, "1aa453a5-9bca-4599-b59d-b7f9ef20e84e", "3ae25ede-a358-4374-8400-c1e27ac4eaaf", "铁建▪渝都总承包项目（标段二）", "50004120181016029", "中铁二十二局集团有限公司", "高新区", false);
            AddProjects(dbContext, accountList, "29e8b519-518a-4cbf-851c-09e35559e382", "b0f94fca-3b8b-4e84-b252-d9b0f611d887", "铁建▪渝都总承包项目（标段一）", "50004120181016030", "中铁十二局集团建筑安装工程有限公司", "高新区", false);
            AddProjects(dbContext, accountList, "74bdda91-5db3-4861-b6a1-76223cfe19b8", "21f91f9e-923b-4fa8-a711-0a26dce3925f", "中国西部农产品冷链物流中心项目（二期）", "50004120181016041", "中国建筑土木建设有限公司", "高新区", false);
            AddProjects(dbContext, accountList, "06ccba6e-9abb-4b76-93bb-fd09e515f22f", "fbe83201-f0c7-4d29-bda2-a91d3513bdd2", "中国铁建·玖城贰号", "50004120181016042", "中铁十一局集团第五工程有限公司", "高新区", false);
            AddProjects(dbContext, accountList, "55926741-cff3-45f8-9a32-d548b9e31275", "5c6dd1a2-5330-452b-a4cc-6dceb71e9ad4", "四川石油管理局物资总公司重庆检测中心迁建工程（主体）", "50004120181016043", "重庆建工第三建设有限责任公司", "高新区", false);
            AddProjects(dbContext, accountList, "f11463ac-1bf0-460e-a45d-8975cdd77181", "ffff1d22-cd54-43a4-8bd8-71fa78300597", "尹朝社周边道路工程设计施工总承包（EPC）", "50004120181016044", "中铁十一局集团第五工程有限公司", "高新区", false);
            AddProjects(dbContext, accountList, "2784d81f-10f5-4036-8106-584addcef7ad", "30c8775e-acd3-423b-a0f9-bde9f94f4ffa", "快速路一纵线（高新区拓展区段）工程（罗家院立交）、高腾大道（二期）道路工程及高新区拓展区森谷路工程（K1+740-K2+014.896）", "50004120181016045", "中冶建工集团有限公司", "高新区", false);
            AddProjects(dbContext, accountList, "0ec0d02d-f2b8-41a9-9ac3-24207d14dc92", "d855e745-6ab7-4fe3-8fae-88db49aabae4", "杰品模具产业基地项目", "50004120181016046", "重庆腾鲁建筑安装工程有限公司", "高新区", false);
            AddProjects(dbContext, accountList, "ba284855-cfc2-4468-96d4-f2d12a3c699f", "cd833ed0-d490-4905-be19-113292317868", "盛资尹朝社项目二期一标段（2-3至2-6号楼及车库）", "50004120181016047", "中国建筑土木建设有限公司", "高新区", false);

            #endregion
        }

        /// <summary>
        /// 添加项目 与 项目帐号
        /// </summary>
        /// <param name="dbContext"></param>
        public void AddAccountsAndProject(DbContext dbContext)
        {
            #region 添加项目信息

            AddAccountsAndProjects(dbContext, accountList, "b4a92f81-220c-4ace-94c6-c7e7394b978", "50000020181019005", "重庆市轨道交通五号线一期工程土建5107标", "6a2296fb-debb-4776-be4a-8791691f70fe", "北京中铁隧建筑有限公司", "5461686b-eef5-4cf4-9adf-86e7e9da2be3", "911100008011269490", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "71f4eff2-70c3-4d6f-805b-22adeea1b32", "50000020181017502", "甘悦大道渝北段工程", "535f5e46-d31c-44f2-abb8-fd4cc1667e31", "攀枝花公路桥梁工程有限公司", "d99004b8-d4fc-4ee8-bfc0-f8f0da3439ae", "91510400204355137E", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "ddf782d7-8d15-488c-b53c-43908738e0a", "50000020181017421", "快速路三纵线红岩村嘉陵江大桥至五台山立交建设工程（一标段）", "4b12a056-340e-42a9-8826-51ee811578b3", "中国建筑第八工程局有限公司", "d559b085-a7f9-4181-b683-388af956b668", "9131000063126503X1", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "4aed7b55-d31b-477f-8d3a-3e8fc242981", "50000020181017561", "龙湖沙坪坝枢纽项目一期工程", "f8c757cf-14dd-4233-83cd-ce774f08ef58", "中国建筑第八工程局有限公司", "7cc256dd-7103-483f-91d2-c570ad5a6fba", "9131000063126503X1", "重庆市", true);
            AddAccountsAndProjects(dbContext, accountList, "3191e310-8b90-4a0f-a291-932c64f604d", "50000020181017561", "龙湖沙坪坝枢纽项目一期工程", "fff9b0a7-3e6b-40a6-8d07-a8557897bc99", "重庆诚业建筑工程有限公司", "58f5373d-3cd0-4580-9e23-e28e839a1f2b", "91500107666403682M", "重庆市", true);
            AddAccountsAndProjects(dbContext, accountList, "6ec00098-160d-4869-8606-41eb912c170", "50000020181017413", "快速路三纵线红岩村嘉陵江大桥至五台山立交建设工程（二标段）", "cf27825e-37e4-495c-9f52-ebc3d9852737", "中国建筑第六工程局有限公司", "b599bedf-b478-44da-8ca0-d6b68bc08de9", "911201161030636028", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "bbac5d72-ccc3-4f2c-ae5b-58b5efce1b1", "50000020181019007", "重庆轨道交通九号线一期工程土建一标", "48c58d1a-7c73-4263-ad9f-fb6b1f88a126", "中国建筑第五工程局有限公司", "7abbdfdc-3f16-4c26-a7a1-6d1746c998b7", "91430000183764483Y", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "f766d1ee-a875-472d-b3b5-08c91fb47ae", "50000020181019010", "重庆轨道交通九号线一期工程土建五标", "85780a39-76e4-49b4-8c94-f0831550b79f", "中建三局集团有限公司", "5b313416-f881-4b0e-8433-e5bb976d76ed", "91420000757013137P", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "693e61d6-05b0-4408-b515-55cde077a51", "50000020181019008", "重庆轨道交通九号线一期工程土建三标", "bb97a73b-3609-445f-ad03-defe95e2dc58", "中建隧道建设有限公司", "1d88309f-5645-41ab-88c9-7a5ed95e5e3e", "91500113MA5U7U4M9E", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "0c7f44a5-d8de-4f8f-b47f-5c07d57cbd0", "50000020181019009", "重庆轨道交通九号线一期工程土建二标", "5c09e9c2-bdbf-444a-baf4-ccdd06e767c7", "中建隧道建设有限公司", "d7fbfaa2-20e9-46dd-a26b-d8fac19187e7", "91500113MA5U7U4M9E", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "3e663e58-9a67-4d89-a0b9-6c4c5c00289", "50000020181019011", "重庆轨道交通九号线一期工程土建四标", "0cfb3f15-b986-40b3-b662-c415f8f3e7b9", "中建隧道建设有限公司", "aa2b3338-c16f-4ee0-abc8-ad002c68531f", "91500113MA5U7U4M9E", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "09f37a9e-643c-4318-bd2f-85bde0b4980", "50000020181019002", "重庆市曾家岩嘉陵江大桥桥梁工程", "79627577-11df-4c5e-ac52-8e0d8400c2d6", "中交二航局第二工程有限公司", "868a1f35-ab3f-423e-b499-782920f508be", "91500000778492034J", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "09dc68af-09ad-48e2-9448-7f4a0191c5b", "50000020181019006", "重庆市轨道交通十号线二期南纪门长江轨道专用桥", "8d37b6f7-528f-478e-b4b9-ae41f6bad26e", "中铁大桥局集团第八工程有限公司", "d07cec30-67fe-44bb-ad79-9c1285e7cac0", "91500109573405793Y", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "2b755c86-d8c4-4bf6-bf2d-b47a250b8a1", "50000020181019016", "重庆市市郊铁路（轨道延长线）尖顶坡至璧山段工程", "aca03a44-40ef-42f8-ae22-1c7dec825985", "中铁二十四局集团有限公司", "f50b09d0-c49e-418c-88e8-a6dc2170720d", "913100001322024481", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "249df702-68d4-4f18-8b49-ee106f9e2d6", "50000020181019001", "重庆轨道交通六号线支线二期一标", "56068584-88e0-4a96-b4bc-fc9e0d05e76d", "中铁十七局集团有限公司", "7d3e2d34-f30b-4266-87bb-3d1ad757d206", "911400001100708439", "重庆市", true);
            AddAccountsAndProjects(dbContext, accountList, "256a4fe6-3875-4547-b044-037e33b1ae9", "50000020181019003", "重庆市曾家岩嘉陵江大桥隧道工程", "acd6e389-d498-4451-b3fc-f95701798c1a", "中铁隧道集团一处有限公司", "5b30cae2-2a24-4bc4-b9a5-bbd13e017b3f", "915000006733598862", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "090e86b7-05d2-4c30-b5cd-c9373c7db65", "50000020181017435", "快速路一横线歇马蔡家段工程歇马隧道东西干道工程PPP项目", "6c4a0007-00be-485c-a80e-193435cc78f8", "中冶建工集团有限公司", "7576c718-b607-4ffb-a282-6cf2875efc07", "91500000795854690R", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "e051bfd8-c112-4e6d-9670-857a4318176", "50000020181019015", "重庆轨道交通六号线支线二期草家湾车辆段工程", "01932bed-9082-4817-9ec8-49a2207c4671", "重庆单轨交通工程有限责任公司", "65b70323-9c37-4aae-8582-a8eb52cfe224", "915000007784693958", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "06f88516-f4b0-4886-b82c-7d92623ba45", "50000020181017489", "重庆第二师范学院综合实验实训大楼建设工程", "3c8f1e15-3135-494b-b05f-3fff3ed0ce62", "重庆建工第二建设有限公司  ", "09d05d11-64f6-48be-afc5-2581315c8489", "915000002028051812", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "c9d04e01-3dd8-4c0b-999a-bd19a9f7e70", "50000020181017461", "重庆市档案馆新馆项目主体及安装工程", "f129940b-db6e-4077-ae69-28091d7cd88b", "重庆建工第三建设有限责任公司 ", "f6dbb361-44b1-42d3-a4dc-e9f902db5975", "915000007339743120", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "e6a6354a-f47d-4c0f-9e54-f9fca2edfe3", "50000020181019014", "重庆轨道交通环线马家岩停车场土建及安装工程", "445b94ad-fb53-48fc-9c89-4f771831d816", "重庆建工集团股份有限公司", "2f447754-097b-4899-bbf1-a39c4d221d2f", "915000002028257485", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "40942bd4-f66f-4272-b4ed-82225fa0f9c", "50000020181019013", "鸡冠石污水处理厂提标改造工程（场平、边坡治理、排洪沟改造设计及施工工程和土建及安装）", "a814eba4-815e-4600-ac92-0c40629a17a1", "重庆建工集团股份有限公司  ", "76245b91-e87d-4b25-bf2c-e8d3f57fd860", "915000002028257485", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "3a88bb23-a871-4fab-a59b-9a0afca144d", "50000020181017508", "新溉路二期工程（一标段）", "50329fcc-7b8e-413f-a5c2-22ddd9778c5a", "重庆建工市政交通工程有限责任公司", "78c1b892-7141-4222-a903-c0ed3e196e82", "915000002028014715", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "8d2fc01e-afd5-4daf-b5f8-a223444b4ba", "50000020181019004", "重庆市洛碛垃圾焚烧发电厂项目", "770aa29f-28f8-4087-8b16-20825c838690", "重庆三峰卡万塔环境产业有限公司", "09e18541-b60c-4043-bca3-dae120cc75d0", "91500104202981978N", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "d4944e44-26d5-4cb9-9e91-289a655d74c", "50000020181017509", "新溉路二期工程（二标段）", "d2fe54cb-92b0-41cf-b0b9-468da2816c49", "重庆市基础工程有限公司", "45052814-5c89-4041-82c9-d081d16b3564", "9150000020282565XH", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "16faf311-41af-4f7e-be97-e428b343e80", "50000020181017493", "卫生学校青杠迁建项目图书馆、办公、教学综合楼", "1ec79bfe-dafb-41dd-bf00-f7d2ca674e89", "重庆中航建设（集团）有限公司", "5e103f78-3f29-4a0e-9ba5-58cd3967f2b9", "915002232036970047", "重庆市", false);
            AddAccountsAndProjects(dbContext, accountList, "3b2dfbb2-032a-4e34-a453-564e8405e0e", "50003020181018080", "忠县忠州中学校迁建工程暨县体育中心", "00a5492e-f224-4b53-ad4f-b5e5cbf78432", "中国二十冶集团有限公司", "f1aec317-6ddb-4cb3-bace-c6858e29cd2a", "91310000739759277B", "忠县", false);
            AddAccountsAndProjects(dbContext, accountList, "2599aa45-858c-4487-a3c2-284bbe346f3", "50003020181018096", "忠州大剧场及附属设施项目[EPC 设计、采购、施工]", "991acec7-3a50-4125-88a9-90f782eecfe2", "中冶建工集团有限公司", "269b3158-125b-4e2e-a62b-9cd57ff8595b", "91500000795854690R", "忠县", false);
            AddAccountsAndProjects(dbContext, accountList, "f6fe0105-d619-49f5-bf25-4b7c72ca2e1", "50001320181015039", "重庆长寿长江二桥工程", "85d598c1-b76b-4b90-b992-cb4029941aad", "中国铁建港航局集团有限公司", "4a890b72-74b0-4e16-a3ca-db883d53c0c3", "914404005796688347", "长寿区", true);
            AddAccountsAndProjects(dbContext, accountList, "67fed13f-2da2-434c-b0e8-872faad1011", "50001320181015023", "长寿经开区科技创新园一期工程设计采购施工总承包", "e4c3502f-af10-4202-ba77-8732f5b1d92d", "重庆钢铁集团建设工程有限公司", "038b95e2-c02d-4e8b-bcd3-7668153966aa", "915000002028768602", "长寿区", false);
            AddAccountsAndProjects(dbContext, accountList, "6d8c5a07-8643-419c-b774-aba7a5c1bd4", "50003120181010074", "云阳金科世界城一期七标段（洋房D16-30，D35，D36高层G17-19，商业s1a.s1b.s2-s4.s5a.s5b.s6a.s6b.岗亭B1b2b6及对应车库土建及普通水电安装工程）", "94aac6d8-81a2-417a-9aa6-42124f47e011", "中建八局第三建设有限公司", "ce904e12-0271-470e-b9ce-39f6a9f0b11d", "91320100134891128H", "云阳县", false);
            AddAccountsAndProjects(dbContext, accountList, "5c4d346e-be27-4259-a404-5e2c7a66f83", "50003120181010102", "逸合。两江未来城8区项目31-33#、35-36#、48-53#楼及车库", "95b8a692-fcd1-441d-b159-30b70af62961", "重庆建工住宅建设有限公司", "71bee89b-1f9a-4422-bb2d-06cc47cfe576", "915000002028009114", "云阳县", false);
            AddAccountsAndProjects(dbContext, accountList, "31450016-43bd-465d-8c03-f230f9e0f08", "50000420181018073", "雷家坡立交", "75f1ef3c-1147-40f7-8e17-70a3bba28d84", "山东天诚市政公路工程有限公司", "48e7c322-ec46-4d73-9da9-6ec044ee69e4", "91370200164324095C", "渝中区", false);
            AddAccountsAndProjects(dbContext, accountList, "2554cdfa-8b07-4f50-af8a-222da106c49", "50000420181011017", "重庆来福士广场T2塔楼", "4f939057-bdc8-42ea-98ef-63ead5b6f4f4", "中国建筑第八工程局有限公司", "8c068e17-f707-4adb-a6b7-3c4128c310fb", "9131000063126503X1", "渝中区", true);
            AddAccountsAndProjects(dbContext, accountList, "af8932a8-7f2b-4455-8eff-b915208c6df", "50000420181019077", "解放碑地下环道三期", "ad5c91a4-e8c6-4b07-924b-f5b4a89ba8b7", "中国建筑第八工程局有限公司", "eb008a22-98cc-4996-aa00-b6a5936e2fcf", "9131000063126503X1", "渝中区", false);
            AddAccountsAndProjects(dbContext, accountList, "7090b416-8b0c-4a41-9853-dcbcdfaa492", "50000420181017048", "化龙桥B11超高层二、三期", "6b2842e1-bdf8-452e-8461-1016a5aa165d", "中国建筑股份有限公司", "4915ee43-2387-4bc7-b118-485828b677ba", "911100007109351850", "渝中区", false);
            AddAccountsAndProjects(dbContext, accountList, "c3f3a72f-22de-426a-bb94-67093f5ed12", "50000420181008001", "重庆复地金融中心 ", "8579efcf-9344-4d26-b78e-70854dda23f5", "中国建筑一局（集团）有限公司", "fc246a1d-ebd2-4cb4-bbb5-b1b840ffb4be", "91110000101107173B", "渝中区", false);
            AddAccountsAndProjects(dbContext, accountList, "87d04ba4-9e10-4c6f-a126-033ced456be", "50000420181016037", "重庆来福士广场项目观景天桥 ", "f13ddb72-9f63-4c4e-9960-1285dc489d26", "中建三局集团有限公司", "2f0eb0b1-3fe6-4cb4-ab8a-532b97bc0c44", "91420000757013137P", "渝中区", true);
            AddAccountsAndProjects(dbContext, accountList, "b9f09414-c208-44ed-b86c-db41472017b", "50000420181018060", "光控朝天门中心二期", "1e5f3be1-b801-4977-a2ec-47b108e617f3", "中建三局集团有限公司", "29b85b27-4a1b-41ef-a48b-44278d198a98", "91420000757013137P", "渝中区", false);
            AddAccountsAndProjects(dbContext, accountList, "30ea1f1d-2a13-41b6-b896-186945dc918", "50000420181011021", "重庆塔项目", "1cde7052-10ef-44bb-b912-88bc57f1a8cc", "中铁建工集团有限公司", "6bf0930e-df18-4d2e-9585-848180493f60", "91110000710921189P", "渝中区", false);
            AddAccountsAndProjects(dbContext, accountList, "aa3607f1-7d67-4687-b39f-464023cb569", "50000420181008004", "重庆罗宾森广场项目T4#楼承包工程（含塔楼下的商业及车库） ", "7eca4c7b-da73-4204-8f4b-6d481560ae38", "中兴建设有限公司", "0c678cf4-cacf-479c-ba47-a5f7d42ed65f", "91321283737840047C", "渝中区", false);
            AddAccountsAndProjects(dbContext, accountList, "5bb60eaa-145e-4729-812d-74279f5c4d8", "50000420181008005", "重庆中心一期", "7d0182c2-6216-43ca-ad6e-560017fa50f7", "中冶天工集团有限公司", "3e003d22-2b78-497b-9cc0-bba0b660e7f2", "91120118789363043U", "渝中区", false);
            AddAccountsAndProjects(dbContext, accountList, "aafd65c0-42d8-4c64-b7b6-6ebd22bda8a", "50000420181017047", "化龙桥片区B14-3地块项目", "f916f88e-6f0a-4e6b-ac82-435641cdce41", "重庆城鹏建筑工程有限公司", "6289b343-3d71-484f-bea6-e6defebe4ec7", "9150010820315005XF", "渝中区", false);
            AddAccountsAndProjects(dbContext, accountList, "2ce0a4bf-10b1-4a8d-84de-e68319f4010", "50000420181011016", "和泓·江山国际三期8、9号楼、S1商业及地下车库 ", "3ca13dab-ae14-48b1-b6b5-4749cfa2e828", "重庆海博建设有限公司", "e3568bcb-7281-4d5c-bf06-d2f1a64af02f", "91500112203510350T", "渝中区", false);
            AddAccountsAndProjects(dbContext, accountList, "9b8c2cd2-eced-4732-8d10-8cf5d9a1012", "50000420181010014", "财信·渝中城三期（1栋及地下车库） ", "72f3c162-bc7f-42fc-a397-8f8f229e38cc", "重庆建工住宅建设有限公司", "4b024996-babc-4ea4-974c-470a4323ad2c", "915000002028009114", "渝中区", false);
            AddAccountsAndProjects(dbContext, accountList, "eec83cfc-91e2-4d4d-ae9b-dc9cdc575c2", "50000420181016038", "重庆融创·凯旋路项目南区三期5#地块（4#楼及裙楼）", "68a855fd-59c6-45f6-9ad8-0699eac75aeb", "重庆万泰建设（集团）有限公司", "b12a55fe-5cae-4de9-8adf-b5bc2a099c1b", "91500112784219544A", "渝中区", false);
            AddAccountsAndProjects(dbContext, accountList, "c8669afa-f39d-4627-8042-51a3d7d66b8", "50000420181017045", "化龙桥片区B5/3、B10/3地块", "2bf286a1-c6dc-48c3-a19d-8405a9eb2ca7", "重庆万泰建设（集团）有限公司", "28fd56f2-9d39-4b06-91d1-9a0e79b06193", "91500112784219544A", "渝中区", false);
            AddAccountsAndProjects(dbContext, accountList, "64cbb106-baa8-440f-9934-9a813233dfe", "50000420181011019", "万科.黄花园项目", "f93782e9-4628-497b-b861-aa240100c84c", "重庆渝发建设有限公司", "56e1aecb-b7ad-4593-b320-611cf2d317d6", "91500115203399701L", "渝中区", false);
            AddAccountsAndProjects(dbContext, accountList, "10321bf0-91d4-478e-a415-2f360682035", "50001120181018418", "华地国际工程（渝北区两路组团的F39-3地块）", "a2597f84-58dd-4c18-b025-768958a19ae4", "黑龙江省建工集团有限（黑龙江省建工集团有限责任公司）", "3ed9cd08-eb30-4daa-8ba3-ba3ba6d4c7fa", "912301107444395341", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "d9bd1bfe-b605-453a-a55f-b780b54cedb", "50001120181018548", "重庆仙桃数据谷商务办公楼一期五标段投融资建设工程", "97baca81-63d5-45de-8dd9-2a6c6ba3754f", "江西省建工集团有限责任公司", "30240190-d597-4f52-b75f-28a240e542fb", "913600005865804353", "渝北区", false);
            AddAccountsAndProjects(dbContext, accountList, "2c5a7f6e-8130-40f8-97e8-6a7a935a54c", "50001120181018499", "鲁能泰山七号三期项目F118-1地块建设工程", "9be2a8a7-9f23-4090-8077-cfa996da8e89", "青岛新华友建工集团股份有限公司", "99858f98-4ae0-43af-b495-e17210ccc143", "91370200706419487B", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "dbe921a3-d506-4464-b725-f996cc199b5", "50001120181018502", "上东汇小区F79-2", "26b87094-8e50-4746-9577-9c70f88144d6", "四川域高建筑工程有限公司", "bc41a595-9c08-4a42-872e-b4bdb62ef6f6", "9151000068417596XN", "渝北区", false);
            AddAccountsAndProjects(dbContext, accountList, "363ac120-40a4-400a-9114-555f5cca67f", "50001120181018494", "大石坝组图B分区27-1号宗地项目施工总承包一标段6.7#楼工程", "a1232cd7-34ce-4448-aecf-369035369d5f", "新疆苏中建设工程有限公司", "608d2ac0-bea6-4d25-96cd-4afda95b4def", "91650204734474723K", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "ff5006ea-5761-4d12-bd57-687e1fc6bea", "50001120181018497", "绿城大石坝组团B分区27-1号宗地项目Ⅱ标段（1-5楼及地下车库）", "8871d623-a237-4208-9725-3ceb83c9ff71", "新疆苏中建设工程有限公司", "16258770-c0af-48ba-b6e5-71b3635395df", "91650204734474723K", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "3b3c2971-8e39-4edd-93b7-6f10a07b8c0", "50001120181018575", "上东汇小区F85-1地块项目", "32550449-4c0e-41e3-b929-f03b3b185228", "正威科技集团有限公司", "7da4149d-4673-4b1b-8573-fde973ab9428", "91321204141366786E", "渝北区", false);
            AddAccountsAndProjects(dbContext, accountList, "b9a9b9e2-e698-405a-bc83-0a7e8b27b36", "50001120181018492", "鲁能泰山七号三期项目F119-2地块建设工程", "e304210b-56e2-4474-a62b-fbdd63c9bad6", "中国建筑第八工程局有限公司", "b27a2bfc-a1e6-4c70-969d-00c905c500ba", "9131000063126503X2", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "95f3ccec-c359-4240-b0c0-7a8c7b02c5a", "50001120181018526", "重庆桃源居国际花园九、十区", "c1c0e824-30e2-4f4d-b2cf-571c1bf4bfd2", "中国建筑第八工程局有限公司", "8e2f95c2-390a-4f1b-a54b-228bfc621567", "9131000063126503X3", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "718bd3ad-381e-45a0-820e-60dbece7e39", "50001120170816312", "重庆市仙桃数据谷三期项目一标段", "2a15b99b-735a-4c6b-8735-b9ed13033a37", "中国建筑第七工程局有限公司", "9accd9ff-b1c7-455b-a94c-e437cabae9d5", "91410000169954619U", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "218a1aa4-d19d-4cab-a782-19e5c9789cc", "50001120181018540", "渝北世贸城2#楼及地下车库（基础和主体除外）工程", "ad14116e-b3a0-44c5-9957-3d7c99eab2a3", "中国建筑第七工程局有限公司", "a8b18ffc-6efc-4749-aa09-0279ba206082", "91410000169954619U", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "8a3b2951-2aff-4e15-9b72-eb695b3d51a", "50001120170913313", "重庆仙桃数据谷二期一标段", "918d933d-1bb3-4286-8115-4778eee619ca", "中国五冶集团有限公司", "4480918d-9beb-4e08-a0ce-f9ae1f28732d", "91510100201906490X", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "79663e90-732e-4862-a0f2-217ee5afbe3", "50001120181022001", "仙桃数据谷三期二标段项目投融资建设工程", "1efc2a49-2dab-421f-99cc-3a04bfa9ae19", "中国一冶集团有限公司 ", "154e7fa9-9a03-438f-aa94-904ac7e12aa7", "914201001777275556", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "9865b19c-8308-4c01-866d-6d3dca668b2", "50001120181018574", "大悦祥云F129-1地块", "229746d6-4328-4dd2-812b-1b5918eb599d", "中建三局集团有限公司", "82dafd54-f621-471c-a610-5dbe14bedb0f", "91420000757013137P", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "ef18270b-463d-4983-8d88-de4c092f2f2", "50001120181018503", "鲁能泰山七号二期项目F120-1地块建设工程", "e12c0af8-7c61-4638-9932-4adac34423d6", "中建三局集团有限公司", "3a2df455-83e6-48f1-ba73-cb92382c0957", "91420000757013137P", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "c708538f-af46-4ee7-8bc1-868b9d8cf27", "50001120181018571", "合景泰富。誉峰一期（F114-2地块二标段）", "f18a433e-9b61-4137-b87f-d2235385898b", "中建五局第三建设有限公司", "b1f271e7-98b9-44bd-ba7a-b5b3fea8c69e", "91430100183853582A", "渝北区", false);
            AddAccountsAndProjects(dbContext, accountList, "0fe45023-982b-4667-ac97-a5e26378e0d", "50001120181018570", "合景泰富。誉峰一期（F114-2地块一标段）", "a60b0f57-84c2-4eb8-8740-e4c16c9c8355", "中建五局第三建设有限公司", "d76878be-47ca-43c8-8df4-f8242dac39fd", "91430100183853582A", "渝北区", false);
            AddAccountsAndProjects(dbContext, accountList, "4fe492bf-cf77-4576-a50b-dd963d68a93", "50001120181018512", "远成.中国（重庆）物流城U11、U12地块项目（U11地块）", "5e0e12b6-2dab-43cb-b49b-0ca4730b05b7", "中建五局第三建设有限公司", "1be94e3e-17b4-41df-b35a-72ac04f7edf6", "91430100183853582A", "渝北区", false);
            AddAccountsAndProjects(dbContext, accountList, "75371e64-71b7-403e-80db-bd1f2fd2da3", "50001120181018513", "远成.中国（重庆）物流城U11、U12地块项目（U12地块）", "b559a44b-6e71-4ffb-9cd3-a37911098c7b", "中建五局第三建设有限公司", "6f6517b7-5f58-40b8-ae83-60b809bf0440", "91430100183853582A", "渝北区", false);
            AddAccountsAndProjects(dbContext, accountList, "d064f713-8a89-4055-ba11-b0f7577555c", "50001120181018565", "重庆龙湖煦筑中央公园项目F125-1地块（5#楼、7-12#楼、门卫、车库）", "ce8980d3-9f85-4303-aca3-601fc4e37665", "重庆诚业建筑工程有限公司", "52806633-5eb0-4c44-9985-98903ad66945", "91500107666403682M", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "1a4d1c2c-0056-487f-a42b-6930a133862", "50001120181018564", "重庆龙湖煦筑中央公园项目F125-1地块(1-4#楼)", "c618bbeb-0717-49f7-804a-26737514a7ca", "重庆诚业建筑工程有限公司", "229f5d1a-80a1-4640-abea-a813dc0e419c", "91500107666403682M", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "ae7425d7-9d70-46e3-b4ed-0bc071213d4", "50001120181018556", "象屿渝北区观音桥组团C分区C8-1/04,C13-1/04，C8-6/02号宗地", "a5d8bb02-4a5b-4e7c-ba90-33e7bcda2ecf", "重庆诚业建筑工程有限公司", "7899e55e-aac0-4421-8292-82d7a54e32d0", "91500107666403682M", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "d7dfc4f2-5f35-45a1-b75e-417172f843c", "50001120181018482", "渝北区两路组团A分区A062-1/03号宗地项目（5-15号楼、门卫房及对应地下车库）", "e462da7c-f793-42cb-852e-a2a1655685ea", "重庆华硕建设有限公司", "e383d823-650e-4fd0-9921-ac1728481207", "915001152034048303", "渝北区", false);
            AddAccountsAndProjects(dbContext, accountList, "06a335af-2093-425f-b28b-624da739fba", "50001120181018455", "泽科港城国际二区34号地块（一期）", "74a6a634-dcab-4871-a27d-4c2e083d9b91", "重庆建工第二建设有限公司", "c01a961b-b476-45bf-9f98-4542a7b416e8", "915000002028051812", "渝北区", false);
            AddAccountsAndProjects(dbContext, accountList, "ff1dad7b-905e-4955-aeb7-2eccddd561b", "50001120181018511", "重庆市渝北区中医院三级甲等医院主体工程施工项目", "8e222bc8-51e8-4e95-af96-13e9b8c7457a", "重庆建工集团股份有限公司", "c1094c16-f7c2-42ca-8256-eabfe504b6a1", "915000002028257485", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "30f62724-510d-4c98-a8ef-789e6f956dd", "50001120181018483", "渝北区两路组团A分区A062-1/03号宗地项目（1-4号楼、16号楼、门卫房及地下车库）", "540dda23-cbea-4fcf-be22-2e5931519423", "重庆拓达建设（集团）有限公司", "2f15d437-c6be-4c03-a50f-2da7f808e1ce", "91500108768876288X", "渝北区", false);
            AddAccountsAndProjects(dbContext, accountList, "48b8642c-e6c3-4139-9419-046c7c8634c", "50001120181018566", "重庆龙湖煦筑中央公园项目一期（F126-1地块1-3#楼）", "7e9a594a-75a6-478e-9a75-647432160f44", "重庆万泰建设（集团）有限公司", "ca71c749-546c-46c9-81f6-dd46419b831d", "91500112784219544A", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "d0814bd8-e971-4d01-9ad5-0b71df532bb", "50001120181018567", "重庆龙湖煦筑中央公园项目一期（F126-1地块4-9号楼、门卫、垃圾站、社区组织用房、车库和设备用房、消防控制室", "446d67e0-5845-46f7-911f-74f11e8e6ee6", "重庆万泰建设（集团）有限公司", "fe2399a4-2b1c-4520-9c1d-63668c5ce076", "91500112784219544A", "渝北区", true);
            AddAccountsAndProjects(dbContext, accountList, "e9eb7a68-bf32-48ea-af70-1cb1bc29854", "50001120181018510", "重庆龙湖医院二期工程", "c8e6e46a-688b-4f1d-91e7-0f43adffec87", "重庆秀安建筑工程有限公司", "bffd8900-2400-49a2-a731-45351a6f4bb4", "91500118203797515X", "渝北区", false);
            AddAccountsAndProjects(dbContext, accountList, "d262d465-9736-4d0c-9175-a931c82fb36", "50001120181018563", "重庆吾悦广场项目（F64-1地块）1至12#楼、14#（垃圾收集点）、15#（车库）、16#（物管用房）", "837efabc-0d99-4a83-8cdf-d254fcb4beb1", "重庆渝发建设有限公司", "5336035f-e2d8-47f1-90e1-d69e1ac57447", "91500115203399701L", "渝北区", false);
            AddAccountsAndProjects(dbContext, accountList, "d860def5-d726-43be-851b-58506e1c9de", "50003720181011019", "酉阳县城投·伴山华府房地产开发项目（三标段）", "21587a59-0444-4ea6-8817-a6e99234b325", "江西省城建建设集团有限公司", "baad908b-dd8d-4de4-979a-77a2a92976ab", "91360983733939357R", "酉阳县", false);
            AddAccountsAndProjects(dbContext, accountList, "11968337-289c-423b-b0f0-02957040c9e", "50003720181011024", "酉阳土家族苗族自治县中医院迁建项目", "f732007f-200f-4a94-b9ed-108b6e6686a4", "中冶建工集团有限公司", "ed749977-e7b0-4053-8f6c-7147f17fbc06", "91500000795854690R", "酉阳县", false);
            AddAccountsAndProjects(dbContext, accountList, "af524887-cd5f-4a17-b4e9-da1980ae2c5", "50003720181011017", "酉阳县城投·伴山华府房地产开发项目（一标段）", "ee17a422-22a7-4f82-8c06-0dd8e5a3b63d", "重庆康悦建设（集团）有限公司", "38e12b7e-b033-4fe2-990c-93fcb0b36591", "91500222203459823F", "酉阳县", false);
            AddAccountsAndProjects(dbContext, accountList, "6ac3f876-97f4-4aba-a629-cfb89cc6b61", "50003720181011018", "酉阳县城投·伴山华府房地产开发项目（二标段）", "05ddd2f0-4961-4c9d-a6dd-834bebd1e79d", "重庆项毅建设（集团）有限公司", "362b5751-f561-46a1-9fda-f24c2c45e874", "9150010967104897X2", "酉阳县", false);
            AddAccountsAndProjects(dbContext, accountList, "c0eda7d4-fc6d-4a8b-bfaf-70a0be337ce", "50001620181008107", "天悦府9-17#、17-1#、15-1#、10-1#门岗、部分地下车库及设备用房建设工程", "05bfb4a4-61b1-4bcc-b89a-0335c107e5e1", "山河建设集团有限公司", "41ac257a-0e81-4d60-870e-c8b4f35ffb95", "91421121272000266B", "永川区", false);
            AddAccountsAndProjects(dbContext, accountList, "eac1efed-99e6-4bd0-be73-2b0b513febe", "50001620181008019", "中船华尚城一期工程", "afa0ff20-19fd-41eb-85e2-06c892469449", "中国核工业二四建设有限公司", "86c96884-b675-437e-8a46-981cf963852d", "91510000621600455J", "永川区", false);
            AddAccountsAndProjects(dbContext, accountList, "c834c9c2-5c92-42a6-98c1-fb649058f50", "50001620181008060", "万达B1B2地块Y20、Y27-Y31、S8-2、S9、D3B车库", "a96809cb-84ff-49a6-b411-ea4a8f0f7055", "中国建筑一局（集团）有限公司", "34e0ac00-93cc-4282-b569-a39d60405d97", "91110000101107173B", "永川区", false);
            AddAccountsAndProjects(dbContext, accountList, "3e62dcf2-a9d9-4f97-bc8f-d90ac4b072e", "50001620181016164", "金科.集美天宸一期（1、7、10-15#楼及相应地下车库、门卫）和二期（1、2、10#、1-2#门岗、警卫室）建设工程", "c4b6fd95-a757-44c7-9311-2baaf67502ef", "重庆大江建设工程集团有限公司", "61fd7804-4803-4256-9654-e5028cc3bc88", "915001056220734903", "永川区", false);
            AddAccountsAndProjects(dbContext, accountList, "6c89de1a-0576-4016-9099-d94864c48c2", "50001620181008077", "金科.集美天宸一期（二标段）2、6、8、9、16、17#大门及地下车库", "4a742dcc-0224-4b4b-b53b-10ee5de45618", "重庆大江建设工程集团有限公司", "508cf838-373f-4285-89a9-c62f7dba2510", "915001056220734903", "永川区", false);
            AddAccountsAndProjects(dbContext, accountList, "f0b2b66b-1c4b-485b-9839-a7a5365d659", "50001620181008076", "置铖荣华府1、2、9-12、16-20#楼及地下车库", "8b04c130-fe4f-4a8b-817c-ae1a3217445c", "重庆吉隆建筑有限公司", "f3d9b3b5-c5a4-4cd0-8eff-c83c1c7f3917", "91500118203791076F", "永川区", false);
            AddAccountsAndProjects(dbContext, accountList, "92969b52-851c-4915-ad11-e11b16cd9e8", "50001620181008036", "永川长岛汇7-10号楼及地下车库", "0b2c1d9b-be22-4586-992f-c6e10c419333", "重庆建工第八建设有限责任公司", "1fe72eac-020f-40d7-9b47-641e444b770c", "91500000203137250B", "永川区", false);
            AddAccountsAndProjects(dbContext, accountList, "a2a2aa5c-bcf1-4262-934d-8ef1440592b", "50001620181008113", "礼悦东方（1-3#、6#、7#、9-10#、12#、13#、15#、16#、18-27#、30#、S2-S4及地下车库）", "a661d0b0-2b02-49a8-aaa4-3fc317bc533c", "重庆建工第九建设有限公司", "d6230f6c-bba3-41bf-9e3e-f6a140e12ac0", "91500000203145189X", "永川区", false);
            AddAccountsAndProjects(dbContext, accountList, "7a5eade3-5371-453b-b930-beb055b9b60", "50001620181008073", "凌云阁项目二期组团建设工程（7-11#、1#商业及地下车库）", "e6b3bac0-a922-4ce3-8418-2501add23868", "重庆建工第九建设有限公司", "d2785dc7-ca99-49cc-abd2-de5367c74c63", "91500000203145189X", "永川区", false);
            AddAccountsAndProjects(dbContext, accountList, "e1cb0650-2151-47bf-9e49-cbe1a5c895f", "50001620181008116", "永川服务外包E区工程", "f37e59b7-99b1-485c-ac5c-d2e892b9f77b", "重庆建工住宅建设有限公司", "11b51732-939f-498d-8d35-45a1e1ef16cf", "915000002028009114", "永川区", false);
            AddAccountsAndProjects(dbContext, accountList, "880083b0-2516-4087-94ef-9566c79386f", "50001620181008059", "凰城御府三期一组团33、35、36、39、40#楼及地下车库", "3eb2242a-e786-4a81-85a2-44f0874ef070", "重庆利安人居建设工程有限公司", "7914080a-d6c0-4b21-8b54-cba258c5d07f", "91500118305024927M", "永川区", false);
            AddAccountsAndProjects(dbContext, accountList, "8540a184-b120-4493-87b0-e698ec15f2b", "50001620181008078", "昕晖.香缇时光D组团6-15号楼、1-5#商业", "efab91d4-a86f-46b2-97af-aea2e0a9b125", "重庆鹏威建筑工程有限公司", "cb91d01c-b600-4a14-bd68-f1e3ad6330e3", "9150010857344416XK", "永川区", false);
            AddAccountsAndProjects(dbContext, accountList, "019d03fc-bd80-4d25-93c6-d9039b05246", "50001620181016096", "协润.凤凰世纪城三期14-18#楼及车库A区", "6c4826b5-9eb1-4e7c-803f-c14f07652824", "重庆腾宇建设工程有限公司", "a1cfeedb-cfc3-4569-83d8-4d93d90fc191", "91500109739828762U", "永川区", false);
            AddAccountsAndProjects(dbContext, accountList, "4d7d25a7-bb57-41a0-97d1-19e71690afe", "50003620181007027", "兴源·黄杨郡二期二标段", "1bccc679-bc26-4379-8f7f-bb81153f4c9d", "湖南众诚建设工程有限公司", "3e4a6b8b-4e55-44ae-9e6c-42bbd9b52209", "91430000707239613F", "秀山县", false);
            AddAccountsAndProjects(dbContext, accountList, "dabd3091-7df6-41d9-bc1d-ea3029b387b", "50003620181005026", "秀山县丹凤华庭建设工程", "ac87ec0d-8b6d-4db7-97ad-c7e0afbc3e84", "中冶建工集团有限公司", "8b40196b-c766-465f-b9f0-c8d8ab2a5815", "91500000795854690R", "秀山县", false);
            AddAccountsAndProjects(dbContext, accountList, "fb36a39c-7ec6-42f2-aa38-fc66dbe118f", "50003620181007001", "秀山凤凰嘉苑二期建设工程", "c5a3d39e-076e-4c72-8ebe-55cd49ee0fca", "重庆万美建设工程有限公司", "913a88a5-ed78-4385-bf51-90a45467719e", "91500102208557377K", "秀山县", false);
            AddAccountsAndProjects(dbContext, accountList, "a2ea889a-08a3-4e19-9aa8-aba138c3821", "50002620181017020", "武隆.江上明珠1.2.3.8号楼", "d6e25cb3-b48c-47b0-af30-b426eb4e6043", "中核华辰建筑工程有限公司", "ebaa42bf-72a4-4f76-8372-efc6c24aec31", "91610000661156408T", "武隆区", false);
            AddAccountsAndProjects(dbContext, accountList, "3cd071a2-aae1-4450-a01f-bf5419e58cb", "50002620181017033", "武隆县职业教育中心改扩建项目", "f3aa9201-36fe-4d06-8321-c97978786358", "重庆建工第八建设有限责任公司", "76460021-d069-4f47-8112-2483f1ce2d84", "91500000203137250B", "武隆区", true);
            AddAccountsAndProjects(dbContext, accountList, "cec7f4a6-3b7f-4560-a7a0-f610c01e940", "50003420181018022", "碧桂园.翡翠郡二期南区28 --37号楼及（地下车库）", "f1a97753-95b6-4b0c-8abe-3ddab0d094a3", "中天建设集团有限公司", "e149bbbb-6cf1-488c-8eda-0bb78eef84b5", "91330783147520019P", "巫溪县", false);
            AddAccountsAndProjects(dbContext, accountList, "344f88fc-0ffe-465a-8bbe-e9286aa2383", "50003420181018008", "巫溪县畔山悦府住宅小区", "12e4c728-1f02-458b-bec7-fb874f3d4211", "重庆渝鸿建筑工程（集团）有限公司", "98c97e25-f4bf-4ceb-a71c-33d2b1b9a7c4", "915001056220060368", "巫溪县", false);
            AddAccountsAndProjects(dbContext, accountList, "61f702b3-8cc1-45d1-ad67-fb63cc159e4", "50003320181008014", "中昂·新天地（10-07地块）（1#-6#楼及地下车库）", "c3021b32-79ee-4d8a-88b4-20a8d50c6200", "北京纽约建设发展有限公司", "a55455c9-d33d-4927-ab0c-60cbab801d97", "911100006635560265", "巫山县", false);
            AddAccountsAndProjects(dbContext, accountList, "af36d289-e364-4d09-bd4f-9c95bd9fd2a", "50003320181008022", "亿丰国际商贸城一期", "e5c62039-51eb-438b-9be3-494909f4232f", "重庆大禹宁河建筑工程有限公司", "30ac4bf9-1c88-4419-8f5d-2a84d2f6ecb4", "91500237092410946X", "巫山县", false);
            AddAccountsAndProjects(dbContext, accountList, "c6215d67-7956-4b89-a49e-9a360d54d41", "50000120181015005", "博翠江岸小区1#、6#至14#楼、门岗", "acfd4773-717c-42a7-a6f9-855e52ed2914", "南部县锦兴建筑安装工程有限公司", "9622709c-40b1-4c2b-8bd5-da6ff52e132f", "91511321584213383R", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "fd3935b8-073a-4044-b582-3b97a6e8ede", "50000120181015095", "万萃城一期B组团项目1、5、6号楼基础及对应地下车库基础工程", "98d675aa-2ac7-41b5-89e9-36b2410b55df", "上海绿地建设（集团）有限公司", "f9daf911-8088-4c20-8ae0-dbedebc07559", "913100001331263364", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "1a4eebe4-7e1b-404f-834d-9b3a75a7600", "50000120181015298", "重庆市万州三峡文化艺术中心一期大剧院", "b054cae4-73b6-4786-a7b9-0e802b2cab69", "浙江精工钢结构集团有限公司", "383a3254-9d60-4c14-a3bd-3afa8c369050", "9133062171252825XY", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "878d695e-8b7c-43c2-963b-ef81f1667e7", "50000120181015177", "万州区旧城功能恢复连接道建设工程-高梁收费站连接道三标段", "f3b5e22a-ff78-4808-a843-fa59e8a75df0", "重庆建工第一市政工程有限责任公司", "fbd1e3a9-438a-4e4f-8f1b-d933019310cb", "91500000202800874X", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "fd050eb5-b6ae-47fc-9e0c-1773b94fb43", "50000120181015025", "金科观澜(二期)高层G-9、G-16至G18号楼；洋房D-38号楼；商业S-5A、5B号楼；岗亭B-3及对应车库及附属工程", "5e992202-0666-417b-a733-ee6f7f604309", "重庆建工住宅建设有限公司", "86aa210d-edcf-457c-bda1-90f6b3f66a47", "915000002028009114", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "15d58005-f8eb-4f5e-865a-0c8c98e696c", "50000120181015137", "万州经开区高峰园创新孵化基地建设工程", "2cb7114d-e677-439d-a3f0-59486c5468e7", "重庆教育建设(集团)有限公司", "92f5276e-6a2d-4c27-8c91-c297cda05b43", "91500000203645116A", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "a6067ea6-1525-4fcc-9fea-7fa46dfd1cb", "50000120181015115", "万州二中江南校区（第二次）", "4642e5bb-c8b5-42b0-8996-12d78ca74a60", "重庆金晟源建设工程有限公司", "f2455afe-9cb9-48c6-9db4-abb333271834", "91500101207934249H", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "459099f1-c5be-4a05-b575-402daef5e09", "50000120181015044", "南滨上院C区一标段", "12af5773-c1e7-4679-9462-968d601603ee", "重庆金晟源建设工程有限公司", "af85e411-0fdf-48a8-a2d9-b6110dfe7a70", "91500101207934249H", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "67ef02b5-a7cc-4b01-80b5-5fb31ad91c1", "50000120181015272", "重报·万州中心二期工程（5#、7#～13#楼及相应车库工程）", "9ec38688-a4ca-4354-bf73-86fee1f33124", "重庆市万州建筑工程总公司", "79ac01d2-d9a2-4eef-8601-befdb5a93780", "915001017116015079", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "25066f38-f23e-4f0d-b110-b5fe13695e9", "50000120181015094", "天生天城二期（B1、B2、B8、B9号楼）", "76574a7c-95ce-44ad-94fd-47314a9eb978", "重庆市万州建筑工程总公司", "b5c84fc3-e638-44b1-a85c-b952742f0dcb", "915001017116015079", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "2fd6be08-0229-404b-84a4-4ca201760e4", "50000120181015044", "南滨上院B区一标段", "f5cc4f68-d4d2-4d24-b35b-240b76ac9598", "重庆市万州建筑工程总公司", "645455d5-b1f9-4175-a2fe-67636a22903a", "915001017116015079", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "d888b092-8ffb-4f3e-86e9-6fb1db6980b", "50000120181015263", "御澜府一期13、15、16号楼及中庭车库", "a230e9f9-87c0-4006-bd61-36432a37c0fa", "重庆市万州水电建筑工程有限公司", "8efce8f2-2fa2-45bf-b67e-696f7c6c4984", "91500101207912963T", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "e25a6cf3-33e8-4ad9-a8e6-548be8f0b3b", "50000120181015105", "万山国际三期33#楼、34#楼", "de4d76b4-7bfb-4263-a6cc-f37c25bcfa78", "重庆市万州水电建筑工程有限公司", "7e647a7b-c150-4c15-973d-24eef03a434c", "91500101207912963T", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "d9dd4fd3-0493-4907-b0ef-6d9567df315", "50000120181015224", "万州区长岭新城区主干道工程——香山大道部分市政工程（二期）", "3e0a7ea6-a99c-4cde-affd-043ea2465501", "重庆市渝海建设（集团）有限公司", "afff54a2-344c-4f25-8c8e-069c109c1d39", "915001017093776733", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "1e24314e-2adf-4b43-870d-6b5c7e31fd3", "50000120181015040", "南滨上院B区二标段", "84a43517-1c75-45d6-bc76-06d603321ae4", "重庆市渝海建设（集团）有限公司", "69d28a96-c8b3-482b-89a6-b01186b4ce99", "915001017093776733", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "6416dba5-b01b-45ad-b149-47d0656303d", "50000120181015071", "书香华庭(2#、3#楼)", "7df36cc6-9526-4d89-88e8-e97f3eaef75b", "重庆太白建设（集团）有限公司", "1ebd26bd-87a2-41a1-8866-acde55da0ac1", "91500101711601021C", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "cc5689bb-4f49-4b36-b23c-756f85203dc", "50000120181015041", "南滨上院B区三标段", "5e6d6c0a-1f95-4839-a462-72117728d2c7", "重庆万港工程建设有限公司", "18100618-7cea-4cf5-bf57-8f8ee4850f0c", "91500101711685747K", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "89d7083c-8d36-468f-85e5-181f8d623e9", "50000120181015103", "万山国际三期3#车库B区", "31df38d5-4ac2-48ca-9a08-d7f62fe4f95a", "重庆万州兴涛建筑工程有限公司", "074fd335-df70-436d-bd11-3b47f5e16048", "91500101207912074U", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "ab018f6c-5ad6-48aa-b5d5-9783715e477", "50000120181015093", "万萃城一期（A组团1号楼-17号楼）", "e1c27d0b-320d-4dcf-9cb4-63ceff7c923f", "重庆欣耀建设工程有限公司", "59d48946-d41c-4136-9dd6-8c76f1488210", "915000007659339370", "万州区", false);
            AddAccountsAndProjects(dbContext, accountList, "d006e3ed-2e3d-43c1-9e48-ed3fe3704dc", "50004120181010038", "滨江壹号.首座", "c1921b8f-ed62-496a-8552-81694d33c503", "四川省第三建筑工程公司", "cc9b1dcb-4194-4325-ba8e-f510ff1c328a", "915100002018080067", "万盛经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "3afa7d2a-d3a9-4f03-a483-725711633cb", "50004120181010016", "万盛经济技术开发区城市综合管廊工程（一期）鱼田堡组团", "272b441e-efec-497b-b3a2-0fdd4017d962", "重庆建工第二建设有限公司", "64be0d0a-61e7-4637-a46d-0a9cbfae3284", "915000002028051812", "万盛经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "2fa99565-c2b3-467b-8f55-d9a3478cfde", "50002220181017001", "工业园区南区标准厂房一标段", "4531aa57-2eb0-44aa-bada-eb573b72ca8c", "中冶建工集团有限公司", "12d896c0-c66e-4c9f-ad07-28618b1363c0", "91500000795854690R", "潼南区", false);
            AddAccountsAndProjects(dbContext, accountList, "5f03d7cd-ef28-4c9e-b83e-94ff6f5076e", "50002220181017004", "重庆市潼南区中医院创建“三甲”医院建设项目", "18a76b03-e703-4d9f-8763-2e5f5a77909d", "重庆建工第三建设有限责任公司", "d4e3950c-87dc-4201-a94d-af0633c37d84", "915000007339743120", "潼南区", false);
            AddAccountsAndProjects(dbContext, accountList, "59ca29d8-c013-4cab-a4c9-dc4230ed582", "50002120181018040", "曜阳·龙泉香榭工程一期（1#、6-8#、13-16#、36#楼，6-8#楼地下车库）", "e9d58b83-f8b5-4fac-b5da-219ff2b73401", "贵州仓达建筑有限公司", "b4527ad3-aaa2-4cc5-a8c7-55839028faed", "915205223373883976", "铜梁区", false);
            AddAccountsAndProjects(dbContext, accountList, "f269d72a-30a3-42f0-bbac-b7c554845a1", "50002120181018037", "泽京·龙樾府2、7至12号楼及一期车库", "156bea04-d093-49ea-a3e6-c193da995fbb", "西双版纳云海建筑工程有限公司", "62a5f68c-71e2-40ea-b1ab-1e6be2e8839d", "91532800080409024X", "铜梁区", false);
            AddAccountsAndProjects(dbContext, accountList, "17e195d0-79e3-4650-8c55-eedd00980bb", "50002120181018068", "台湾中小企业园生活配套服务区“博悦.悦城”（二期10#、11#楼）", "86062d22-09ff-4fbf-9985-dd2b6eb25ce1", "重庆灯塔建筑工程有限公司", "3dbd505a-91d6-4faa-8fc4-b8542ba71ce2", "91500109203200088U", "铜梁区", false);
            AddAccountsAndProjects(dbContext, accountList, "cd06573d-8c14-4c89-b7ab-28c46c0d7b1", "50002120181018006", "嘉利世纪广场", "48659793-715f-456f-a3d7-799cf93f6942", "重庆市茂森建筑工程有限公司", "4b6a21a9-4a36-431d-a9bd-a1e4a37f693b", "91500224203705246R", "铜梁区", false);
            AddAccountsAndProjects(dbContext, accountList, "39cf9505-6a46-4b04-84b1-7cb68562726", "50003520181018004", "黄金水岸一期4#楼", "116b8682-d6ac-4f1d-a05c-1263b503fe14", "中冶建设高新工程技术有限责任公司", "ada75e6c-9887-4c5c-8b73-4cf270599dea", "91110000100023357T", "石柱县", false);
            AddAccountsAndProjects(dbContext, accountList, "978c0e6b-d24d-496e-8860-948b012e89f", "50003520181018003", "财信城二期II标段1#、2#楼", "e0f3f923-3e2e-4ad7-bc3f-fd6f33e3a12e", "重庆桂佳建筑工程有限公司", "c1aa4534-81ca-4a87-9b63-086944769dbe", "915002317935449124", "石柱县", false);
            AddAccountsAndProjects(dbContext, accountList, "db311018-9ee7-44e2-9763-aa00dbd4325", "50003520181018001", "业宇峰.寰宇世家8#楼及周边车库、10#楼、12#楼", "1e2d925e-223a-4d9b-ab90-c8c82652053b", "重庆建工第十一建筑工程有限责任公司", "a4406ad6-010d-474f-8e0c-1506b5405c48", "915001072031018215", "石柱县", false);
            AddAccountsAndProjects(dbContext, accountList, "4f59bdb7-84d5-429e-90c9-e405952fb29", "50003520181018002", "碧桂园天麓府一期（1-3#、5-7#楼及2#车库、8-13#、15-21#楼及3#车库）", "5dcab672-d0a0-4e1e-b9eb-348df383f133", "重庆市巴南建设（集团）有限公司", "b003488a-50b6-4ccc-ad9b-0a9e96315bd9", "91500113203436859R", "石柱县", false);
            AddAccountsAndProjects(dbContext, accountList, "4b229c25-cddc-44a5-875b-df3f548be25", "50000720181013112", "富力城二期2R组团（A1号楼、车库）", "ea111f6c-310a-4d0f-a78a-39b4226688e6", "广州天富建设工程监理有限公司", "5f8bdc87-ac26-47bd-964f-00a515942cd8", "91440101734893848B", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "a82592cc-65d3-4bcc-964c-89ae9be551f", "50000720181021051", "富力城二期2F组团一期（A1-A6号楼、北区地下车库）", "2da642a4-1534-4101-8013-a83b544f17aa", "广州天力建筑工程有限公司", "6e892b14-a00a-4ac3-afc8-4791127befa7", "91440101190498128T", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "d91e688a-6240-41f1-b923-6b15c2525bd", "50000720181009019", "富力城二期2R组团二期", "23aab36d-bdac-41aa-804b-b8ae97835b13", "广州天力建筑工程有限公司", "f9452730-2670-4a96-866a-6a5077987e77", "91440101190498128T", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "9b17108e-4975-4295-a8e1-9d7797137b0", "50000720181021097", "富力城二期2R组团（A2-A7号楼、S2-S4号楼、车库）", "6344536a-1dbe-4e9e-abe4-6370792c1c3c", "广州天力建筑工程有限公司", "cb479fe0-a1ad-48fb-88f5-1c564c93db43", "91440101190498128T", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "22eaf69f-01f6-4d87-bbfa-09eded820fe", "50000720181010125", "美的金科郡（30#、31#、39#-41#、51#-52#楼及地下车库）", "d7f80983-6bb9-4b76-bfd0-d1584bb376fe", "国基建设集团有限公司", "14ffee69-f39e-4e62-a209-34a8bb267554", "911400007159997781", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "0303781d-2579-4ae6-afc8-74aa207337a", "50000720181004067", "金融街.融府项目双碑组团E分区E52-1-1/04地块一期", "71abf889-40ee-4ced-87d6-d20efec70b32", "上海建工一建集团有限公司", "fdc5f0de-b4a6-476d-9eb2-a97e7feb9f3b", "913101151324008074", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "c347b6fc-8bbd-4e70-b1c0-29fbbf01754", "50000720181001010", "金阳.大学城第一农场一期五组团（89#、91#）", "de3b0815-61e4-4a06-8869-af0b66f6d140", "四川飞腾建筑工程有限公司", "e2732bc7-af89-4106-949e-adb9b255c84e", "915117247208860494", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "f9372bad-e5b4-4d68-bf7b-f84f5483de2", "50000720181010127", "金阳.大学城第一农场一期四组团（39#-69#）、一期五组团（88#、90#）", "2ef4c2f4-727c-4a43-bbb4-2cb63279c051", "四川飞腾建筑工程有限公司", "2a2610b6-23d1-4990-a2f0-ea78fafd8c10", "915117247208860494", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "c5cb70a2-2d65-447c-a4df-4913c1ec782", "50000720181004064", "万科沙坪坝区沙坪坝组团B分区B12/02地块项目2#、3#、6#主体工程、车库基础及2#、3#、6#装饰工程", "9259c0ec-78bb-44bc-8d72-056a840b9ecd", "四川时宇建设工程有限公司", "c4e16e5c-1075-4f5c-b4fd-708c745cad70", "91510000595089285M", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "c86cddb6-0883-4b18-96dc-aa52f13492a", "50000720181012028", "首创天阅嘉陵项目一组团（1-8号楼、20、21号楼、001#地下车库、门卫室）", "234d772e-5f99-4daf-99ec-d5f30a442acc", "中国华西企业股份有限公司", "71d25819-f50e-4802-9b95-69d945d704ab", "91510000201808890B", "沙坪坝区", true);
            AddAccountsAndProjects(dbContext, accountList, "1461c6a3-df26-4bf2-95ce-334369f236b", "50000720181004030", "重庆万达文化旅游城A-08（L77-1/01）地块项目", "eb64c564-c9d7-47bd-969c-fd15279ad051", "中国建筑第八工程局有限公司", "aff25aad-d4bf-4c9a-a6b2-0041f517cdfc", "9131000063126503X1", "沙坪坝区", true);
            AddAccountsAndProjects(dbContext, accountList, "723bd3fa-c4a2-4243-a598-de28dd30885", "50000720181013007", "重庆万达文化旅游城B-01-1(L81-101)及B-01-2(L81-201)地块项目", "31476cbd-c04c-4477-b5e3-aef6229470c9", "中国建筑第八工程局有限公司", "69da0ecb-72c7-4b76-97e6-611864810c66", "9131000063126503X1", "沙坪坝区", true);
            AddAccountsAndProjects(dbContext, accountList, "8ea0b793-bbfc-480e-b3d9-3d24818eb37", "50000720181011133", "重庆万达文化旅游城万达茂（L68/05）地块项目", "f83926fd-036b-4601-a0e5-67bf06d6927d", "中国建筑第八工程局有限公司", "6aa06b12-24bc-4e84-ad39-9dda38777743", "9131000063126503X2", "沙坪坝区", true);
            AddAccountsAndProjects(dbContext, accountList, "b20885ff-f216-41a9-a7b2-a90ab52f233", "50000720181003118", "重庆万达城酒店群L74-1/01地块一期项目", "d3ab4aa5-b068-4de6-9cda-b4a3b785226d", "中国建筑一局（集团）有限公司", "69a66c34-da04-49f1-88f9-813477ccac05", "91110000101107173B", "沙坪坝区", true);
            AddAccountsAndProjects(dbContext, accountList, "a1577359-7c8b-462a-8117-5be9c1d7a88", "50000720181014025", "西永组团Ah01-Ah24标准分区道路工程", "5e094f8f-9eab-4694-abba-755c211fe5c6", "中国十九冶集团有限公司", "0f8245a8-317b-4183-8f05-f287aa7b135c", "91510400204350723Y", "沙坪坝区", true);
            AddAccountsAndProjects(dbContext, accountList, "28e1fee0-93de-43b5-8969-0520f44c258", "50000720181029132", "协信.天骄名城四期（二组团）", "c7c894f3-969f-451e-8541-9db928fd4e94", "中建八局第三建设有限公司", "103ecc20-cf7f-48cd-8729-8043ad3f6632", "91320100134891128H", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "fb81997e-245a-4883-b169-b2b46b643f0", "50000720181029137", "美的金科郡(13#~26#楼及地下车库、幼儿园)", "5c720c2c-3aca-4338-b67f-4d3f127fdf8b", "中建二局第三建筑工程有限公司", "adbf18ff-4060-4974-8041-519574c2591b", "9111000063370987XW", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "e9da59e2-a3bc-4a68-8b88-4000aedfa3c", "50000720181015078", "美的金科郡(42#-50# 门岗4、地下车库）", "0b36fd97-ed59-44f4-ba52-d9701525cb86", "中建二局第三建筑工程有限公司", "ab083327-3544-4b8c-8fe1-0eff256f6a59", "9111000063370987XW", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "68b6cc7f-e29e-4bb3-bd03-c90c8cec541", "50000720181011103", "美的金科郡（1-12号楼及地下车库）", "b3610e55-1a72-4f80-9ee9-ec24db343314", "中建二局第三建筑工程有限公司", "95a1a36a-7398-4bc0-8506-e36233b9f20c", "9111000063370987XW", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "09bd7800-4eb5-4b07-9951-f4d3cf2a910", "50000720181007109", "恒大未来城1#地块（1-9#楼、地下车库、门岗、含室内新装修）", "ecd75db4-ca5c-4b45-a220-f07e1edc0a98", "中兴建设有限公司", "d67efca0-5b25-4d65-8f74-659a485eec29", "91321283737840047C", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "62a2e4a6-0f44-49d1-ad79-4c6dfb289cc", "50000720181009016", "重庆科技学院产教融合实验实训实习基地工程", "d635754d-4d78-42a0-8641-a847a2c808d7", "中冶建工集团有限公司", "30e7721e-f5cf-4ad4-96e0-f87bab6401ff", "91500000795854690R", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "ee5cc80e-a5b3-4bc4-a924-7c69e5da0b8", "50000720181009039", "西永组团L22-1/05地块(龙湖西永核心区项目)1#楼、15#～25#楼、S1#、S20#～S22#楼及地下车库", "383b18a8-92b5-4e35-b3eb-e78b253ea963", "重庆诚业建筑工程有限公司", "45ec0d77-597d-439b-97de-63f0cc7ca5af", "91500107666403682M", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "2dc90785-2d32-4821-b350-e4970f7b6e0", "50000720181017089", "重庆沙坪坝西永L23号地块项目（L23-1/04、L23-2/04、L23-3/04号地块）", "90f920f8-8c38-488d-ab58-9f77da90d4c9", "重庆诚业建筑工程有限公司", "86e6471a-53c7-4f3b-b3c2-c7a739f82f72", "91500107666403682M", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "bdcdb637-2b39-4bf6-a19d-b4eb0da733a", "50000720181009014", "金科西永一期L29-1/03、L29-2/03地块", "8702478d-9124-4772-9585-87182e95935a", "重庆海博建设有限公司", "4dd915a3-f41b-4e89-89ad-1ae5530ccc45", "91500112203510350T", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "7809b728-9a97-48bb-a270-b3caa2866d7", "50000720181021094", "龙湖西永微电园项目（L30-1地块）1#～3#楼、S1～S4#楼及地下车库", "239598b2-3ec9-47bf-ab65-d6ec4ba39dd8", "重庆建工第三建设有限责任公司", "d081afce-2e0e-4ab6-a251-c6bbb02a38fd", "915000007339743120", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "c898fdb4-1a79-4f8f-8a02-5188a2458c1", "50000720181009129", "渝开发·格莱美城一组团总承包", "bae95bed-d57b-4461-9daa-00fd9ebbc6ed", "重庆建工第三建设有限责任公司", "f7e5b4fa-d3fb-4e07-839f-bd3fe8065e09", "915000007339743120", "沙坪坝区", true);
            AddAccountsAndProjects(dbContext, accountList, "759470e4-3db9-4897-9780-97ee1f34beb", "50000720181021056", "重庆大学城中央公园暨城市功能项目（重庆.熙街)四期总承包工程", "0b168655-e70d-448a-a111-01bc3a5a7d85", "重庆建工集团股份有限公司", "c5235d76-0c45-4933-a72a-1040a8fe7b91", "915000002028257485", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "d8b895a2-b55f-4b94-9935-dcc4d37effa", "50000720181012024", "重庆铁路口岸公共物流仓储项目工程总承包（EPC）", "efe98792-9e8b-425d-b250-ce2dcde82e34", "重庆建工集团股份有限公司", "c6ff4725-e80c-4a38-a3d6-5a875ab7667c", "915000002028257485", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "d4f2b971-dc23-4e16-a482-b5d098da33b", "50000720181023044", "金科西永二期L29-3/03、L29-4/03地块", "116a72ec-095e-4f54-96ba-514261d72cdd", "重庆建工住宅建设有限公司", "351efe61-a09a-4dff-8486-fbe74da53231", "915000002028009114", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "73f2c01a-8313-44a6-b1fa-5fd62b6b92d", "50000720181004064", "金科西永天宸L48-2/03地块3-1、3-2、3-5~3-9#楼及对应商业及对应3-15#车库、岗亭3-13、3-14#楼", "01f2e826-2e9a-4701-bfd3-0c74a667d699", "重庆建工住宅建设有限公司", "748d757f-4328-4558-a989-6bf341aee65e", "915000002028009114", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "6fce48e2-8726-47f1-a778-c59cd7f036c", "50000720181011104", "融创朗裕特钢项目E38-7-1/04地块（二期）", "10b6a47e-329b-4053-9650-9fe278cf14cc", "重庆市巴南建设（集团）有限公司", "eda54417-ac8b-49a9-81f5-2f233670b324", "91500113203436859R", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "6105238c-5f90-47f9-a864-b0f30a02c0d", "50000720181009041", "融汇泉景D区7号楼及商业和地下车库（部分）", "2ad32f43-8155-4844-aa7b-dfdeba0f8103", "重庆市佳诺建筑工程有限公司", "77b004f9-e73a-4981-98fb-eb80bb90f44c", "91500115203353877R", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "5b71fe43-05ef-4782-82c1-3e51ef7f4a7", "50000720181015106", "融汇泉景D区二期8、9号楼及部分商业和地下车库部分", "1ad5b221-9a36-478a-be12-6bd2f8d28941", "重庆市佳诺建筑工程有限公司", "d3828c59-2f90-4317-904c-86464d5ff0a8", "91500115203353877R", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "f5e38146-f820-43b8-925e-93e30412d12", "50000720181012023", "南方新城（一期）C11-1地块G、E组团.公园华府5号楼及其对应车库", "5a5d34e9-631a-4ad5-aabc-93b1b72d2fc3", "重庆拓达建设（集团）有限公司", "c2cc1124-21ab-408f-adee-f54f1102a65b", "91500108768876288X", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "bc052f64-5b0f-49d7-9d2d-628c3d116c1", "50000720181004063", "万科沙坪坝区沙坪坝组团B分区B12/02地块项目1#、4#、5#主体及1#、4#、5#装饰工程", "27a4d34b-9de1-45b5-9f41-cd55b2fda36f", "重庆万泰建设（集团）有限公司", "2f87ce0b-87ad-4d63-9ec5-b25ac029a953", "91500112784219544A", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "a591e78c-e2ee-497c-9aa0-2dee5a749e7", "50000720181016088", "重庆沙坪坝西永L23号地块项目（L23-4/04、L23-5/04、L23-6/04号地块）", "612f55d1-4096-4380-ba3b-8a9b373ed283", "重庆万泰建设（集团）有限公司", "ce17e928-a343-45ce-97cf-f006fc79a05b", "91500112784219544A", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "d4886419-6fe3-4c46-959b-7c13ca731b8", "5000072018100938", "西永组团L21-1-1/05地块（龙湖西永核心区项目）1#～12#楼、13#楼（幼儿园）、S1#～S3#楼、S9#楼及地下车库", "15a4bfb7-e121-4043-8996-e6d61cd20687", "重庆先锋建筑工程有限公司", "52d461bc-0338-4fdf-9faa-8ddb8180e50f", "915001137094571042", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "d6f78235-11ef-4370-b1b6-89456fdf739", "50000720181021050", "西永组团L22-1/05地块(龙湖西永核心区项目)2#～14#楼、S2#、S4#～S7#楼及地下车库", "e2d6f1d9-7a18-4b31-9ade-3f9ba69917dd", "重庆先锋建筑工程有限公司", "d63b3cf7-6fce-4313-82ba-c0d9837dce20", "915001137094571042", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "c4692dcd-4be9-4d91-8839-3e5476b8556", "50000720181007110", "西永组团W13-1地块（龙湖科技学院项目1号地块5组团）3#楼～8#楼、15#楼～18#楼、28#楼-1、29#楼-4、29#楼-1及地下车库", "45706be1-bf76-4ce1-96c4-743fce25daa4", "重庆先锋建筑工程有限公司", "127178d4-e8ef-4706-8053-8159cca1b7f3", "915001137094571042", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "caa70beb-b18c-4640-b22d-d13e40a5a8f", "50000720181016084", "西永组团W13-1地块（龙湖科技学院项目4组团1标段A区）1号楼、11号楼、13号楼、21-25号楼、28#-2号楼、28#-3-A号楼、29#-3号楼及地下车库", "a420eaa4-fa14-4fa3-a8ef-11b57d602f9b", "重庆先锋建筑工程有限公司", "39647f21-1a2e-4673-86f9-1aaaa0ddb35f", "915001137094571042", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "249d021a-4fa3-4cce-afb2-310e11babd1", "50000720181009012", "西永组团W14-1地块（龙湖科技学院项目2号地块2组团）1～2号楼、12～22号楼、34～41号楼、门卫、部分设备用房及车库", "5fd4ff69-ace9-41bb-8aa0-56882649dd40", "重庆先锋建筑工程有限公司", "d052c197-bfcc-449d-9bd7-230dced3ec7e", "915001137094571042", "沙坪坝区", false);
            AddAccountsAndProjects(dbContext, accountList, "14c3e313-628d-4681-a645-4a4b5b124fe", "50002320181008004", "荣府.天一城", "a5c83e8e-e39b-4692-9d89-768506507f16", "十九冶成都建设有限公司", "7094c1ba-974d-4cfc-864c-6cd4b1e7b67d", "91510100562038799X", "荣昌区", false);
            AddAccountsAndProjects(dbContext, accountList, "98dba6f5-9a67-4982-b823-e512d6cb687", "50002320181008014", "金科世界城二期34#-39#、46#、49#、50#楼及44#部分车库", "131fbb94-fcdf-4874-9676-e5e0c6e9a6dd", "重庆黔程建设（集团）有限公司", "519738f7-4e9e-4d68-9e3a-2f2295a0b7d0", "91500114213901275F", "荣昌区", false);
            AddAccountsAndProjects(dbContext, accountList, "d3483ed2-7cf4-433d-b027-1513c198b35", "50002320181008036", "昌宁苑", "b7f4d7b3-b235-470a-bd39-40744dbcc25a", "重庆跃城建筑工程有限公司", "e834e3d2-194b-4fe8-8341-e1def1f2248d", "915002265842852727", "荣昌区", false);
            AddAccountsAndProjects(dbContext, accountList, "0c0904ee-e032-4af2-9da2-2f2c84c75f9", "50000220181015021", "万丽·城市客厅旅游度假综合体一号地块1期B组团10#、13#、15#楼及地下车库项目", "5fe4478f-2b8c-48c6-9126-441dfc1d0f89", "重庆坤飞建设（集团）有限公司", "bdc1a92d-85c6-4125-bd6b-58dfc923e4a9", "9150010920320015XF", "黔江区", false);
            AddAccountsAndProjects(dbContext, accountList, "53d9caec-ca5c-43eb-a8e8-329c9f2b64c", "50000220181015025", "黔江碧桂园三期三标段工程（102#、103#、105#、106#及车库）", "552de071-4040-4a5a-ba83-2d2b90d36eb9", "重庆渝发建设有限公司", "5cfa8e6e-1bc6-4320-87c2-b5f263e81782", "91500115203399701L", "黔江区", false);
            AddAccountsAndProjects(dbContext, accountList, "82ef9808-0643-414a-8973-0ffab23d233", "50000220181015022", "中科·中央公园城二组团", "dce413cc-8d4c-4472-9e79-17e1c039b98b", "重庆中科建设(集团)有限公司", "0432c039-363d-4c0a-82ca-e170ebd339ee", "91500102709427140L", "黔江区", false);
            AddAccountsAndProjects(dbContext, accountList, "47b901ed-1cb0-4a80-a0ea-702ea8acbc0", "50001820181010139", "奥园金澜湾一期", "81eedb5e-328a-4ede-b287-bff790976743", "广东蕉岭建筑工程集团有限公司", "a3889aca-c8ab-4047-9419-3bc8c86cbb0b", "91441427196671139Q", "綦江区", false);
            AddAccountsAndProjects(dbContext, accountList, "0b1f842c-06b5-47dd-8ece-8628c59634c", "50001820181010155", "阳光城翡丽云邸", "81019455-b5e9-4884-b458-27cc167486e4", "中国建筑第二工程局有限公司", "c92eb6e8-f85b-4496-acfa-0d242ac8692e", "91110000100024296D", "綦江区", false);
            AddAccountsAndProjects(dbContext, accountList, "c5bb4438-5a55-460b-8e0f-af6d6692d37", "50001820181010135", "东部新城金凤-新城商贸", "4d194299-87b8-4307-a124-1d5dba4f026d", "重庆教育建设(集团)有限公司", "f8f541ba-8f35-47e8-b176-536c825e5812", "91500000203645116A", "綦江区", false);
            AddAccountsAndProjects(dbContext, accountList, "5f6e4d15-c634-48df-9f93-39867ac0878", "50001820181010124", "康德城市花园二期（一标段）", "ddf4e793-35b9-4354-a575-ae495255f8fc", "重庆锦庆建筑工程有限公司", "f483d499-5e1b-455e-a213-58ceffa659ef", "91500110203906048J", "綦江区", false);
            AddAccountsAndProjects(dbContext, accountList, "49c4d90b-9f45-4df4-89ac-4be7b708ea8", "50001820181010123", "重庆计华办公楼", "be0042bb-fa36-4d10-b1e3-05a4bf838471", "重庆市渝北区海华建筑工程有限公司", "6578d1bb-dbc4-4227-b0b6-5fa4b5c3b295", "91500112203540058R", "綦江区", false);
            AddAccountsAndProjects(dbContext, accountList, "8b688ec5-3fc7-48b3-8a84-e55069dfbbd", "50001820181008061", "綦江区城市交通综合体二标段", "aa6a0870-421b-4403-bcba-21fe879c6a87", "重庆拓达建设（集团）有限公司", "6c6ab4e2-7aa2-48c0-b6d8-b996f19710a3", "91500108768876288X", "綦江区", false);
            AddAccountsAndProjects(dbContext, accountList, "a86e78f7-c9fe-436f-9984-1480ba16e9f", "50001820181010117", "綦江红星国际小区B区6#楼", "184784bb-7ca3-4116-83f3-05e5ff32630f", "重庆中科建设(集团)有限公司", "102220b5-ede7-4299-b785-a148573464c6", "91500102709427140L", "綦江区", false);
            AddAccountsAndProjects(dbContext, accountList, "07ab55ac-5d67-4f95-9ef3-2a998429387", "50003820181018060", "彭水新领域（二期）工程", "8d8c390c-c655-4720-82ab-c216141774a0", "重庆建工第九建设有限公司", "b66c3851-af4f-4712-9f7c-7c6e98f566a5", "91500000203145189X", "彭水县", false);
            AddAccountsAndProjects(dbContext, accountList, "7fbb71fc-8a77-497c-833b-3db452aaadb", "50003820181010019", "鑫沃世纪城（二期）工程", "c03c95f2-da3f-4f81-a893-06eac3c72c8b", "重庆紫东建设工程（集团）有限公司", "6d8cb18b-aa9e-40ed-8812-09317c5294bb", "915001092032313197", "彭水县", false);
            AddAccountsAndProjects(dbContext, accountList, "58225469-e946-4a1d-bb39-d8823f14b5b", "50001720181008001", "鼓楼新天地房地产开发项目", "0c05c1ce-0379-48da-aa41-5aff729ec7a9", "四川省第三建筑工程公司", "4f290c10-1391-4e9d-8d02-9d0a6a799b58", "915100002018080067", "南川区", false);
            AddAccountsAndProjects(dbContext, accountList, "b61b37d8-2867-4003-a4fe-2fdcc36182a", "50001720181008003", "天馥城一期二标段", "6240e4c3-4203-4192-ad99-95845012296b", "重庆崇景建设有限公司", "34d2a9eb-55a2-4aa0-bc62-ba630acc5aad", "915001017116844455", "南川区", false);
            AddAccountsAndProjects(dbContext, accountList, "cc97e8d9-5aad-4e01-8430-be02ed68564", "50001720181008002", "南川区凤江晓月农转非安置还房建设工程", "ddff511f-45a2-4220-9af6-ad0b9c90f1a2", "重庆建工住宅建设有限公司", "1aeb0080-80ef-41a5-a038-b49ece72dcf9", "915000002028009114", "南川区", false);
            AddAccountsAndProjects(dbContext, accountList, "2a65efcb-1cb3-4689-9806-2888b6fc17e", "50001720181008086", "天馥城一期一标段", "46339633-0a4f-4dd1-a9af-f46aaf9d7861", "重庆建工住宅建设有限公司", "ffc97598-3b2d-417e-a7e9-64b6a87e52af", "915000002028009114", "南川区", false);
            AddAccountsAndProjects(dbContext, accountList, "bc1469c3-6206-433c-b707-605c95d23bc", "50000920181008101", "重庆金隅大成新都会项目(A23-2/07地块)", "e1d97b99-d889-4bd6-82bb-9869f0c9fb3f", "北京建工四建工程建设有限公司", "73ca40a9-c2c2-470b-8f54-beaef70f4d61", "91110000101510712K", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "0c5dd029-2202-4c1d-b88e-d6e9f4ad9e2", "50000920181008030", "金辉城三期一标段二区2#楼、3#楼、14-2、14-3、50#楼(门卫房和电梯）及部分地下车库", "be91afa5-df03-4aad-abc4-f8c00a8c733a", "福建宏盛建设集团有限公司", "324c763e-a2e2-47df-9a00-466191b296af", "91350100611325517C", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "14c67a93-e15b-4dfb-bfc6-a64e4f3a0b4", "50000920181017186", "保利二塘项目二期B1组团二标段一区", "a58eaf59-eb45-43b6-b1d5-e5b84d9195a2", "海天建设集团有限公司", "6d614c5e-c5d9-4200-b942-07276c4a7698", "91330783785663700R", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "64a67086-297c-4924-9446-ae3274a1ca2", "50000920181008133", "保利茶园项目（一期）", "60ef1e4d-de3f-4fde-b89b-8f36ef65a34c", "海天建设集团有限公司", "5492d350-3d3b-4a56-9a28-34a80c6db11c", "91330783785663700R", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "f80b8e58-52e0-4e12-81d2-7b1e018ea0d", "50000920181008058", "南坪组团M分区M10-3-2/06号宗地项目", "284e1411-4a4e-4a68-9140-1c7297fa8344", "华建利安建设集团有限公司", "12c432ea-2753-4ee0-b777-2b0413993fb4", "91510000749633694W", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "468195b0-6f0f-4865-8075-3f4f4a106a6", "50000920181008045", "君临南山项目（1#-32#楼、1#-4#车库）", "e912ff82-bd4f-48d0-82ed-3f952700431f", "江苏仪征苏中建设有限公司 ", "e9ead0c5-5ba0-4262-b82a-3d4d2f9bb855", "913210817307396915", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "a847465d-d0b5-4390-9d0e-d8cfa096313", "50000920181008049", "茶园新城区K标准分区住宅20组团2期二标段", "2cb3efb4-7faa-4198-bba3-5f8199e8cb7b", "山西恒业建筑安装有限公司", "2c4ce981-cda9-41fd-a05c-14b4d02d54e4", "91141100746033626W", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "a767cd26-e8d1-4dd9-abf7-8d27c3c5f27", "50000920181008102", "金地·首创弹子石项目", "79743f95-65da-46c7-83d5-d2be56713b05", "浙江欣捷建设有限公司", "e0b07001-dc6e-450c-9670-778d8b2e98dc", "913302001448768377", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "3cc8b5ce-c93d-4065-a124-877f7f3badf", "50000920181008113", "长嘉汇小区A组团1#楼", "38e67dca-f021-4381-8440-79f78f18c0aa", "正威科技集团有限公司", "3dc6fdc2-e678-45d9-acd3-057f292b3e37", "91321204141366786E", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "3b678650-f0f3-43d2-99bb-fb0c78c22f7", "50000920181008057", "保利二塘项目二期B2组团", "bce5e863-b095-4f2d-98d6-ab3ebadaf915", "中建三局第三建设工程有限公司", "e5b31176-49f0-4c02-afe5-ccce3d5746c1", "914201001776930413", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "3f2714d8-be80-4d6d-9c16-69baa216afd", "50000920181008023", "国家文物保护综合服务设施项目", "e7e8b91c-12a5-4afb-98fa-58ea934d14e0", "中建欣立建设发展集团股份有限公司", "b5905d8f-f0e7-4ca9-8f1a-557bcc697a9e", "915116816841895878", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "e627bb69-d579-43b9-a458-959bc30fa89", "50000920181008008", "中交漫山三期", "304050a5-7108-4526-a346-b3842f8448c0", "中交第二航务工程局有限公司", "33ef5f56-0ad1-4d22-93df-9221f20d4421", "914201001776853910", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "e99c3a51-ed50-47ef-80eb-7c393df0a26", "50000920181008111", "长嘉汇小区（G3-4/02地块F1组团）II标段", "78de228a-d967-4a74-a75c-ae1aff6262ae", "重庆诚业建筑工程有限公司", "273a3970-32ef-476c-8856-5bfbd1f7f9ac", "91500107666403682M", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "a1f4acd9-df08-433e-ac66-d594e336030", "50000920181008086", "天盈首原二期(天盈海峡路项目1.5.6.7#楼)", "74da847b-3479-4814-8d7b-d164f1b39870", "重庆诚业建筑工程有限公司", "e367ce5f-f70e-4eaa-919d-7dbbb0db1e23", "91500107666403682M", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "9898b9f1-a8d7-4cce-bbc3-4111fd9b29d", "50000920181008085", "至元成方弹子石项目二期(A17-1/05地块)", "c5057184-208e-424b-9822-1a4fc028ad5c", "重庆诚业建筑工程有限公司", "5d3b9073-f07b-43d1-b92d-bf5d53e008e2", "91500107666403682M", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "b563b896-5412-4849-be6d-5da8f7dc0b3", "50000920181008021", "南岸区M组团（金域学府）翰林二期C期项目", "d82b4869-324e-4832-86f6-ff50a2edbcd3", "重庆城鹏建筑工程有限公司", "a14d58d9-ebdb-46e9-889b-21c8ac1aa8e2", "9150010820315005XF", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "07884732-a6e7-4eac-9314-4996f73bdd2", "50000920181008050", "保利二塘项目一期C组团", "ef064e8c-7914-4eec-bdd0-e6c93df00262", "重庆第六建设有限责任公司", "2f321682-addd-44fd-9fc5-c9bf25683e9c", "915000002028326067", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "0029d807-faf6-401b-9a2a-4d36ac88b54", "50000920181008088", "鸿笙苑（秀苑华俊）（BCD）", "f97f54b3-4554-49b3-b79e-ca462c04b8b0", "中国电建集团重庆工程有限公司（原名：重庆电力建设总公司）", "18a9914b-d721-468f-ad88-c86919dc5ee8", "91500108202801156A", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "de98890d-5a9b-4284-aec9-fba0e6561af", "50000920181008096", "茶园堃运项目一期（B35-1/03地块）（17#楼-21#楼及附属商业、部分车库）（D-24轴-D-42轴/E-J轴-F-P轴）     ", "75c8bc99-9205-431b-8d09-51a013cded25", "重庆光宇建设开发（集团）有限公司", "1804a5ac-0ac3-4e15-9dc4-58d7976ecb40", "915001152033974580", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "31eee593-f586-4e6f-8dee-2b796d2c4f0", "50000920181008112", "长嘉汇小区（G3-4/02地块F1组团）I标段", "502d2967-ddfd-41c8-b724-a7f1bb664d9e", "重庆华硕建设有限公司", "68f7d990-9ac7-482e-b8de-c457f9b2ed49", "915001152034048303", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "70467660-d3a8-432e-9a8e-fd83c15b4d5", "50000920181008127", "荣盛华府三期(茶园新区A74-1/02地块)", "8f023b37-fe39-49af-a0ec-338f11346598", "重庆嘉逊建筑营造工程有限公司", "7fb6cf72-2ec9-4551-bb9a-137ff65b4e20", "91500000778457538G", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "fa3c8e40-bc2a-4190-bf0b-0ec0c2e0b8c", "50000920181008022", "三江花园", "f2273b12-7f53-4c16-b951-39031c4b08b7", "重庆建工第二建设有限公司", "7db22e23-2f75-4832-b4e5-36b40a55b9a8", "915000002028051812", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "d6f02758-e931-48a3-8842-e64283e7118", "50000920181008051", "茶园新城区K标准分区住宅20组团2期一标段", "edf950b9-f3b1-4bc9-9777-61ed97f13257", "重庆庆华建设工程有限公司", "aa78840b-46e7-4015-a87b-d3c71047978e", "91500109753062492U", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "ffd135d1-4793-43c3-b3cd-ae9e6bceed0", "50000920181008017", "茶园组团B分区项目（B44-1、B48-3、B44-2）一期总包工程（标段二）", "bfb4342d-73f8-42c2-86c1-260958d8ac53", "重庆荣达建设（集团）有限公司", "39f54a8f-bb53-4c5a-af7b-ad3b6c42d862", "91500105203041718K", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "96a25d82-7654-4bb9-afdb-cee8f8e4fff", "50000920181008018", "茶园组团B分区项目（B44-1、B48-3、B44-2）一期总包工程（标段一）", "942fe394-902f-42df-83dc-a846c6403ac0", "重庆市辰河建筑工程有限公司", "d349cca0-fcb7-4909-8596-8a859cf9d7bb", "91500233208153735T", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "e4e63bfb-b0a6-455d-8393-1386a64959a", "50000920181008132", "重庆金隅大成南山郡（南岸区黄桷垭组团B、C分区宗地）8号地块 ", "25a321f4-b4a3-463c-a9c9-02a51fd4676e", "重庆市阜泰建设（集团）有限公司", "ee2498f4-540c-4f3f-97a6-727472ee5811", "915001162035865740", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "44ecce8a-5621-4faa-8abe-801a0d7052a", "50000920181008048", "美心长江一期A7-1地块（一标段）", "580ad2ed-036a-4c05-b806-6e7e512ba4b5", "重庆市南城建筑工程有限公司", "47082ebc-8545-4a46-b618-e9543545db91", "91500113203447988E", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "3460d5b0-1e62-4d0b-bfc0-d9af8755f22", "50000920181008073", "奥园越时代一期三标段", "80db0a0f-f305-46bc-86d1-a9ae8a7b5541", "重庆拓达建设（集团）有限公司", "2b644516-d63b-484a-a404-b25903108bce", "91500108768876288X", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "ef332274-4193-4401-aaab-5c894aa9dc5", "50000920181008099", "重庆金隅大成南山郡(9号地块)", "6b3556ac-3642-4d00-9a7d-69068eaea77c", "重庆渝发建设有限公司", "6f8f35a5-a6cc-4422-93ce-a841e79f2dab", "91500115203399701L", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "3b60e45f-47ac-41ec-a765-be28aa20b62", "50000920181008098", "渝信阳光居住小区二期（局部设计方案修改）工程", "be640605-5694-4b03-852f-47086ba93ea0", "重庆渝圣建筑工程有限公司", "28239723-e2b7-486f-afca-dfb96dfd4cb8", "915001132034143856", "南岸区", false);
            AddAccountsAndProjects(dbContext, accountList, "6e81985b-152d-4904-a9d6-1ca6e05beaf", "50003920181008621", "北京城建·龙樾生态城（C30-2/06地块）一标段", "f443e5ec-1182-4958-a812-cc9033de9095", "北京城建北方集团有限公司", "857d5ae8-4b83-46de-bd7c-7902353b1bb7", "9111000073555651X9", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "704781d6-245c-42b9-a1d0-9ce1ecd6667", "50003920181008535", "北京城建·龙樾生态城（C30-2/06地块）二标段", "ac1bc6a2-63b9-4dd8-96d1-e122c8e08190", "北京城建九建设工程有限公司", "4d291590-1e80-4a51-874b-f0ce7ffa0876", "91110108722601000X", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "d5f8d8f6-bd4d-47fd-871b-d61d9d51d70", "50003920181008730", "中央铭著（C77-1/03）", "a984e497-1c97-430e-b71f-7c2f0839ee84", "福建宏盛建设集团有限公司", "dd7fd5ad-f985-4daa-827e-ed4ac07a525b", "91350100611325517C", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "57ab293d-5982-47f2-95c6-eba36042558", "50003920181008496", "中央铭著（C80-1/02）", "60d79d01-f625-4129-90c7-3832d3ae30f7", "福建宏盛建设集团有限公司", "67d021ba-a403-4ab3-98d2-c474cea8b2c4", "91350100611325517C", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "d95ea188-69b9-4697-86f4-8a873828845", "50003920181008749", "礼悦江山（1-5、8-10、12、13、24#楼及地下车库）", "8ca46cba-3aab-4385-b240-35a2fd1a661e", "福建省晓沃建设工程有限公司", "07c5f90d-0227-4727-97ed-501cd5560007", "91350122154700712W", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "fd7754f3-d07b-4947-9d65-a472976d920", "50003920181008766", "礼悦江山（11-1,11-2,14~16,17-1,17-2,18,19,20-1,20-2,21-1,21-2,22,25,26#楼及地下车库）", "bc8a6375-c4a4-452b-9179-9a48127061fa", "福建省晓沃建设工程有限公司", "273fb6ec-3408-4384-9dc1-e70c7eaf1c51", "91350122154700712W", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "5a2415a5-b635-414e-829c-972cb63c50f", "50003920181008713", "鸳鸯派出所业务用房新建项目", "aaf229b0-eaba-4574-9372-3ad0d148eda5", "福建省中兴建设发展有限公司", "f711ac8d-cbe2-47e0-94d3-f8c909b9a664", "91350521768557105C", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "4fd4937b-1a74-483f-a026-ba5ce171f96", "50003920181008636", "保利溉澜溪项目G08-4/02、G11/02号宗地项目（G08-4号地块）一标段 C1号楼、C-1号楼、C2号楼及对应车库工程", "21bc509e-2abe-48a9-a9d2-b00456be9370", "海天建设集团有限公司", "c21d82ff-c78f-4c56-916d-50d5140efd0d", "91330783785663700R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "04d9618d-61d0-4004-b682-15520e7f99e", "50003920181008624", "保利溉澜溪项目G08-4地块、G11/02号宗地项目（G08-4号地块）（二标段）C3号楼、C4号楼、C5号楼、C-2号楼对应车库工程", "47b3f74d-9953-4052-92ce-9f4896649cda", "海天建设集团有限公司", "2fe87d6f-4164-4a48-81d6-ac4320624d1e", "91330783785663700R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b0833be0-54c8-44e0-8689-4ca5720abc4", "50003920181009165", "L9路西段道路及配套工程", "3c9657c0-5774-4b50-a6b6-dcad8c1e0cd6", "杭州萧宏建设环境集团有限公司", "2117a66e-53ac-431c-8235-609698820e85", "91330108253934178H", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b186fd39-1599-4e93-865b-c70e85e16ad", "50003920181008533", "华润·公园九里（C65-3/04）二标段", "7c7942a7-7fdb-49ad-b756-f510078afeed", "华润建筑有限公司", "96a70121-e54a-4097-b705-125ec2d5264e", "911101017109278399", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "5b64f5ed-fdd3-42f2-ae7a-ba8f58f86cc", "50003920181008720", "华润·公园九里（C69-1/02）", "775e3667-22e2-4807-ba9e-63ad01315ee8", "华润建筑有限公司", "292065fa-fc86-45ef-b565-d1fa737b9a72", "911101017109278399", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "07a8128b-ded5-4a0e-80eb-eb9ae687efc", "50003920181008711", "中国摩（重庆）项目I49-5/03、I50-1/03、I51/03号宗地（I51/03地块）（一标段B2-B6、B8-B14、S3、S4、B2#地下车库） ", "759d314a-541e-4ceb-a463-498ad5b0fd3f", "嘉兴大源建筑工程有限公司", "76fcaa20-bd2f-401c-af8f-c35fb6e01fb6", "91330402683141389T", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "3f08bdfa-313c-4b32-8900-4d8f57a7745", "50003920181022003", "中国摩（重庆）项目I49-5/03、I50-1/03、I51/03号宗地（I51/03地块）（三标段A1~A7、A9、S6、S7及相应A1#地下车库）", "0a70dc8a-6ef9-464c-b9f0-0c64b8034907", "嘉兴大源建筑工程有限公司", "6aa8b0c0-57f0-4ae1-89fc-ee5696d25ac4", "91330402683141389T", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "c221d0d4-41f6-4a79-87c1-72ca57d03b9", "50003920181008712", "中国摩（重庆）项目I49-5/03、I50-1/03、I51/03号宗地（I51/03地块）（二标段B1、B7、B15、A8、A10~A12、S1、S2、S5及相应A1#地下车库、南、北门厅）", "9d495b93-b2d7-4dd9-916d-3a4d7949313e", "嘉兴大源建筑工程有限公司", "a47b96a5-58f4-4365-a409-e80da894dc38", "91330402683141389T", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "94923b7e-974a-4b0e-bc39-9a4d217ee56", "50003920181008534", "渝高·观澜A组团（一期）2号楼、3号楼、7-8号楼、16-22号楼、24-25号楼、27号楼及相应地下车库及二期", "a96504f3-76d2-49cb-b578-df558681f934", "江苏省建工集团有限公司", "30ab3284-a8b9-4c70-a1a4-36c62e631380", "913200001347521875", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "4ad98f7b-5109-4a30-9e5d-cdb837e78bd", "50003920181022006", "北部新区人和组团O标准分区O13（部分）号地块（渝兴·星座天地）", "42da3a05-753d-4d97-8381-3e4fb27706d9", "江西润财建设有限公司", "18ea59f9-533a-460a-b115-273944c86c1a", "91361100561082712Q", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "3aa6e384-b4d1-437f-8a0b-bf03e96108d", "50003920181022007", "华商悦江府（D04-4/5地块）1#、4#、5#、6#楼、6#楼商业及对应车库", "2eb27804-a02b-410f-8bcc-06fb96416e11", "龙盘建设工程集团有限公司", "a8622f9a-0032-4b04-96bd-3303d0a96af5", "91510300204053518T", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "80643305-6941-4c19-bb5a-1a57a9b2917", "50003920181008727", "重庆龙湖怡置新大竹林项目（二期二组团A区）8~18号楼、22~25号楼、28~31号楼、41~42号楼及部分1号地下车库（A区）", "8feb8ca1-0261-41f7-909b-597333d533a1", "南通德胜建筑工程有限公司", "bbd64191-758f-4956-ad11-01dd61401fc1", "91320684138791968Q", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "681ec243-c51d-4512-ae19-b4aacb8e9c6", "50003920181008926", "高新园大竹林组团A区市政道路工程（横一路K0+200-K0+620段）、北部新区大竹林组团A区纵九路道路及配套（纵九路K-1+860-K0+000段）", "8835d84b-4c6a-4a3b-8cb6-46fdb4153429", "衢州市政园林股份有限公司", "00898714-81ca-43a7-9ed0-d627935a08d1", "91330800609808544L", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "1ce1fd4d-a2c2-4df9-810f-78e875050b5", "50003920181008363", "旭原创展大竹林项目08-07/02地块（1-3号、9-14号楼住宅、S1号楼商业及部分地下车库）", "74650c37-7cca-412c-83b1-7c0a2b181225", "山河建设集团有限公司", "6483764f-7b58-4d79-b4e4-71bad545a101", "91421121272000266B", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "df3e5d4f-4d6d-4719-a21d-942512cbade", "50003920181008368", "旭原创展大竹林项目08-07/02地块（4-8号、15-16号楼住宅、S2、S3号楼商业及部分地下车库）", "3c331cc3-da66-4627-a41e-353d1425ea18", "山河建设集团有限公司", "7985c874-1a6f-4896-bc1a-43ae9879a06f", "91421121272000266B", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "d71036d8-2b54-466d-9806-24804d7175f", "50003920181008597", "旭原创展大竹林项目（08-02/02号地块5-8、18-48、52号楼及对应车库）", "eb72a0c5-8d92-4606-b2b1-0de0392bee78", "山河建设集团有限公司", "b30696e5-1084-4438-a3ce-deed2f30252c", "91421121272000266B", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "509d3951-c301-45df-8776-85d7bc0299e", "50003920181008596", "旭原创展大竹林项目（08-02/02号地块）1-4、9-17、49-51号楼及对应车库", "4f214f33-236c-4038-8032-90ceb900e456", "山河建设集团有限公司", "a5fe87bc-8988-4c30-b819-6ac4fd0c29fc", "91421121272000266B", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "92daa85a-b2b3-4706-89b9-6b590cf53c7", "50003920181008579", "盛泰礼嘉项目（A29-3号地块）1-6号楼及部分车库", "baa7b05e-de45-401a-b0ac-3fd34d3deccf", "山河建设集团有限公司", "176499c9-54ba-4282-b621-c35f169b6925", "91421121272000266B", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b948aa93-7c75-4aa2-b56b-969ed8e6bac", "50003920181008517", "盛泰礼嘉项目（A37-5-1号地块1-12、物管用房及地下车库）", "e132a52d-fea1-44ab-8a1d-6044c6dc7188", "山河建设集团有限公司", "b69d71d4-52b7-48b6-84e8-2f841a8d1c40", "91421121272000266B", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "d1b49952-ffb8-4e97-8b9f-6c35e411626", "50003920181008708", "雅居乐富春山居一期2号地块（B1#-B5#、B9#部分车库） ", "2f2188d0-e02e-40c1-ad52-04eafab0aa45", "山河建设集团有限公司", "6ebf22fe-96a3-4949-9299-c82c5a4cb011", "91421121272000266B", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "7e18e85b-89c3-418b-b1d3-11be5e36e3f", "50003920181008619", "重庆华侨城置地项目B20-4/05地块三标段", "99de9998-7f5c-4d72-8c09-ef31b2847366", "陕西建工集团有限公司", "daf00e17-265e-4966-aa04-87e4aa202f0f", "91610000220521879Y", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "3da5c6f8-3707-43a9-896a-2836f927217", "50003920181008769", "格力两江商住（P25-1/01地块）", "d08cf51e-dec8-4b62-bf72-476ca92fc50d", "上海建工一建集团有限公司", "884025be-fee8-4727-90e5-cab9ff03946c", "913101151324008074", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "e2b4e82e-cf9f-4f27-b1b7-440a2b03ba7", "50003920181008770", "格力两江商住（P26-1/01号地块）", "bd95b172-6750-486c-9b57-433a45e6a019", "上海建工一建集团有限公司", "fa22cb08-98cd-43d2-a302-75297befcaa9", "913101151324008074", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "bf8b78bd-a2f2-4f50-a762-a8633d173a2", "50003920181008602", "格力龙盛总部经济区商业一期（P22-2/02地块）", "27427641-7680-4498-a9a6-db4622de502f", "上海建工一建集团有限公司", "1a7edd18-ca6a-441e-9900-6245720a8252", "913101151324008074", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "5c170574-cb91-4a77-9fad-b18ce652b2c", "50003920181008722", "万科园博园项目（一期）（C28-2-3/03地块）", "74603254-6d89-43ee-b0a3-012a4d1744f4", "四川大诚建筑工程有限公司", "681e6489-ca6f-4cf8-8ccf-82c3bb5de5dd", "91510000560738089M", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "ad3b265d-7720-4248-899c-f8a3c52172d", "50003920181008765", "万科园博园项目（一期）（C30-1-1/03地块）", "b394f595-cf46-4e05-b2cc-180c2ebaaacc", "四川大诚建筑工程有限公司", "0205eb35-52eb-40b1-928b-4d098ddb3983", "91510000560738089M", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a52f8623-6b85-4ce6-a4f2-9195359430f", "50003920181009172", "曾家岩北延伸穿越内环新增通道工程（二标段）", "4139d7ac-83bb-4008-b29a-e180c56b9c8f", "四川公路桥梁建设集团有限公司", "a6b556d9-667b-49a9-bdf7-6cbdfaa4fe16", "9151000020181190XN", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b23fa0bf-2754-40b5-bc2e-399f1bcb8dd", "50003920181008401", "远洋九曲河项目（1#-5#、7#、8#、49#-64#、地下车库、设备用房）", "78130cca-d74c-4610-a4cc-50a3df862254", "四川永存建筑工程有限公司", "69444052-150b-4156-8df4-ebf51abedf18", "91510000633164363L", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "c6695d6d-6f47-4381-a831-7ed26444e81", "50003920181008466", "远洋九曲河项目（18-43#、2#门岗及相应地下车库）", "badd4bc9-15d6-4744-8004-00c9d2b87fc9", "四川永存建筑工程有限公司", "0d1e30ec-89de-4026-b0c2-7e58d580ab59", "91510000633164363L", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "3e1fdef8-df39-4034-8116-36f9819a7f3", "50003920181008473", "远洋九曲河项目（6#楼、9-17#楼、44-48#楼、65#及相应地下车库）", "8ecde306-079a-403f-8d94-8188582492df", "四川永存建筑工程有限公司", "0a9949d6-e1d4-4012-8476-fb4efa7565e4", "91510000633164363L", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a3fe7171-bc20-44a3-9eb7-9bd61951dcb", "50003920181008345", "重庆怡置北郡·黄桷水库B14-2/02、B14-4/02及B14-8/02号地块项目（B区75~87#楼、4A高区车库）", "d9684b3e-76af-479a-8d57-03404262d7e6", "四川域高建筑工程有限公司", "a9ecfc68-3c14-4f17-9e26-f11450a004e8", "9151000068417596XN", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "1f660ee5-7726-49f0-ab0e-eb47819384a", "50003920181008627", "重庆怡置北郡·黄桷水库B14-2/02、B14-4/02及B14-8/02号地块项目（C区96~111号楼、6号车库）", "38cbdd7b-6f71-4c5d-8a01-0d07daabfa8c", "四川域高建筑工程有限公司", "c9d2deb3-8a3c-42ff-a776-c927ca3e8828", "9151000068417596XN", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "55e6a9d1-607f-49a7-9bb4-41d1497d85f", "50003920181008438", "重庆龙湖怡置新大竹林项目（二期一组团A区、B区）1-95号楼、物管用房、地下车库剩余部分", "f1964399-7eb5-4bfc-8848-951ef47dac3b", "四川域高建筑工程有限公司", "c1a8d0ba-4640-43c6-a5fe-4d5c2e9ad72e", "9151000068417596XN", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "d79382a6-284e-417e-99a8-1c671480d04", "50003920181008453", "金科亿达·重庆两江健康科技新城二期二部分（E8-1/01地块北区）", "d772798a-699f-4096-8c1e-b51566b40c78", "四川住总建设有限公司", "f4d03b51-7d39-45ea-b9c6-808d74350ed9", "91510521204864994Q", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "aaf75c8f-45ee-4c58-b13e-bc49a8df2c2", "50003920181009152", "H5-1路道路及配套工程（二标）", "7fb329fa-9a88-4023-9e3b-c0b65a906409", "太原市政建设集团有限公司", "6ca5477e-0409-4169-a3e8-52ed43a09b2a", "911401001100834259", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "8f31d550-5d02-4c34-88e5-8ebc01fd329", "50003920181022008", "华润.公园九里（C81-1/03）", "4801f868-a01f-40f5-b833-f0d3bfbba6cc", "西南建工集团有限公司", "acbca156-f911-4ad5-a0e4-081f541947df", "915116007118166988", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "927a2fe0-187d-4d93-9797-edb5009a4a4", "50003920181009227", "远海建工集团技术研发中心", "5c32de42-a7db-447a-bc4a-e11a3aa53949", "远海建工（集团）有限公司", "e2c5de35-7625-466f-ac5b-72e141faf399", "9150000071160023XL", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "5c8268f8-38bb-4cc2-8e32-c45be548052", "50003920181010001", "碧桂园龙兴国际生态城（H34-1/02地块一期别墅和二期)", "17069b7c-07b8-4dd5-90fb-26be8dd84a60", "浙江新东阳建设集团有限公司", "12ec4b12-9ba1-449e-8643-4ab37cba64db", "91330783147581401A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b09e9789-8479-4525-a7ba-b0f9865064c", "50003920181008694", "碧桂园龙兴国际生态城（H34-1/02地块一期洋房)", "7fe89ac9-860b-4a4e-baf4-f57645b49227", "浙江新东阳建设集团有限公司", "68c9f219-fd71-48ea-b8dd-b0fdcada141a", "91330783147581401A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "010ca346-16da-4e0e-96c1-f79d752e7d9", "50003920181008600", "重庆华侨城置地项目B20-4/05地块二标段", "dcf9f5a0-3f98-4b7b-bfc7-67c912f1c670", "正威科技集团有限公司", "b8c8e664-d45e-463e-9082-88b40b7a2a4c", "91321204141366786E", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b99868c4-8dda-4848-98e0-d2174ab8bc0", "50003920181008629", "重庆华侨城项目（B21-2/05号地块）", "87aed5c4-aa94-4161-af59-1983a91c9844", "正威科技集团有限公司", "08b96812-c18b-4b55-8d2e-fe63f9078fd1", "91321204141366786E", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "003e940a-eb45-4951-a695-e407d630776", "50003920181022002", "两江龙兴体育城", "5fc5e2aa-4c8a-4069-b594-adaa322a027d", "中东建设集团有限公司", "c114208a-5813-4f7d-9956-949db8f63334", "91350521768564006U", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "8fdb436d-c11e-444f-973e-72a65cf70eb", "50003920181008802", "中航两江体育公园（E07-01/01）", "d72e0703-d5f9-46b4-af42-05dfe25efe62", "中国航空技术国际工程有限公司", "a00782ce-526b-4a5b-8cdc-9ba7e05acbd5", "911100001000010001", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "ce3f31a4-73e8-460b-ad44-86743a2576e", "50003920181022005", "中航两江体育公园（E16-01/01)", "756bfa6b-033f-4fbb-8040-25fc7639a428", "中国航空技术国际工程有限公司", "c503c61b-a96d-42a9-963e-f5e2d6a76ecb", "911100001000010001", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "4c4cb5e0-5c1b-42a1-8aff-0b63a1a97ac", "50003920181008547", "两江新区悦来组团C分区望江府一期（C48/05地块）A区10#-11#、17#-26#及27-1#、27-2#部分地下车库", "2de5247d-e2b2-4958-a9ac-6cf9bef627b7", "中国核工业中原建设有限公司", "84894b9d-ac6a-46b7-a582-b4a13e59f216", "911100001000124711", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "27262b10-9edf-432b-b94d-5e9afab41bf", "50003920181008759", "两江新区悦来组团C分区望江府项目（C41/05地块）", "024f3505-774c-4593-a382-fad65b1a65aa", "中国核工业中原建设有限公司", "26a57d0c-8496-4ec0-b87b-6e28e1be87ef", "911100001000124711", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "3181ff9f-5877-4c7a-b380-9d5877c58b5", "50003920181008748", "重庆两江新区礼嘉组团A标准分区A09-4-1/07号宗地项目（12#~27#楼及对应地下车库）", "88086b2c-96ea-4cf4-b715-b443c2894dbf", "中国建筑第八工程局有限公司", "cece2f01-f699-43ee-a529-60b8ed55b975", "9131000063126503X1", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "952583bc-01d9-4e51-b3b8-4010466705f", "50003920181008747", "重庆两江新区礼嘉组团A标准分区A09-4-1/07号宗地项目（1~11#楼、S1~S5#楼及地下车库D2-U轴至D3-AP轴交D3-1轴至D3-31轴）", "35e3edba-18e4-41d2-96b2-04ef81059b24", "中国建筑第八工程局有限公司", "31789f60-e266-4ec8-923d-c7131cc2b849", "9131000063126503X1", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "338fc990-15bc-4766-b73d-563e7bdf364", "50003920181008544", "鲁能·星城外滩5#地块1#-10#楼、S1#、S2#楼及附属车库建设工程", "7d8c2515-a6c0-4a18-af91-f48dcf8672f8", "中国建筑第八工程局有限公司", "636b4a45-6a0d-4983-bc86-a59f04b9b43b", "9131000063126503X1", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "6d807f04-0f0d-47b4-ae57-8e897bb1e6b", "50003920181008637", "中交·中央公园项目C108地块总承包一期工程（1、3、4、7、8、9、11、12、13号楼及地下室）", "51421926-ed68-46d6-b5cb-3a866daedd31", "中国建筑第二工程局有限公司", "3f7ae9ef-1c87-4a53-a7fa-3482835afd2f", "91110000100024296D", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "6c1b55d2-009b-44f4-a386-bb84669572a", "50003920181010002", "中交·中央公园项目（C109地块）", "bacf984a-a82c-4523-b6d1-60c99a9c62cf", "中国建筑第二工程局有限公司", "311273ff-f20b-4ae5-b375-f91f5b3ee84c", "91110000100024296D", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "be131a72-0f7d-4897-86d3-2cbcf904742", "50003920181008599", "重庆华侨城置地项目B20-4/05地块一标段", "13c7804f-0fa0-42d7-aad8-0206702ecbd5", "中国建筑第二工程局有限公司", "262d7720-4797-4a89-97f7-b9d15c48c0f7", "91110000100024296D", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "82628bca-3642-4bda-9c2c-313a956f2b3", "50003920181008549", "重庆华侨城项目地产二期高层A69-11/06号地块", "64bf65e8-2071-42b2-98e3-f84be55c6071", "中国建筑第二工程局有限公司", "b44b48e2-f93c-4447-bc3a-226d1ca30674", "91110000100024296D", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "90652650-efbf-4800-b9aa-60352e75912", "50003920181022012", "金科天元道（一期）商业1、2号楼及车库", "d442aa41-e7cc-4c43-9fce-ebacf153e8e5", "中国建筑第二工程局有限公司", "2362b259-4718-4d61-9281-ad26f77afe9d", "91110000100024296D", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "be90623f-3170-4545-b4d2-f35ae4b4fd8", "50003920181008415", "重庆华侨城项目（地产一期多层B21-1/05地块11#—24#楼、B22-1/05地块64#—132#楼）二标段施工总承包招标", "84784065-c542-4881-b084-7ec1af77619f", "中国建筑第六工程局有限公司", "277b1adf-e9d9-4fb1-ac28-181adb78fb3c", "911201161030636028", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a034fe66-c6bd-46c9-a582-297b2daa709", "50003920181009163", "两江中心（环球欢乐世界）", "bdc4c55c-cfad-469f-9223-bee4cd5bc7fb", "中国建筑第七工程局有限公司", "3f09ac1d-8a13-4452-b527-3a4b989fb4e5", "91410000169954619U", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "d7da3c36-b171-49fa-b1eb-c0df64737aa", "50003920181008656", "力帆红星广场工程B2组团（B3-2、B3-3、B3-6、B3-8、B3-11、B3-14、B4-1~B4-12、B5-9~B5-15、B6-1~B6-10、部分车库）和力帆红星广场二期工程C13组团（C13、C14、C15、地下部分）", "c1d26639-5fbc-4393-94c7-035eb4424b51", "中国建筑第七工程局有限公司", "1ee9574f-8cc2-4a1b-8b59-50a539cf4b6b", "91410000169954619U", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "46bce0ad-4e93-4839-a42f-362014d8213", "50003920181009187", "两江·中迪广场（原重庆两江创新科技园项目部分）（C35-1-1/05地块）", "e796e461-1269-48b1-ac20-3890690ac00f", "中国建筑一局(集团)有限公司", "d896fd07-af8c-40a7-a8d2-052aeb436ef3", "91110000101107173B", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "301826b2-7dbc-4caa-b14c-cae447b2f34", "50003920181008261", "中国摩（重庆）项目一期I54/02号地块工程二标段（5#、8-10#、14#楼及相应车库）", "c4568e93-13be-414f-bf2b-58fa51549ab8", "中建八局第三建设有限公司", "567102a8-9d98-4f0a-88e7-cca8ccfd4659", "91320100134891128H", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "fd7babed-5c0e-45d9-9436-9c0fc6e5e65", "50003920181008479", "中国摩（重庆）项目一期、二期（二期I53/02号地块）一标段", "eb7ca554-ab56-486e-b1c8-98f19e98dbe5", "中建八局第三建设有限公司", "26a34391-7cae-4c8b-824d-f998cfd4ccbd", "91320100134891128H", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "681c37e4-b38e-47b0-a3bf-38cfd289d8c", "50003920181008538", "中国摩（重庆）项目二期I53/02号地块三标段（18、19、26-37号楼及相应地下车库）", "4203773a-4ddc-4404-b773-f071db641275", "中建八局第三建设有限公司", "a6b2f0dc-6a7e-4026-80d5-c57e1750cf43", "91320100134891128H", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "308caabf-8e9c-46f0-b541-6f3f3299702", "50003920181008507", "金科御临河（一期）H32-3/01地块（3-1#至3-14#楼、D3地下车库）、H39-2/01地块（5-1#、5-2#楼）", "a2a4a08d-4fa7-444d-9a4b-fae7945bddef", "中建八局第三建设有限公司", "6ea277bf-0740-496d-a4ae-809f543b8d61", "91320100134891128H", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "74b934ef-87f2-40de-bc64-2733f22bdc9", "50003920181008612", "金科御临河（一期）H40-1/01地块（6-1#至6-16#楼、D6地下车库）", "fec881d6-1096-4a49-884f-e4517b72332f", "中建八局第三建设有限公司", "958675c0-bc5e-4beb-a867-f6d53a2cd9bd", "91320100134891128H", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "6f28aec9-6bd1-4817-ae2f-4dec23a68ec", "50003920181008673", "中交·中央公园项目（C106地块）1-1、1-2、1-3、2、3、4、5、6、7、8、9号楼、地下车库及设备用房 ", "f3765a9e-9af7-413c-86e9-6025eeae610b", "中建八局第四建设有限公司", "2bcd6478-c8a6-424f-b6fe-ae5c249d3a54", "9137021271370434XD", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "377ce226-0bf5-49f8-a1a5-6591cdf6362", "50003920181008754", "中交•中央公园项目（C110、C111地块）3-3~3-4号楼及对应地下车库（3-5号楼）", "134a4deb-e4c9-4597-8b90-eca5afdf1b84", "中建八局第四建设有限公司", "e9805f81-2181-4193-89cf-e6e19c796a0f", "9137021271370434XD", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "23da613a-96c1-4892-8467-c4c60ca5f1d", "50003920181008385", "力帆红星广场二期工程C10组团（C10、C11号楼）", "b0d2aebd-48c1-433f-a823-999b68442865", "中建三局第一建设工程有限责任公司", "a6f53e2d-a599-4678-8fd3-f752be00008a", "914201007483157744", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "1aa6d406-27aa-45a5-81d0-ec66797db44", "50003920181022013", "龙兴组团H分区H48-2/01号地块项目", "cc4e6a02-5a09-4314-b466-523b0a1e8338", "中建三局集团有限公司", "5f681c03-a915-4828-aa6a-6498feb1586b", "91420000757013137P", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a28d7c1e-8ae8-4088-9575-38bede25618", "50003920181008306", "保利溉澜溪项目G11/02号宗地项目三期（二标段）", "e9010b63-6d2f-4e80-ba6a-4e677231b968", "中建四局第三建筑工程有限公司", "f706ae81-6004-4853-b566-9bc77a56476a", "91520302214780129Y", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "3f1e31f1-8be8-47d2-a909-89c1f694e14", "50003920181008684", "龙兴组团H分区H46-1/01、H47-2/01、H48-2/01、H50-1/01、H51-2/01、H52-2/01号地块项目（H46-1/01地块）", "416a1128-01ee-46d7-9548-cf8f7fceeb4a", "中建四局第三建筑工程有限公司", "fbf96e95-7853-41a2-9c08-3674a664eb29", "91520302214780129Y", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "4737c3bb-25c4-4a01-8605-85f5d4291e7", "50003920181022010", "恒大江北嘴项目（G26-1/05地块项目一期）", "8a3b35e4-37c2-464f-89e3-b27605fe187e", "中建五局第三建设有限公司", "73b32545-43f9-4f2e-b888-b804f8d7daba", "91430100183853582A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "c2b8ee57-6972-41e9-8397-3ea1d81658e", "50003920181008396", "悦来会展中心北辰项目一期（C05-1/07号地块（1号楼）、C09-2/06号地块、C18-6/06号地块）施工总承包", "6ea007de-35e5-479e-98f3-2b3bb5cc5bf5", "中建五局第三建设有限公司", "10e1cb0f-4dd7-4dbb-b4dc-7c5a9cab0351", "91430100183853582A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "c32b0552-b15d-412c-99dd-9677ad49a47", "50003920181008613", "悦来会展中心北辰项目二期（C05-3/07地块、C18-1/07号地块）施工总承包", "c3de225e-a1e6-4421-a225-77407925aa42", "中建五局第三建设有限公司", "a47a9fbc-ec41-4a5f-83d3-ec1bc8f83c2c", "91430100183853582A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "fe1cb3c3-113e-496a-ac63-eb3bd2c7512", "50003920181008419", "金科天元道（二期）020-7、020-9号地块二标段", "1fd30fa3-1ffb-4ca0-8e7f-832f00120f46", "中建五局第三建设有限公司", "1c39229d-77c1-450c-b065-e90393349569", "91430100183853582A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "8a0b6a4d-9600-4100-b139-3fec79c597c", "50003920181008472", "金科天元道（二期）O20-7、O20-9号地块一标段", "c4ac03d0-ff7d-46fb-a73e-02639ea19b71", "中建五局第三建设有限公司", "f0599aa5-1990-45e7-a843-1e6d302f035b", "91430100183853582A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b3a74d91-f3e2-4e33-a02e-def21af4e0a", "50003920181008490", "首地悦来项目（重庆首地人和街小学校）", "d0d3c73b-02a5-4a72-810d-2508d4f88e06", "中建五局第三建设有限公司", "9cac4c16-5f14-4349-af4b-e85d4bf2a4e5", "91430100183853582A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "83b4dfe3-4694-4452-b22e-a3996b5f8d6", "50003920181008601", "北大资源·博雅东二期二批次", "e9ba5d0f-80c2-410b-9d91-4aeaf057aa4b", "中建一局集团第五建筑有限公司", "b3a988da-166e-4a97-b5f7-3134e52424af", "91110000101638302P", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "004902d5-c12d-419d-a609-cccd5a89aee", "50003920181016005", "重庆雅居乐富春山居（一期8号地块工程C85-1/05地块）G3栋、G4栋、G9-G18栋、G19部分车库）", "049e0b11-aef9-4f5e-b0c1-92a8d868c08c", "中欧国际建工集团有限公司", "e77b3e07-6526-4c42-a9ac-06522352e82f", "91511402621133375W", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "57ea0592-07a6-486c-90af-ce894194608", "50003920181008740", "雅居乐富春山居一期2号地块（B6-B8#，部分B9#车库）", "674ed0b5-e72d-4d7b-90f7-ed78d24db3cc", "中欧国际建工集团有限公司", "6c91e6a4-a62e-49d1-8a56-20c25702f26e", "91511402621133375W", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b1c8bdde-e304-432a-b3f7-f6176177902", "50003920181008477", "两江新区悦来组团C分区望江府一期（C50/05地块）", "183587ff-07f3-476d-8e5d-f77d84674d14", "中天建设集团有限公司", "640b254a-7dc6-49fb-90d0-4a7f1cbab3b9", "91330783147520019P", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "c7fc5ab7-d65e-40fe-b993-4e6033d13a3", "50003920181008580", "北部新区B标准分区B14-7-1/04号地块一期(碧桂园中俊·天玺）33-42、45-50、52、53号楼及对应车库", "ed7d3776-f03b-4192-81f5-30392e6f6322", "中天建设集团有限公司", "2075ea46-2555-4f33-9c4f-5db7521a25e3", "91330783147520019P", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a40e3afa-acb2-4a5a-8f5c-b5ad42e0b99", "50003920181008563", "北部新区B标准分区B14-7-1/04号地块（一期）44、51、57、58号楼及对应地下车库", "14e2199c-874e-49f7-a654-20ca68c6ac49", "中天建设集团有限公司", "ed747d5b-34cf-4530-ac05-63b5422da35a", "91330783147520019P", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "1f5a7f89-622f-4266-86d8-6c83765eb52", "50003920181008497", "寸滩项目A-34-1-16号地块一期（标段一）（1#、2#及其地下室）", "e318731b-2029-4551-b318-afa3d288b10a", "中铁城建集团第一工程有限公司", "aa32587b-38fd-4538-aafe-a199268e37b9", "911400000870963902", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a181f5bd-2330-4d1e-b796-0eeb339aa52", "50003920181022004", "中航两江体育公园（E04-02/01）", "35600b14-fc7b-4346-ba3c-06197e11769c", "中铁二局集团有限公司（原名：中铁二局工程有限公司）", "9262adc7-7b71-429d-94b1-88ba233faff2", "91510100MA61RKR7X3", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "17d99db2-687a-4bf0-8da8-56ca9cb9740", "50003920181008498", "寸滩项目A-34-1-16号地块一期（标段二）（3#及其地下室）", "16ec5fc5-114f-4759-9f6b-8b4554e88dc4", "中铁二十三局集团有限公司", "17e088f2-f5b8-4b7a-9df6-d6200102a6e2", "91510100740338242L", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "9b3bcbc2-3ffe-453f-8650-f11f00b65e9", "50003920181008707", "寸滩项目A-34-1-16号地块二期（15#楼）", "f3ba98c1-6117-480e-a518-fb4989bd2e0f", "中铁二十三局集团有限公司", "6fe9d0b5-0a60-4661-a1bf-c4c8e9f7e878", "91510100740338242L", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a3d6949b-9d03-4fe5-99b3-96ea536d60e", "50003920181008355", "首地悦来项目（一期四组团）", "0d3c3f4f-4732-4a87-85b1-059cd0c2b92a", "中铁建设集团有限公司", "0d521dc2-85ab-48a4-addb-b108e9b4854d", "9111000010228709XY", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "615e3242-784d-4fc4-b19a-f758754da0b", "50003920181009190", "龙兴隧道项目（一标段）", "4069268b-1221-492b-b7bf-48ea6a64d378", "中铁十一局集团第五工程有限公司", "5b7941ac-cfbe-414a-8d2e-e40b7cdb8637", "91500000202805587B", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "736d3abb-053d-4bf6-917e-ec58328d123", "50003920181008875", "北部新区古木峰立交工程F匝道", "630f9496-6fb3-4626-b0df-34ce4d59f387", "中铁隧道股份有限公司", "d737c907-2534-4e23-9bbe-c2cfa0b64bb5", "9141000017292850XF", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "89652c2f-7431-448a-af44-cc6dfe3e5d2", "50003920181008371", "恒大世纪城38#、75-83#、87#、90-91#、93-96#", "8db4d5f4-5abb-41ce-b929-621e4dc02b49", "中兴建设有限公司", "5d60679e-4966-4f9c-87d8-f889e13f6fe8", "91321283737840047C", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "afccd1d8-1fe0-4426-86e7-3e098d86a77", "50003920181008508", "恒大世纪城B1-1/03地块100#~105#、III号车库", "20ab2d72-4ee2-4716-a519-a4ee60c1f56f", "中兴建设有限公司", "302702d9-0bdb-4788-9634-76aa4533a0a2", "91321283737840047C", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "85fc9f16-0e70-4048-9c20-b2e7f7e7295", "50003920181008456", "恒大照母山项目六期（11-14号楼）", "f399a0f9-2405-4045-858f-33d712b6bc2f", "中兴建设有限公司", "4f6e3691-9e14-4856-85f7-17d10c52313c", "91321283737840047C", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "0f4f85fd-53b9-4f80-a882-8e699afad82", "50003920181008332", "恒大照母山项目六期（山水城38-42号楼、IV、V、VI号车库）（含边坡支护以及室内精装修）", "e41753c6-c7b3-4774-a4d1-11c539f29d42", "中兴建设有限公司", "d7938992-bee3-4d03-8134-fe947e44158b", "91321283737840047C", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b3298816-cac4-43ec-8276-291e1427014", "50003920181010005", "两江曲院风荷项目（C03-1/01地块）、（C07-1/01地块）", "6f2fcdde-7163-4682-a80a-f441505c60c1", "中冶建工集团有限公司", "33519a86-5375-4320-842e-51130d0d4dda", "91500000795854690R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "36b73d90-6836-4d40-b189-8d9a2380849", "50003920181008662", "水土污水处理厂二期扩建工程土建及设备采购安装调试工程", "934d3f08-9c4e-46f6-89e7-8731cf0d18d3", "中冶建工集团有限公司", "28740663-2ea6-46d1-8d64-156616f3a335", "91500000795854690R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "7b53e318-f2f7-4581-92da-dea565e5d04", "50003920181008397", "江北嘴金融城4号工程", "690dd29c-2eca-4bdf-8a60-228bdd828ba2", "中冶建工集团有限公司", "e19e7dba-5d82-4755-80a2-3a6a3f7d5620", "91500000795854690R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "ab4ce048-c93c-4747-9cec-1b02adbfcad", "50003920181009185", "蔡家大桥南引道（金山寺立交）工程", "1153910c-967e-463c-b102-5dc2bd2d1be5", "中冶建工集团有限公司", "898563da-c138-4da1-a2f0-787dc596bfab", "91500000795854690R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "3cc1b5af-0062-4f00-ad85-ba6e05b0ac9", "50003920181009161", "重庆北部新区金童初级中学工程", "d319f6a8-2f1b-4d82-a6df-8564785ffe1c", "中冶建工集团有限公司", "db2c2bad-aea9-41bc-a864-bb29ac42aa5b", "91500000795854690R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "e4e1d7b0-00d3-4619-a331-b32a52182e1", "50003920181008539", "重庆华侨城项目（地产一期高层B区）B1-B7", "185c1824-4c80-4479-99de-1e4753b60fc5", "中冶建工集团有限公司", "287b5f7f-2dc8-45b1-bb92-39386e850b8a", "91500000795854690R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "ffe996c5-a1b4-452e-a419-cae83ec7efd", "50003920181009113", "重庆际华目的地中心项目（启迪乔波冰雪世界）", "96451210-bbf4-4e98-9ea9-2f06f99f0c93", "中冶建工集团有限公司", "907845d4-d698-4436-87cb-b2ceafca8324", "91500000795854690R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "22c63215-9660-4909-a286-6f2a2d84647", "50003920181008682", "高科江澜二期一组团", "6ca0c38a-b8bc-4430-b9d9-ab4481f0f733", "中冶建工集团有限公司", "a2e94133-a5e9-4576-a751-63cabb6afa8d", "91500000795854690R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "e53ce84e-9dc2-41d8-b467-a3b55ada328", "50003920181009183", "龙兴隧道（三标段）", "f8104485-93ad-46a2-85f0-e999ef46cb3e", "中冶建工集团有限公司", "87393e1b-da3e-4f03-aee0-14069c3e8197", "91500000795854690R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "973ef033-5d8d-4875-b051-be509d47fb2", "50003920181022014", "龙盛路（两江大道）南北延长段-石河立交工程", "ecfd7b01-c282-4425-b95e-684268cd5d9d", "中冶建工集团有限公司", "0b50a26d-3536-43d1-95e3-bda59528eafa", "91500000795854690R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "bdf92202-b3e2-4304-87d3-ef43469337f", "50003920181009132", "重庆北部新区重光小学校工程", "93387c39-8ad3-4f0b-ba90-5578ad34b247", "重庆博达建设集团股份有限公司", "09e05c13-ae74-4624-b7ef-73e4ff024627", "91500000203592894J", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "39f5fe7a-0c5b-44a1-b384-0b2d67afa6a", "50003920181008752", "中交·中央公园项目（C110、C111地块）2-1~2-4号楼、2-11~2-15号楼及对应地下车库", "fd5bd1e9-5712-4a31-bec6-f732379e2434", "重庆昌林建筑工程有限公司", "f7b21888-3d8e-412b-966a-1f24901a123d", "91500000668945637C", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "17d047ca-a058-4ae5-a664-6f05b28d4e7", "50003920181008753", "中交·中央公园项目（C110、C111地块）2-5~2-10号楼及对应地下车库（2-16号楼）", "adfc20bb-3e8e-41d6-ad86-abe6c369d9e9", "重庆昌林建筑工程有限公司", "5c8c709f-380b-4134-b0f7-757ec55e1411", "91500000668945637C", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "9252ab5f-6617-4e78-8531-459af4c1525", "50003920181008568", "大竹林G标准分区G20B项目II标段", "2254c738-c800-4f8a-9d2c-aa2efd042066", "重庆诚业建筑工程有限公司", "e378e2ca-6c77-45fb-a853-741f48b701f8", "91500107666403682M", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "2390fd10-7e7f-4348-a951-988e7d52ed8", "50003920181008281", "重庆龙湖怡置新大竹林项目一期（022-7号地块C组团）1号楼、2号楼、6-1b号楼、6-2号楼、地下车库(1~19轴/A~L轴）", "2a86b4c2-d63b-40d9-ae72-7314bad1e9a5", "重庆诚业建筑工程有限公司", "d7b6e792-28f2-4e29-bf14-7c3506b4cf83", "91500107666403682M", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "6ebf2ff0-38ba-474f-99e8-fb33fe19b19", "50003920181008389", "重庆龙湖怡置新大竹林项目（二期三组团）1、5、6-14号楼和A、D区商业、部分地下车库及边坡支护", "4e3a46d2-6e2f-4134-b97c-b1f1d03154cf", "重庆诚业建筑工程有限公司", "87f0b04a-0367-4d33-93ef-657146d6d905", "91500107666403682M", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "04379be7-fed7-4182-9bc1-ece26e81327", "50003920181008728", "重庆龙湖怡置新大竹林项目（二期三组团）2号楼、B区商业、部分C区商业及部分地下车库的剩余部分", "a6295b15-25d0-4eab-8149-569d83335699", "重庆诚业建筑工程有限公司", "1972368f-3bb0-4414-b02a-8655968126b5", "91500107666403682M", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "6b3a5e9b-4da2-4e80-9b1a-b991b438b15", "50003920181008567", "龙湖礼嘉新项目四期A63-2地块5#~8#、地下车库", "c6998493-43dc-4161-97a1-51ffc89ff68b", "重庆诚业建筑工程有限公司", "23d37043-6bac-447f-af2d-f33aff9d1c2c", "91500107666403682M", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "d112fa46-18eb-4ea8-a085-df171e3fd36", "50003920181008578", "龙湖礼嘉核心区项目（A57-2/05地块一批次）", "fa389c29-1b0e-429e-a66a-55a12581ba75", "重庆诚业建筑工程有限公司", "69cad272-beb5-422d-846a-d0f704de803f", "91500107666403682M", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "f9f6c038-eefa-4e73-9a96-b7dad5ec987", "50003920181008483", "龙湖礼嘉核心区项目（A58-1/05地块）11-13号楼、20号楼、21号楼部分商业、24号楼幼儿园、25号楼地下车库及26号楼部分地下车库 ", "399fde4e-4a4a-44b6-a6b1-48dd11105ed6", "重庆诚业建筑工程有限公司", "c800dabe-3485-470e-9c66-0ba0dbfd8f9d", "91500107666403682M", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "e95dcdfd-1a2f-4693-95d1-e2c571c1adc", "50003920181008911", "北部新区金开大道（金州大道节点）改造、人行天桥", "a38b3f99-aa8f-42d9-827a-f86bf19c7080", "重庆城建控股（集团）有限责任公司", "8da06937-fc36-496a-9bf7-15d654e8fe50", "91500000709441516W", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "fb8b7317-dab9-43af-bf49-73f146135ed", "50003920181009164", "寨子路三期工程", "e4726540-df4d-4fef-b821-b5f6f597a59d", "重庆城建控股（集团）有限责任公司", "e2d73123-bebf-497d-8a85-78b61b198be2", "91500000709441516W", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a2faa25a-0c09-4d3e-b7a7-7ecd9d52440", "50003920181022011", "重庆水土嘉陵江大桥工程施工", "2e51fb4f-d3b3-4c0a-8576-2b41035d384a", "重庆城建控股（集团）有限责任公司", "d05c29ea-9eaf-4a9e-9f6a-f7c48fd6fcc5", "91500000709441516W", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "c7410fef-7ad9-4f3d-ae93-baaba6b108d", "50003920181008750", "万科园博园项目（一期）（C28-2-1/03号地块、C28-2-4/03号地块）", "018048ef-d303-4671-ba03-9172414bbf94", "重庆城鹏建筑工程有限公司", "8d610445-101f-414d-9713-991cc5ab5fce", "9150010820315005XF", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "5d46731c-adf0-4cf3-b932-b201ba09b10", "50003920181008452", "葛洲坝融创悦来项目建设工程三期（D19-3)二标段（8-2#、8-3#、8-S2#、门岗及地下车库）", "0058ef3c-1d00-4297-a020-43cc681f8f35", "重庆创设建筑工程有限公司", "cb8a6191-b344-4573-adcf-0b38c7cc852c", "9150011266088805XJ", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "72df3f6a-34d0-4006-8f17-873a05807e1", "50003920181008445", "葛洲坝融创悦来项目建设工程三期（D19-3）三标段（8-4#、8-5#、8-S2#、8-S3#、门岗及地下车库）", "70a07464-c22e-424b-ab36-a3f4541355c5", "重庆创设建筑工程有限公司", "8c2b64d2-de37-4127-935b-90ed6baa9155", "9150011266088805XJ", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "9b8a95e0-8cc4-49c7-a0e4-750abe7b93f", "5000392018100865", "葛洲坝融创悦来项目建设工程四期10-1、10-2地块（D17-4、D17-5地块）1#-7#、门岗1、D1#车库", "839b8ed9-ed2c-4b60-ac21-905d387ff1f5", "重庆创设建筑工程有限公司", "329751bf-4303-413a-816b-360e2bf8e1f4", "9150011266088805XJ", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "215a7b7d-3ee4-4641-86bf-9f64bf41c16", "50003920181008647", "葛洲坝融创悦来项目建设工程四期10-1、10-2地块（D17-4、D17-5地块）8#-12#、门岗2、D2#车库", "383af018-34e1-4253-8935-2c0586e8483a", "重庆创设建筑工程有限公司", "2800fe04-ae10-4044-968f-5dbd3ccabaa5", "9150011266088805XJ", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "23efcc6f-0dc6-4067-87ed-2b869ed7cc2", "50003920181008514", "金瓯.理工国际项目二期工程", "661f465b-d777-4c51-8d1b-db38e93d6ff9", "重庆达康建筑工程有限公司", "e1225f72-ddb9-4efc-89a5-6c2fc98b91a8", "91500107203139790L", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "8434f435-1903-4473-82b8-e62fcb21a20", "50003920181008680", "联发·山水谣（三期）5-9、32号楼", "15690b82-cb75-425a-95cf-1287148b1251", "重庆大江建设工程集团有限公司", "eeb61996-b317-4875-8349-8a018dddfcff", "915001056220734903", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "570c267a-63b7-4dda-b1cb-0494a98c888", "50003920181019002", "融创中航项目（E15-01/01号地块）二标段", "841e99ec-dee6-4f13-81df-1934afbee4b5", "重庆大江建设工程集团有限公司", "2ab981c9-bc5f-468a-9a6e-def8c02d5296", "915001056220734903", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "7773ca2e-2313-449f-bf2d-45bb7a95510", "50003920181008440", "北部新区B标准分区B18-1/03号宗地C区（五期）", "e89ff39e-ce05-40e8-bb74-9e179693f563", "重庆对外建设（集团）有限公司", "dd2b03d0-5057-4c78-b15a-8ed12d26536d", "91500000202803864J", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "ef0a7bed-97be-43a0-b4df-444126e9f84", "50003920181009141", "重庆保税港区空港皓月小区公租房项目工程（二标）", "c9efe7cd-12c6-49bb-8247-135436d43073", "重庆对外建设（集团）有限公司", "0261fa92-b34e-42d0-a567-e22fb2b82684", "91500000202803864J", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "d3357a5c-20a7-4627-8303-1bc5dcfe898", "50003920181008666", "金科亿达·重庆两江健康科技新城（E5-1/01地块）", "83e88ef0-0c6e-4824-865d-1872af2c1bbd", "重庆海博建设有限公司", "e0786e12-9c97-4e5e-a42d-206489c775c3", "91500112203510350T", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "3606fedb-1926-42f4-8b9d-1fc15ef103d", "50003920181008654", "大地工谷标准厂房（二期）37#-40#、 45#-46#、 50-54#、 57#、 58# 及地下车库", "a4651b10-bb32-4588-bdf2-d377fde9e431", "重庆宏鑫建设集团有限公司", "3ccfdc82-7cc4-424d-8254-14c8f2712e55", "915001177093882418", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "d75650b6-06ee-4c72-884d-867179e481f", "50003920181008450", "绿地·保税中心四期一标段（1、7、8号楼住宅、8-S#商业及地下车库）", "e8e73a6f-954f-41b1-b681-e9d76a8733b5", "重庆华硕建设有限公司", "27b8ac01-9a69-4a88-ad1f-289a81b8532c", "915001152034048303", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "0f242e6a-ec04-4748-9264-041b6507415", "50003920181008679", "绿地·保税中心四期二组团5#、6#、地下车库", "a27760d1-239f-444c-9032-58e9b4c25a45", "重庆华硕建设有限公司", "3ac17316-ff73-4398-8b31-82183afbd112", "915001152034048303", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "23c61a98-3a9a-4d32-8d40-869bab94d1b", "50003920181008390", "重庆龙湖怡置新大竹林项目（二期三组团）2、3、4号楼和B、C区商业、部分地下车库及边坡支护", "72c97939-28d1-4590-8f9a-40348b959864", "重庆华硕建设有限公司", "b4c5ea3f-6221-480f-82b3-96a621a0e623", "915001152034048303", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "6d5e003b-a62f-49f2-ac9e-92be4341ff4", "50003920181008360", "金科·空港城（南区A110-1/03地块二期2-1#至2-3#、B2#至B8#楼及D2部分车库）", "111f47d9-50b9-4210-bf21-efa13e5b788c", "重庆华硕建设有限公司", "49f78c78-adf9-45d1-af97-1fd967076d49", "915001152034048303", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "0d2c6fab-4ae0-4622-9479-f2818a2303a", "50003920181008571", "金科亿达·重庆两江健康科技新城（E3-1地块6#、7#楼）", "3f38af66-14d2-4844-97ea-03fe11ddcc00", "重庆华硕建设有限公司", "d40f73a3-a6d6-44cf-9b58-eb94efbb45e5", "915001152034048303", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "1f67a7dd-b199-4732-a7a4-1ed57976418", "50003920181008335", "金科天元道（二期）020-6、020-8号地块一标段", "4f65d488-2260-4c0b-90e1-20516f89fa66", "重庆华硕建设有限公司", "c278c20a-ac0e-453c-8edf-582926901b65", "915001152034048303", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "f4ead182-b1db-4c36-89b2-de14cc0ffff", "50003920181008370", "金科天元道（二期）020-6、020-8号地块二标段", "64422894-810d-4e4a-87b8-9951d80d6ee7", "重庆华硕建设有限公司", "3dc46500-749d-4ded-b117-e526df7d44c1", "915001152034048303", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "1966d70c-854b-4f07-9f02-6f926b73cf2", "50003920181008510", "龙兴组团J标准分区J46-1/01、J47-1/01号地块项目（一期J47-1/01号地块）二标段", "8f70ecb6-8fa3-4d20-9dde-02af5910a4ee", "重庆华硕建设有限公司", "6616ab90-89e1-4a7f-837d-5e89db7443bd", "915001152034048303", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "7ffd1c6b-9714-445f-9c3a-fcd216e696d", "50003920181008782", "龙湖龙兴项目（H65-1地块)", "596aa7bc-398e-480a-8743-a560826b0c6c", "重庆华硕建设有限公司", "138ea1f6-9490-40c6-8e8b-d6bb787a24dc", "915001152034048303", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "ed0b6897-31da-4494-b71c-13b6913fda9", "50003920181008710", "龙湖龙兴项目（H66-1地块一期）", "c464acd7-d2a5-4751-bca3-b5ac5a0f0ec8", "重庆华硕建设有限公司", "b7b8ee9f-9f4d-4edd-b70e-48a424031a91", "915001152034048303", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "c19e60ca-bc4c-4930-8bd3-e9acbe189c5", "50003920181008565", "华御澜湾二期（F01-9、F01-6号地块第一批次）", "3eb95d23-c314-41f5-8263-2f275a74cfd1", "重庆华姿建设集团有限公司", "abef100c-47e6-4f02-9346-be0863ceb82b", "91500106203050024F", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "9e652e6e-fb31-4afe-95e0-0d730c82af6", "50003920181008718", "盛泰礼嘉项目A37-2号地块", "79e31f6d-131b-4326-91e2-cbadb890905a", "重庆华姿建设集团有限公司", "bc437c41-f366-4cba-b50f-8fe9587d1fb7", "91500106203050024F", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "2cc4b329-1a3a-4f1a-8a72-099cea86f36", "50003920181008505", "盛泰礼嘉项目（A29-3号地块）7、9、11号楼及部分地下车库", "eb1c692f-4640-42f8-a2ac-b3c299d1cddc", "重庆华姿建设集团有限公司", "e6fe15a2-266f-41fe-9d88-112e2f8ea353", "91500106203050024F", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "8f259127-0eed-4d02-9225-92619727efc", "50003920181008757", "盛泰礼嘉项目（A39-1号地块）", "7e82d57b-ad3b-462c-89f9-88ab8e91e042", "重庆华姿建设集团有限公司", "4ccdf3ec-c0bd-4b1c-8e0d-33ea76e371c4", "91500106203050024F", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "501803c1-1e78-4234-a800-73ac5aef050", "50003920181008664", "联东U谷.重庆两江新区国际企业港二期5号地二批次（1#、6#-7#、12#-15#）", "4c6a9662-aa87-40cc-bb74-cf3ac047d088", "重庆慧河建设有限公司", "79a3eb92-f080-439d-8ac9-111af09f423f", "91500107771776659A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "ebdc90bb-0829-4571-a0de-477e8575c4b", "50003920181008525", "金科九曲河项目（F03-10/05）一、二期（B-1#、B-8#、B-9#、B-S1至B-S3#、B-29#、B-30#、B-32#、B-33#、B-36#至B-39#、B-S7#至B-S9#及B区部分地下车库）", "c9056133-fb64-4e92-b081-e3b3d71c0a87", "重庆佳宇建设（集团）有限公司", "25a1aa1a-6916-4d73-8e5f-b5a0e503df1b", "91500107203114390A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "07744ac3-cbdd-4cd2-b06f-90704033039", "50003920181008423", "旭原创展大竹林项目（08-01-4/03）地块项目一期二期二标段", "18674445-5c4a-4948-831e-16f26bad6168", "重庆嘉逊建筑营造工程有限公司", "55aa32c3-7b5e-41ba-b1ff-dcd7fc09f4a2", "91500000778457538G", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a190db4e-cfcb-4094-89a3-351f93a45f7", "50003920181008604", "旭原创展大竹林项目（08-04-1/02、08-04-2/02号地块）南区1-7号楼及车库", "cd551bae-19b6-49ec-94ed-4efc8cb6f3fd", "重庆嘉逊建筑营造工程有限公司", "a2a8eae8-562e-4170-b7aa-07888ec80a10", "91500000778457538G", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "2fae9f5d-403f-425d-9232-136f7d842a8", "50003920181008519", "旭原创展大竹林项目（08-04-1/02、08-04-2/02号地块）（北区1-13号楼及对应车库）", "253c6c8c-4259-4dbd-b01a-6feca78cfe58", "重庆嘉逊建筑营造工程有限公司", "741e8aef-6687-4aaf-bfbc-9630398e96a9", "91500000778457538G", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "d6f8ccd1-0c67-4828-b0d1-d4dfb565496", "50003920181008378", "重庆龙湖创安照母山项目G17/05号地块（一期二组团一批次）15、17-22、24-28、40、64、67号楼及部分地下车库", "b9f33464-0cb2-488d-a6f1-26b5e0416800", "重庆嘉逊建筑营造工程有限公司", "fece8de7-ca9b-4cb7-8517-b05c6232ef4c", "91500000778457538G", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "ba6b05fb-bb98-44ce-bc59-c4a89a8c325", "50003920181008437", "重庆龙湖创安照母山项目（G16号地块）11-18号楼、20-25号楼、S2号商业及部分地下车库", "093dbb99-2f74-47f2-860a-c3229c013be7", "重庆嘉逊建筑营造工程有限公司", "89e72143-0cc2-46f3-a79e-ab8e03625072", "91500000778457538G", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "01f0468d-5dff-4083-b5af-2023eec81bf", "50003920181022001", "两江新区G11-5/03公共停车楼项目", "1bfc21bc-35ab-4e11-a6b5-b9602921024a", "重庆建工第八建设有限责任公司", "7dfd780d-e69c-44d4-84c8-86f96f5da028", "91500000203137250B", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "67de0660-f1c1-44e8-a88e-bb88440a7e7", "50003920181009173", "曾家岩北延伸穿越内环新增通道工程（三标段）", "2e59c844-dc21-4d07-b4d0-be93a733ca8b", "重庆建工第二市政工程有限责任公司", "e037432d-f4dd-4542-85a8-223be8d93806", "91500000202801455F", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "49076cbe-7d5d-44da-85a5-f6176977801", "50003920181009162", "江北嘴公司溉澜溪广场项目——主体工程、溉澜溪广场西侧道路工程", "b44dd244-bbcb-45b1-a382-5481cd7745e0", "重庆建工第九建设有限公司", "a3d4b9f8-5929-42e3-b2d0-2e2e7f64e39a", "91500000203145189X", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "496f8df8-8ad5-49fe-ab6a-c6f5748fb2f", "50003920181008648", "重庆北部新区金州初级中学校工程", "e365bd8b-0d95-4d0b-b638-35a2e743e22e", "重庆建工第九建设有限公司", "6a2fe913-0247-46aa-b235-44915e95f705", "91500000203145189X", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "f6f6b864-c2bc-4c33-bb24-77a7b032d65", "50003920181008600", "鸳鸯公交站场上盖物业综合开发项目", "37a6c9bf-403a-4fc0-8194-4a20929cafe3", "重庆建工第九建设有限公司", "cdd10629-af26-4001-bcd3-59a310b72d3f", "91500000203145189X", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "78cefd92-14d8-49bb-b41b-a472113109f", "50003920181008669", "两江国际新能源汽车创新创业孵化园（定制厂房六、定制厂房七、门卫房1-4及地下车库）", "1a8239e7-572b-4769-8467-2e29c7795262", "重庆建工第三建设有限责任公司", "a6e351df-cef3-4acf-b450-412f8efdd2c2", "915000007339743120", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "8c62d99a-2881-4156-9340-2d5f322fc12", "50003920181008641", "两江御园项目（F13-1、F14-1号地块（一期））G01#-G30#", "22f24f39-1bc7-48ce-ad1a-1619c6c8d655", "重庆建工第三建设有限责任公司", "76021490-7940-4770-b7c8-2b4f7bd8d5cf", "915000007339743120", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "987f047a-69ce-49f2-9143-aadef4d7276", "50003920181008717", "两江曲院风荷项目（C04-1/01地块）、（C05-1/01地块S01#楼） ", "7dea88f0-b7e7-4eb2-9593-02830127da0a", "重庆建工第三建设有限责任公司", "0149fa61-3bbb-44b7-9e71-ade164f0b774", "915000007339743120", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "ade3df45-0b7e-4dcf-b4e5-4712021df68", "50003920181009139", "和合家园D组团工程", "227e1352-e0fe-4a51-b917-f3ce7ce84e9c", "重庆建工第三建设有限责任公司", "910b271c-e565-4003-8ec2-4653a2eb6fa0", "915000007339743120", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "51380327-09f7-49e3-89af-c944dc58350", "50003920181022009", "恒大御府（B17-2号地块一期）（14#-44#、75#-79#及地下车库、门卫一）、恒大御府（B17-2号地块二期）（59#-60#、63#-66#及地下车库B区）", "72d51b97-3972-4935-b356-ef7e6fa794d5", "重庆建工第三建设有限责任公司", "88b71772-2c4c-41bb-9509-739448ac30b2", "915000007339743120", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "3af11615-2b8c-4c72-9f6c-7216483ceab", "50003920181008560", "民生两江新区综合物流基地项目（F30-1/02号地块）", "ca816c53-2399-4bfe-a4ad-5953df6f4476", "重庆建工第三建设有限责任公司", "c0930760-898a-4889-b37e-926a914b2a17", "915000007339743120", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "6519dbfe-5912-428a-96c8-e0d2dac1924", "50003920181008743", "重庆两江新区龙兴住宅项目（H83-1/01、H85-1/01号地块）", "74791d56-b7a9-4efa-bb2e-a8cc1736ff66", "重庆建工第三建设有限责任公司", "b1ab6b5e-584b-4d04-93cf-0fe7b8dfb240", "915000007339743120", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "bed10e98-245e-4c5a-adbd-5bd15f67aab", "50003920181008189", "重庆儿童医疗中心综合楼", "1c711463-2c54-4364-a593-2c282f59f9e1", "重庆建工第三建设有限责任公司", "a669cb33-e479-4e03-a7be-884dc0c8ba36", "915000007339743120", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "f5f82844-4e7f-498e-9c1c-d7010c1cbfa", "50003920181008307", "重庆市人民医院", "06c97f14-bf6a-4f25-b10a-dd3631266a0f", "重庆建工第三建设有限责任公司", "4561aa3d-fb54-453d-9597-39e323a55e99", "915000007339743120", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "c6ee209f-8e25-4416-a031-74cc224d245", "50003920181008635", "重庆龙湖创佑九曲河项目（二期2组团F05-16地块）1#~18#楼、M1#岗及地下车库", "eade3e2d-6bec-493f-af3e-9a386dcc6287", "重庆建工第三建设有限责任公司", "941609ea-788c-49ed-a298-1b32575f5fe2", "915000007339743120", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "680aea8d-3d3e-443b-b7c4-ccf24cf8552", "50003920181008402", "高科江澜一期总承包", "f82bfb11-9d34-46b5-8799-d6ca3dc7134a", "重庆建工第三建设有限责任公司", "c2e27655-bcce-4e4f-95cd-bebe518e057e", "915000007339743120", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "2c63173a-9904-4b5d-be79-1b1e3bd641d", "50003920181008531", "龙湖礼嘉新项目四期A63-2地块1#~4#、S3、部分地下车库", "3d4881a0-399f-4bee-bbcb-ce8237385994", "重庆建工第三建设有限责任公司", "e9193bb7-108b-4983-ace2-f05bfbbfddfb", "915000007339743120", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "27c53926-32c5-4c4c-92d8-9f10d59d10c", "50003920181008460", "中国西部黄金珠宝国际交易结算中心", "fbba1dc5-d253-4dfe-bd8d-787c03179d8f", "重庆建工第四建设有限责任公司", "3dedce0f-5b8f-49e6-9c63-18383d813709", "91500000202800890K", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "64b57529-124b-4a95-9299-0e04e39fbd2", "50003920181008304", "北部新区G5G6地块项目D区", "1b74d56d-0640-4543-8304-cc88ad0b78c0", "重庆建工第四建设有限责任公司", "2bc4498f-1b64-4ab1-9fd5-baa01d02ccc1", "91500000202800890K", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "e04dc435-fe1c-4ff8-bb40-17923e64393", "50003920181008907", "北部新区礼嘉商务中心区L42路西段道路及配套", "d0f488f7-f56b-4979-8e36-879f4f0bde47", "重庆建工第四建设有限责任公司", "4e75ae83-14d8-4690-a3ba-9975a23fd15a", "91500000202800890K", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "9496c77f-fa7c-4241-97c6-655f23b6b90", "50003920181008516", "重庆市人和中学校", "ad8bf455-13df-48e9-874b-914c3d048d83", "重庆建工第四建设有限责任公司", "de8cf485-31f6-435c-b005-444285074bc3", "91500000202800890K", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b0748fb9-e46f-453f-9c15-fc513a572f5", "50003920181008924", "观音桥组团A标准分区A34-1等地块道路及综合管网工程（二期）", "e2a8d521-8e91-4e63-978b-3144384e5159", "重庆建工第一市政工程有限责任公司", "9f2d7bc1-fffa-4a50-979c-af06b45d08d5", "91500000202800874X", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "3d8a97ec-2432-401e-8e27-713b5fb4c99", "50003920181008347", "太阳座", "d7691c68-f3a1-4be9-8268-e4e38192e086", "重庆建工集团股份有限公司", "5861d58d-fc92-4590-a0a5-00696c40ab9d", "915000002028257485", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "48809561-942b-42bf-857e-ea599ff0dab", "50003920181009142", "重庆保税港区空港皓月小区公租房项目（一标）", "4e0ba942-b20c-4524-8647-9ac0de702aa6", "重庆建工集团股份有限公司", "7ad2582b-7723-466d-9a81-d516233c5fee", "915000002028257485", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "4652677a-6d1b-4cb4-afc2-4d55f94a24b", "50003920181008053", "重庆照母山G4-2/02、G4-3/02号地块项目三期二组团1-2#楼、9#楼及部分10#地下车库（轴交：12轴-1轴纵", "04a24385-c310-44ce-86ba-075a4a6f8c40", "重庆建工集团股份有限公司", "2171cbd6-bd0d-4600-b85a-f98a0762f527", "915000002028257485", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "83de70c2-8187-462c-bde3-0a7df4ab867", "50003920181009225", "中德（水土）智能制造产业园厂房总承包工程", "7865a077-500a-4857-a96c-c9671e54483f", "重庆建工住宅建设有限公司", "87ef1e8e-ce95-4b6a-8ffb-17b5f9f4c44f", "915000002028009114", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "c2e0f9e7-62aa-42bb-83ce-7d3b16eb5dd", "50003920181008903", "新南立交工程", "5bdc09fa-9fdc-4e7a-85a0-1c9d88e6ed8f", "重庆建工住宅建设有限公司", "e1ae8db2-102a-4afc-8ac6-77f8de88bf14", "915000002028009114", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "ee0ff453-4f0d-446c-a3da-001fadc78bc", "50003920181008614", "金州苑三期", "7e68af28-876e-4836-8fcf-64273fe3af10", "重庆建工住宅建设有限公司", "4a2287db-7e63-42ef-a5b0-55faf63e219c", "915000002028009114", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "7c2e82a7-a6a8-4d5a-97e3-72388290147", "50003920181008372", "金科九曲河项目（F03-11-1/05）A-1#至A-13#、A-16#至A-19#、A-S1#、A-S2#及A区部分车库", "e96485a5-f1ca-4114-a312-3084176b55e9", "重庆建工住宅建设有限公司", "29953cb6-038a-425d-be16-b7ed82fc8396", "915000002028009114", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "4f45e217-53f6-4a7d-acde-5fd7bca07d6", "50003920181008724", "金科天元道项目商业地块（二期）", "c2b28d51-6aaf-465c-b322-d4d5edd20a16", "重庆建工住宅建设有限公司", "e42cd0d4-a5b6-4551-83e1-a9423eb152f9", "915000002028009114", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "35506032-e0c3-4327-8304-231652dfc19", "50003920181008524", "金科天元道（一期）023-1号地块1-24栋及地下车库", "8766692d-0e01-4721-8ec9-85c22c2a34ac", "重庆建工住宅建设有限公司", "bf86dac0-96a9-469b-a34e-7a05c4045d4b", "915000002028009114", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "d9b5b6f6-7e2f-41fd-b1de-aa7805c1183", "50003920181008685", "金科御临河二期一批次H45-1/01地块（8-1#至8-3#楼、D8地下车库）、H45-4/01地块（9-1#至9-14#楼、9-S1#楼、9-S2#楼、D9地下车库）", "0fb74659-27d6-479b-ab63-8ef37a52c368", "重庆建工住宅建设有限公司", "bda5fb67-7a7f-4b2f-b382-14c670614b19", "915000002028009114", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "05669e04-d198-4194-9806-d7bc8ed7fa0", "50003920181008506", "金科御临河（一期）H33-4/01地块（4-1#至4-15#楼、D4地下车库）", "6236c01f-24d7-4d8c-ae1a-fb1959ed9cff", "重庆建工住宅建设有限公司", "51e1a135-0318-4040-b604-d248300a0757", "915000002028009114", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "1913d42a-9538-4728-bbf7-b8fdae9cc4e", "50003920181008715", "两江御园项目（F12-1/03地块一期）（H03#- H04#、H16#- H17及相应车库）、（F12-1/03地块二期）（HS01#、HS02#、H01#、H02#、H18#、地下车库） ", "f12e5700-dc33-49b0-b4ac-d84c23235574", "重庆教育建设(集团)有限公司", "7ff40614-6a80-4722-b733-409672389173", "91500000203645116A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b471b044-29f9-472b-972e-566c33a026a", "50003920181008642", "两江御园项目（F13-1、F14-1号地块（一期））FD01#、F01#-F41# ", "6c983750-998a-4569-a7a3-7da28443aeb4", "重庆教育建设(集团)有限公司", "652f962a-c3cc-48f5-acad-c17d54dd5b60", "91500000203645116A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "684f9b70-c456-47c6-9a38-1f71b6b93b9", "50003920181009045", "重庆市巴蜀初级中学校", "67c4a24c-f92c-482c-ba81-29c87eaa55d9", "重庆教育建设(集团)有限公司", "581b7105-ab01-445a-8bc7-2b0cdb74ed05", "91500000203645116A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a3bf91c5-00de-4c0f-a836-4f6895cbe48", "50003920181008184", "重庆涉外商务区B区二期", "504e0ef0-79e4-4342-9da5-378b94bd2f7c", "重庆教育建设(集团)有限公司", "448d71bc-6744-43cc-a277-b7a7ca69c9ff", "91500000203645116A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a36b3398-b011-4b20-a884-0e849bf1d7f", "50003920181008520", "重庆龙湖创佑九曲河项目（二期1组团F05-8/05号）一批次", "4740fc16-b061-4c4d-b6d2-a0187438f944", "重庆教育建设(集团)有限公司", "e7a7803a-cb26-4d4a-aded-cb58cf2f5558", "91500000203645116A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "c4b16abf-19e1-4567-a218-25241cbca3b", "50003920181008726", "龙湖礼嘉工程三期28号楼", "80c7626a-602f-4914-bbbf-b560a326fad4", "重庆教育建设(集团)有限公司", "2a838423-2ff8-4b3b-8e27-8bd041f2d5fc", "91500000203645116A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "0cc7d35e-dc72-4d74-aea1-67991303549", "50003920181008482", "龙湖礼嘉核心区项目（A58-1/05地块）8-10号楼、14-15号楼、19号楼、21号楼部分商业、22号楼商业、26号楼部分地下车库 ", "9186a7ca-84bc-482c-9ba1-f414ef2e2d50", "重庆教育建设(集团)有限公司", "42ea5ed9-cebd-4fa2-8c1f-d8b00570b17a", "91500000203645116A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "bac8ac35-5832-4199-934a-733120c84fa", "50003920181008455", "北部·湖霞郡住宅小区开发项目二期建设工程", "5aa95c07-4485-40bf-b656-425c586ec964", "重庆两江建筑工程有限公司", "96c2f14e-85c5-49b4-b7c2-5357e2901037", "91500234621193706E", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "56b3afd2-46f4-41c5-9ae1-d40f9b454e5", "50003920181008526", "金科九曲河项目（F03-10/05）二期（B-13#至B-17#、B-21#至B-28#、B-31#、B-34#、B-35#、B-S6#、B-S10#及B区部分地下车库）", "266439e6-b0fe-453c-b47c-a0496ebeab5f", "重庆龙润建筑安装工程有限公司", "7c8813f1-e3bc-4c88-a189-2fdbbdd5dd37", "91500000793536752C", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "6a077e55-fe05-4b67-9ca9-b59ec1bcc99", "50003920181008702", "天蓬樾府（C51-3/01）1#、2#、7#、8#、9#、10#及对应车库建设工程 ", "b451c0d8-9354-4e2c-a895-237c46625d55", "重庆桥强建筑工程有限公司", "0f06889b-40f2-4544-b6ba-e1da904d20b5", "91500242MA5UR91H0U", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "28e9b71e-623a-4209-b2d9-6e6f2b241be", "50003920181008480", "桥达·漫生活街区（5#楼、6#楼、7#地下车库）", "9afb854c-2b0d-48ac-bbe4-f5d51723be00", "重庆桥强建筑工程有限公司", "f3cb7f39-ef41-496a-b126-06108076823c", "91500242MA5UR91H0U", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a8113064-af05-45f4-8d3b-047edf85ef7", "50003920181008550", "南极·凤麟苑（下沉式商业广场）", "d0cd93ca-e5de-4df1-9936-71d8c79beeb8", "重庆山海建设(集团)有限公司", "57cf0885-c947-46a7-b9d4-89cf24f8f08b", "915001032028648861", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "cf4831d0-dfaa-4ac9-a1bc-8bb90cddd35", "50003920181008638", "两江新区悦来组团C分区望江府（C58-1/06地块）一标段（1-4#及部分地下车库） ", "333384a7-5a8a-4494-9dba-1fedfff63973", "重庆市巴南建设（集团）有限公司", "55ef33eb-ee63-470e-bffd-c845341be7e4", "91500113203436859R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "2932b92f-7b78-4a7b-b28b-0bfa42663bc", "50003920181008639", "两江新区悦来组团C分区望江府（C58-1/06地块）二标段（5-9#、门卫1、门卫2及部分地下车库）", "112e247f-5884-442b-9cf9-01a68eaeb648", "重庆市巴南建设（集团）有限公司", "8de10d0f-f5ff-408e-bbf5-33eff2e85147", "91500113203436859R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "e62c56c9-e097-4e50-97ec-91d08514750", "50003920181008686", "葛洲坝融创悦来项目建设工程D分区D16-1号地块 ", "ce2a4f5b-312d-4fd4-940f-5837677fc8cc", "重庆市巴南建设（集团）有限公司", "6b541410-d055-40e3-8e77-2567dd765016", "91500113203436859R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "fbf3c50a-770d-4d10-8620-fb9bc9bd897", "50003920181008617", "龙兴组团J标准分区J46-1/01、J47-1/01号地块项目（二期J46-1/01号地块）二标段", "bf94f4e0-f1dd-47fc-ac80-eacfb06b82ff", "重庆市巴南建设（集团）有限公司", "203e3405-4571-4c34-870b-e83e64b1aa86", "91500113203436859R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b4900c05-a06b-4d13-be7f-3e5864e27c4", "50003920181009253", "重庆祥胜物流有限公司新建标准厂房项目", "de48bb41-0a89-4c22-a524-95ff29672788", "重庆市开州区渝东建筑工程有限公司", "1e1c3bca-f62d-4b1d-9e2f-bc777cdd5ee3", "91500234208109988P", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "9ca8641c-e923-4aca-a524-63ccd27074a", "50003920181008296", "两江总部智慧生态城吉田总部二标段（1-8#楼、11-18#楼）", "57453a33-0682-498a-a590-0c303395f919", "重庆市万州区地方建筑有限公司", "38a8913a-fa41-4669-9d80-35ebcaa2bee0", "915001012079017115", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "8c3d618e-b9a3-4d48-85bf-647bfd5fb74", "50003920181009291", "水木云天平基土石方工程及基坑边坡支护工程", "4887c9cb-3e66-4e90-945c-ef9501a4562f", "重庆市渝北木鱼石建筑工程有限公司", "6b77a03c-35a4-466f-ab23-ebfce182226f", "915001127339876234", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "ee4e1b96-0479-4e5c-9d90-5074f7fc419", "50003920181008165", "绿地·保税中心二期二组团二标段（4-A办公、4-B办公、4-商业及地下车库）", "38d3d329-6df6-441a-8e1d-7fabf221d289", "重庆市渝北区海华建筑工程有限公司", "464d86ad-161f-4a07-afb6-24c41e1efb62", "91500112203540058R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "3259ffa8-a55e-49cf-a7a1-eaa76632d4a", "50003920181008424", "旭原创展大竹林项目（08-01-4/03）地块项目一期二期三标段", "0e87fb2e-3d2d-478c-8979-612942b753c8", "重庆拓达建设（集团）有限公司", "bbae9724-379a-45ee-9f96-b535972ceb4f", "91500108768876288X", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "5b33ec95-3bd3-4bc4-8dab-85bf8ac9bc5", "50003920181008338", "棕榈泉悦来项目一、二期（B1-B9号楼、B10-1号楼、B10-2号楼、B11地下室）", "8c3da1c5-8472-4523-b5d6-68d044e26f2d", "重庆拓达建设（集团）有限公司", "dc5c8863-fad9-4dab-9bc6-adb739177e24", "91500108768876288X", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "5ec3c508-1d6a-43f4-b3b8-29ddc8d08e0", "50003920181008351", "重庆龙湖创佑九曲河项目（一期2组团F06-1地块）1、5~7、12~15、17-1、17-2#楼及19#部分（地下车库）", "0e501631-000f-4a5e-a714-f5ea8d0a5e75", "重庆拓达建设（集团）有限公司", "8bc80bf0-f5cc-4f0f-a06a-656fa773a5df", "91500108768876288X", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "587b6f91-eefa-4e8a-a473-09249d2460c", "50003920181008744", "重庆龙湖创鑫新照母山项目", "09a72007-ca22-4030-bfc9-280ebff6de70", "重庆拓达建设（集团）有限公司", "86ee796f-45be-48c3-8092-ec220b9bf554", "91500108768876288X", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "8d1d3e4d-8633-403a-b5bd-cfd8a6b4d2e", "50003920181008282", "重庆龙湖怡置新大竹林项目一期（022-7号地块C组团）3号楼、4号楼、5号楼、8号楼、地下车库（19~40轴/A~L轴）", "86823fd5-b0d4-48b2-9f85-87f4fdcd1415", "重庆拓达建设（集团）有限公司", "3b258a1a-7410-4459-9325-a9c1dafc32ca", "91500108768876288X", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "e301e100-83ed-4163-b476-d8a35d91c45", "50003920181008732", "雅居乐·常乐府（A21-4-1/06地块）2-14#—2-24#楼及地下车库", "7af09e75-3e10-458f-962f-4a0b129f0c02", "重庆拓达建设（集团）有限公司", "92f768d8-c9ee-49db-9b32-15d225a2ab26", "91500108768876288X", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "80a53e63-9858-4a27-b289-d648f73e5d7", "50003920181008709", "龙湖龙兴项目（H60-1地块）、（H61-1地块） ", "d4d2a5ad-0d84-41b2-8196-e260fc7abc58", "重庆拓达建设（集团）有限公司", "73ceee39-4d0d-411a-80d1-e02277ace524", "91500108768876288X", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "6cf8a5aa-a632-42d0-92cd-9f30d360a4d", "50003920181008471", "中交·中央公园项目（C104-1/02、C105-1/03、C107-1/02）二标段施工总承包工程一期", "293c7cd5-230f-4451-939d-a4013ebd65d3", "重庆万泰建设（集团）有限公司", "8c63e2c5-26c7-47ea-ad16-e6c9f5aab7f1", "91500112784219544A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b95017e7-5112-4400-bf8b-b474a55c41b", "50003920181008393", "北大资源·悦来二期（一标段）（含65-1#商业、66#、67#、68#、69#、70#楼及对应车库、配套用房）", "7eb969e3-515d-41da-a8a6-b470537fffe9", "重庆万泰建设（集团）有限公司", "547ad634-2214-4f5b-aa65-54480d22d1e8", "91500112784219544A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "abe09ab3-cea9-4fd9-b47a-6393fd9595e", "50003920181008592", "北大资源·悦来二期（二标段）（含57#-65#、71#-74#楼及对应地下车库）", "ce64bbd5-d045-40c7-a6a6-b63b0bcf8303", "重庆万泰建设（集团）有限公司", "7597d50c-ed6a-458b-8b12-80f34df1279f", "91500112784219544A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "9bc16c15-edfc-4a85-81c0-ec4f47e15da", "50003920181008276", "北大资源博雅东二期一批次（含3-27#至3-30#楼、3-40#至3-41#楼及对应地下车库）", "d55e1322-ae2b-46ef-8bfa-c2ff1fd7e579", "重庆万泰建设（集团）有限公司", "bd6c67aa-ce67-4b44-bbfd-a3e3a5e0022b", "91500112784219544A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "023e0527-ad9d-4220-a497-a770420fba5", "50003920181008422", "旭原创展大竹林项目（08-01-4/03）地块项目一期二期一标段", "27f801aa-bbfe-41a3-917e-863a306c4246", "重庆万泰建设（集团）有限公司", "14a97f02-9992-4f59-ab11-b871482503f8", "91500112784219544A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "7a5e0a57-6c9c-49c2-a303-da78b57bcb7", "50003920181008451", "葛洲坝融创悦来项目建设工程三期（D19-3)一标段（8-6#、8-7#、8-S4#、门岗及地下车库）", "72af1ed0-6992-47ce-9ec2-291bb13915f0", "重庆万泰建设（集团）有限公司", "db2b3298-4d7e-43d9-b10d-c9393d857586", "91500112784219544A", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "44187c66-18ac-4636-891f-6e8bfb72152", "50003920181008377", "重庆龙湖创安照母山项目G17/05号地块（一期二组团一批次）11-14、16、29-39、65号楼及部分地下车库", "e35b7ca6-13db-44bf-9706-05b46390700f", "重庆先锋建筑工程有限公司", "3210bccc-938a-4fc3-8327-b90cbe25fab7", "915001137094571042", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "9ecdf2cc-8f21-4c91-8a6c-9380b2cd7ce", "50003920181008786", "重庆龙湖创安照母山项目（G13-4-2地块）", "353fb4c7-c0f3-4cdf-9640-77fdc8218205", "重庆先锋建筑工程有限公司", "b4f78387-8518-4d8f-bb19-a233bf50586f", "915001137094571042", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a54df58a-7b02-4cc8-98c4-d5498336768", "50003920181008436", "重庆龙湖创安照母山项目（G16号地块）1-10号楼、19号楼、26号楼、S1号商业及部分地下车库", "a693f41e-86ce-409f-addf-874111d0181e", "重庆先锋建筑工程有限公司", "e8ffa7fb-f2cf-4c8b-bbc8-e9d4c3166cd5", "915001137094571042", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "71ce35fb-d68f-416d-ac1d-7ec6354562c", "50003920181008361", "龙湖礼嘉新项目A62-4/03地块（5~10#、11-2#地下车库、12#门卫）", "4db22c9c-d9be-4632-992a-4b207a3d4e90", "重庆先锋建筑工程有限公司", "9956b6eb-4f6c-462e-a673-50c8e0bd144b", "915001137094571042", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "48ef0170-2e44-4068-a5b9-347e6f5776c", "50003920181008417", "金科九曲河项目（F03-10/05）一期（B-2至B-7#、B-10至B-12#、B-S4#、B-S5#楼及B区部分车库）", "6c674041-4206-4167-bff0-ee74be37baaa", "重庆祥瑞建筑安装工程有限公司", "d6f2d914-be2a-46ce-9a5f-7bcd0127c3a4", "915001167453400690", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "78e7fb5f-89de-4519-bcdd-ffc87868828", "50003920181008373", "金科九曲河项目（F03-11-1/05）A-14#、A-15#、A-20#至A-46#及A区部分车库", "7a7cbec1-427a-4310-8739-bf16582f7564", "重庆祥瑞建筑安装工程有限公司", "1ad8e744-eeea-4b69-8ab1-2a97ca660676", "915001167453400690", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "055687bc-7d15-4c0e-80c9-32869cf559f", "50003920181008495", "绿地·保税中心四期一组团二标段（2#、3#、2-S#商业、3-S#商业、部分地下车库）工程", "074b6569-b552-48af-be66-0657ce00e309", "重庆欣耀建设工程有限公司", "8d4f2ada-bad9-474c-9353-00570f9d3680", "915000007659339370", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "10962956-024e-4fa3-a7f0-8ef79130be9", "50003920181008543", "两江新区悦来组团C分区望江府一期（C48/05地块）A区二标段（1-2#、4-9#、12-16#、门岗M1#及27-1#、27-2#部分地下车库）", "297cc356-63b9-45e6-84d7-6845edefe292", "重庆新科建设工程有限公司", "522a99f5-d93e-4356-af36-47252797b7b7", "915000002035362864", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "dc315537-e9f7-455b-bfc4-a73ea3073d8", "50003920181008391", "葛洲坝融创悦来项目三期5、7号地（D16-4/08、D18-3/09地块）5号地块", "0051e608-e728-4e2d-a94e-d0bf28d7c1be", "重庆新科建设工程有限公司", "6324e7e4-d78d-4fcb-9f97-5148b1b1e900", "915000002035362864", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "2bb084ee-1f1d-4d60-aa1e-f69e6ec17a4", "50003920181008553", "金弓动力综合发展园", "51b12e51-099e-4b3b-a6d1-d8bd5ab4b070", "重庆耀文建设（集团）有限公司", "e690de7e-f18c-436b-b5a2-6488aff1eba2", "915001122029003492", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "67366465-09da-447a-87cb-6dd480c0a03", "50003920181008785", "北部新区经开园A23号（晟景台）6#、7#楼", "4e90a2d5-2e51-4f04-8b73-89cddb670617", "重庆一建建设集团有限公司", "903614ea-3d4a-46f6-bf11-502b4f290a1a", "915000002028150726", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "d2c90606-cc09-4e57-9b6c-081b6eb84ca", "50003920181008548", "晟景台项目1#、2#、13#、1号车库、农贸市场部分项目", "32350d0d-8e3c-4658-8f9a-859c4f42d039", "重庆一建建设集团有限公司", "1e133c7e-f988-488b-96cb-b90bf3bf3a37", "915000002028150726", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "06e949fe-680e-4ce6-b188-4683ae64b7a", "50003920181008418", "渝高·观澜A组团一期（1#楼、4#楼、5#楼、6#楼、11#楼-15#楼及相应地下车库）", "c528a8a7-0d93-48f4-bd33-89c1536db11d", "重庆益欣荣建设工程有限公司", "eeb0ae20-547f-4916-817a-3b8021aa7b8b", "915002437717917369", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "09d659c0-0610-4685-aece-ef34a8c2987", "50003920181008502", "万科城四期G26-6地块项目", "f1c45047-dcea-4410-aafb-bdfcc67c717f", "重庆渝发建设有限公司", "bfc1af07-3942-4e0c-a128-026b2974debe", "91500115203399701L", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "264b2acb-ff46-4e89-b673-0c24aaf4e38", "50003920181008344", "万科溉澜溪二期（4-1#、5-1#~5-5#楼）", "8cf9f864-c498-4e0a-baa8-64325d9c5977", "重庆渝发建设有限公司", "7d73e69e-989f-4f60-916f-b2211b55b407", "91500115203399701L", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "edb52779-4ab9-4b74-921d-e0162191baf", "50003920181008581", "万科溉澜溪项目（G12-1/02地块三期10#~13#楼及001#车库部分）工程", "aad4bcbb-5be2-42e3-b699-779863859488", "重庆渝发建设有限公司", "f5e09f09-ed1b-472c-b02c-fab023d5b30a", "91500115203399701L", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "dd72a9b1-b55d-4cd2-83ff-84c76d0e822", "50003920181008631", "万科溉澜溪项目（G12-1/02地块三期二标段7#楼及对应001#车库部分）工程", "7923bd03-7b29-418e-a55b-a87bad0156cc", "重庆渝发建设有限公司", "11f1c8fe-0c68-4d57-a86a-6545ac2cb113", "91500115203399701L", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b3954b05-937c-412d-812d-39742eb2137", "50003920181008470", "中交·中央公园项目（C104-1/02、C105-1/03、C107-1/02）一标段施工总承包工程一期", "e11b51fe-a47f-42db-a3f6-c06e5778cd05", "重庆渝发建设有限公司", "afc2dee0-6bd9-4949-9652-cc5da3d71531", "91500115203399701L", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "699f3890-90e6-4596-b2b8-e2ff4e5ad7c", "50003920181008257", "恒大照母山项目三期（1-6号楼、12-18号楼及一号车库）", "7ad87dde-beb6-4a94-9b03-6616d81ab29b", "重庆渝发建设有限公司", "fd65e041-eb9b-4ecc-a575-d2e80cbaef6a", "91500115203399701L", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a2cf099b-11b2-468a-ad15-81585091a20", "50003920181008545", "国瑞·两江城（一期）3、5、6、10、11、12、32、32-1、33、33-1号楼及地下一组团车库", "dba594ef-1f69-4d1f-8198-377650d83cc6", "重庆渝建实业集团股份有限公司", "1bb0b6a3-f38a-4de4-b143-bb68407b131f", "91500000203888388W", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "e5c79437-0015-4e81-a4f4-e4344398eec", "50003920181009200", "重庆康菲动力科技有限公司年产20万台高性能发动机零部件项目（一期）-物流中心", "7d4a5595-aecc-4bfe-9b73-2f654d1071f3", "重庆致辉建筑工程有限公司", "4afae13b-eec4-47c7-bb1b-d1dc92e48aca", "91500106668918719G", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b7ffaa1c-5c4a-4c2b-925d-8362be61c36", "50003920181009182", "礼悦路道路及配套工程", "006d63e0-edab-4df2-bbfe-d0145022403f", "重庆中环建设有限公司", "9389596a-6e43-478d-aa96-4e731fa2f69a", "9150000020289820XH", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b6798fe2-f1d9-4ce9-8c89-09d33582b9e", "50003920181008644", "华夏航空培训中心（学校）项目一期工程", "b9a7bc67-c4fa-4f7d-bd3c-187ae66eaf26", "重庆中科建设(集团)有限公司", "b00f1f45-6666-4041-a4d4-6bdade2f013d", "91500102709427140L", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "116a4519-38d1-43c4-b896-ea785c09e45", "50003920181008439", "金海岸", "7b13f424-73b1-42d5-9b7f-71014b878995", "重庆中伦建筑工程有限公司", "b45e4a22-11e2-49c3-929d-4754d054e157", "91500112622194380R", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "7e13ade5-3625-4fd8-83f7-0f7535ca34e", "50003920181008492", "北部新区B标准分区B14-7-1/04号地块（一期）1-5、16-19、43、54号楼及对应车库", "a162ba2f-b421-4399-b942-b770f53491dd", "重庆中通建筑实业有限公司", "e7fd3df2-07a4-40e1-a381-28162bfcaaba", "91500107203046316P", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a50dc1c6-08ac-4fce-ad52-c9dc5baec1d", "50003920181008723", "北部新区B标准分区B14-7-1/04号地块（二期）", "d461b5a8-5fcb-4bfd-aaa2-39858e405cb1", "重庆中通建筑实业有限公司", "f0057b4c-338f-40f6-9182-ea666d4cf5ff", "91500107203046316P", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "87a1a0a4-a2ac-4ade-93ce-958ad5d5a89", "50003920181008570", "北部新区B标准分区B14-7-1/04地块一期（碧桂园中俊·天玺）6-12#、14#、20-32#及对应地下车库、大门", "6964c046-38aa-439e-a5ce-253aaaaa9f99", "重庆中通建筑实业有限公司", "6c77bdea-bae6-443c-93da-774746bec78a", "91500107203046316P", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "d109c1f1-e95e-41ee-81ef-e3fdbcb48a7", "50003920181008527", "华商·悦江府一期一标段（1#、2#、9#、10#、11#、12#、13#、14#、15#、16#、17#）及对应车库", "fb022047-e622-4d09-9f04-8012227a08fb", "自贡华兴建筑安装工程有限公司", "433c8b42-8a77-4982-95a0-cbfc5b8a3d2c", "91510300204053518T", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "d7ac968e-bddf-480e-a1e1-57d4a11ba68", "50003920181008607", "华商悦江府（D04-1-1/07地块）二标段（3#、4#、5#、6#、7#、8#、18#、19#楼）及对应车库", "736574e0-6614-4f3b-bb57-94862dab2bf8", "自贡华兴建筑安装工程有限公司", "680c7942-333b-4d20-b3f4-b38c548d3489", "91510300204053518T", "两江新区", false);
            AddAccountsAndProjects(dbContext, accountList, "eda5065f-f1f0-476f-89f3-d8db1b4d017", "50002520181017084", "天誉一期", "95924af2-362e-46e7-ae1d-4198a3c375df", "山河建设集团有限公司", "eccd1023-cfea-47d1-ba91-6dbdb481d17d", "91421121272000266B", "梁平区", false);
            AddAccountsAndProjects(dbContext, accountList, "1ec9c421-845b-49b0-a6be-7d7dff0eeb0", "50002520181017087", "梧桐郡1#、2#、3#、4#、5#、6#、7#、8#、门禁、车库", "c4bc1adc-3c17-4bea-a1db-593407ef9bc8", "重庆鹏威建筑工程有限公司", "50ad3ecd-2c50-45dc-b63a-22943b65c1ae", "9150010857344416XK", "梁平区", false);
            AddAccountsAndProjects(dbContext, accountList, "7e900538-28eb-46cb-a6e3-5b866e9c02a", "50002520181017042", "立城·香韵华府1.2.3.4.7.8.9.10栋、地下车库、小区门岗", "a0440c9c-bbf8-4a36-b6b4-bd850c2bc421", "重庆厦宇建筑工程有限公司", "c8b8ca94-fd17-49c0-a181-40c810bb1f93", "91500118073660106L", "梁平区", false);
            AddAccountsAndProjects(dbContext, accountList, "f2b89c62-3d08-476f-a47a-71d84e7861b", "50002520181017039", "巨源·南泊湾一期一标段（1、5、12、13号楼、S2商业及地下车库）", "5530dde9-0c0d-4fb0-b183-8c7bfe8106e3", "重庆市华东建筑集团有限公司", "0891a9f6-8ca4-4a3c-8a91-301e290d6a07", "91500228753052710C", "梁平区", false);
            AddAccountsAndProjects(dbContext, accountList, "662a8e33-e863-453d-bd26-a99c3c12331", "50002420181007100", "金科云玺台三期", "f8d8eb2e-f22a-481d-9d5b-0795e667ceb9", "中建八局第三建设有限公司", "6b6db7fe-9ade-441b-b179-3ce8669654b7", "91320100134891128H", "开州区", false);
            AddAccountsAndProjects(dbContext, accountList, "26637fdc-76dc-4fe9-9ccf-6f4fc3ca138", "50002420181006024", "万开周家坝－浦里快速通道工程一标段", "02845131-b519-43c8-bb04-d6d72b263c98", "中铁二十五局集团有限公司", "25cb9643-d4fd-4bf1-80dd-7707647495e2", "9144000019043049X8", "开州区", false);
            AddAccountsAndProjects(dbContext, accountList, "40504dc8-991a-4094-852e-f90942b5e09", "50002420181006025", "万开周家坝－浦里快速通道工程二标段（K5+300-ZK11+594）施工", "cf8140ea-91dc-4294-858d-d0aa80c3b49e", "中铁十一局集团有限公司", "2bf7991c-15cc-41d4-928f-a61b3be4ea1c", "91420000179315087R", "开州区", false);
            AddAccountsAndProjects(dbContext, accountList, "9c614d9a-ff4d-4094-b93e-79e4fe45e19", "50002420181007000", "开县大丘邻里中心", "7d22aa19-c0dd-4874-ac9c-ae3860f75d75", "重庆城建控股（集团）有限责任公司", "599546c6-f69c-421b-8fa7-0817727597e0", "91500000709441516W", "开州区", false);
            AddAccountsAndProjects(dbContext, accountList, "90ff7ac1-740f-4dea-8c30-8fdddb1bf69", "50002420181008038", "开县北部新区基础设施建设工程-滨湖北路丰太段", "8ddad728-7840-4c00-8f57-47115f3bca16", "重庆建工市政交通工程有限责任公司", "0fd00c70-0830-4cff-9aa7-85dd05b68fe4", "915000002028014715", "开州区", false);
            AddAccountsAndProjects(dbContext, accountList, "d067dfc3-7057-41b3-b58a-9f57ddf672b", "50000820181015029", "北京城建华岩住宅小区二期19-38#楼、1号地下车库B区及临街商业、物管用房及边坡支护", "a81f9d18-8888-43b9-a4ac-47e9077dd689", "北京城建七建设工程有限公司", "108355c9-4afe-4978-b5d8-0a8bff295453", "91110000101639946H", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "f8aed952-d9c7-436f-bcdb-e24a41c0877", "50000820181015081", "中昂别院1#、2#、4#、5#、19A#（1#、2#、4#、5#对应地下车库）及边坡支护", "7b88b55c-9f25-4507-bf83-8e8fd55d929e", "北京纽约建设发展有限公司", "8707fca1-4363-4aba-b922-6a46659e113c", "911100006635560265", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "00e2b969-ff79-4b9a-9c61-2454b64640c", "50000820181015092", "保利爱尚里二期B组团（B1、B2号楼、B001号地下车库及边坡支护）", "efd1d751-92c1-4805-b073-33728130853e", "富利建设集团有限公司", "d918cc37-2c90-484d-bf03-aed48672a7a1", "91440101190527857P", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "ddc0fc8f-a758-4834-a5cf-7437d5ec200", "50000820181015093", "柏景西雅图（B5-1塔楼、B5-1裙房、B5-2塔楼、B5-2裙房、一期车库）", "ea7bae35-9815-4019-9277-d6983745f19c", "南通四建集团有限公司", "91c75571-d25b-4824-b714-6045e352eb40", "913206121387235782", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "39569a23-3e87-41ce-91e3-901fbde8ec8", "50000820181015040", "昊华小区一期（1-1#~1-5#、1-S1#、1-S2#、独立风井、001#地下车库及基坑支护）", "60bb4834-f9e9-47b1-8f57-508aaf57137a", "中国华西企业股份有限公司", "814c45ff-9eba-4aa8-9496-e963aafa19a4", "91510000201808890B", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "c07763a6-b16f-4950-b984-21d3f3ed14a", "50000820181015041", "西海岸（C36-2/02地块）17#、18#、21#、22#、25#、26#楼及地下室一区（17#、18#、21#、22#、25#、26#楼对应部分）", "af8b2b8d-8d5b-409b-bf80-a763a88e2a1d", "中国华西企业股份有限公司", "95d8c66f-9a85-4dd6-a686-21de3f9ac3f9", "91510000201808890B", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "d906f7f8-1f3c-43c0-babd-2b3163ba8ae", "50000820181015010", "重庆中迪广场一期6#楼标段、8#楼塔楼标段工程", "459d0baa-851d-4a22-adaa-828a330a4fa8", "中国建筑一局(集团)有限公司", "5a96fa31-a7eb-4512-ac19-ec85dc4a27a4", "91110000101107173B", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "f5b100ad-d776-44bf-8609-25b34dfc71d", "50000820181015013", "卓越中寰（1#、2#、3#楼、地下室及边坡支护）", "f3e6a0f3-cf67-442c-9e72-757588f5d85b", "中建三局第一建设工程有限责任公司", "669e5674-b2bd-4740-940c-7f7a2a691b15", "914201007483157744", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "bcb66c71-e934-440f-baf6-30162609461", "50000820181015021", "中建龙玺台", "18c1e934-3190-4da3-9e0f-cf70267464e9", "中建五局第三建设有限公司", "a6144c4f-7c29-488b-922b-10d5d0b92dc9", "91430100183853582A", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "32de0e5e-5b2d-4573-8fab-982ceea2f5a", "50000820181015107", "恒大林溪郡三组团", "01809c6f-74de-45b7-b9d1-33f131e1b2e7", "中兴建设有限公司", "d73eb460-d2f3-4fe5-b704-5d09f996738c", "91321283737840047C", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "8410ed14-3cb1-4da3-bb61-6e2639f2b82", "50000820181015108", "恒大林溪郡五组团", "03680996-3171-48bb-922e-dc2b957c5631", "中兴建设有限公司", "cdc6001d-b213-49ee-92c3-2bd6a19c5151", "91321283737840047C", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "9a27e3a0-fca0-4e2e-a3e0-f3a03a657e6", "50000820181015104", "快速路二纵线华岩至跳蹬段工程（一标段）施工", "9b27ff42-9a16-49df-a722-29fb3af9526c", "中冶建工集团有限公司", "82c21b75-88e0-4fd3-8cdd-b2a8e660e908", "91500000795854690R", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "690a79be-fb89-45a1-ad01-8014575c06e", "50000820181015106", "五洲文化创业中心", "bcff86bb-c400-4712-ab42-5b912679c680", "重庆对外建设（集团）有限公司", "e8836d8f-e1c4-47a8-9c37-8466381e3da4", "91500000202803864J", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "29186d0f-95aa-439b-9ce2-7216032ef46", "50000820181015035", "云鼎栖山1号院（1-11#楼及车库）", "f17f1e05-4156-4b99-949a-8d835c211403", "重庆宏宇建设工程(集团)有限公司", "ed2b9279-af6e-4b7c-be19-3dbf51a24d4c", "915002436608590130", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "7438234d-6f96-4fca-b080-65672def914", "50000820181015091", "盘龙广场及基坑支护工程", "a4690f55-4a3c-4faa-9d26-ab01f77d39a2", "重庆佳宇建设（集团）有限公司", "3804ac6d-13fc-4350-abae-ea88e6fd5719", "91500107203114390A", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "1d09ef67-27bf-44d4-9262-c5a83574802", "50000820181015105", "快速路二纵线华岩至跳蹬段工程（二标段）施工", "9477ed39-2538-4054-be35-9b11e9eebc86", "重庆建工第三建设有限责任公司", "220f3cbf-b923-4343-a363-0f1baafbd4f4", "915000007339743120", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "4c6012c0-1487-45db-bbc3-02a1e8929ac", "50000820181015109", "西郊路27号排危改造项目", "c86c6b1e-f432-4816-9cf8-42c87a2b8ceb", "重庆建工第三建设有限责任公司", "1958bdbe-1c52-4d45-85a3-c4cfb72e4adc", "915000007339743120", "九龙坡区", true);
            AddAccountsAndProjects(dbContext, accountList, "f0d1dca0-dcbf-425e-841e-c1d0a5ce5dc", "50000820181015079", "九龙坡区精神卫生中心迁建项目", "a4da805d-8381-42e9-a6ba-45ccc19b1c85", "重庆建工第四建设有限责任公司", "bba72914-1bdc-484f-9042-e63aa4ab42bd", "91500000202800890K", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "b6b899b2-1054-46b9-85f9-a79ea88caaa", "50000820181015103", "嘉华大桥南延伸段三期南段工程", "0cdc818b-474d-4998-91e4-d3289a5bae66", "重庆建工第一市政工程有限责任公司", "b874e387-31a0-427f-83c4-cd3ac300c447", "91500000202800874X", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "71cf9817-0616-45e8-9744-ebe32731537", "50000820181015101", "华岩隧道西延伸段项目二标段", "00140bbf-4817-49f5-9d9b-3c4cb8f3f49a", "重庆建工集团股份有限公司", "cff3533c-5e2d-4b58-812a-b4da6f9380f4", "915000002028257485", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "1cd911fb-1a89-4525-a203-facf1c6afc5", "50000820181015096", "九龙坡区九龙镇蟠龙小学扩建项目", "ef6db296-9f17-40be-851d-afb70369468b", "重庆建工住宅建设有限公司", "a024af51-13b3-44a7-bc15-a565bb18a327", "915000002028009114", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "83a2c651-8bed-43ea-a8fd-fced1244210", "50000820181015009", "云飞九龙香山S-1商业、Z-3、Z-4、Z-5、Z-6、幼儿园、地下车库工程、车库屋面及边坡支护", "9cadd47f-566d-4503-9c48-5267770af412", "重庆庆华建设工程有限公司", "9076229f-8fd4-4ccb-80e7-23ce1130771a", "91500109753062492U", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "6893f5b3-5457-49a1-94cc-eef3e75aaf3", "50000820181015102", "华岩隧道西延伸段项目五标段", "3f141e98-b560-4978-8c74-7d88daaecd11", "重庆市爆破工程建设有限责任公司", "e88e2466-2670-418e-9eaf-6286c9900f62", "91500000202883649J", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "7ec0431b-108f-4b7b-93d2-25f19a1eccc", "50000820181015027", "北大资源燕南五地块（大渡口C12-1/06、C14-1/04）（包括5-1#楼A栋、5-1#B栋、5-2#楼、5-3#楼、5-1#商业裙房、岗亭及地下车库）", "83631002-3684-44a0-83fb-17e6066b6b24", "重庆万泰建设（集团）有限公司", "6842cf16-4aa9-489c-b160-30aec2a7cedd", "91500112784219544A", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "5cadb947-ecd3-4fe2-9c79-87a8f6b1f5d", "50000820181015065", "重庆壹本科工城（A/B）标准厂房（B-1、B-2及汽车坡道（B区））", "14602a21-e352-4fd5-80df-00c854bd7d1f", "重庆中科建设(集团)有限公司", "80a1d5ee-b9b3-4ee4-bf7e-696c3a884dd9", "91500102709427140L", "九龙坡区", false);
            AddAccountsAndProjects(dbContext, accountList, "11dae6cc-85b5-46ad-b8ae-3def991a466", "50004020181008015", "重庆九州通医药物流中心（三期）", "7affa449-fecc-4525-8cdf-83268e597173", "湖北信义兄弟建设集团有限公司", "569dc260-38f2-4dbe-855a-eefbf00e5d9a", "91420116558449739P", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "f2694073-ede4-46a2-ba2f-de801f12056", "50004020181008019", "碧和原茶园项目二期", "3a77f6ff-a544-4a14-8d21-9bef78e2af06", "山河建设集团有限公司", "905a6fbc-ceb0-4fda-86cd-434e051292dd", "91421121272000266B", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "e6b2fe07-e89a-41e3-91e1-87c2f0eac34", "50004020181008022", "金科.博翠园F13-1/02地块", "4badb8d5-1f03-41e7-8fad-1c1f162ad4fd", "山河建设集团有限公司", "1b3aa753-0503-4538-ad04-5c6a9c39eab1", "91421121272000266B", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "95c14e25-f4f5-4e4a-a854-758b9288410", "50004020181008023", "金科.博翠园F13-8/02地块", "da61864a-1a08-4168-97a8-d1ffa356a133", "山河建设集团有限公司", "79507330-0dc2-4b72-b101-96299b3d028f", "91421121272000266B", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "7d204592-1778-4a82-97f5-8bc36e74c8c", "50004020181008024", "茶园组团J分区J1-2-1/03地块", "b51a11ab-b96d-4341-bd8e-1ee5b9cb51dc", "四川域高建筑工程有限公司", "8648c848-f005-4360-a201-f26074bbbc49", "9151000068417596XN", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "685549d3-9e9f-4c1c-8d3b-3b8e5b2e36e", "50004020181008028", "重庆永翔现代物流产业园一期", "041dc993-ed19-4a5a-b684-4089664f09db", "浙江天勤建设有限公司", "4561f374-f640-4070-932b-9d26d4ba3f2f", "913306217368750966", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "8a5d8ba7-cf44-4d36-99ca-30b7015bee7", "50004020181008026", "领安重庆东港物流园", "5f327558-4957-42bd-9112-2dc1617f7dc2", "浙江天勤建设有限公司", "dbf0f868-2400-4f1b-8b6e-04d5b7b55751", "913306217368750966", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "f72c880d-4a4c-4db6-8312-0bc4d17deab", "50004020181008027", "经开区C-D连接桥工程", "1e0678f6-91c1-42cd-a2e5-7062c7e003d3", "重庆城建控股（集团）有限责任公司", "3f594895-85c7-4608-a43b-0ddba2cbbc64", "91500000709441516W", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "de2f6d81-f5df-4716-92df-9561cf457eb", "50004020181008020", "金科.博翠园F13-3/01地块", "9216d24e-f138-4458-a7ab-43c77628b828", "重庆海博建设有限公司", "098f3ea7-60d3-4288-9d4e-3ea1b18f13e5", "91500112203510350T", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "d9de3488-6db7-44be-993a-759d9ffbf05", "50004020181008021", "金科.博翠园F13-4/02地块", "a5b63eaf-0933-42b5-9549-9de947e84463", "重庆海博建设有限公司", "5858654c-1f73-4f28-85db-58dacabaf249", "91500112203510350T", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "5c345935-ded5-4799-80a1-a63584a0417", "50004020181008009", "和坤商城", "39e12e67-3f8d-46be-8cfe-1e2ccd7774ca", "重庆南洋建筑安装工程有限公司", "9dcad828-11f1-4362-8002-be39104d8bd9", "915001132034257973", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "34c88749-0d70-442c-b19e-55110b0a8d0", "50004020181008018", "昕晖.依山郡", "c77c1ed6-9a09-4e9d-8f52-672b588126cf", "重庆鹏威建筑工程有限公司", "e5ecc859-c5a4-44e6-b324-68f091707636", "9150010857344416XK", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "ebbda58a-674e-4419-b449-96a66f794d8", "0004020181008011", "恒大同景国际城博达地块项目", "b61f4511-fe39-4745-ab95-0f20c773af24", "重庆万泰建设（集团）有限公司", "8cd6e5e3-3a4b-45e5-bb66-15ec21149bca", "91500112784219544A", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "13460235-15d8-4d8c-af37-8e8634f50cb", "50004020181008013", "恒大滨河左岸项目（7-12号楼、车库、大门）", "c68e2e33-abea-48f2-b004-5166f54890ce", "重庆渝发建设有限公司", "1b09d588-9df2-499e-8c90-43343d25ee06", "91500115203399701L", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "27e30380-8662-43a5-8c67-976604384a7", "50004020181008007", "渝能•长悦府施工总承包工程（第二次）", "e1e225ee-f74d-44d3-a123-8b67c92739d6", "重庆渝能建筑安装工程有限公司", "ae21d949-46f8-496f-b7e5-02b5afe98bd4", "9150010820292907XR", "经开区", false);
            AddAccountsAndProjects(dbContext, accountList, "1d14e687-8d53-4851-8626-6be2fa6eb62", "50001420181008017", "金科集美郡一标段", "c7b25198-d18d-4f30-a78f-087ecf1b74d1", "南部县锦兴建筑安装工程有限公司", "919e7510-be7c-40df-9fce-25dd16c887ff", "91511321584213383R", "江津区", false);
            AddAccountsAndProjects(dbContext, accountList, "ca2a9cc1-cebc-426d-ae40-b907e7d07c5", "50001420181008067", "华远江和墅一期（1-46#楼、73#楼、车库A-D）", "af2ea850-32d8-41a1-a26b-388254bdd936", "南通华新建工集团有限公司", "1838dc17-3f76-437e-bdc6-d2331a6b77b9", "913206217037355485", "江津区", false);
            AddAccountsAndProjects(dbContext, accountList, "b55f0258-8a3f-41fb-a6d2-d0e79c479ec", "50001420181008052", "中梁首府项目一标段（1-10#、20-21#、26-27#、35-39#楼及对应地下车库）", "0b2a828c-79b8-4337-8e86-f5df347a9aea", "中国建筑第七工程局有限公司", "295b9e71-d108-400f-90c4-3ea7c43d1ceb", "91410000169954619U", "江津区", false);
            AddAccountsAndProjects(dbContext, accountList, "1afa9cc8-d888-4c58-a00e-6dcbdf4e1ef", "50001420181008029", "中骏·云景台", "a6f3cefb-883a-4a79-b276-e15ce32db078", "重庆华力建设有限公司", "6348134e-c962-45dd-8f7e-f2a9c3e8f063", "915001032028344687", "江津区", false);
            AddAccountsAndProjects(dbContext, accountList, "566f6c4c-c796-42fc-b7aa-46f00967096", "50001420181008122", "江津区第二人民医院扩建工程（含住院综合楼、儿科综合大楼）", "a87f2ede-711f-476d-920d-e8b7d28c63ee", "重庆建工第七建筑工程有限责任公司", "fa9c2a41-6da1-4e70-8cbc-d107e27630c7", "915000002030377780", "江津区", false);
            AddAccountsAndProjects(dbContext, accountList, "ae82d3fa-5bd6-44e9-9f36-f11075cd94a", "50001420181008036", "恒大国际文化城A区A17-A22#楼及地下车库", "3334538e-8a15-498c-9aa7-9929a5c9edbb", "重庆建工第三建设有限责任公司", "00c760f7-ec13-4906-90a7-1bdb7848bfd9", "915000007339743120", "江津区", false);
            AddAccountsAndProjects(dbContext, accountList, "0fe3f260-7c5e-42aa-aaf9-4eb7f566897", "50001420181008028", "恒大金碧天下三期695-701#、704-705#楼", "f9d0b1cc-d223-4253-bf22-f330e329bdaf", "重庆建工第三建设有限责任公司", "a31903a1-b195-47e1-9aee-1e848598b746", "915000007339743120", "江津区", false);
            AddAccountsAndProjects(dbContext, accountList, "9f18492c-74ed-4945-bb4e-cf8bd43a007", "50001420181008040", "恒大国际文化城A区A13-A16#楼及地下车库", "480d69aa-9533-4b5e-8412-989ad091fd68", "重庆庆华建设工程有限公司", "c135d2b2-8964-42a9-8589-0971fc8e35bc", "91500109753062492U", "江津区", false);
            AddAccountsAndProjects(dbContext, accountList, "05f3ab6c-8a8b-43b3-985b-e1e83c092bb", "50001420181008080", "城海滨江春城二期（3-4#楼、9-10#楼、14-15#楼、18#商业裙房及对应地下车库）", "3fc64a42-2438-485c-b3b4-c27f1a5c39a5", "重庆拓达建设（集团）有限公司", "a7ef261d-0151-4ce3-8c2e-21ce6f33ad60", "91500108768876288X", "江津区", false);
            AddAccountsAndProjects(dbContext, accountList, "e05a4c30-953f-4b6a-b2c0-150fbd72d32", "50001420181008134", "御景江城三期（3-6#、8-9#及部分地下车库）", "3d4a3888-5c62-4804-93ca-b49bd2dae6de", "重庆一品建设集团有限公司", "b011b600-d097-48ae-9c81-d0032e1cb27d", "91500113203404742P", "江津区", false);
            AddAccountsAndProjects(dbContext, accountList, "7f7be416-f3d0-4056-89d7-ec0cf95bcf0", "50000620181017017", "龙湖·春森彼岸五期T2-1、T3-3、5#车库", "27c00893-5b73-4139-9da6-96056bf9d0da", "成都建工第八建筑工程有限公司(原名：成都市第八建筑工程公司)", "7d91d120-61ef-4fe9-a4fe-239bde13b6c3", "91510100201912401J", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "ae3d1032-8d82-46d3-bd79-aa35b9df5c6", "50000620181017002", "华融现代城项目一期施工招标", "0dd506ae-91fa-42f8-848d-1542a9426ff7", "福建省惠东建筑工程有限公司", "9a3a5748-57f4-4891-8ac6-0f9b648d1842", "91350521156220149Q", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "444cd24f-0e97-474a-b62a-e6875a8a281", "50000620181017001", "中冶铁山坪F-2/02地块项目施工总承包工程", "a1c47311-29f7-45f4-87da-c1a6ab6d2d19", "上海宝冶集团有限公司", "d89b4fb6-6288-43b0-9926-07c195f9132d", "91310000746502808A", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "3f0ef910-284f-4f16-a601-827ba987628", "50000620181017015", "重庆航天职业技术学院教学综合大楼", "c86d7155-4ec5-4ebd-a787-9a797d11d6ab", "四川航天建筑工程有限公司", "154bcd47-de00-4c2c-aa99-47ffc0d56a73", "91510112210450739B", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "67436475-3cc1-406c-8457-29cbca1eca3", "50000620181017008", "港城小时代", "865cfb91-9bbf-4541-aaa0-a31e14234431", "业兴实业集团有限公司", "1f2fd6e5-e68e-4fdc-ad98-492a7986b998", "91500112203526977P", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "7204ff85-f68a-4c40-afe2-65335edb0b8", "50000620181017007", "洺悦府", "a298cd45-83a3-4d1c-9296-e5bc2ac95a31", "中电建建筑集团有限公司", "8534efe4-aafb-4cd2-97d2-4915b4f25598", "911100001011159077", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "c2082098-5ebd-48ef-b985-05d909198bb", "50000620181017005", "御龙天峰项目（原信和北滨路项目）四期H01、H02号楼、S5商业及D03#车库工程", "c3dd66dc-c0d7-437b-897f-fc0551111718", "中建五局第三建设有限公司", "ecd78c41-b4ee-44ac-8c03-6895e41f9bf3", "91430100183853582A", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "271049f0-a594-40f9-b226-832c11ce510", "50000620181017014", "重庆科技金融中心L01-4-02地块3#楼、6#楼及地下车库建设工程", "8dc1e04f-700f-4aab-a22a-b891b20ce950", "中欧国际建工集团有限公司", "5ad45978-2f50-4b29-b3fe-6e61d7fd67ab", "91511402621133375W", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "dda5ef39-6ba5-4c15-9a15-7437ae29e3f", "50000620181017012", "观音桥F地块", "66566da9-6c71-451c-9e07-d40b6df0ceb2", "重庆城鹏建筑工程有限公司", "bc2c2957-4aa5-44ed-ad3f-08646b95ca19", "9150010820315005XF", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "d45225e6-4f86-4126-b735-c385e175531", "50000620181017016", "金融街.融景城B1（021-3/02）地块项目三期工程", "3b2159be-95a0-44c0-b522-6cc796f92c0f", "重庆浩龙建设集团有限公司", "21618e15-c123-4b7f-b473-fb6142aa64ff", "91500112750085744T", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "4a731f03-800b-4b45-ae68-10f57d46d9c", "50000620181017006", "旭辉江北区E14项目", "0d229abd-edfd-41b5-bb91-50b972b54c53", "重庆佳宇建设（集团）有限公司", "371e7b85-6249-4504-b7e8-cc61303a56d0", "91500107203114390A", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "d155638d-2992-4689-abe3-2fde74d15c6", "50000620181017013", "重庆市蜀都中学校改扩建工程（主体及部分配套施工）", "af786319-dda0-42d9-8a08-00b02cf4adff", "重庆建工第八建设有限责任公司", "15f16883-7c9e-4fa9-a9b1-f53df899603a", "91500000203137250B", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "0feb8c25-e280-4a8e-9719-e100c20e79b", "50000620181017018", "龙湖观音桥项目二期一组团工程（6#、S4#及地下车库）", "d458f93a-6613-4822-ac2d-29d154e789cb", "重庆建工第三建设有限责任公司", "27633666-7585-4ab9-8782-c8af077ea4ea", "915000007339743120", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "5b4a1e73-07dd-464c-a765-54ae53aae19", "50000620181017019", "武江路立交", "1cddf7d2-2432-482c-aef4-e28591e33ea4", "重庆建工集团股份有限公司", "cd2ff20c-9ded-4906-b007-3d60ff8b865c", "915000002028257485", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "e484638a-2483-4027-973c-5e901d546d3", "50000620181017020", "港城西路延伸段工程", "dea32ba5-a2f5-43a2-be5b-4d41791a658f", "重庆建工市政交通工程有限责任公司", "fac91655-dcad-487a-9bf3-36e6f669233b", "915000002028014715", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "8174394b-0a66-462c-8663-29661b79912", "50000620181017011", "绿地.海外滩四期三组团E区（9、10、13号楼、14-2号楼及地下车库）工程", "76c8c035-f59a-417b-a595-a20589099518", "重庆市渝北区海华建筑工程有限公司", "da4f3a33-b34a-4a0b-9541-6af4a8d3d7ce", "91500112203540058R", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "ba315dae-bec7-47f7-98f2-8c86b5abfa4", "50000620181017010", "绿地.海外滩二期2、3标段工程（重新招标）", "3a0214b1-d4d7-40f3-a44d-1694a2d4f319", "重庆市渝泽建筑工程有限公司", "a1085f70-819e-4325-8682-d90cd6c3c660", "915001182037507761", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "b25f4cb0-da13-4737-9d8e-4c3339cf066", "50000620181017003", "华融现代城项目二期（施工招标）", "b723d9f3-7caa-4021-acd7-43efe7b42e63", "重庆松龙建筑(集团)有限公司", "a51d7d6b-deab-4db4-832c-ced02840e72d", "915001122035067490", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "2e109bb3-63af-4da6-b8e9-c38ebd00379", "50000620181017009", "漫山居", "a7e70415-aa4e-4d04-8b84-f301ddfb43ed", "重庆亿瑞建设工程有限公司", "c1a24e93-72eb-4944-b3fc-6cf90eb40b07", "915001126221017891", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "b6abb9eb-7c84-4463-a508-7f65f6d7be5", "50000620181017004", "御龙天峰项目（原信和北滨路项目）三A期12-T16号楼、S3、S4商业及D02#车库", "cdbad60a-8258-40c0-acda-a752c9dd5f3d", "重庆渝发建设有限公司", "95dce193-08bc-4108-8aa0-fa6f08cfbb5b", "91500115203399701L", "江北区", false);
            AddAccountsAndProjects(dbContext, accountList, "3543e4d2-1487-47c8-960d-e1f88c2ff70", "50001520181018082", "鹏润悦秀御江府（一期)", "157af539-8b46-4b53-a362-41add4521b01", "贵州建工集团第五建筑工程有限责任公司", "e24eabd6-a15a-41e1-b313-135e36648b04", "91520000214410777M", "合川区", false);
            AddAccountsAndProjects(dbContext, accountList, "fae506ed-ab6e-4646-849f-498f3eb79b9", "50001520181018063", "北新•御龙湾I期一街区", "70d82998-345d-4887-85a7-8bdc60f75e8a", "湖南北新天际建设工程有限公司", "730a7846-a8e6-44bb-8c09-fbbd9d11bb50", "914300006985786391", "合川区", false);
            AddAccountsAndProjects(dbContext, accountList, "73cc8bf5-54bc-45f4-a65a-cf9fb999cf6", "50001520181018075", "重庆合川花滩医院一期工程", "fc09f37f-9d35-44ac-afcc-eed6c748a156", "天元建设集团有限公司", "aca705ec-32b6-493b-9e1d-d0c5de11f79d", "913713001682510225", "合川区", false);
            AddAccountsAndProjects(dbContext, accountList, "5a0f0f65-0438-4aa8-b602-ac9ec754f11", "50001520181018096", "碧桂园•智慧家一期", "ee1a16ab-cedd-4db4-9089-c927cf65b01b", "中亚建业建设工程有限公司", "980de269-2630-401d-87c3-4ac2b54c11a3", "91511402207304853R", "合川区", false);
            AddAccountsAndProjects(dbContext, accountList, "2567800f-e960-4ee2-a067-b580174baae", "50004120181016012", "融堃▪彩云里", "e635f651-3de8-48a7-aa23-ef30529d76eb", "业兴实业集团有限公司", "4cc11246-b1c2-49de-a4fb-6e31b280d337", "91500112203526977P", "高新区", false);
            AddAccountsAndProjects(dbContext, accountList, "cfb74f10-e8bb-43dd-bd8b-0ade95fba79", "50004120181016047", "盛资尹朝社项目二期一标段（2-3至2-6号楼及车库）", "81bf2ffd-7f49-47f8-8472-982c92598c91", "中国建筑土木建设有限公司", "419ffe42-f5cd-4ce3-a6fb-6b5fa8744150", "91110000100007314T", "高新区", false);
            AddAccountsAndProjects(dbContext, accountList, "c923aaa9-c895-4e5c-9d06-fe8b9792cd4", "50004120181016041", "中国西部农产品冷链物流中心项目（二期）", "9a26a3d7-4e06-4788-a926-2a64bd6f308c", "中国建筑土木建设有限公司", "6d434aa5-b7ae-4c04-a40a-c9718fe471b0", "91110000100007314T", "高新区", false);
            AddAccountsAndProjects(dbContext, accountList, "b95f87ac-be6f-4baf-8835-448c263c6e8", "50004120181016029", "铁建▪渝都总承包项目（标段二）", "fe288804-5fd1-4ed4-a796-67504a84bf68", "中铁二十二局集团有限公司", "e5f5b687-5341-4295-a97b-162b328c2d98", "9111000071092227XH", "高新区", false);
            AddAccountsAndProjects(dbContext, accountList, "1aa24fc8-75ad-447a-90e6-d319d360a2d", "50004120181016030", "铁建▪渝都总承包项目（标段一）", "03ecdcf0-1068-4262-931e-a3b7e1d287fd", "中铁十二局集团建筑安装工程有限公司", "f5db6e76-7e8c-4c2c-9352-98cba9ba241d", "91140000111071637N", "高新区", false);
            AddAccountsAndProjects(dbContext, accountList, "1263d25e-dd75-430b-945d-f0bea3bb88a", "50004120181016044", "尹朝社周边道路工程设计施工总承包（EPC）", "26f1e32e-6232-4b71-a78e-6a675cad0d47", "中铁十一局集团第五工程有限公司", "ec96b3da-f06d-40d9-8337-14c3a92514bd", "91500000202805587B", "高新区", false);
            AddAccountsAndProjects(dbContext, accountList, "5862702b-83d8-40ee-aa5c-ef3bce45462", "50004120181016042", "中国铁建·玖城贰号", "ca01e89e-b5ca-403c-b8e0-2e3ac86d0c66", "中铁十一局集团第五工程有限公司", "3766d184-a7f2-4fcb-aa03-88cb0696acb6", "91500000202805587B", "高新区", false);
            AddAccountsAndProjects(dbContext, accountList, "27cb9c95-d275-45f0-84dd-0967a3c020d", "50004120181016045", "快速路一纵线（高新区拓展区段）工程（罗家院立交）、高腾大道（二期）道路工程及高新区拓展区森谷路工程（K1+740-K2+014.896）", "b33432ed-d065-4154-b3c7-3d5a03d8fd46", "中冶建工集团有限公司", "2d27e891-ce3a-4956-83d7-17e3b6d2a5b7", "91500000795854690R", "高新区", false);
            AddAccountsAndProjects(dbContext, accountList, "13f242fc-59f7-40fa-bcf3-401bf656f19", "50004120181016043", "四川石油管理局物资总公司重庆检测中心迁建工程（主体）", "a12a57b8-afe1-45d5-a3fa-fae424fad914", "重庆建工第三建设有限责任公司", "a750b12c-7d43-4040-84b8-99bfd216489e", "915000007339743120", "高新区", false);
            AddAccountsAndProjects(dbContext, accountList, "a82425b8-975d-423c-9349-7cbceb3de93", "50004120181016046", "杰品模具产业基地项目", "23e5746b-3d70-4e98-b313-564612c919dd", "重庆腾鲁建筑安装工程有限公司", "a4c1cec5-d24d-4a3c-a012-d132cad6dbc5", "91500105450448910H", "高新区", false);
            AddAccountsAndProjects(dbContext, accountList, "0ce6a680-44c4-4033-9701-6cfd30843e4", "50000320181018201", "金科·中央公园城二期31-46号楼、86号门岗、对应83号楼B区车库及边坡附属工程", "bc6fe21f-b876-4a6c-a4cf-d4b0991c8dc4", "中国建筑土木建设有限公司", "32e023d2-ecd6-4dcc-8353-468b18e0ecd4", "91110000100007314T", "涪陵区", false);
            AddAccountsAndProjects(dbContext, accountList, "c4dc4f31-368f-435e-be88-f7ae8d22ebf", "50000320181018051", "重庆涪陵万达广场", "040fc451-1ea3-4b07-a731-ceaaa02ac959", "中国建筑一局(集团)有限公司", "175f66c5-bae5-45e1-8d39-b4bc94b51bb0", "91110000101107173B", "涪陵区", false);
            AddAccountsAndProjects(dbContext, accountList, "35dc696d-40f2-4eef-a526-b2ac1e18aa6", "50000320181018020", "金科.集美郡项目1-25号楼及26号车库工程", "c59736d2-6207-4e53-a8fd-71afedb46843", "重庆建工第四建设有限责任公司", "7ea3b3a4-7496-451e-a5af-f8b703cdd961", "91500000202800890K", "涪陵区", false);
            AddAccountsAndProjects(dbContext, accountList, "bfaf2ff0-db13-4d29-a153-e2602af6e20", "50000320181018207", "贵博·瀚林学府（1-1号楼、1-2号楼、2号楼至7号楼、8-1号楼、8-2号楼、8-3号楼、1号车库至4号车库）", "092cbcdc-16a0-4871-b2ba-009db25a9c29", "重庆展宇建筑工程有限公司", "98d1b8bb-8d58-4dc8-8b37-b29a621aec97", "91500102561607291C", "涪陵区", false);
            AddAccountsAndProjects(dbContext, accountList, "4b5bf56a-fb90-43fc-8d14-d0c001fc51b", "50000320181018200", "涪陵新区中央商务区公寓楼工程（5#楼）和涪陵区新城区商务广场（生化池）打包项目", "a8e054f0-cb06-450a-acd2-b91f958d434b", "重庆中科建设(集团)有限公司", "608ca85b-d6a6-480a-8661-037fc1a602d4", "91500102709427140L", "涪陵区", false);
            AddAccountsAndProjects(dbContext, accountList, "bff9a2f4-331e-4008-bbb3-894d94fd247", "50003220181008012", "飞洋·阳光佳苑二标段（A3、A4、A5、B1、B2、B3、B4、S3、S4、S6、S7、幼儿园）", "2573a18f-7fc1-46d3-a390-5a01a5a01fb4", "重庆飞洋控股集团宝宇建筑工程有限公司", "2732b031-6c71-4f92-b82b-2ecf62d6916c", "91500236736587780W", "奉节县", false);
            AddAccountsAndProjects(dbContext, accountList, "cfe00a2b-31b4-40ca-8bd9-bf9a4ff604a", "50003220181008002", "飞洋·阳光佳苑（A1、A2、A6、A7、B5、B6、S1、S2、S5）", "c92741dd-0191-4027-a72a-2acbf0e9625c", "重庆飞洋控股集团宝宇建筑工程有限公司", "66d0fc65-2e4c-447a-9d57-46039d168a64", "91500236736587780W", "奉节县", false);
            AddAccountsAndProjects(dbContext, accountList, "3b10d16f-0dec-4edc-a560-a332eca95de", "50003220181008126", "滨湖上院A区车库及商业、A15、A16栋", "7b28f024-6e90-4a55-91a7-5eb232c69e27", "重庆市银峡建筑工程集团有限公司", "4fec64c5-c8eb-496e-8341-a92aa715d22a", "915001012079140610", "奉节县", false);
            AddAccountsAndProjects(dbContext, accountList, "2589c68d-ada8-43b9-b76b-8a34ea1744e", "50003220181008123", "滨湖上院B区B1、B2、B3、B4、B7、B8、B9、B10、B11、B12栋", "f1bccdeb-5050-4b28-817c-885370f51be7", "重庆市银峡建筑工程集团有限公司", "c637821f-a9e1-486f-9202-c1d46989a866", "915001012079140610", "奉节县", false);
            AddAccountsAndProjects(dbContext, accountList, "e4199830-15a8-4ac8-81e1-04b344eb902", "50002820181012005", "丰都县峡南溪安置房A、B、C地块和三合街道瓜草湾拆迁安置房工程", "656bfc0f-cd9e-4b23-b9d4-8825b7253759", "广西建工集团第五建筑工程有限责任公司", "9c971727-5b83-420a-9fbf-5c59a529617b", "91450200198614605J", "丰都县", false);
            AddAccountsAndProjects(dbContext, accountList, "4f6931f2-cbb8-41df-a4b7-9aa60105347", "50002820181012044", "龙城华府C区（三期）二组团一标段", "f948095c-2c91-4dc1-88c6-2af62b889661", "重庆北部双龙建设(集团)有限公司", "b0394370-9705-4e16-b896-9726e9fbeb4f", "91500112622100436A", "丰都县", false);
            AddAccountsAndProjects(dbContext, accountList, "7658d190-3e16-40d1-a282-909290c4777", "50002820181012034", "恒安·金色星城（A1-A9#楼及A区地下车库）", "4bd6d72d-404a-4079-81c7-a33c6947226c", "重庆市庆东建筑工程公司", "01e180c5-1843-4a51-8b2e-12f672097dc1", "91500230208753984C", "丰都县", false);
            AddAccountsAndProjects(dbContext, accountList, "1098c0ff-8626-4659-ad55-87c03e55fe4", "50002820181012026", "久桓城三期（F区）", "51d544e5-cc86-4bd2-b7ea-bf4e792d4ae0", "重庆市兴远建筑工程有限公司", "3d8e184e-e1a5-41c8-9138-2753c4105e68", "91500227203905942M", "丰都县", false);
            AddAccountsAndProjects(dbContext, accountList, "98ed2015-1633-46d8-8e14-978ddeb37fb", "50002820181012037", "贵博·东方明珠一期（2#、3#、4#、5#、6#楼及地下车库）", "a3bd1b54-61fd-41a8-8d8c-fe51261a5514", "重庆展宇建筑工程有限公司", "b2eb4054-06bc-464a-bed9-65843b1359c0", "91500102561607291C", "丰都县", false);
            AddAccountsAndProjects(dbContext, accountList, "8de07277-f734-4c58-a240-f93f0fcd80b", "50002820181012032", "帝景江山（4#、7#、8#、10#及车库）", "90f47659-61f1-41ad-9a7e-15a6bb1e7407", "重庆长坪建设集团有限公司", "0f7cde0f-1f5d-4568-9e3b-2761ad310388", "915001132034066685", "丰都县", false);
            AddAccountsAndProjects(dbContext, accountList, "97438c1e-7f4a-4820-b5a1-b9de978ba09", "50002820181012023", "丰都金科黄金海岸4期（B1~B4、B14~B19-2、B31~B32号楼及车库）", "4fb2dfa0-9c6e-4ab4-8bf1-a7982fae9fd0", "重庆中通建筑实业有限公司", "4023b878-3d2b-4756-bef3-27deea771074", "91500107203046316P", "丰都县", false);
            AddAccountsAndProjects(dbContext, accountList, "3f79845e-2a0e-4eb3-9bf0-5e02f73c29a", "50002920181016071", "丹香御府商住小区建设项目（二期一标段）", "233c3997-7cd0-437e-abd1-c635aaf9f9ad", "中建四局第三建筑工程有限公司", "16c94c67-e921-46cc-866c-d3fc16fd038c", "91520302214780129Y", "垫江县", false);
            AddAccountsAndProjects(dbContext, accountList, "6d351b28-84a0-4feb-a7f6-6fe033ebe98", "50002920181016030", "丹香御府商住小区建设项目总承包工程（二期二标段）", "a8e9dc75-8bd7-4832-afc8-bdaaa7944b53", "中建四局第五建筑工程有限公司", "d4c56238-4875-4b6d-aa68-f756cb4025a0", "91522701216250420B", "垫江县", false);
            AddAccountsAndProjects(dbContext, accountList, "ccd8199b-1c23-4cd3-9c67-97af46b0b45", "50002920181016008", "丹湖时代城（一期）1#-3#、8#-10#、16#、17#楼", "0aec4bd0-1f65-45da-bcf8-c08d6df8269e", "重庆市垫江县建筑有限公司", "76582414-561c-4afd-99a8-1badd42d18a7", "915002312086533341", "垫江县", false);
            AddAccountsAndProjects(dbContext, accountList, "c70abdfe-6fe5-4f53-bf97-c855d27748d", "50001920181009076", "大足金科中央公园城2地块二期一标段7#一13#楼、M2、Y1#楼及对应部分D1#车库", "4a4050bb-25ce-4df8-a75f-689758f5ea8d", "中国建筑土木建设有限公司", "71ca0ed8-bb6f-4026-93e3-eb6e65159bba", "91110000100007314T", "大足区", true);
            AddAccountsAndProjects(dbContext, accountList, "455e8790-7435-45c1-bf61-56ff062c375", "50001920181010153", "重庆大足柏林广场4.5.6.7号楼及三期地下车库", "e979a39f-b289-41cd-ab28-5acd9e9cc601", "重庆建工住宅建设有限公司", "182c3b56-0859-4cad-a8f6-b5507e025192", "915000002028009114", "大足区", true);
            AddAccountsAndProjects(dbContext, accountList, "e5715454-2ebc-4156-8d8e-ba48b367c37", "50000520181015058", "联发西城首府大渡口组团N23-3-1、N23-3-2地块（1#-20#楼、26#楼、39#楼、地下车库）", "9f2a117c-146d-4427-8eb2-1bceaa3d035f", "上海宝冶集团有限公司", "fe8ffbcc-f1fb-4433-abfe-eac82a4e7976", "91310000746502808A", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "edb8e083-f281-447a-98f1-fe0417e9cf1", "50000520181015063", "金地大渡口项目一期（中梁山组团L22地块)(4、5、6、10、11、12、13号楼、幼儿园、门卫房及2号地下车库）", "c20e86fc-9261-43cf-8f3f-18e5cf3a759c", "浙江欣捷建设有限公司", "8b0f8eff-259f-4e42-8fd7-88bb6e065ddd", "913302001448768377", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "7071e6a2-e733-4fc6-97a5-137b749b595", "50000520181016080", "佳兆业滨江新城（H15-7-1/04号地块A区）（6-17#楼及部分车库）项目 ", "d316bd83-f035-4239-8624-d523b0510e0f", "浙江新东方建设集团有限公司", "e6acd183-6e80-4c07-91d4-434fb119ecbb", "9133072777825022XY", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "a5d128d4-8cd1-4b05-a07d-6bbbd4f5382", "50000520181015041", "中洲重庆大渡口P2-14号地块项目一标段总承包工程", "c307f6e4-e6e2-4e57-a946-5bfb79311248", "中国建筑第二工程局有限公司", "08a8b613-322f-43c5-abc1-2372b19db97b", "91110000100024296D", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "0ea12037-4dfd-4128-8e9a-e377eb60734", "50000520181016090", "天安江畔珑园二期", "36b93aca-bd9b-48e9-9ff5-cdee9c8e73d0", "中国建筑一局(集团)有限公司", "1146aeb9-addd-4086-8d9b-5907a5899256", "91110000101107173B", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "d36e31a6-3f82-4e08-ab09-431d3cce024", "50000520181015016", "重庆大渡口万达广场", "b631a469-1bb6-460c-8995-58c655268085", "中国建筑一局(集团)有限公司", "7d0b391d-e73a-4fde-8619-1bde635c7d35", "91110000101107173B", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "0bf5161d-5a43-4086-ac6b-51afd0082fc", "50000520181015042", "中洲重庆大渡口P2-14号地块项目二标段总承包工程", "cb1649e8-b2b9-4934-8e49-0c2290913de4", "中建二局第一建筑工程有限公司", "04f5d741-9251-4a7a-bb47-687dace9d6df", "91110000104341301L", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "953b7787-235e-40e4-b406-f28289373d9", "50000520181015040", "金科博翠长江项目", "53b2aa1a-0fa9-4f47-9420-ca388f73542d", "中建三局第二建设工程有限责任公司", "ccf24cb2-8e46-41e9-aa7b-54c5275da6b5", "91420100177739097A", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "850ba9c8-c06c-4128-86cf-13f946b3024", "50000520181015048", "金科•集美阳光M37-1地块（一期、二期）", "5456ef0f-49a8-43ec-abc2-33d2a7e1c074", "中建三局集团有限公司", "39a20edc-55d8-42a6-be07-89350ef31609", "91420000757013137P", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "7d6adaa2-e9d2-4770-bdde-9805576e1c0", "50000520181016088", "恒大麓山湖I58-1地块", "a12ea35f-c8ad-4a24-bc95-912bd5a0cf0d", "中建五局第三建设有限公司", "803030b1-230a-4073-ad6b-f60503f43c59", "91430100183853582A", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "9019def4-24d6-4020-9714-aa761da0e74", "50000520181015049", "佳兆业滨江新城（H15-6/04号地块）（3、4、5、6#楼及部分车库）项目", "21ab8c80-4a29-4b89-93c1-6bc49c7cfd45", "中天建设集团有限公司", "e270d735-7247-4847-a5d2-d821cbed7585", "91330783147520019P", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "ba36fdbc-6e8c-466c-a826-bae526b7a44", "50000520181015050", "金地大渡口项目一期（中梁山组团L22地块)(1、2、3、7、8、9、14、15号楼、门卫房及1号地下车库）", "d98653d9-6e21-4344-aee4-6f661a266631", "中天建设集团有限公司", "5f35d3d4-9e47-4101-afac-0b09d7579baa", "91330783147520019P", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "63dadca8-4aaa-4a96-8626-ed0dd68dcaf", "50000520181015052", "荣盛城-观麟郡I11-2地块（7#-12#楼、D13地下车库）、I15-1地块（1#-6#楼、S3#楼、S5#楼、S6#楼、D7地下车库）", "08220a96-3907-4475-922e-60cb6cd82f8d", "中冶建工集团有限公司", "d99fdf9f-d978-4f0c-b40d-5c8534073a5d", "91500000795854690R", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "a38e41c7-90e3-4f15-8a8b-5e9de374cf8", "50000520181015064", "华润万象汇二期（1#、2#、3#、2-T1#、2-T2#及地下车库）（调整）工程", "b43ae541-6a24-46d1-ab64-60e87cd6cff0", "中亿丰建设集团股份有限公司", "c1af36fc-a0c0-4848-8217-4140ff20054b", "91320500137690962B", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "4b50a7ac-57ba-427d-a4ff-6faed76a024", "50000520181015031", "新郭伏路南段（新郭伏路K6+280-K8+874.875段）", "5d98d14a-132e-499f-aa22-7dbf32edbb99", "重庆城建控股（集团）有限责任公司", "61523820-2a72-4395-994d-7f803aa5967b", "91500000709441516W", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "1ccb7179-3991-48f7-b33c-6332a3b09c5", "50000520181015067", "融科滕王阁3#（H15-14-1/04）地块（暂定名）（一期）（1.2.3号楼）", "79defd9e-b28d-4f38-b587-d2087fbfd7f7", "重庆第六建设有限责任公司", "9cebe66f-a8fe-42ad-83a9-d8d3e58042e3", "915000002028326067", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "29b85062-d005-41b0-a4fa-d72c99e09e6", "50000520181015039", "朵力•迎宾大道F6-2地块二标段（B5#、B6#、B7#、B8#、B9#、B10#及部分地下室）、幼儿园（三标段）工程施工", "c2219b91-9b51-4486-91c1-87a1e91363df", "重庆钢铁集团建设工程有限公司", "9234b7e4-f2d7-46dc-aa13-843627499243", "915000002028768602", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "0d1aa2ee-a107-4f5d-ae69-e945085a75a", "50000520181015066", "朵力•迎宾大道F6-2地块四标段（B1#、B2#、B3#及部分地下室）工程施工（第二次）", "bf379a29-935f-4473-8589-ad671cbf7653", "重庆钢铁集团建设工程有限公司", "7d7ead8a-dd9b-4eee-a7fc-d1654403d9fc", "915000002028768602", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "ae9e2547-013d-4bad-a5cc-011a7caab1c", "50000520181015068", "融科滕王阁3#（H15-14-1/04）地块（暂定名）（一期）（5号楼）", "75dbffc7-bd71-4dfa-9316-f3d5ff928f2a", "重庆光宇建设开发(集团）有限公司", "38d12b07-9c91-4ac5-ab07-638176919a28", "915001152033974580", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "d8a37b53-ffe2-459d-8301-c9e459e4f92", "50000520181015051", "恒大麓山湖I57-1地块（暂定名）", "61dde85f-a219-4748-bf51-4babb57305c6", "重庆嘉逊建筑营造工程有限公司", "0bcbc678-ac70-4761-a9f6-0d001aa0c736", "91500000778457538G", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "f01a522e-4e7f-4d70-8ce0-0c1ccbece35", "50000520181014008", "金科星辰三期（一批次）", "3849c909-9466-440c-a258-715dbee8523a", "重庆建工住宅建设有限公司", "664e694d-25e8-40dc-bfeb-2ad1df21d3b1", "915000002028009114", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "b714be39-c7c3-4cdb-8ff0-7ba2a1d9377", "50000520181015053", "金科星辰三期（二批次） ", "066e3e07-ec7c-4e6d-9e29-a83e6e8f29fb", "重庆建工住宅建设有限公司 ", "df1ff27d-efba-4b55-a9d7-d29880315ca9", "915000002028009114", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "f07ebabf-b29c-440f-8e02-730de272b0a", "50000520181015060", "林语春风M01-4、M02-2地块（暂定名）", "ed9a5852-f38b-4d33-813d-dfdebbed4fea", "重庆鹏帆建设集团有限公司", "90bb9893-b163-4401-ab8d-bfcbfed3af76", "91500113203408583G", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "53f9eec2-f486-4487-b378-be30f7bc874", "50000520181015065", "金地大渡口项目一期（大渡口组团N32-1地块）", "db632a98-2fa6-4566-a95b-b3dbb12133b8", "重庆万泰建设（集团）有限公司", "f66e39a9-487c-47e9-887f-b9c855601b50", "91500112784219544A", "大渡口区", false);
            AddAccountsAndProjects(dbContext, accountList, "60908316-1fe9-49b3-a206-3e4459ae25d", "50002720181008015", "永缘·新宸壹號4号楼、5号楼、10号楼", "806d3c9b-cea1-4108-a126-1295c24e792d", "重庆倍广建筑工程有限公司", "aff5236d-fc5c-4b78-b242-d45673b5d8be", "91500109588032053B", "城口县", false);
            AddAccountsAndProjects(dbContext, accountList, "092737de-35bb-484d-b3a7-a4c280d3d4f", "50002720181008013", "美都香榭二期", "7908c26c-b301-40e3-a3b7-e49957ef891c", "重庆慧河建设有限公司", "d311e0db-b995-485e-a59f-316a5474169c", "91500107771776659A", "城口县", false);
            AddAccountsAndProjects(dbContext, accountList, "098b155e-7541-43ca-8897-9d4ae70cfc2", "50002720181008002", "佳合锦绣城一期工程", "b036f81c-57f1-44d1-8d12-fddd2af3033a", "重庆覃家岗建设（集团）有限公司", "4a337600-3005-4d8b-889f-3281a72e61b2", "91500106203099717F", "城口县", false);
            AddAccountsAndProjects(dbContext, accountList, "49e1260b-95ac-43e7-84ee-6abc09ec27d", "50002020181008035", "鸥鹏·凤凰城一期三组团19-22#楼、I标段车库工程", "ece004ff-449d-474a-9bc7-0e324544bec5", "泰兴一建建设集团有限公司", "eeb1eca0-27d9-4419-804d-b69ffc6fb286", "91321283745563684C", "璧山区", false);
            AddAccountsAndProjects(dbContext, accountList, "3c8bf242-25b7-4f5a-9a70-21c828923c2", "50002020181008018", "金科·博翠天悦（1#~29#楼、S1#~S3#楼、D1地下车库）", "c4c07a42-6e3c-4fd8-81cd-e7f5bbca895e", "中国建筑第二工程局有限公司", "d06c0c1c-22c7-4feb-b289-3ddf1b7092c7", "91110000100024296D", "璧山区", false);
            AddAccountsAndProjects(dbContext, accountList, "4903a12a-2632-4078-9176-5bc4b77bd88", "50002020181008037", "中瓯·璧河名都7#、12#楼", "22a7cc20-5975-410c-968a-4dc443601e90", "中国建筑第二工程局有限公司", "eb638e80-44d0-42b3-8bc4-5b58ad583f6a", "91110000100024296D", "璧山区", false);
            AddAccountsAndProjects(dbContext, accountList, "6067851c-a1e8-4158-bd28-046c143fec0", "50002020181008044", "中建·璧山印象（一期）（A区）（2#、3#、18#楼）", "0290a691-13f5-4239-864e-2535fc29d4b8", "中建五局第三建设有限公司", "835fd175-bb2c-4fb3-91ed-076cd2f4ad61", "91430100183853582A", "璧山区", false);
            AddAccountsAndProjects(dbContext, accountList, "d379a8e5-ad72-45de-a10f-2a0a83846fb", "50002020181008034", "交投·香漫溪岸（四期6#、7#及五期13#、14#、20#、21#楼、D1车库）", "cd6641cf-1427-46c6-9980-fedd3ca232d1", "中铁十一局集团第五工程有限公司", "f4e1c3ae-c2b5-42ba-87c6-7c4d21208435", "91500000202805587B", "璧山区", false);
            AddAccountsAndProjects(dbContext, accountList, "ce5ed5a3-eeed-46c2-9dd1-b24b0046420", "50002020181008066", "璧山区两山丽苑定向经济适用房项目", "c5d0df08-bc19-49f7-8b8e-fe42300c48a6", "重庆中科建设（集团）有限公司", "b861d525-63f4-4a4c-838b-39ed13ce70ac", "91500102709427140L", "璧山区", false);
            AddAccountsAndProjects(dbContext, accountList, "dde76ebe-8892-4abe-9b4c-a56b4c00c83", "50001020181016063", "华润·琨瑜府（32-45栋、垃圾站、1#地下室）", "eb5d48f0-f325-475e-a86d-91c44efe8c18", "四川省第四建筑工程公司", "77cb1a72-411a-48a5-a88a-a906b8442178", "91510600205100611N", "北碚区", false);
            AddAccountsAndProjects(dbContext, accountList, "343a48a6-e96a-4468-86a0-35b8d5dc2a2", "50001020181016047", "华润·琨瑜府（1、2栋，5-31栋，2#地下车库）", "a2327681-1cb8-4a68-b6c0-d993cd982329", "西南建工集团有限公司", "55a3e051-98f7-4d12-8577-414f8609c761", "915116007118166988", "北碚区", false);
            AddAccountsAndProjects(dbContext, accountList, "44b723e6-bd2b-47b3-893e-6bb2de761d3", "50001020181016026", "金科蔡家M分区项目M35-1/05、M44-1/04地块一标段土建及普通水电安装工程", "0dc130c8-e91c-4ab7-ad3a-60ee2975f42a", "中建八局第三建设有限公司", "814203bd-85d1-433d-a56c-f6399b6f9473", "91320100134891128H", "北碚区", false);
            AddAccountsAndProjects(dbContext, accountList, "d97cf1d7-2677-45ed-8684-871b633fd69", "50001020181016039", "北碚区歇马农转非安置房工程", "1516a602-bb27-4a1c-af16-b4520ccd03e5", "中冶建工集团有限公司", "7aaae78c-650e-4e58-b3ce-b44f4fa38f63", "91500000795854690R", "北碚区", true);
            AddAccountsAndProjects(dbContext, accountList, "4adfaf47-c966-4202-8c4b-fa26dd645f5", "50001020181016066", "金科蔡家M50/04地块项目华硕标段", "0469deff-d00d-43ff-bac8-831219998c2c", "重庆华硕建设有限公司", "c1ac4ff8-79fb-4cd6-8056-72a81db7b279", "915001152034048303", "北碚区", false);
            AddAccountsAndProjects(dbContext, accountList, "f85d165c-45e2-4bb2-b1ea-a1c8e051863", "50001020181016082", "中共重庆市北碚区委党校校舍项目", "821854a3-7dce-47f8-a700-54e44e6eb10b", "重庆建工第三建设有限责任公司", "886a5932-575d-4404-961f-779581c531f8", "915000007339743120", "北碚区", true);
            AddAccountsAndProjects(dbContext, accountList, "50171cf2-862d-4844-a9bc-08b93553abf", "50001020181016086", "嘉运大道一号住宅小区项目N01-06/03地块10-17、40-43#、113#楼、岗亭及D1-5、D5#地下车库", "ce0a199b-f8d6-4a58-9a83-0c62761af4c8", "重庆建工第三建设有限责任公司", "32b47f2c-a9fc-4fd5-90f5-0361d72631da", "915000007339743120", "北碚区", false);
            AddAccountsAndProjects(dbContext, accountList, "1641d450-63a6-4262-ae56-7b7ba089d2f", "50001020181016028", "嘉运大道一号住宅小区项目（N01-02/03地块1至24号楼、30号楼、36号楼、43至47号楼及48号（48-1）地下车库）", "c908145d-a6b8-4c1b-9c10-b63c4df8b186", "重庆建工第三建设有限责任公司", "5aa56f28-b27c-4ee3-8079-0fbaeba7239c", "915000007339743120", "北碚区", false);
            AddAccountsAndProjects(dbContext, accountList, "a4ed3bbd-a4ba-4f90-9c83-bf0a344fca8", "50001020181016045", "嘉运大道一号住宅小区项目（N01-06地块44至68#楼及D3#、、D4#地下车库）", "0a5e7bfe-a38a-4256-a63a-34b73d8c17d6", "重庆建工第三建设有限责任公司", "4e0c138a-40c5-4dd3-b9da-5af766faf937", "915000007339743120", "北碚区", false);
            AddAccountsAndProjects(dbContext, accountList, "2844c967-d371-4591-ad27-b6e0767bebb", "50001020181016034", "金科蔡家P分区M23-02/03地块三标段", "7140088a-28a6-4672-a075-f8543ad13704", "重庆先锋建筑工程有限公司", "daedbc10-287d-483e-adf1-dd85083a3d3b", "915001137094571042", "北碚区", false);
            AddAccountsAndProjects(dbContext, accountList, "ba996b05-f598-455d-a4d4-5246f8d3c37", "50001020181016052", "万科蔡家N分区项目", "48ed358a-ba61-475a-90fa-ac1c6a8e205e", "重庆中通建筑实业有限公司", "bf377d32-70da-4cd8-9d94-e70ac109b02a", "91500107203046316P", "北碚区", true);
            AddAccountsAndProjects(dbContext, accountList, "42e9a92c-a1d2-4b92-8c28-8afd99a1898", "50001020181016060", "万科蔡家项目（M04-01-1/04、M04-01-02/04地块）", "64ea7a5a-67c3-48ab-9328-0111ae78a15a", "重庆中通建筑实业有限公司", "45c98e20-b9c7-4386-8d3c-3eea117fcb93", "91500107203046316P", "北碚区", false);
            AddAccountsAndProjects(dbContext, accountList, "a78bd696-c0e7-493c-a9aa-852cf1fc9b6", "50001220181018024", "融信龙洲府（S1、S2、1-19号楼及C区、D区地下车库）", "3899d2fb-7621-439d-a3de-bec6aa858572", "福建省荔隆建设工程有限公司", "84c0e0b5-331c-4494-af41-53d47a186ba2", "91350300671935165J", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "02d10a6e-9992-4732-9f56-d9d1d09497b", "50001220181018025", "融信龙洲府（S3、S4、20-46号楼及A区、B区地下车库）", "4283aaa4-4a92-466d-aa49-a3f58a1e2e09", "福建省荔隆建设工程有限公司", "266572af-0650-485f-9d74-e199fd8d315d", "91350300671935165J", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "5daef0a6-7dcf-4839-8a6f-1c4e6878b51", "50001220181018006", "保利云禧项目Q21-5号地块（1#楼至8#楼、13#楼、14#楼、S1#楼及地下车库A轴线到AF轴线向上2米）", "8fdc44bb-767a-4c1c-8118-67adebbc439d", "福建众诚建设工程有限公司", "57f4d6b4-abed-4c6c-9534-e10cabb0fcad", "913507001570228818", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "3e9af117-bbb4-4d81-9844-06a80c1509c", "50001220181018028", "融汇半岛十五期（1#楼、2#楼、3#楼、商业及车库）", "16c7027a-f790-4622-86d9-80c4f5702dc4", "四川省第三建筑工程公司", "32dacda0-c506-4334-a308-58b0cb25d82d", "915100002018080067", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "55834731-39a8-4b99-91ab-d38d16b4456", "50001220181018015", "洺悦城一期（4#楼-6#楼、12#楼、15#楼-21#楼、D1-1#地下车库）", "4835fcf8-ba3f-4568-b88d-5c826132ad22", "中电建建筑集团有限公司", "ad5345c8-1b6b-4ce6-90a7-62adbe245a72", "911100001011159077", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "b5005a80-7a8b-4d52-8f70-d06dd92b434", "50001220181018010", "华远·海蓝城（G26-1/03地块）22#-37#和地下车库", "47c4ab4b-1b76-4ef2-aafc-ce7635421dda", "中国建筑第二工程局有限公司", "57343bad-34f4-4441-8441-ead536f5e5d1", "91110000100024296D", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "745b5e0f-ab8d-4a1a-b6fd-0b6ed9b4fcd", "50001220181018018", "联发·龙洲湾1号一期C组团（1-C-1#楼~1-C-5#楼、1-C-3-2#、1-C-4-2#、1#地下车库（二期））", "af501dd6-8999-406f-9fda-31bad7561ac1", "中建三局第一建设工程有限责任公司", "8b564f89-ca83-432e-8c67-f16d62249fe7", "914201007483157744", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "32047c53-da93-466c-9f9f-d1ec68d9d9e", "50001220181018023", "蓝光美渝森林（三期）【8#-12#楼、2#地下车库部分（2-4至1-34轴交1-A至1-P轴、3-5至1-34轴交1-A至1-P轴)】", "14613240-daa9-4e6b-891f-c380330a3daa", "中建四局第五建筑工程有限公司", "864b3c04-b0f3-4b89-9179-95f26df421c8", "91522701216250420B", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "c19aabc0-4e22-48f4-a439-5b96f2583de", "50001220181018004", "中建·瑜和城（一期）", "ee8151a4-3787-4899-a272-b8c07213ba0e", "中建五局第三建设有限公司", "a7d892d8-11a5-4a12-a50e-f9acd71724a0", "91430100183853582A", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "3e0d3394-f1cc-43d4-8479-c86e4c50f95", "50001220181018005", "中建·瑜和城（三期）施工", "ff32873a-2bc7-4fc3-88cd-c0eeb4e5e662", "中建五局第三建设有限公司", "72e0ca8c-0e7c-4e19-afbe-1c90820cd050", "91430100183853582A", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "f2b2b6f6-8376-4b14-8691-165843d18d5", "50001220181018014", "恒大新城V6-1地块（4#至10#楼、59#楼、D区车库）", "b4d75ace-d02b-4097-a8d7-a4d8d6bca78e", "中建五局第三建设有限公司", "5e366ab8-ac81-408e-adb5-4a712f87257c", "91430100183853582A", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "cca82ac9-6c44-47db-be41-d69b01ee206", "50001220181018011", "协信·星澜汇四期S9-2/03地块2-1#至2-15#楼及地下室、S15-3/03号地块5-1#至5-9#楼及地下室", "e97a8c22-3646-4285-8ffd-3d781af67fbf", "中建新疆建工（集团）有限公司", "00eefe39-c0f8-452c-9048-84c449426676", "9165000022859700XU", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "6c639dd7-135c-4236-b52f-78e20515ca3", "50001220181018008", "华熙LIVE·重庆鱼洞（巴南区体育中心综合整治）项目——住宅（1#楼-3#楼、7#楼、9#楼、地下车库及设备用房（1-1轴交1-17轴交1-b轴至1-L轴交2-1轴至7-2轴））", "27d70cc5-4e5b-40af-8de7-1c6ac0d3cf67", "中建一局集团建设发展有限公司", "c9afa4da-875a-4504-8aab-a567f4bc23eb", "91110000101715726A", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "a7b93127-a0a7-40c3-b769-5ac7713c5f4", "50001220181018001", "中交·锦悦（Q04-4/02地块一标段2号楼、3号楼、4号楼、34号楼、地下车库）", "d44001d8-ca01-419e-852c-34c1a3729daa", "中交第二航务工程局有限公司", "a970fbcc-16a2-47f6-90d6-36ced38bbfd9", "914201001776853910", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "778379e3-ce6b-453f-bd14-9c5aa2e0f27", "50001220181018003", "中交·锦悦（Q04-4/02地块二标段1号楼、5号楼、6号楼、7号楼、8号楼、11号楼、地下车库）", "c8ba6fe9-2e09-4bea-9e0f-44acac96e028", "中交一航局第二工程有限公司", "51ace119-294a-4b3d-9c21-a6264be8454a", "913702001635708411", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "5e6b8f7a-bf1e-4d68-b112-d83c7d9c520", "50001220181018002", "中交·锦悦（Q04-4/02地块三标段9号楼、10号楼、12-33号楼、地下车库）", "e8b7035d-455b-40ed-970c-20a5b96ea918", "中交一航局第四工程有限公司", "9b15d666-818f-462d-90c2-7fef8a6e53bc", "911201161036225156", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "b03be400-c56c-4f33-9075-32e70e3d896", "50001220181018017", "碧桂园·渝南首府（三期）21-1#、21-2#、22#至25#楼、地下车库）", "746241ab-5207-4efd-b0c9-c73bf098b549", "中天建设集团有限公司", "24f53320-d4c8-4cfc-8798-00610290723e", "91330783147520019P", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "27a4f965-8b25-4227-ad4f-d69c53f7e0c", "50001220181018020", "荣盛锦绣南山Q21-1地块（1#-3#楼、S1#-S3#商业及D15#地下室（1~25轴交A~E轴、25~33轴交E~G轴）", "26f81fde-f22d-4f7c-87db-acea93aea8bf", "中冶建工集团有限公司", "df3782f4-e831-4cdc-bf6c-a64bf87203f7", "91500000795854690R", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "9081dcac-cdbc-4700-ab01-df1d181133b", "50001220181018016", "界石组团N分区N12/02地块（1#楼、11#楼、18#-27#楼、2#车库（2-4轴~2-22轴部分））", "026628a9-5794-44ab-ae48-ebfdd00d151b", "重庆光鸿建筑工程有限公司", "8766c342-cfad-4790-862d-20e4381dd19c", "91500108060538419U", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "007692cf-5ab1-46a4-86bf-e14f5b24701", "50001220181018026", "融创·金裕三期（M20/2地块）四标段（20-G5#楼、20-G6#楼、20-Y7#楼、20-Y8#楼、20-Y11#楼至20-Y15#楼、岗亭3及002#地下车库（1~24轴线部分））", "b0e7c3f3-605b-43b9-8363-74e74f54402e", "重庆光宇建设开发(集团）有限公司", "8bc848d0-14b1-4c49-9468-27d6cb1c799d", "915001152033974580", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "755e1d04-1988-47a4-9371-18eaaa8a884", "50001220181018030", "金碧雅居·鹿角项目（N16-1/02、N16-3/02、N17/02、N18/02号地块）1#-14#楼、22#楼、21#楼（车库一期）", "d323b8e4-b8de-4659-ac43-4d4e9d6f7cfd", "重庆海博建设有限公司", "d9f83e72-ee82-4a4f-b674-f17a0df58dbb", "91500112203510350T", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "3b8c3c2c-62b5-4e89-9395-fdb8074d16c", "50001220181018009", "华熙LIVE·重庆鱼洞（巴南区体育中心综合整治）项目——住宅（4#楼-6#楼、8#楼、10#楼、11#楼、地上人防设施及其他、地下车库及设备用房（1-17轴交1-30轴交1-b轴至1-A轴交7-2轴至9-9轴）", "58e8bd00-0359-4319-8e48-20a7fcf7482f", "重庆华力建设有限公司", "f35f9092-a434-4901-a89b-7e1123e73836", "915001032028344687", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "f8ffc27e-7c39-4038-a3fb-bdfef5314c3", "50001220181018022", "蓝光水岸公园（2#、5#至8#楼、P2#地下车库）", "b0b61669-b683-487a-87b1-a7bef880fb3f", "重庆华力建设有限公司", "685cb884-cbb8-4088-8dd6-e38d07d1ac77", "915001032028344687", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "3d7c1bef-3410-498f-97f2-24fea52cfcf", "50001220181018027", "融创·金裕五期-1期（M21-1/03地块）（1#、2#、3#、4#、5#、6#、7#、8#楼（地下车库及设备用房）", "384b5d78-071f-4c26-a9bd-7f4d700225ea", "重庆华硕建设有限公司", "a627a1ba-1edb-44ba-bde7-23c00d5b934d", "915001152034048303", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "a4353192-2fc5-465f-8398-b6753190ee2", "50001220181018007", "华宇龙湾二期（M4-4地块）(1#、2#、3#、4#及5#车库）", "21f37dd6-d2fc-40bf-be24-1a766fa84c61", "重庆华姿建设集团有限公司", "bb1dd141-c6cb-4fc6-a009-801380b3eb2b", "91500106203050024F", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "0dc933d0-2446-4c55-820e-b92799caa25", "50001220181018019", "荣盛滨江华府二期（L2-1、L1-3地块）（L2-1地块：15#至20#、D2地下车库29-45交A-AB轴，32-45交AB-HB轴）", "463593ad-1dc6-4fdd-9e7c-25baaa09ebca", "重庆建工第八建设有限责任公司", "460cd8ca-66a0-4c20-ae54-9c8d8c571e23", "91500000203137250B", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "00a557d7-252f-4739-af6b-1872a86a38f", "50001220181018012", "协信汽车城(D11-1/04地块）（18#、地下车库）", "7c75fade-bf4d-43b4-a966-458e880040f9", "重庆建工第七建筑工程有限责任公司", "cb22a2df-e0ee-473c-a89a-8df2c53cce76", "915000002030377780", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "af199fe2-eab4-4118-8363-1e4e12eb8db", "50001220181018013", "启迪协信·敬澜山（一期）（1#楼、2#楼、7#~12#楼、15#~18#楼、29#楼、31#~38#楼、S1#~S3#楼、D1车库（1轴~11轴/V轴~A轴+11轴~26轴/N轴~V轴+26轴~32轴", "ff536b7d-97fb-4a87-9ccc-df2fbfea69cd", "重庆建工住宅建设有限公司", "7f24fda9-267b-453b-a18d-b7529f47e9da", "915000002028009114", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "ccd800a5-c50c-4af0-abde-1e51229f4ea", "50001220181018021", "蓝光水岸公园（1#、3#、4#、9#至16#楼、P1#、P3#地下车库）", "433f4872-edd6-4e6a-8102-f929b1486f96", "重庆拓达建设（集团）有限公司", "41da2077-1545-4ae5-963d-f1d536524c81", "91500108768876288X", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "a1392b47-c54f-46a9-bf15-ab58d4a3f9f", "50001220181018029", "金碧辉公司66#地块（G21-3/03地块）（4~8#楼、S2~S4#及1-A~3-C/1-1~1-25轴车库）", "2ca9ecde-72c9-4457-898e-f8cce1725a37", "重庆万泰建设（集团）有限公司", "d1f1874d-cccd-409a-a1ed-6cdcea641708", "91500112784219544A", "巴南区", false);
            AddAccountsAndProjects(dbContext, accountList, "faa0d655-07fb-4c0f-85df-d5606094e9c", "50004320181019010", "智能网联汽车试验基地一期工程-汽车综合性能试验道建设项目EPC总包", "7a950293-09c8-43e9-b2bf-a4c7d0c6f9f9", "重庆建工住宅建设有限公司 ", "022e5dd0-209f-4cd1-8a51-3a3d6b50569f", "915000002028009114", "双桥经开区", false);

            #endregion
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="dbContext"></param>
        public void SaveData(DbContext dbContext)
        {
            foreach (var item in accountList)
            {//质监站帐号
                dbContext.Set<Account>().AddOrUpdate(item);
            }

            foreach (var item in companyAccountList)
            {//企业帐号（公司，施工单位）
                dbContext.Set<Account>().AddOrUpdate(item);
            }

            foreach (var item in projectAccountList)
            {//项目帐号
                dbContext.Set<Account>().AddOrUpdate(item);
            }

            foreach (var item in projectList)
            {//项目，工程
                dbContext.Set<DP_Project>().AddOrUpdate(item);
            }
        }

        #region Private method

        /// <summary>
        /// 添加审核帐号的方法
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="accountList"></param>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="number"></param>
        /// <param name="type">账号类型</param>
        private void AddAccounts(DbContext dbContext, List<Account> accountList, string id, string name, string number, AccountType type)
        {
            var entity = new Account
            {
                Id = Guid.Parse(id),
                Name = name + "质监站",
                Number = number,
                Type = type,
                Status = 1,
                Password = number.ToMD5(),
                SSOID = Guid.Empty,
                CreatedTime = DateTime.Parse("2018-10-28")
            };

            accountList.Add(entity);
        }

        /// <summary>
        /// 添加项目的方法
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="accountList">账号列表</param>
        /// <param name="projectId">项目id</param>
        /// <param name="accountId">账号id</param>
        /// <param name="name">项目名称</param>
        /// <param name="code">项目</param>
        /// <param name="builder">施工单位</param>
        /// <param name="area"></param>
        /// <param name="isBim"></param>
        private void AddProjects(DbContext dbContext, List<Account> accountList, string projectId, string accountId, string name, string code, string builder, string area, bool isBim)
        {
            //根据施工单位名称获取公司账号信息
            var builderAccount = accountList.FirstOrDefault(x => x.Name == builder.Trim());

            var areaAccount = accountList.FirstOrDefault(x => x.Name == area.Trim());
            
            //添加项目
            var project = InitProject(projectId, Guid.Empty, name, code, string.Empty, area, areaAccount, isBim, builderAccount);

            var projectAccount = new Account
            {
                Id = Guid.Parse(accountId),
                Name = project.Name,
                Number = project.Code,
                Password = project.Code.ToMD5(),
                Type = AccountType.项目帐号,
                Status = 1,
                CreatedTime = DateTime.Now
            };

            dbContext.Set<DP_Project>().AddOrUpdate(project);
            dbContext.Set<Account>().AddOrUpdate(projectAccount);
        }

        public DP_Project InitProject(string projectId, Guid parentId, string name, string code, string childCode, string area, Account areaAccount, bool isBim, Account builderAccount)
        {
            return new DP_Project
            {
                Id = Guid.Parse(projectId),
                ParentId = parentId,
                Name = name.Trim().Replace("(集团)", "（集团）"),
                Code = code.Trim(),
                ChildCode =  childCode.Trim(),
                Area = area,
                AreaId = areaAccount.Id,
                AuditId = areaAccount.Id,
                AuditName = areaAccount.Name,
                AuditTime = DateTime.Parse("2018-10-28"),
                IsBIM = isBim,
                Status = AuditStatus.未报审,
                ChangeType = ChangeType.正常,

                //ConstructionUnit = x.ConstructionUnit,
                ConstructionUnitId = Guid.Empty,
                BuilderUnitNames = builderAccount.Name,
                BuilderUnitIds = builderAccount.Id.ToString(),
                //SupervisionUnit = x.SupervisionUnit,
                SupervisionUnitId = Guid.Empty,
                //TestingUnit = x.TestingUnit,
                TestingUnitId = Guid.Empty,
                //PremixedSupplier = x.PremixedSupplier,
                PremixedSupplierId = Guid.Empty,
                //DesignUnit = string.Empty,
                DesignUnitId = Guid.NewGuid(),
                //SurveyUnit = string.Empty,
                SurveyUnitId = Guid.NewGuid(),

                //Address = string.Empty,
                Location = "106.556905,29.570056",//重庆市政府坐标
                //Image = x.Image,
                CreatedTime = DateTime.Now
            };
        }

        public class Company
        {
            public string CompanyName;
            public string CompanyCode;
        }

        List<Company> builderList = new List<Company>();
        List<Account> companyAccountList = new List<Account>();
        List<DP_Project> projectList = new List<DP_Project>();
        List<Account> projectAccountList = new List<Account>();

        /// <summary>
        /// 获取监管账户ID的方法
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GetAccountIds(string name)
        {
            string AccountIds = "";
            var list = accountList.Where(x => x.Name.Equals(name.Trim()));
            //if (!list.Any()) //return "";
            AccountIds = string.Join(",", list.Select(x => x.Id).ToArray());
            return AccountIds;
        }

        /// <summary>
        /// 获取企业账户ID的方法
        /// </summary>
        /// <param name="companyCode">企业信用代码</param>
        /// <returns></returns>
        public string GetCompanyAccountIds(string companyCode)
        {
            var account = companyAccountList.Where(x => x.Number.Equals(companyCode.Trim())).FirstOrDefault();
            if (companyCode == "915000002028257485")
            {
                var asddsf = 1;
                var sdss = asddsf;
            }
            //if (account == null) return "";
            //获取到第二级ID

            var accountIds = account.AccountIds.Split(',').ToList();
            if (accountIds == null) accountIds = new List<string>();
            if (!accountIds.Contains(account.Id.ToString()))
            {
                accountIds.Add(account.Id.ToString());
            }
            return string.Join(",", accountIds);
        }

        /// <summary>
        /// 添加企业账号和项目账号项目信息的方法
        /// </summary>
        /// <param name="dbContext">数据库操作</param>
        /// <param name="accountList">账号列表</param>
        /// <param name="projectId">项目id</param>
        /// <param name="projectCode">工程项目编码</param>
        /// <param name="accountId">账号id</param>
        /// <param name="projectName">项目名称</param>
        /// <param name="builder">施工单位</param>
        /// <param name="projectAccountId">企业账号id</param>
        /// <param name="companyCode">企业信用代码</param>
        /// <param name="area">所属区域</param>
        /// <param name="isBim">是否是BIM项目</param>
        private void AddAccountsAndProjects(DbContext dbContext, List<Account> accountList, string projectId, string projectCode, string projectName, string accountId, string builder, string projectAccountId, string companyCode, string area, bool isBim)
        {
            try
            {
                var areaName = area.Trim() + "质监站";
                //施工单位信息
                Company company = new Company
                {
                    CompanyName = builder.Trim().Replace("(集团)", "（集团）"),
                    CompanyCode = companyCode.Trim()
                };
                if (!builderList.Contains(company)) builderList.Add(company);

                var companyAccountId = Guid.Parse(accountId);
                //添加企业账号

                var companyNameList = company.CompanyName.Split(',');
                var companyCodeList = company.CompanyCode.Split(',');

                //foreach (var code in companyCodeList)
                for (int i = 0; i < companyCodeList.Count(); i++)
                {
                    var name = companyNameList[i];
                    var code = companyCodeList[i];
                    var companyAccount = companyAccountList.FirstOrDefault(x => x.Number == code.Trim());
                    if (companyAccount != null)
                    {
                        companyAccount.AccountIds = GetAccountIds(areaName);//关联企业与项目 账号
                    }
                    else
                    {
                        companyAccount = new Account
                        {
                            Id = companyAccountId,
                            Name = name,
                            Number = code.Trim(),
                            Type = AccountType.企业帐号,
                            Status = 1,
                            AccountIds = GetAccountIds(areaName),//关联账号
                            Password = code.ToMD5(),
                            CreatedTime = DateTime.Parse("2018-10-28")
                        };
                        companyAccountList.Add(companyAccount);//帐号关联
                    }
                }

                for (int i = 0; i < companyCodeList.Count(); i++)
                {
                    var name = companyNameList[i];
                    var code = companyCodeList[i];

                    //根据施工单位名称获取公司账号信息
                    var builderAccount = companyAccountList.FirstOrDefault(x => x.Number == code);
                    var areaAccount = accountList.FirstOrDefault(x => x.Name == areaName);

                    var project = projectList.FirstOrDefault(x => x.Code == projectCode);
                    var projectChild = new DP_Project();
                    if (project.IsNull())
                    {//项目不存在，添加项目信息
                        project = InitProject(projectId + "1", Guid.Empty, projectName, projectCode, string.Empty, areaName, areaAccount, isBim, builderAccount);
                        projectChild = InitProject(projectId + "2", project.Id, projectName, projectCode, "000", areaName, areaAccount, isBim, builderAccount);
                        projectList.Add(project);//项目
                        projectList.Add(projectChild);//项目下的工程，通过项目Id关联 到 ParentId
                    }
                    else
                    {
                        //更新项目
                        var builderUnitIds = project.BuilderUnitIds.Split(',').ToList();
                        builderUnitIds.Add(companyAccountId.ToString());
                        project.BuilderUnitIds = string.Join(",", builderUnitIds);

                        var builderUnitNames = project.BuilderUnitNames.Split(',').ToList();
                        builderUnitNames.Add(name);
                        project.BuilderUnitNames = string.Join(",", builderUnitNames);

                        //更新工程
                        projectChild = projectList.FirstOrDefault(x => x.Code.Contains(projectCode) && x.ParentId == project.Id);
                        projectChild.BuilderUnitIds = string.Join(",", builderUnitIds);
                        projectChild.BuilderUnitNames = string.Join(",", builderUnitNames);
                    }

                    var projectAccount = new Account();
                    if (!projectAccountList.Any(x => x.Number == project.Code))
                    {
                        projectAccount = new Account
                        {
                            Id = Guid.Parse(projectAccountId),
                            Name = project.Name,
                            Number = project.Code,
                            Password = project.Code.ToMD5(),
                            Type = AccountType.项目帐号,
                            Status = 1,
                            AccountIds = GetCompanyAccountIds(code),
                            CreatedTime = DateTime.Parse("2018-10-28")
                        };
                        projectAccountList.Add(projectAccount);
                    }
                    else
                    {
                        projectAccount.AccountIds = GetCompanyAccountIds(code);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
