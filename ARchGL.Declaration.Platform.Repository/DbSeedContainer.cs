﻿using ARchGL.Declaration.Platform.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using TDF.Core.Configuration;
using TDF.Data.EntityFramework;

namespace ARchGL.Declaration.Platform.Repository
{
    public partial class DbSeedContainer : ISeedContainer
    {
        public DbSeedContainer()
        {

        }

        List<Account> accountList = new List<Account>();

        public void Seed(DbContext dbContext)
        {
            //todo 初始化平台数据

            #region 管理员

            dbContext.Set<Admin>().AddOrUpdate(x => x.Account, new Admin()
            {
                Account = "admin",
                Id = Guid.Parse("f760df2b-b183-4d78-bd1c-fa62956a1037"),
                Password = "123456",
                CreatedTime = DateTime.Parse("2017-05-30"),
                ModifierName = string.Empty,
                CreatorName = string.Empty
            });

            #endregion

            #region 审核员

            var accountId = Guid.Parse("3ae37820-da81-11e8-a2c2-5254003c2d54");
            var accountName = "系统管理员";
            dbContext.Set<Account>().AddOrUpdate(new Account()
            {
                Id = accountId,
                Name = accountName,
                Number = "admin",
                Password = "123456".ToMD5(),
                Type = AccountType.管理员,
                Status = 1,
                AccountIds = string.Empty,
                Roles = string.Empty,
                SSOID = Guid.Empty,
                CreatedTime = DateTime.Parse("2018-10-28"),
            });

            #endregion

            AddAccount(dbContext);//添加审核帐号

            //AddProjectAccount(dbContext);//添加企业账号

            //AddProject(dbContext);//添加项目

            AddAccountsAndProject(dbContext);//添加 项目信息、项目账号

            SaveData(dbContext);//保存数据

            #region 权限角色信息

            #region 角色信息

            var role = new Sys_Role
            {
                Status = 1,
                Modules = string.Empty,
                CreatorId = accountId,
                CreatorName = accountName,
                CreatedTime = DateTime.Parse("2018-10-28"),
                ModifierName = string.Empty,
            };

            role.Id = Guid.Parse("818998ca-da84-11e8-a2c2-5254003c2d54");
            role.Name = "审核帐号";
            dbContext.Set<Sys_Role>().AddOrUpdate(role);

            role.Id = Guid.Parse("8d19a32f-db1b-11e8-a2c2-5254003c2d54");
            role.Name = "企业帐号";
            dbContext.Set<Sys_Role>().AddOrUpdate(role);

            role.Id = Guid.Parse("949755cb-db1b-11e8-a2c2-5254003c2d54");
            role.Name = "项目帐号";
            dbContext.Set<Sys_Role>().AddOrUpdate(role);

            #endregion

            #region 模块（包含菜单）信息

            var modules = new Sys_Modules()
            {
                Status = 1,
                Actions = string.Empty,
                CreatorId = accountId,
                CreatorName = accountName,
                CreatedTime = DateTime.Parse("2018-10-28"),
                ModifierName = string.Empty,
            };

            modules.Id = Guid.Parse("8a90ebe2-da84-11e8-a2c2-5254003c2d54");
            modules.Name = "帐号管理";
            modules.Code = "account-manage";
            modules.Url = "https://www.baidu.com";
            dbContext.Set<Sys_Modules>().AddOrUpdate(modules);

            modules.Id = Guid.Parse("20f8407f-db1d-11e8-a2c2-5254003c2d54");
            modules.Name = "项目管理";
            modules.Code = "project-manage";
            modules.Url = "https://www.baidu.com";
            dbContext.Set<Sys_Modules>().AddOrUpdate(modules);

            modules.Id = Guid.Parse("6ddebde5-db1d-11e8-a2c2-5254003c2d54");
            modules.Name = "审核管理";
            modules.Code = "audit-manage";
            modules.Url = "https://www.baidu.com";
            dbContext.Set<Sys_Modules>().AddOrUpdate(modules);

            modules.Id = Guid.Parse("287ec3dd-db1d-11e8-a2c2-5254003c2d54");
            modules.Name = "报审记录";
            modules.Code = "declaration-manage";
            modules.Url = "https://www.baidu.com";
            dbContext.Set<Sys_Modules>().AddOrUpdate(modules);

            modules.Id = Guid.Parse("992bc717-db1d-11e8-a2c2-5254003c2d54");
            modules.Name = "项目信息";
            modules.Code = "project-info";
            modules.Url = "https://www.baidu.com";
            dbContext.Set<Sys_Modules>().AddOrUpdate(modules);

            modules.Id = Guid.Parse("a05b2278-db1d-11e8-a2c2-5254003c2d54");
            modules.Name = "上报资料";
            modules.Code = "declaration-upload";
            modules.Url = "https://www.baidu.com";
            dbContext.Set<Sys_Modules>().AddOrUpdate(modules);

            #endregion

            #region 操作项，不一定需要

            var action = new Sys_Action()
            {
                Status = 1,
                CreatorId = accountId,
                CreatorName = accountName,
                CreatedTime = DateTime.Parse("2018-10-28"),
                ModifierName = string.Empty,
            };

            action.Id = Guid.Parse("3A8B426F-59A6-4788-8D26-4B4C58B29FB8");
            action.Name = "添加项目";
            action.Code = "add-project";
            dbContext.Set<Sys_Action>().AddOrUpdate(action);

            action.Id = Guid.Parse("E3AFCEAC-91C6-474A-BC3E-1B0975E7C1A4");
            action.Name = "添加文件";
            action.Code = "add-file";
            dbContext.Set<Sys_Action>().AddOrUpdate(action);

            action.Id = Guid.Parse("01F95F3C-6721-4D6B-A359-FB951F7AB7D5");
            action.Name = "查看模型";
            action.Code = "view-model";
            dbContext.Set<Sys_Action>().AddOrUpdate(action);

            action.Id = Guid.Parse("5F1F7A9A-1E64-418A-B10A-5BF1EFC05B86");
            action.Name = "查看项目";
            action.Code = "view-project";
            dbContext.Set<Sys_Action>().AddOrUpdate(action);

            action.Id = Guid.Parse("99025660-0A91-46AC-A77E-EBF6D8F275E5");
            action.Name = "提交审查";
            action.Code = "upload-declaration";
            dbContext.Set<Sys_Action>().AddOrUpdate(action);

            action.Id = Guid.Parse("CD3FC139-BA33-406F-86FE-E033239B0C33");
            action.Name = "下载审查";
            action.Code = "download-declaration";
            dbContext.Set<Sys_Action>().AddOrUpdate(action);

            action.Id = Guid.Parse("A271E6B0-97D2-4A05-B008-1155DB6C9921");
            action.Name = "流转下一";
            action.Code = "download-declaration";
            dbContext.Set<Sys_Action>().AddOrUpdate(action);

            #endregion

            #endregion

            #region 登录信息

            dbContext.Set<Sys_Session>().AddOrUpdate(new Sys_Session()
            {
                Id = accountId,
                AccountId = accountId,
                AccountName = accountName,
                Number = "admin",
                Token = "123456".ToMD5(),
                Type = AccountType.管理员,
                Roles = string.Empty,
                ExpiredTime = DateTime.Now.AddDays(1),
                CreatedTime = DateTime.Parse("2018-10-28"),
            });

            #endregion

            #region 项目相关

            #region 项目分类

            var projectCategoryList = new List<DP_Project_Category>();//分类集合，用于后面创建项目时，初始化上传文件

            var projectCategory = GetProjectCategory("4E0F7FFC-0DD5-4DBE-81E8-869F0C260FD6", 1, 1);
            projectCategory.ParentId = Guid.Empty;
            projectCategory.Name = "地基与基础";
            AddCategory(dbContext, projectCategoryList, projectCategory);

            var categoryIds = new Tuple<Guid, Guid, Guid, Guid>(
                Guid.Parse("E7EFF8D5-E40C-4944-B8B3-3DC049B40E4E"),
                Guid.Parse("7D7A1AA9-342E-41B8-92BC-74E64ED13D24"),
                Guid.Parse("C4CFFE12-2E1F-43EB-AFD0-800ED6A72476"),
                Guid.Parse("FEBBB165-87C9-4E88-9BD6-A85F20317861")
            );
            AddProjectCategorySecondChildren(dbContext, projectCategoryList, projectCategory.Id, categoryIds);//创建二级分类

            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item1, 1, "8B9CED01-3424-4327-963A-6C70546803DD", "验收表-12", "71011592-49BF-42F8-B00B-17F8E4129773", "地基与基础分部工程质量验收记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item1, 2, "6CB02CBD-DFA9-4171-8803-21C35274004F", "渝市政验收-12", "EB459E63-5F46-452C-B9F3-4095A1011D5E", "地基与基础分部工程质量验收记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item1, 3, "95C4AEDB-76EE-41F9-BAC8-38330FB67CBC", "验收表-13", "D38DE91A-893A-4E7C-B3E7-37CEE106D620", "_____子分部工程质量验收记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item1, 4, "370455AF-6857-4E23-9CB0-75CA240310F5", "渝市政验收-13", "12C0DC5F-FDE9-4115-87E0-F7A3E43BDC3C", "_____子分部工程质量验收记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item2, 5, "9D086B6B-CC40-44AD-93EA-0CB73B3EA55C", "验收表-12-1", "A8F84021-FC9A-499B-A936-CE8C8CC034E1", "地基与基础分部工程质量控制资料核查记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item3, 6, "AF362D53-72FE-40DD-B4DC-C9E310012372", "验收表-12-2", "42066644-AA14-4E73-8929-8A3592D9BCC8", "地基与基础分部工程安全和功能检验资料核查及主要功能抽查记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item4, 7, "3F2D4567-9854-47A8-8D87-EAC812FDD569", "验收表-12-3", "9D6F1391-7B8A-4E68-9A2F-3FE0DB4F3525", "地基与基础分部工程观感质量检查记录");//

            projectCategory = GetProjectCategory("9F5AE573-67CA-4CEB-9458-71A7CCAFAE1A", 1, 2);
            projectCategory.ParentId = Guid.Empty;
            projectCategory.Name = "主体结构";
            AddCategory(dbContext, projectCategoryList, projectCategory);

            categoryIds = new Tuple<Guid, Guid, Guid, Guid>(
                Guid.Parse("8AA57CE8-ABA1-415B-9468-1A6D8C3E8CC4"),
                Guid.Parse("9CD73513-DEAF-4660-AE34-E2EB26A8D51A"),
                Guid.Parse("C9BAA1C6-C8A0-4E7D-B87E-4E370BC80D3C"),
                Guid.Parse("C554BFFC-57A4-4D92-82DD-A31A32ADE3D7")
            );
            AddProjectCategorySecondChildren(dbContext, projectCategoryList, projectCategory.Id, categoryIds);//创建二级分类

            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item1, 1, "4802FAC8-ADDB-4541-BF75-4BCDBD30BF5F", "验收表-12", "5F114B93-0499-4CDA-8C59-5DB6F38B6803", "主体结构分部工程质量验收记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item1, 2, "F7CEC0DE-0204-4DB2-9926-F274C3E72423", "渝市政验收-12", "7EA27D1B-3301-4AC6-A79E-9E6EAFCEAC1E", "主体结构分部工程质量验收记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item1, 3, "604E4C71-A9A7-4712-B5E1-686C55C31AD2", "验收表-13", "C363F977-D037-4022-9DA4-5AB221054367", "_____子分部工程质量验收记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item1, 4, "ADFA79F7-6545-46ED-9C48-4063CA2A6C6C", "渝市政验收-13", "6F638246-FAED-4D01-BCBC-60FDBE7FDA77", "_____子分部工程质量验收记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item2, 5, "3EAC46D6-CEC0-4786-85C3-A65C239A6C4A", "验收表-12-4", "806EEABC-799F-42C9-B4C8-3848104638F5", "主体结构分部工程质量控制资料核查记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item3, 6, "2185CC64-C480-4C42-A708-0B6ECA55B82D", "验收表-12-5", "7C2BC361-194F-4144-9E9B-F4F657D55540", "主体结构分部工程安全和功能检验资料核查及主要功能抽查记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item4, 7, "D80A2D49-837A-4A35-AB6F-2248707A93BB", "验收表-12-6", "8F5FC626-F587-4C78-962B-B9C0B10A6723", "主体结构分部工程观感质量检查记录");//

            projectCategory = GetProjectCategory("A1688A0C-0C29-4567-9E05-42FBEE8BB825", 1, 3);
            projectCategory.ParentId = Guid.Empty;
            projectCategory.Name = "建筑节能";
            AddCategory(dbContext, projectCategoryList, projectCategory);

            categoryIds = new Tuple<Guid, Guid, Guid, Guid>(
                Guid.Parse("7BDC7684-ADA9-46FC-A303-A54926E64C2E"),
                Guid.Parse("DE6A8C0B-5BBC-4FC0-B69E-6CBB37CC0A00"),
                Guid.Parse("48BFF3A5-F935-4A70-847A-A4C2438B973C"),
                Guid.Empty
            );
            AddProjectCategorySecondChildren(dbContext, projectCategoryList, projectCategory.Id, categoryIds);//创建二级分类

            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item1, 1, "6A77B8AD-780D-417E-8C86-A2EFB81A8B73", "验收表-12", "28122B9A-1EA8-47F6-AFF0-C8FB5712B4F9", "建筑节能分部工程质量验收记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item1, 2, "C0EFC15B-E9A8-4ED9-BF95-F14371ADB411", "渝市政验收-12", "05D0B158-D104-47C4-AAB7-8772DD393B2F", "建筑节能分部工程质量验收记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item1, 3, "B3962329-DB4E-4309-9F19-44FF4C23F361", "验收表-13", "59C731B9-C6C8-4290-9BA2-D0F01897A64E", "_____子分部工程质量验收记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item1, 4, "C2531DBA-8473-4317-82AD-AF3581A8A831", "渝市政验收-13", "6CB3E168-D739-4D97-8390-A40730D73E72", "_____子分部工程质量验收记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item2, 5, "80D7A7B2-A623-4375-9FD2-AA3B0AA4DD24", "验收表-12-28", "032766CD-2307-41C5-B509-5A519648F920", "建筑节能分部工程质量控制资料核查记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item3, 6, "2A54C916-DD6E-4430-8C34-8DC895CB6646", "验收表-12-29", "2DA11175-2EEC-42E4-B71B-BA79C0C4D715", "建筑节能分部工程安全和功能检验资料核查及主要功能抽查记录");//

            projectCategory = GetProjectCategory("D71BB378-1A55-4DAC-95EC-93174C7FED47", 1, 4);
            projectCategory.ParentId = Guid.Empty;
            projectCategory.Name = "竣工验收";
            AddCategory(dbContext, projectCategoryList, projectCategory);

            categoryIds = new Tuple<Guid, Guid, Guid, Guid>(
                Guid.Parse("BA19DD18-955C-431D-AC9F-A39820C7B004"),
                Guid.Parse("0F306933-F78C-4347-8D8D-2022C55AB4AF"),
                Guid.Parse("853DB6CF-BFFB-426D-B544-806FCF1BAC0A"),
                Guid.Parse("427F1F78-CCAC-4626-B171-1B0DF64BC4BD")
            );
            AddProjectCategorySecondChildren(dbContext, projectCategoryList, projectCategory.Id, categoryIds);//创建二级分类

            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item1, 1, "60D3E9C9-488E-4735-8462-3A38401E7192", "验收表-8", "67F9B14B-80BF-4B19-85B9-97BC2DE0BB0B", "单位（子单位）工程质量竣工验收记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item1, 2, "38D193CE-DF37-4A24-BEF9-F7B3E242DB0F", "渝市政验收-8", "FB3C366D-43FA-4026-B971-A3096D0D1730", "单位（子单位）工程质量竣工验收记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item2, 3, "37D6CF8E-3FCF-4EBE-B33D-5E0CA741C4D6", "验收表-9", "478197AD-E5FE-43B3-A432-EB094C3C97A1", "单位（子单位）工程质量控制资料核查记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item2, 4, "A6C20895-7173-4019-B1F8-57C59903CBF6", "渝市政验收-9", "F03847B6-4ADF-459A-9C27-E3D03F2EEDE6", "单位（子单位）工程质量控制资料核查记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item3, 5, "46454CA4-9634-40E8-B138-6B1C72BB5478", "验收表-10", "3EB57C4C-B249-4650-8554-EC6BF69D3A67", "单位（子单位）工程安全和功能检验资料核查和主要功能抽查记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item3, 6, "CA142539-A86B-4FA5-A875-51262C0C98B0", "渝市政验收-10", "AEC2441C-90F0-447B-BDD5-A66EC51AA72D", "单位（子单位）工程安全和功能检验资料核查和主要功能抽查记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item4, 7, "CA38F240-8E4E-4C3B-BBD5-176A8AD3B5D9", "验收表-11", "98AFE4A4-8B47-4A47-BE1F-8D76D1502377", "单位（子单位）工程观感质量检查记录");//
            AddProjectCategoryOtherChildren(dbContext, projectCategoryList, categoryIds.Item4, 8, "66495FB3-CC8C-48BC-8D54-A560DC4F0671", "渝市政验收-11", "7AEBCB4E-AC1C-429E-9CA9-6670E119B009", "单位（子单位）工程观感质量检查记录");//

            #endregion


            /*
            var project = new DP_Project
            {
                CreatedTime = DateTime.Parse("2018-10-28"),
            };
            project.Id = Guid.Parse("638A5439-AC20-40F2-9506-8D9BF0F6A16D");
            project.Name = "江津第二人民医院";
            project.Code = "51010020181208120";
            project.AreaId = accountId;

            project.ConstructionUnit = "天元建设集团有限公司";
            project.ConstructionUnitId = Guid.Empty;
            project.BuilderUnitNames = "湖南北新天际建设工程有限公司";
            project.BuilderUnitIds = Guid.Empty;
            project.SupervisionUnit = "中国建筑土木建设有限公司";
            project.SupervisionUnitId = Guid.Empty;
            project.TestingUnit = "重庆腾鲁建筑安装工程有限公司";
            project.TestingUnitId = Guid.Empty;
            project.PremixedSupplier = "中冶建工集团有限公司";
            project.PremixedSupplierId = Guid.Empty;

            project.AuditId = accountId;
            project.AuditName = accountName;
            
            project.IsBIM = true;
            project.Project_File = new List<DP_Project_File>();

            var fourChildren = projectCategoryList.Where(x => x.Level == 4);
            foreach (var item in fourChildren)//循环第四级分类创建文件数据
            {
                var parent = GetProjectCategoryById(projectCategoryList, item.ParentId);
                var shortName = string.IsNullOrWhiteSpace(parent?.Name) ? string.Empty : parent?.Name;//获取三级分类

                var rootParent = GetProjectCategoryById(projectCategoryList, parent.ParentId);//通过三级分类，获取一级分类

                project.Project_File.Add(new DP_Project_File
                {
                    Id = Guid.NewGuid(),
                    ProjectId = project.Id,
                    ShortName = shortName,
                    Name = item.Name,
                    Size = 1024,
                    Extension = ".pdf",
                    FirstCategoryId = rootParent.ParentId,//一级分类Id
                    SecondCategoryId = parent.ParentId,//获取到第三级，取第三级的 ParentId 也就是第二级
                    ThreeCategoryId = parent.Id,
                    UploadTime = DateTime.Parse("2018-10-31"),
                    CreatedTime = DateTime.Parse("2018-10-28"),
                });
            }
            dbContext.Set<DP_Project>().AddOrUpdate(project);

            */

            #endregion

            if (Configs.Instance.EnvironmentType == EnvironmentType.Dev)
            {
                DevSeed(dbContext);
            }
            else if (Configs.Instance.EnvironmentType == EnvironmentType.Formal)
            {

            }
            else if (Configs.Instance.EnvironmentType == EnvironmentType.Test)
            {

            }
        }

        private void DevSeed(DbContext dbContext)
        {
        }

        /// <summary>
        /// 获取项目分类
        /// </summary>
        /// <param name="id"></param>
        /// <param name="level"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        public DP_Project_Category GetProjectCategory(string id, int level, int sort = 0)
        {
            return new DP_Project_Category
            {
                Id = Guid.Parse(id),
                Level = level,
                Sort = sort,
                CreatedTime = DateTime.Parse("2018-10-28"),
            };
        }
        /// <summary>
        /// 根据id查询项目分类
        /// </summary>
        /// <param name="projectCategoryList"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DP_Project_Category GetProjectCategoryById(List<DP_Project_Category> projectCategoryList, Guid id)
        {
            return projectCategoryList.FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// 添加分类
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="projectCategoryList"></param>
        /// <param name="projectCategory"></param>
        public void AddCategory(DbContext dbContext, List<DP_Project_Category> projectCategoryList, DP_Project_Category projectCategory)
        {
            dbContext.Set<DP_Project_Category>().AddOrUpdate(projectCategory);
            projectCategoryList.Add(projectCategory);
        }

        /// <summary>
        /// 添加项目二级分类子级
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="projectCategoryList"></param>
        /// <param name="parentId"></param>
        /// <param name="categoryIds"></param>
        public void AddProjectCategorySecondChildren(DbContext dbContext, List<DP_Project_Category> projectCategoryList, Guid parentId, Tuple<Guid, Guid, Guid, Guid> categoryIds)
        {
            var projectCategory = GetProjectCategory(categoryIds.Item1.ToString(), 2, 1);
            projectCategory.ParentId = parentId;
            projectCategory.Name = "质量验收记录";
            AddCategory(dbContext, projectCategoryList, projectCategory);

            projectCategory = GetProjectCategory(categoryIds.Item2.ToString(), 2, 2);
            projectCategory.ParentId = parentId;
            projectCategory.Name = "质量控制资料核查记录";
            AddCategory(dbContext, projectCategoryList, projectCategory);

            projectCategory = GetProjectCategory(categoryIds.Item3.ToString(), 2, 3);
            projectCategory.ParentId = parentId;
            projectCategory.Name = "安全和功能检验资料核查和主要功能抽查记录";
            AddCategory(dbContext, projectCategoryList, projectCategory);

            if (categoryIds.Item4 == Guid.Empty) return;//建筑节能二级分类只有三个，所以排除部分
            projectCategory = GetProjectCategory(categoryIds.Item4.ToString(), 2, 4);
            projectCategory.ParentId = parentId;
            projectCategory.Name = "观感质量检查记录";
            AddCategory(dbContext, projectCategoryList, projectCategory);
        }

        /// <summary>
        /// 添加项目三级、四级分类
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="projectCategoryList"></param>
        /// <param name="parentId"></param>
        /// <param name="sort"></param>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="childrenId"></param>
        /// <param name="childrenName"></param>
        public void AddProjectCategoryOtherChildren(DbContext dbContext, List<DP_Project_Category> projectCategoryList, Guid parentId
             , int sort, string id, string name, string childrenId, string childrenName)
        {
            var projectCategory = GetProjectCategory(id, 3, sort);

            projectCategory.ParentId = parentId;
            projectCategory.Name = name;
            AddCategory(dbContext, projectCategoryList, projectCategory);//三级

            projectCategory = GetProjectCategory(childrenId, 4);
            projectCategory.ParentId = Guid.Parse(id);
            projectCategory.Name = childrenName;
            AddCategory(dbContext, projectCategoryList, projectCategory);//四级
        }
    }

    public static class sadfasdf
    {
        /// <summary>
        /// 加密为 MD5
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToMD5(this string str)
        {
            return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(str + "a7273bce-23a1-450d-902d-7c30a4456635", "MD5");
        }
    }
}
