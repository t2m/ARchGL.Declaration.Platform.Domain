﻿using System.Reflection;
using Autofac;
using TDF.Core.Ioc;
using TDF.Core.Reflection;
using TDF.Data.EntityFramework.Repository;

namespace College.WebApi.Public
{
    /// <summary>
    /// 
    /// </summary>
    public class RepositoryDependencies : IDependencyRegistrar
    {
        public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {

        }

        public int Order => 1;
    }
}