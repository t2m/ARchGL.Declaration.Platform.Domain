﻿using System.Reflection;
using Autofac;
using ARchGL.Declaration.Platform.Service;
using TDF.Core.Ioc;
using TDF.Core.Reflection;

namespace ARchGL.Declaration.Platform.Web
{
    /// <summary>
    /// 
    /// </summary>
    public class ServiceDependencies : IDependencyRegistrar
    {
        public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            var assembly = Assembly.GetAssembly(typeof(IProductionService));
            builder.AutoRegisterByNamespace(
                new[] { "ARchGL.Declaration.Platform.Service" }, assembly,
                r => r.InstancePerLifetimeScope());
        }

        public int Order => 2;
    }
}