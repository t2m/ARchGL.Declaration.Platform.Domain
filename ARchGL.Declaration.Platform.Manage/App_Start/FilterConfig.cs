﻿using System.Web.Mvc;
using TDF.Web.Attributes.Mvc;

namespace ARchGL.Declaration.Platform.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandlerErrorAttribute());
        }
    }
}
