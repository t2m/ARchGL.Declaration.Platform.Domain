﻿var Login = function () {
    var form1 = $('#defaultForm');

    var handleLogin = function () {
        $('.login-form').validate({
            errorElement: 'span',
            errorClass: 'alert-danger',
            focusInvalid: false,
            rules: {
                LoginName: {
                    required: true
                },
                Password: {
                    required: true
                }
            },

            messages: {
                LoginName: {
                    required: "请输入帐号和密码"
                },
                Password: {
                    required: "请输入帐号和密码"
                }
            },

            invalidHandler: function (event, validator) { 
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function (element) { 
               
            },

            success: function (label) {
                $('.alert-danger', $('.login-form')).hide();
            },

            errorPlacement: function (error, element) {
            },

            submitHandler: function (form) {
                if ($('#verifyCode').length>0&&$('#verifyCode').val().trim() === "")
                    $('.alert-danger', $('.login-form')).text('请输入验证码').show();
                else {
                    $('.alert-danger', $('.login-form')).text('\u8BF7\u8F93\u5165\u5E10\u53F7\u548C\u5BC6\u7801').hide();
                    if ($("#btnSubmit").attr("disabled") !== undefined) {
                        return;
                    }
                    var l = Ladda.create($("#btnSubmit")[0]);
                    l.start();
                    $.ajax({
                        type: 'POST',
                        url: $('#defaultForm').attr("action"),
                        data: { UserName: $('#LoginName').val(), Password: $('#Password').val(), Code: $('#verifyCode').val() },
                        dataType: 'JSON',
                        success: function (result) {
                            if (result.Success) {
                                //location.replace(result.url);
                                location.replace($("#IndexUrl").val());
                            } else {
                                $('.alert-danger', $('.login-form')).text(result.Message).show();
                            }
                            l.stop();
                            
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            l.stop();
                            $('.alert-danger', $('.login-form')).text('登录失败')
                        },
                        async: true
                    });
                }
            }
        });

        $('.login-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit();
                }
                return false;
            }
        });
    }

    var handleForgetPassword = function () {
        $('.forget-form').validate({
            errorElement: 'span',
            errorClass: 'help-block', 
            focusInvalid: false, 
            ignore: "",
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },

            messages: {
                email: {
                    required: "\u4E0D\u80FD\u4E3A\u7A7A"
                }
            },

            invalidHandler: function (event, validator) { 

            },

            highlight: function (element) {
                $(element)
                    .closest('.form-group').addClass('has-error'); 
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function (form) {
                form.submit();
            }
        });

        $('.forget-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    $('.forget-form').submit();
                }
                return false;
            }
        });

        jQuery('#forget-password').click(function () {
            jQuery('.login-form').hide();
            jQuery('.forget-form').show();
        });

        jQuery('#back-btn').click(function () {
            jQuery('.login-form').show();
            jQuery('.forget-form').hide();
        });

    }

    var handleRegister = function () {

        function format(state) {
            if (!state.id) { return state.text; }
            var $state = $('<span><img src="/Content/Metronic4.5.6/global/img/flags/' + state.element.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>');

            return $state;
        }

        if (jQuery().select2 && $('#country_list').size() > 0) {
            $("#country_list").select2({
                placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Select a Country',
                templateResult: format,
                templateSelection: format,
                width: 'auto',
                escapeMarkup: function (m) {
                    return m;
                }
            });

            $('#country_list').change(function () {
                $('.register-form').validate().element($(this)); 
            });
        }

        $('.register-form').validate({
            errorElement: 'span',
            errorClass: 'help-block', 
            focusInvalid: false,
            ignore: "",
            rules: {
                fullname: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                address: {
                    required: true
                },
                city: {
                    required: true
                },
                country: {
                    required: true
                },

                LoginName: {
                    required: true
                },
                Password: {
                    required: true
                },
                rpassword: {
                    equalTo: "#register_password"
                },

                tnc: {
                    required: true
                }
            },

            messages: {
                tnc: {
                    required: "Please accept TNC first."
                }
            },

            invalidHandler: function (event, validator) { 

            },

            highlight: function (element) { 
                $(element)
                    .closest('.form-group').addClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                if (element.attr("name") == "tnc") { 
                    error.insertAfter($('#register_tnc_error'));
                } else if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function (form) {


            }
        });

        $('.register-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.register-form').validate().form()) {
                    $('.register-form').submit();
                }
                return false;
            }
        });

        jQuery('#register-btn').click(function () {
            jQuery('.login-form').hide();
            jQuery('.register-form').show();
        });

        jQuery('#register-back-btn').click(function () {
            jQuery('.login-form').show();
            jQuery('.register-form').hide();
        });
    }

    return {
        
        init: function() {

            handleLogin();
            handleForgetPassword();
            handleRegister();
            $('#LoginName').focus();
            $.backstretch([
                "../../Scripts/assets/pages/media/bg/1.jpg",
                "../../Scripts/assets/pages/media/bg/2.jpg",
                "../../Scripts/assets/pages/media/bg/3.jpg",
                "../../Scripts/assets/pages/media/bg/4.jpg"
            ], {
                fade: 1000,
                duration: 8000
            });


        }
    };

}();

jQuery(document).ready(function () {
    Login.init();
});