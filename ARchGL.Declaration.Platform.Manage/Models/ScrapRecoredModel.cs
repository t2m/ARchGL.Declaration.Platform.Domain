﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ARchGL.Declaration.Platform.Service.Dtos;
using System;

namespace ARchGL.Declaration.Platform.Web.Models
{
    public class ScrapRecoredModel : ScrapRecoredDto
    {
        /// <summary>
        /// 时间范围
        /// </summary>
        public DateTime[] RecordTimeRange { get; set; }
    }
}