﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Models
{
    /// <summary>
    /// 列
    /// </summary>
    public class ColumnModel
    {
        public ColumnModel() { }

        public ColumnModel(string fieldDisplay,string field)
        {
            Field = field;
            FieldDisplay = fieldDisplay;
        }

        public string FieldDisplay { get; set; }

        public string Field { get; set; }
    }
}