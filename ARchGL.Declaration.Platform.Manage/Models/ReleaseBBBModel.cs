﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Models
{
    public class ReleaseBBBModel
    {
       public List<Guid> BBBStackIds { get; set; }

        public Guid MachineId { get; set; }
    }
}