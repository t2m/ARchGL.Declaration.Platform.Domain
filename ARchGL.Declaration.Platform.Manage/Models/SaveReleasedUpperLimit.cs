﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Models
{
    /// <summary>
    /// 保存释放的最大堆叠数
    /// </summary>
    public class SaveReleasedUpperLimit
    {

        /// <summary>
        /// 最大堆叠数
        /// </summary>
        [Required, Range(0, int.MaxValue)]
        public int Value { get; set; }

        /// <summary>
        /// 模具组
        /// </summary>
        [Required]
        public string Mould { get; set; }

        /// <summary>
        /// 工作时间
        /// </summary>
        [Required]
        public Guid WorkTimeId { get; set; }
    }
}