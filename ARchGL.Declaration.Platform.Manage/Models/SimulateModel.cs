﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Models
{
    /// <summary>
    /// 模拟出入库
    /// </summary>
    public class SimulateModel
    {
        /// <summary>
        /// 区域Key
        /// </summary>
        [Required]
        public int AreaKey { get; set; }

        /// <summary>
        /// 机位Key
        /// </summary>
        [Required]
        public int MachineKey { get; set; }

        /// <summary>
        /// 推车Key
        /// </summary>
        [Required]
        public int CarKey { get; set; }

        /// <summary>
        /// 产品Key
        /// </summary>
        [Required]
        public int ProductKey { get; set; }

        /// <summary>
        /// 产品数量
        /// </summary>
        [Required]
        public int ProductCount { get; set; }

        /// <summary>
        /// 模拟类型：1入库 2出库
        /// </summary>
        [Required]
        public int Type { get; set; }
    }
}