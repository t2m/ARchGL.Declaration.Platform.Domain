﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ARchGL.Declaration.Platform.Service.Dtos.Machine;
using ARchGL.Declaration.Platform.Service.Dtos.Production;

namespace ARchGL.Declaration.Platform.Web.Models
{
    public class ProductionRuleConfigModel
    {
        public ProductionRuleConfigModel()
        {
            BBBConfig = new BootstrapTableModel();
            ProdctionRule = new BootstrapTableModel();
        }

        public BootstrapTableModel BBBConfig { get; set; }

        public BootstrapTableModel ProdctionRule { get; set; }

        public void LoadBBBConfig(List<MachineRuleDto> machineRules)
        {
            var columns = machineRules
                .Select(x => new BootstrapColumns(x.MachineId.ToString(), x.MachineNumber,"7%"))
                .ToList();
            BBBConfig.columns.AddRange(columns);
            BBBConfig.data = new List<Dictionary<string, object>>();
            var dic = machineRules.ToDictionary(x => x.MachineId.ToString(), x => (object)x.EnabledBBB);
            BBBConfig.data.Add(dic);
        }

        public void LoadProdctionRule(List<ProductionRuleMouldDto> prodctionRules)
        {
            ProdctionRule.columns = new List<BootstrapColumns>();
            ProdctionRule.columns.Add(new BootstrapColumns("MouldName","产品", "10%"));
            ProdctionRule.columns.AddRange(BBBConfig.columns);
            ProdctionRule.columns.Add(new BootstrapColumns("ReleasedUpperLimit", "BBB最大堆叠数", "7%"));

            foreach (var prodctionRule in prodctionRules.OrderBy(x=>x.ProductMould.MouldName).ToList())
            {
                var columnsDic = new Dictionary<string,object>();
                columnsDic.Add("MouldName", prodctionRule.ProductMould.MouldName);
                columnsDic.Add("PropertyInfos", string.Join(",",prodctionRule.ProductMould.PropertyInfos));
                columnsDic.Add("ReleasedUpperLimit", prodctionRule.ReleasedUpperLimit);
                foreach (var kv in prodctionRule.ProdctionRatioColumns)
                {
                    columnsDic.Add(kv.Key.ToString(),kv.Value);
                }
                ProdctionRule.data.Add(columnsDic);

            }
        }
    }

    public class BootstrapTableModel
    {
        public BootstrapTableModel()
        {
            columns = new List<BootstrapColumns>();
            data = new List<Dictionary<string, object>>();
        }

        public List<BootstrapColumns> columns { get; set; }
        public List<Dictionary<string,object>> data { get; set; }
    }

    public class BootstrapColumns
    {
        public BootstrapColumns()
        {
            
        }

        public BootstrapColumns(string field,string title,string width)
        {
            this.field = field;
            this.title = title;
            this.width = width;
        }

        public string field { get; set; }

        public string title { get; set; }

        public string width { get; set; }
    }
}