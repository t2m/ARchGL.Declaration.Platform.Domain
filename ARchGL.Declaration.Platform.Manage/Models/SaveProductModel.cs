﻿using System;
using System.Collections.Generic;
using ARchGL.Declaration.Platform.Service.Dtos;

namespace ARchGL.Declaration.Platform.Web.Models
{
    public class SaveProductModel
    {
        public Guid? Id { get; set; }
        
        public Guid ProductTypeId { get; set; }

        public int? ProductMinimumCount { get; set; }

        public List<ProductPropertyDictionaryDto> Propertys { get; set; }
    }
}