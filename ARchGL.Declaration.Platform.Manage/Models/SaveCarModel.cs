﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ARchGL.Declaration.Platform.Service.Dtos;

namespace ARchGL.Declaration.Platform.Web.Models
{

    public class SaveCarModel : CarDto
    {
        [Required(ErrorMessage = "车号不能为空")]
        [MaxLength(15, ErrorMessage = "车号最大15位")]
        [Remote("IsCarNumberExisted", "Car", AdditionalFields = "Id", ErrorMessage = "该车号在该区域已存在")]
        public override string Number { get; set; }
    }
}