﻿using System;
using System.Collections.Generic;
using ARchGL.Declaration.Platform.Service.Dtos.Product;

namespace ARchGL.Declaration.Platform.Web.Models.Api
{
    public class ProductApiModel : IApiResultItems
    {
        /// <summary>
        /// 产品ID
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 自动增长列
        /// </summary>
        public int Key { get; set; }

        /// <summary>
        /// 产品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 区域ID
        /// </summary>
        public Guid AreaId { get; set; }

        /// <summary>
        /// 产品类型ID
        /// </summary>
        public Guid ProductTypeId { get; set; }

        /// <summary>
        /// 预警数量
        /// </summary>
        public int? ProductMinimumCount { get; set; }

        /// <summary>
        /// 产品属性
        /// </summary>
        public List<PropertyInfoSimpleDto> PropertyInfo { get; set; }
    }
}