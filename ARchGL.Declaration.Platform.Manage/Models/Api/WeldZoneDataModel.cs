﻿using System.Collections.Generic;
using ARchGL.Declaration.Platform.Service.Dtos.Production;

namespace ARchGL.Declaration.Platform.Web.Models.Api
{
    /// <summary>
    /// 焊接区看板数据
    /// </summary>
    public class WeldZoneDataModel
    {
        /// <summary>
        /// 生产中的产品
        /// </summary>
        public List<ProductSimpleDto> ProductionPlans { get; set; }

        /// <summary>
        /// 生产完成的产品
        /// </summary>
        public List<ProductSimpleDto> CompleteProducts { get; set; }

        /// <summary>
        /// BBB堆叠数据
        /// </summary>
        public BBBInfoDto BBBInfo { get; set; }
    }
}