﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Models.Api
{
    public class CarInfoApiModel
    {
        /// <summary>
        /// int Id
        /// </summary>
        public int Key { get; set; }

        /// <summary>
        /// 标签的 Tag.Key 非 Tag.TagKey
        /// </summary>
        public int TagKey { get; set; }

        /// <summary>
        /// int Id
        /// </summary>
        public int AreaKey { get; set; }

        /// <summary>
        /// 流水号
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid ProductId { get; set; }

        /// <summary>
        /// 产品名称
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 机位Id
        /// </summary>
        public int MachineKey { get; set; }

        /// <summary>
        /// 机位号
        /// </summary>
        public string MachineNumber { get; set; }

        /// <summary>
        /// 推车号
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// 产品数
        /// </summary>
        public int ProductCount { get; set; }
    }
}