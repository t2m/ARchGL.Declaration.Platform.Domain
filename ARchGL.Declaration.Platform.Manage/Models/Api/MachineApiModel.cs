﻿using System;

namespace ARchGL.Declaration.Platform.Web.Models.Api
{
    public class MachineApiModel : IApiResultItems
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// int 主键
        /// </summary>
        public int Key { get; set; }
        /// <summary>
        /// 机位号,区域唯一，重复时提示：该区域已存在该几位号
        /// </summary>
        public string Number { get; set; }
        /// <summary>
        /// 标识（整个系统唯一，重复时提示“该机位标识已存在”）。用作机位条码生成。
        /// </summary>
        public string MID { get; set; }
        /// <summary>
        /// 区域Id
        /// </summary>
        public System.Guid AreaId { get; set; }
        /// <summary>
        /// 区域Key
        /// </summary>
        public int AreaKey { get; set; }
    }
}