﻿using System;
using System.Collections.Generic;

namespace ARchGL.Declaration.Platform.Web.Models.Api
{
    public class CarApiModel : IApiResultItems
    {
        /// <summary>
        /// 推车ID
        /// </summary>
        public Guid CarId { get; set; }
        /// <summary>
        /// 推车ID
        /// </summary>
        public int Key { get; set; }
        /// <summary>
        /// 区域Id
        /// </summary>
        public Guid? AreaId { get; set; }
        /// <summary>
        /// 区域Key
        /// </summary>
        public int AreaKey { get; set; }
        /// <summary>
        /// 车号
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// 车子最多可以存放商品的数量
        /// </summary>
        public int MaximumCapacity { get; set; }

        /// <summary>
        /// 推车 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 标签集合
        /// </summary>
        public List<TagApiModel> Tags { get; set; }
    }

    /// <summary>
    /// 标签
    /// </summary>
    public class TagApiModel
    {
        /// <summary>
        /// 标签ID
        /// </summary>
        public Guid TagId { get; set; }

        /// <summary>
        /// 自动增长列
        /// </summary>
        public int Key { get; set; }

        /// <summary>
        /// 标签Key
        /// </summary>
        public string TagKey { get; set; }
    }

}