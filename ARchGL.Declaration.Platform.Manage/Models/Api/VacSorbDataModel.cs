﻿using System.Collections.Generic;
using ARchGL.Declaration.Platform.Service.Dtos.Production;

namespace ARchGL.Declaration.Platform.Web.Models.Api
{
    /// <summary>
    ///真空吸附区看板数据
    /// </summary>
    public class VacSorbDataModel
    {
        /// <summary>
        /// 生产中的产品
        /// </summary>
        public List<ProductSimpleDto> ProductionPlans { get; set; }

        /// <summary>
        /// 生产完成的产品
        /// </summary>
        public List<ProductSimpleDto> CompleteProducts { get; set; }

        /// <summary>
        /// BBB堆叠数据
        /// </summary>
        public BBBInfoDto BBBInfo { get; set; }

        public List<ProductStatueDto> ProductStatues { get; set; }
    }
}