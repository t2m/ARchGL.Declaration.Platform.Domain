﻿using System;

namespace ARchGL.Declaration.Platform.Web.Models.Api
{
    public class AreaApiModel : IApiResultItems
    {
        /// <summary>
        /// 区域Id
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// 区域Key
        /// </summary>
        public int Key { get; set; }
        /// <summary>
        /// 区域名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 产品类型
        /// </summary>
        public Guid ProductTypeId { get; set; }
    }
}