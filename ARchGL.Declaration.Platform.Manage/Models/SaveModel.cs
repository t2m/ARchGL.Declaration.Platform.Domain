﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ARchGL.Declaration.Platform.Web.Models
{
    /// <summary>
    /// 保存的规则
    /// </summary>
    public class SaveRuleModel
    {
        /// <summary>
        /// 机位Id
        /// </summary>
        [Required]
        public Guid MachineId { get; set; }

        /// <summary>
        /// 生产比例
        /// </summary>
        [Required]
        public int Value { get; set; }

        /// <summary>
        /// 模具组
        /// </summary>
        [Required]
        public string Mould { get; set; }

        /// <summary>
        /// 工作时间
        /// </summary>
        [Required]
        public Guid WorkTimeId { get; set; }
    }
}