﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using ARchGL.Declaration.Platform.Domain.Entities;
using ARchGL.Declaration.Platform.Service.Dtos.Product;
using ARchGL.Declaration.Platform.Service.Dtos.Production;
using ARchGL.Declaration.Platform.Web.Models;
using ARchGL.Declaration.Platform.Web.Models.Api;
using TDF.Core.Models;
using TDF.Web.Attributes.WebApi;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    public partial class ProductionController
    {
        /// <summary>
        /// 获取BBB堆叠数据
        /// </summary>
        /// <param name="machineId"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResult GetBBBInfo(Guid machineId)
        {
            var result = ProductionServcie.GetBBBInfo(machineId);
            return Success(result);
        }

        /// <summary>
        /// 释放BBB堆叠
        /// </summary>
        /// <param name="releaseModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ModelValidation]
        public ApiResult ReleaseBBBStack(ReleaseBBBModel releaseModel)
        {
            ProductionServcie.ReleaseBBBStackToProductionPlan(releaseModel.MachineId,releaseModel.BBBStackIds);
            return Success();
        }

        /// <summary>
        /// 获得当前机位的生产列表
        /// </summary>
        /// <param name="machineId"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResult GetProductionPlan(Guid machineId)
        {
            var result = ProductionServcie.GetProductionPlan(machineId);
            return Success(new ApiItemModel<ProductSimpleDto>(result));
        }
    }
}