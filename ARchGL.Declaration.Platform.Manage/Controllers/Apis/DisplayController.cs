﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using ARchGL.Declaration.Platform.Service;
using ARchGL.Declaration.Platform.Service.Dtos.Production;
using ARchGL.Declaration.Platform.Web.Models;
using ARchGL.Declaration.Platform.Web.Models.Api;
using TDF.Core.Models;

namespace ARchGL.Declaration.Platform.Web.Controllers.Apis
{
    /// <summary>
    /// 看板数据接口
    /// </summary>
    public class DisplayController: ApiControllerBase
    {
        /// <summary>
        /// 生产接口
        /// </summary>
        public readonly IProductionService ProductionService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productionService"></param>
        public DisplayController(IProductionService productionService)
        {
            ProductionService = productionService;
        }

        /// <summary>
        /// 获取焊接区数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResult GetWeldZoneData(Guid machineId)
        {
            var productionPlans = ProductionService.GetProductionPlan(machineId);
            var completeProducts = ProductionService.GetMachineLastProducts(machineId,3);
            var bbbInfo = ProductionService.GetBBBInfo(machineId);
            return Success(new WeldZoneDataModel()
            {
                BBBInfo = bbbInfo,
                ProductionPlans = productionPlans,
                CompleteProducts = completeProducts
            });
        }

        /// <summary>
        /// 获取真空吸附区数据
        /// </summary>
        /// <param name="machineId"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResult GetVacSorbData(Guid machineId)
        {
            var areaId = Guid.Parse("85d0f6eb-9c57-4c9b-8450-49a0be78bacb");
            var productionPlans = ProductionService.GetProductionPlan(machineId).Take(4).ToList();
            var completeProducts = ProductionService.GetMachineLastProducts(machineId, 3);
            var bbbInfo = ProductionService.GetBBBInfo(machineId);
            var productStatue = ProductionService.GetProductStatues(areaId);
            foreach (List<BBBColumn> groups in bbbInfo.Sources)
            {
                var column = groups.FirstOrDefault();
                if (column == null)
                {
                    continue;
                }
                var name = column.ColumnName;
                var mould = column.Mould;
                if (groups.All(x => x.ReleasPropertyValue != "FL"))
                {
                    groups.Insert(0,new BBBColumn()
                    {
                        ReleasedUpperLimit = int.MaxValue,
                        Mould = mould,
                        ColumnName = name,
                        Items = new List<ProductSimpleDto>(),
                        ReleasPropertyValue = "FL"
                    });
                }
                if (groups.All(x => x.ReleasPropertyValue != "FR"))
                {
                    groups.Insert(1, new BBBColumn()
                    {
                        ReleasedUpperLimit = int.MaxValue,
                        Mould = mould,
                        ColumnName = name,
                        Items = new List<ProductSimpleDto>(),
                        ReleasPropertyValue = "FR"
                    });
                }
                if (groups.All(x => x.ReleasPropertyValue != "RL"))
                {
                    groups.Insert(2, new BBBColumn()
                    {
                        ReleasedUpperLimit = int.MaxValue,
                        Mould = mould,
                        ColumnName = name,
                        Items = new List<ProductSimpleDto>(),
                        ReleasPropertyValue = "RL"
                    });
                }
                if (groups.All(x => x.ReleasPropertyValue != "RR"))
                {
                    groups.Insert(3, new BBBColumn()
                    {
                        ReleasedUpperLimit = int.MaxValue,
                        Mould = mould,
                        ColumnName = name,
                        Items = new List<ProductSimpleDto>(),
                        ReleasPropertyValue = "RR"
                    });
                }
                groups.Sort((x,y)=> string.Compare(x.ReleasPropertyValue, y.ReleasPropertyValue, StringComparison.Ordinal));
            }
            return Success(new VacSorbDataModel()
            {
                BBBInfo = bbbInfo,
                ProductionPlans = productionPlans,
                CompleteProducts = completeProducts,
                ProductStatues = productStatue
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResult GetTime()
        {
            return Success(string.Empty, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        }
    }
}