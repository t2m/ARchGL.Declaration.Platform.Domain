﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ARchGL.Declaration.Platform.Domain.Entities;
using ARchGL.Declaration.Platform.Web.Models;
using TDF.Core.Models;
using TDF.Web.Models;

namespace ARchGL.Declaration.Platform.Web.Controllers
{
    /// <summary>
    /// 机位的生产计划管理
    /// </summary>
    public partial class MachineController
    {
        /// <summary>
        /// 机位的生产计划管理页面
        /// </summary>
        /// <param name="machineId"></param>
        /// <returns></returns>
        public ActionResult ProductionManage(Guid machineId)
        {
            var machine = MachineService.GetById(machineId);
            var properties = ProductionService.GetProductPropertys(AreaId);
            var bbbProperties = ProductionService.GetProductBbbPropertys(AreaId);
            var area = BasicBusinessService.GetById(AreaId);
            ViewBag.Columns = properties.Select(x => new ColumnModel(x.Key, x.KeyId.ToString())).ToList();
            ViewBag.BBBColumns = bbbProperties.Select(x => new ColumnModel(x.Key, x.KeyId.ToString())).ToList();
            ViewBag.IsOneToOne = area.OutputType == OutputType.OneToOne;
            return View(machine);
        }

        /// <summary>
        /// 获取生产计划列表
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="machineId"></param>
        /// <returns></returns>
        public ActionResult GetProductionRulePagedList(PagedCriteria criteria,Guid machineId)
        {
            var result = ProductionService.GetProductionPlanPagedListByMachineId(criteria,machineId);
            var rows = result.Rows.Select(x =>
            {
                var dic = x.PropertyInfos.ToDictionary(item => item.PropertyId.ToString(), item => (object)item.Value);
                dic.Add("MachineId",x.MachineId.ToString());
                dic.Add("Sort", x.Sort);
                dic.Add("ProductionPlanId",x.ProductionPlanId);
                dic.Add("CreatedTime", x.CreatedTime);
                return dic;
            }).ToList();
            var pagedList = new PagedList<Dictionary<string,object>>();
            pagedList.PageIndex = result.PageIndex;
            pagedList.PageSize = result.PageSize;
            pagedList.Rows = rows;
            pagedList.TotalCount = result.TotalCount;
            return pagedList.ToBootstrapTableModel();
        }

        /// <summary>
        /// 获得该机器能生产的所有产品列表
        /// </summary>
        /// <param name="machineId"></param>
        /// <returns></returns>
        public ActionResult GetCanProductionProducts(Guid machineId)
        {
            return Success(ProductionService.GetCanProductionProducts(machineId));
        }

        /// <summary>
        /// 获得该机器能生产的所有产品模块列表
        /// </summary>
        /// <param name="machineId"></param>
        /// <returns></returns>
        public ActionResult GetCanProductionMoulds(Guid machineId)
        {
            return Success(ProductionService.GetCanProductionMoulds(machineId));
        }

        /// <summary>
        /// 添加一个生产计划到机器
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="machineId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddProductionPlan(Guid productId,Guid machineId)
        {
            ProductionService.AddProductionPlan(productId, machineId);
            return Success();
        }

        /// <summary>
        /// 添加一个生产计划到机器
        /// </summary>
        /// <param name="productIds"></param>
        /// <param name="machineId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddProductionPlans(List<Guid> productIds, Guid machineId)
        {
            ProductionService.AddProductionPlan(productIds, machineId);
            return Success();
        }
        

        /// <summary>
        /// 删除生产计划
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteProductionPlan(Guid planId)
        {
            ProductionService.DeleteProductionPlan(planId);
            return Success();
        }

        /// <summary>
        /// 删除该机位上的所有生产计划
        /// </summary>
        /// <param name="machineId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ClearAllProduction(Guid machineId)
        {
            ProductionService.ClearAllProduction(machineId);
            return Success();
        }

        [HttpPost]
        public ActionResult ModifyMachineProduct(Guid planId,Guid productId)
        {
            ProductionService.ModifyMachineProduct(planId, productId);
            return Success();
        }

        [HttpPost]
        public ActionResult SwitchProductionPlanSort(Guid planIdSource,Guid planIdTargetId)
        {
            ProductionService.SwitchProductionPlanSort(planIdSource,planIdTargetId);
            return Success();
        }

        public ActionResult GetProductionBBBPagedList(PagedCriteria criteria, Guid machineId)
        {
            var result = ProductionService.ProductionBbbStackGroupDto(criteria, machineId);
            var rows = result.Select(x =>
            {
                var dic = x.PropertyInfos.ToDictionary(item => item.PropertyId.ToString(), item => (object)item.Value);
                dic.Add("BBBStackCount", x.BBBStackCount);
                dic.Add("GroupCount", x.GroupCount);
                dic.Add("StackIds",x.BBBStackIds);
                return dic;
            }).ToList();
            var pagedList = new PagedList<Dictionary<string, object>>();
            pagedList.PageIndex = 0;
            pagedList.PageSize = rows.Count;
            pagedList.Rows = rows;
            pagedList.TotalCount = rows.Count;
            return pagedList.ToBootstrapTableModel();
        }

        public ActionResult ReleaseBBB(List<Guid> stackIds,Guid machineId)
        {
            ProductionService.ReleaseBBBStackToProductionPlan(machineId, stackIds);
            return Success();
        }
    }
}