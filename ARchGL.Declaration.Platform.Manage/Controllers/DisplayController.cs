﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ARchGL.Declaration.Platform.Service;
using ARchGL.Declaration.Platform.Service.Dtos.Product;
using TDF.Core.Ioc;
using TDF.Core.Json;

namespace ARchGL.Declaration.Platform.Web.Controllers
{
    /// <summary>
    /// 显示器看板
    /// </summary>
    [AllowAnonymous]
    public class DisplayController : AdminControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        public readonly IMachineService MachineService;

        /// <summary>
        /// 库存管理
        /// </summary>
        protected readonly IProductWarehouseService ProductWarehouseService;

        /// <summary>
        /// 生产服务
        /// </summary>
        protected readonly IProductionService ProductionService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="machineService"></param>
        /// <param name="productionService"></param>
        /// <param name="productWarehouseService"></param>
        public DisplayController(IMachineService machineService
            , IProductionService productionService
            , IProductWarehouseService productWarehouseService)
        {
            MachineService = machineService;
            ProductionService = productionService;
            ProductWarehouseService = productWarehouseService;
        }

        /// <summary>
        /// 焊接区看板
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult WeldZone(string number)
        {
            var areaId = Guid.Parse("7067c0c8-6bff-46ac-acf3-b025988da3b9");
            var machine = MachineService.GetByAreaIdAndNumber(areaId, number);
            return View(machine);
        }

        /// <summary>
        /// 真空吸附区看板
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult VacSorb(string number)
        {
            var areaId = Guid.Parse("85d0f6eb-9c57-4c9b-8450-49a0be78bacb");
            var machine = MachineService.GetByAreaIdAndNumber(areaId, number);
            return View(machine);
        }

        /// <summary>
        /// 实时库存
        /// </summary>
        /// <returns></returns>
        public ActionResult ShowStorage()
        {
            return View();
        }

        #region Ajax Action

        /// <summary>
        /// 焊接区实时库存
        /// </summary>
        /// <returns></returns>
        public ActionResult GetProductList()
        {
            var criteria = new ProductCriteria
            {
                PageIndex = 0,
                PageSize = 999,
                ProductTypeId = Guid.Parse("03c873da-a2a9-4d18-84bf-9d7212363737"),
            };

//#if DEBUG
//            criteria.ProductTypeId = Guid.Parse("03c873da-a2a9-4d18-84bf-9d7212363737");
//#endif

            var list = ProductionService.GetProductInfoBasePagedList(criteria);

            return list.Rows.ToBootstrapTableModel();
        }

        #endregion
    }
}
