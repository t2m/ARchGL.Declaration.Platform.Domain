﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ARchGL.Declaration.Platform.Service;
using TDF.Core.Ioc;
using TDF.Web.Attributes.Mvc;
using TDF.Web.Authentication.Attributes.Mvc;
using TDF.Web.Authentication.Models;
using ARchGL.Declaration.Platform.Service.Dtos;
using ARchGL.Declaration.Platform.Domain.Entities;
using ARchGL.Declaration.Platform.Web.Controllers;

namespace ARchGL.Declaration.Platform.Web.Controllers
{
    /// <summary>
    /// 机位
    /// </summary>
    public partial class MachineController : AdminControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly IMachineService MachineService;

        /// <summary>
        /// 
        /// </summary>
        protected readonly IProductionService ProductionService;

        /// <summary>
        /// 库存管理
        /// </summary>
        protected readonly IProductWarehouseService ProductWarehouseService;
        protected readonly IBasicBusinessService BasicBusinessService;

        /// <summary>
        /// 初始化Service
        /// </summary>
        /// <param name="machineService"></param>
        /// <param name="productionService"></param>
        public MachineController(IMachineService machineService, IProductionService productionService
            , IProductWarehouseService productWarehouseService,IBasicBusinessService basicBusinessService)
        {
            MachineService = machineService;
            ProductionService = productionService;
            ProductWarehouseService = productWarehouseService;
            BasicBusinessService = basicBusinessService;
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit()
        {
            return View();
        }

        /// <summary>
        /// 入库日志
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Log(Guid id)
        {
            ViewBag.MachineId = id;
            return View();
        }

        #region Ajax Action

        /// <summary>
        /// 根据数据Id移除
        /// </summary>
        /// <param name="id">数据Id</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Remove(Guid id)
        {
            MachineService.Remove(id);

            return Success();
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="dto">业务数据</param>
        /// <returns></returns>
        public ActionResult Save(MachineDto dto)
        {
            dto.AreaId = AreaId;
            MachineService.Save(dto);

            return Success();
        }

        /// <summary>
        /// 获取机位-分页数据集合
        /// </summary>
        /// <param name="criteria">筛选条件</param>
        /// <returns></returns>
        public ActionResult GetMachinePagedList(MachineCriteria criteria)
        {
            criteria.AreaId = AreaId;
            var list = MachineService.GetPagedList(criteria);

            return list.ToBootstrapTableModel();
        }

        /// <summary>
        /// 获取入库日志-分页数据集合
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ActionResult GetWarehouseRecordPagedList(WarehouseRecordCriteria criteria)
        {
            criteria.AreaId = AreaId;
            var list = ProductWarehouseService.GetWarehouseRecordPagedList(criteria);

            return list.ToBootstrapTableModel();
        }

        #endregion
    }
}
