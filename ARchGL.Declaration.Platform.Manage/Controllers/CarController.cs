﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ARchGL.Declaration.Platform.Service;
using ARchGL.Declaration.Platform.Service.Dtos;
using ARchGL.Declaration.Platform.Web.Models;
using TDF.Web.Attributes.Mvc;

namespace ARchGL.Declaration.Platform.Web.Controllers
{
    /// <summary>
    /// 推车
    /// </summary>
    [System.Web.Http.AllowAnonymous]
    public class CarController : AdminControllerBase
    {
        /// <summary>
        /// CarService
        /// </summary>
        protected readonly ICarService CarService;

        /// <summary>
        /// 初始化构造函数
        /// </summary>
        /// <param name="carService">CarService</param>
        public CarController(ICarService carService)
        {
            CarService = carService;
        }

        /// <summary>
        /// Car 管理页面
        /// </summary>
        /// <returns></returns>
        public override ActionResult Index()
        {
            ViewBag.AreaId = AreaId;
            ViewBag.MaximumCapacity = CarService.GetMaximumCapacity(AreaId);

            return View();
        }

        #region Ajax

        /// <summary>
        /// 保存 Car 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ModelValidation]
        public ActionResult Edit(SaveCarModel model)
        {
            CarService.SaveCar(model);
            return Success();
        }

        /// <summary>
        /// 移除 Car
        /// </summary>
        /// <param name="id">Car Id</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            CarService.RemoveCar(id);
            return Success();
        }

        /// <summary>
        /// 分页列表
        /// </summary>
        /// <param name="criteria">条件</param>
        /// <returns></returns>
        public ActionResult GetPagedList(CarCriteria criteria)
        {
            criteria.AreaId = AreaId;
            var list = CarService.GetCarPagedList(criteria);
            return list.ToBootstrapTableModel();
        }

        /// <summary>
        /// 检查：Number在该区域唯一
        /// </summary>
        /// <param name="id">推车ID</param>
        /// <param name="number">车号</param>
        /// <returns>true 已存在 false 不存在</returns>
        [HttpPost]
        public ActionResult IsNumberExisted(string number, Guid? id)
        {
            var existed = CarService.IsNumberExisted(id, AreaId, number);

            return Json(!existed, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}