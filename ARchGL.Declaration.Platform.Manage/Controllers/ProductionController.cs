﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ARchGL.Declaration.Platform.Domain.Entities;
using ARchGL.Declaration.Platform.Service;
using ARchGL.Declaration.Platform.Service.Dtos;
using ARchGL.Declaration.Platform.Service.Dtos.Area;
using ARchGL.Declaration.Platform.Service.Dtos.Machine;
using ARchGL.Declaration.Platform.Service.Dtos.Production;
using ARchGL.Declaration.Platform.Web.Models;
using TDF.Core.Ioc;
using TDF.Web.Attributes.Mvc;
using WebGrease.Css.Extensions;

namespace ARchGL.Declaration.Platform.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class ProductionController : AdminControllerBase
    {
        /// <summary>
        /// 生产服务
        /// </summary>
        protected readonly IProductionService ProductionService;
        protected readonly IBasicBusinessService BasicBusinessService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productionService"></param>
        public ProductionController(IProductionService productionService,IBasicBusinessService basicBusinessService)
        {
            ProductionService = productionService;
            BasicBusinessService = basicBusinessService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override ActionResult Index()
        {
            var workTimes = ProductionService.GetWorkTimes(AreaId);
            return View(workTimes);
        }

        /// <summary>
        /// 生产计划配置页面
        /// </summary>
        /// <param name="workTimeId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RuleConfigruation(Guid workTimeId)
        {
            var model = new ProductionRuleConfigModel();
            var area = BasicBusinessService.GetById(AreaId);
            List<MachineRuleDto> machineRules = null;
            machineRules = ProductionService.GetMachineRule(AreaId, workTimeId)
                .OrderBy(x => x.MachineNumber).ToList();
            model.LoadBBBConfig(machineRules);
            var prodctionRules = ProductionService.GetProdctionRuleMouldList(AreaId, workTimeId);
            model.LoadProdctionRule(prodctionRules);
            ViewBag.ShowBBBConfig = area.OutputType != OutputType.OneToMany;
            return PartialView(model);
        }

        /// <summary>
        /// 启用BBB
        /// </summary>
        /// <param name="enabled"></param>
        /// <param name="machineId"></param>
        /// <param name="workTimeId"></param>
        /// <returns></returns>
        public ActionResult EnableBBB(bool enabled,Guid machineId,Guid workTimeId)
        {
            ProductionService.EnableBBB(machineId,enabled,workTimeId);
            return Success();
        }

        /// <summary>
        /// 保存生产比例
        /// </summary>
        /// <param name="ruleModel">保存的规则</param>
        /// <returns></returns>
        [HttpPost]
        [ModelValidation]
        public ActionResult SaveRule(SaveRuleModel ruleModel)
        {
            ProductionService.SaveProdcutionRule(ruleModel.MachineId,ruleModel.Mould,ruleModel.Value, ruleModel.WorkTimeId);
            return Success();
        }

        /// <summary>
        /// 设置最大释放堆叠数
        /// </summary>
        /// <param name="releasedUpperLimit"></param>
        /// <returns></returns>
        [HttpPost]
        [ModelValidation]
        public ActionResult SetReleasedUpperLimit(SaveReleasedUpperLimit releasedUpperLimit)
        {
            ProductionService.SetReleasedUpperLimit(releasedUpperLimit.Mould,releasedUpperLimit.Value,releasedUpperLimit.WorkTimeId,AreaId);
            return Success();
        }
    }
}